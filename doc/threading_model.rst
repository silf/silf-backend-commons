
Model wątkowy zarządzania urządzeniem (:class:`silf.backend.commons.device.api.Device`)
=======================================================================================

Dostęp do urządzeń
------------------

Załorzenie jest takie:

1. Mamy :class:`silf.backend.commons.device.api.Device` które siedzi w innym
   procesie niż eksperyment
2. Transparentnie dajemy dostęp eksperymentowi dostęp do as


Generalnie mamy takie założenia:

#. Wszystkie metody wykonywane na jednym Device będą wykonywane
   z jednego wątku.
#. Metody na device powinny się relatywnie szybko kończyć, ale
   nie jest to wymagane. Ich blokowanie nie wpłynie na działanie
   eksperymenu tj. jeśli :class:`silf.backend.commons.device.api.Device`
   się powiesi nie znaczy to że powiesi się eksperyment. Jednak
   cała komunikacja z eksperymentem odbywa się pomiędzy wywołaniami
   pojedyńczych metod na `device`.
#. Dostępna jest też specjalna metoda :meth:`silf.backend.commons.device.api.Device.loop`,
   która będzie wykonywana cyklicznie i to do niej powinno się przenieść
   wszytkie długo trwające zadania.

Po