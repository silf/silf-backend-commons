Tests
=====

.. _tests-params:

Config
------

You need XMPP server and at least 3 user accounts and room on it to run client tests.

Configure accounts on server (preferably modified Tigase), add them as participants to room.

.. warning::
    You need to disable history (delayed messages) in room, to run tests properly, senders disconnect
    when sent message comes back (ie. when clients receives message from itself). History messages
    on room introduce race condition.

Fill sections ``[Sender]``, ``[Receiver]`` and ``[Sender2]`` in test config file with proper login credentials.

You can add ``[Listener]`` account to see labdata communication during tests (use :ref:`listener` program).

Example config files for local Openfire server and Tigase are supplied in ``config/`` directory.

