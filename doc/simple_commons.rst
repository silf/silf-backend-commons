How to write experiment using simple api
========================================

Inherit from ``IExperimentManager``
-----------------------------------

1. Inherit from ``silf.backend.commons.experiment.IExperimentManager``,
   and override all methods.
2. Write ``experiment.ini`` config like you did before and pass path to
   your class as ``ExperimentManagerClass`` entry in ``[Experiment]`` section.
3. Most methods will be called in response to messages from user,
   notable exception is ``loop_iteration`` that is called from program main
   loop.

How to send results
*******************

There is no ``get_results`` method. All communication with user is done by
the ``experiment_callback`` attribute that is instance of ``ExperimentCallback``
and has following methods: ``send_results``, ``send_series_done`` and
``send_experiment_done``. So just call ``send_results``

Inherit from ``MultiModeManager``
---------------------------------

1. Create classes implementing modes
2. Inherit from ``MultiModeManager``, implementation should look like that:

   .. code-block::

      class AttManager(MultiModeManager):

         EXPERIMENT_NAME = "Attenuaiton"

         MODE_MANAGERS = {
           'Material': MaterialMode,
           'Background': BackgroundMode
         }


How to implement a mode
***********************

Create subclass of ``silf.backend.commons.simple_server.simple_server.ExperimentMode``.
Most of methods are very similar to those for ``ExperimentManagerClass``.

All methods should call ``super()`` appropriate methods from super class.

You can send results in two ways:

* Just call ``self.experiment_callback.send_results``
* Or use result creators functionality in this case you'll need to override:
  ``create_result_creators`` that return a list of result creators, and then
  call ``push_results`` which will:

  * transform results using ``result_creators``
  * send them to client

.. note::

   ``MultiModeManager`` defaults to different (from older commons)
   behaviour when exception raises an exception. Default behaviour was to
   send an error messahe than do nothing.

   ``MultiModeManager`` sends error messahe then finishes the current series.

   This change was introduced because any exception from ``loop_iteration``
   tended be repeatedly sent from each one of loop iterations, which spammed
   the room and client ui.

   To change this behaviour you'll need to override ``on_miscallenous_error``
   method.

