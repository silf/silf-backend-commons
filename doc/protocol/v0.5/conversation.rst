
.. _protocol-conversation-0.5:

Conversation in protocol v0.5
=============================

In this document we will define what stanzas are exchanged during the experiment.

* Operator sends :ref:`proto-silf-mode-get`

  * Experiment responds with message containing possible modes.

* Operator sends :ref:`proto-silf-mode-set`

  * Experiment sets the mode, and then sends description of the interface.

* Operator may send :ref:`proto-silf-settings-check` (possibly many times), this message
  contains settings set by the user. This settings will be validated

  * Experiments validates the settings and sends response containing validation results

* Operator sends :ref:`proto-silf-series-start`. It contains settings set by the user.

  * Experiment response varies on the validity of settings:

    * If settings are valid measurement series is stated.
    * If settings are invalid user get's an error and must send them via
      :ref:`proto-silf-series-start` again

* Experiment sends :ref:`proto-silf-results` until the session ends.

* During experiment session user may change settings that are ``live``
  (see :ref:`proto-input-fields`). It should use: :ref:`proto-settings-update`.

* Data series ends when experiment sends :ref:`proto-silf-series-stop`, it may end it
  in following circumstances:

  * After series is finished (in this case experiment sends: :ref:`proto-silf-series-stop` on its own)
  * After operator requests it by sending :ref:`proto-silf-series-stop`
  * When experiment detects series that take to long, and drops it (shouldn't occour).
  * When experiment is ending.

* After end of series user may either:

  * Start a next one
  * Change mode
  * End experiment

* Experiment ends when experiment server sends: :ref:`proto-silf-series-stop`.

  * Experiment is idle for too long time.
  * Operator requests it by sending :ref:`proto-silf-series-stop`.
  * Bot that is operating the experiment detects end of reservation, and
    requests stop by sending: :ref:`proto-silf-series-stop`.