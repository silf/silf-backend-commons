Establishing protocol version
=============================

Version is negotiated between experiment and experiment operator. Version is
established from when experiment sends ``silf:protocol:version:set`` until
experiment is finished.

.. note::

    Currently experiment is finished when **experiment** sends
    ``silf:experiment:stop`` with type ``done``. But precise moment may depend
    on protocol version.

Version values
--------------

We use `semantic versioning v. 2.0.0 <http://semver.org/spec/v2.0.0.html>`_ to
version the protocol.

Experiment may specify that it supports version ranges that have following syntax:
``1.1.1-2.0.0`` which specifies versions from (including) ``1.1.1`` up to
(excluding) ``2.0.0``.


Conversation
------------

In this document we will define what stanzas are exchanged during the
protocol negotiation. All communication is done inside ``labdata``
messages, :see:`proto-1.0-labdata`.


**Happy path**

* Operator sends ``labdata``: ``silf:protocol:version:get``

 * Server responds with : ``silf:protocol:version:get`` that contains list of
   avilable versions.

* Operetor sends:  ``silf:protocol:version:set``, this stanza contains
  selected version.

  * Server responds with: ``silf:protocol:version:set`` with version selected
    by client.

    From now on version is set.

**Other proper paths**

Operator does not need to check avilable versions from the server, it can
just send  ``silf:protocol:version:set`` and hope for the best, if he sent
version that can't be handled by the experiment, experiment will send
and ``error`` stanza.

Error conditions
****************

Folowing error conditions are possible:

* Client sends version that is not acceptable for the experiment (was not in
  avilable version list). In this case experiment sends
  ``silf:protocol:version:set`` with type: ``error``.
* Client can't handle any version sent by the experiment. In this case
  client does not send ``silf:protocol:version:set`` and just disconects
  from the experiment room.
* Client sends any other stanza not specified by the protocol. In this case
  experiment sends appropriate ``error`` stanza, or just ignores the message.

Joining experiment after version is established
***********************************************

If observer joins the room after session negotiation he is informed about
protocol version by the athena bot before he joins the room.

If experiment operator joins the room after session negotiation has taken place
Athena bot should send experiment version **before** operator is given voice
in the room. If the operator is given voice in the room, and he didn't get this
information he can start version negotiation.

In both cases athena bot sends ``silf:protocol:version:set`` with type
``result`` and the same content that experiment sent earlier.

Stanza content
--------------

``silf:protocol:version:get``
*****************************

Client sends **empty** ``labdata`` with type ``query``.

Server responds with a message of type ``result``, and
list of protocol versions this experiment supports,
in the following list:

.. code-block:: json

    {
        "versions": ["1.0", "1.0.2", "0.0.0-0.6.0"]
    }



``silf:protocol:version:set``
*****************************

Client sends ``labdata`` with type ``query`` and sends selected version:

.. code-block:: json

    {
        "version": "0.5.9"
    }

Server responds with ``labdata`` with type ``result`` and *the same content*.

From now on up to the moment when experiment closes the experiment session
protocol version siginified in before mentioned stanza is used.

If client sends improper version (one not present in list of versions),
experiment responds with error.

.. note::

    Server responds by confirming selected version just so all elements of the
    system can know version just by checking single stanza.

Example conversation
--------------------

.. code-block:: xml

    <message from='exp@muc.ilf/operator' to='exp@muc.ilf' type='groupchat'>
        <labdata xmlns="silf:protocol:version:get" type="query" id="q1"/>
    </message>

    <message from='exp@muc.ilf/experiment' to='exp@muc.ilf' type='groupchat'>
        <labdata xmlns="silf:protocol:version:get" type="result" id="q1">
        {
            "versions": ["1.0", "1.0.2", "0.5"]
        }
        </labdata>
    </message>


    <message from='exp@muc.ilf/operator' to='exp@muc.ilf' type='groupchat'>
        <labdata xmlns="silf:protocol:version:set" type="query" id="q2">
        {
            "version": "1.0"
        }
        </labdata>
    </message>

    <message from='exp@muc.ilf/experiment' to='exp@muc.ilf' type='groupchat'>
        <labdata xmlns="silf:protocol:version:set" type="result" id="q2">
            {
                "version": "1.0"
            }
        </labdata>
    </message>