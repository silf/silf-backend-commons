
.. _proto-1.1-labdata:

Protocol ``labdata`` stanza
===========================

XMPP extension
--------------

All communication is passing throught XMPP Multi-User-Chat room. Inside
groupchat messages we attach ``labdata`` XML tag, that contains all silf
specific information.

Example:

.. code-block:: xml

    <message from='exp@muc.ilf/operator' to='exp@muc.ilf' type='groupchat'>
        <labdata xmlns="silf:protocol:version:get" type="query" id="q1"/>
    </message>

Labdata stanza
--------------

Labdata contains following features:

``namespace``

    Namespace used for ``labdata`` tag is used to: define action that this
    ``labdata`` performs and partially defines content of labdata tag.

``type``

    SILF uses very simple protocol for conversational state, ``type`` signifies
    whether this message is query or response.

    It also (with ``namespace`` defines content of labdata tag.

    There are three possible values: ``query``, ``result``, ``data`` and
    ``error``.

``id``

    ID is used to identify query-response pairs of the same conversation.
    If query (labdata with type ``query``) has ``id`` value of ``foo``,
    responseto this message should have the same ``id`` (in most cases
    namespace will be the same).

    From protocol standpoint ``id`` is a unique blob of text without any
    structure. Current implementation usess ``uuid`` as ``id``s contents.

``content``

    We always use ``JSON`` content for labdata contents. Specific JSON structure
    is defined by ``namespace`` and ``type`` tags.

Conversation
------------

Labdata uses query-response conversation pattern.

**Stand alone messages**

Stand-alone messages can have two types: ``data`` and ``error``. ``Data`` is used to
send some information out of conversation, ``error`` to send an error condition
out of conversation.

**Conversation messages**

Conversation is simple:

* One party sends message with type ``query``, namespace ``foo`` and a particular ``id``
* Other responds with ``message`` with the same namespace and ``id``, type can be either
 * ``result`` which means that any operation requested by ``query`` was succesfull.
 * ``error`` which signifies error condition.






