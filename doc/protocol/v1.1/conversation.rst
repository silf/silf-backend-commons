.. _protocol-conversation-1.1:

High level overviev of protocol v. 1.1 DRAFT
============================================

.. warning::

    This document describes protocol **after** establishing version.
    Part of protocol for establishing version is described in
    :doc:`../version-selection`.

Whole conversation is done inside ``labdata`` tag, see: :ref:`proto-1.1-labdata`.


Experiment protocol
-------------------

* Operator sends :ref:`proto-v1.1-silf-lang-set` along with list of preferred languages for GUI

  * Experiment responds with language code which determines language tha will be used for GUI texts in following stanzas

* Operator sends :ref:`proto-v1.1-silf-mode-get`

  * Experiment responds with message containing possible modes.

* Operator sends :ref:`proto-v1.1-silf-mode-set`

  * Experiment sets the mode, and then sends description of the interface.

* Operator may send :ref:`proto-v1.1-silf-settings-check` (possibly many times), this message
  contains settings set by the user. This settings will be validated

  * Experiments validates the settings and sends response containing validation results

* Operator sends :ref:`proto-v1.1-silf-series-start`. It contains settings set by the user.

  * Experiment response varies on the validity of settings:

    * If settings are valid measurement series is stated.
    * If settings are invalid user get's an error and must send them via
      :ref:`proto-v1.1-silf-series-start` again

* Experiment sends :ref:`proto-v1.1-silf-results` until the session ends.

* During experiment session user may change settings that are ``live``
  (see :ref:`proto-v1.1-input-fields`). It should use: :ref:`proto-v1.1-settings-update`.

* Data series ends when experiment sends :ref:`proto-v1.1-silf-series-stop`, it may end it
  in following circumstances:

  * After series is finished (in this case experiment sends: :ref:`proto-v1.1-silf-series-stop` on its own)
  * After operator requests it by sending :ref:`proto-v1.1-silf-series-stop`
  * When experiment detects series that take to long, and drops it (shouldn't occour).
  * When experiment is ending.

* After end of series user may either:

  * Start a next one
  * Change mode
  * End experiment

* Experiment ends when experiment server sends: :ref:`proto-v1.1-silf-series-stop`.

  * Experiment is idle for too long time.
  * Operator requests it by sending :ref:`proto-v1.1-silf-series-stop`.
  * Bot that is operating the experiment detects end of reservation, and
    requests stop by sending: :ref:`proto-v1.1-silf-series-stop`.

.. _proto-v1.1-late-join:

Joining groupchat late
----------------------

.. note::

    This is implemented in protocol version 1.1.0 and above.

When an user (irrevelant whether he is operator or not) joins groupchat he is
updated with current experiment state.

First thing he gets information about protocol version used,
see: :ref:`version_selection`. Then Athena bot updates client's state
in the following manner:

* First Athena sends current series using following namespaces:

 * :ref:`proto-v1.1-silf-mode-set`
 * :ref:`proto-v1.1-silf-series-start`
 * :ref:`proto-v1.1-silf-results`

 These messages have the same content and metadata as messages that experiment
 sent during normal (i.e. not joining late) operation.

 If any of these messages were not sent during current session they are not sent
 now either.

 See also: :ref:`Result compression <proto-v1.1-results-compression>`

* After sending all above stanzas (that is syncing with current experiment state)
  Athena should give voice (allow him to write to the room)
  to user if he is an operator.

* Then athena sends results from finished series belonging to
  current session. Each session is sent by sending three stanzas:

  * :ref:`proto-v1.1-silf-mode-historical` this one sends controls and
    result field description
  * :ref:`proto-v1.1-silf-series-historical` this one sends settings
  * :ref:`proto-v1.1-silf-results-historical` this one sends results for the
    series.

  All three stanzas should be send consecutively, however client can join them
  by comparing ``experimentId`` and ``seriesId``.

  See also: :ref:`Result compression <proto-v1.1-results-compression>`

.. _proto-v1.1-results-compression:

.. note::

    **Results compression** There is no requirement to send archival
    results in single
    stanza, but athena should compress these results so data is send in concise
    way.