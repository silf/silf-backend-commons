Labdata
=======

.. _labdata-namespaces:

Labdata namespaces
------------------

 - silf:mode:get
 - silf:mode:set
 - silf:state
 - silf:results
 - silf:settings:check
 - silf:settings:update
 - silf:series:start
 - silf:series:stop
 - silf:experiment:stop


.. _labdata-types:

Labdata types
-------------

 - data
 - query
 - result
 - ack
 - done
 - error

