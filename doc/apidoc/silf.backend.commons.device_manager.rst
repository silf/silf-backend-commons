device_manager Package
======================

High level overview
-------------------

Conctete subclasses of :meth:`.IDeviceManager` represent a
:class:`silf.backend.commons.device._device.Device` to
the :meth:`.ExperimentManager`, it has following responsibilities:

* Manage modes of the `Device`
* Store and return metadata that enables construction of experiment GUI
* Aggregate and modify resuts from the `Device` to and return modivied
  results to the user.
* Optionally separate `Device` inside its own subprocess.

Mode management
^^^^^^^^^^^^^^^

`DeviceManager` is aware of current mode as it gets it as a
parameter for :meth:`.IDeviceManager.power_up` method.

Moreover methods that return metadata, also get mode as a parameter, in
this case it means: "Get metadata for appripriate mode".

Metadata management
^^^^^^^^^^^^^^^^^^^

Each `DeviceManager` should describe input fields it needs for particular mode.
These input fields are returned from :meth:`.IDeviceManager.get_input_fields`.
Output fielfs are retuned from :meth:`.IDeviceManager.get_output_fields`.

Default `DeviceManager` implementations allow to define metadata declaratively:
see: :class:`.DefaultDeviceManager` and :class:`.SingleModeDeviceManager`.

Result aggregation
^^^^^^^^^^^^^^^^^^

All `DeviceManager` concrete implementation derive from :class:`.ResultMixin`
that provides sensible way to aggregate results.



Basic device manager classes
----------------------------

.. automodule:: silf.backend.commons.device_manager._device_manager
    :members: IDeviceManager, DefaultDeviceManager, SingleModeDeviceManager,
        ResultMixin, SingleModeDeviceMixin,
        DeclarativeDeviceMixin, DeviceWrapperMixin
    :private-members:
    :show-inheritance:


.. automodule:: silf.backend.commons.device_manager._result_creator
    :members:
    :undoc-members:
    :show-inheritance:

Series manager
--------------

This package contains contains device managers for one use-case, we want student
to capture series of points, and device is able to capture only single point
at a time.



.. automodule:: silf.backend.commons.device_manager._series_manager
    :members:
    :undoc-members:
    :show-inheritance:
