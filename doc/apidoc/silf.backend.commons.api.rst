api Package
===========

:mod:`api` Package
------------------

.. automodule:: silf.backend.commons.api
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`const` Module
-------------------

.. automodule:: silf.backend.commons.api.const
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`exceptions` Module
------------------------

.. automodule:: silf.backend.commons.api.exceptions
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    silf.backend.commons.api.stanza_content

