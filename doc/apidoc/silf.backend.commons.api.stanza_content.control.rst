control Package
===============

:mod:`control` Package
----------------------

.. automodule:: silf.backend.commons.api.stanza_content.control
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`_const` Module
--------------------

.. automodule:: silf.backend.commons.api.stanza_content.control._const
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`_control_api` Module
--------------------------

.. automodule:: silf.backend.commons.api.stanza_content.control._control_api
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`_controls` Module
-----------------------

.. automodule:: silf.backend.commons.api.stanza_content.control._controls
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`_suite` Module
--------------------

.. automodule:: silf.backend.commons.api.stanza_content.control._suite
    :members:
    :undoc-members:
    :show-inheritance:

