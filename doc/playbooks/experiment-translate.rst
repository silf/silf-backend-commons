How to translate an experiment
==============================

Version check
-------------

Translations are supported in version: 2.0.1 of this library and for version 1.1.0 of the protocol.

Mark strings as translateable
-----------------------------

For each file:

1. Add following import ``from silf.backend.commons.api.translations.trans import _``.
2. Mark every string that needs to be translated by wrapping in a ``_()`` function call,
   for example if you have ``ChartField(..., label="Result Chart")`` you need to
   replace it with ``ChartField(..., label=_(Result Chart"))``.

.. note::

    When doing this in a big experiment I found that using following live template
    in my PyCharm is useful: ``_($SELECTION$)`` (Settings -> Live Templates -> local -> add``.
    Then select string, press Ctrl+Alt+J.


Prepare experiment.ini
----------------------

Add following keys in the ``Experiment`` section:

``AvailableLanguages``
    Contains a comma separated list of language codes that this experiment supports.

``TranslationsRootDir``
    Contains a path to directory containing the translation files. This path will be
    resolved relative to the current directory when
    ``expmain.py`` script is called. This script is the sole consumer of the ``experiment.ini``.

``ExperimentName``
    It should be already present in the file, but it will be used.

For example::

     [Experiment]
     ExperimentName=Example Experiment
     AvailableLanguages=pl,en
     TranslationsRootDir=translations

Create translation files
------------------------

To create translation files use following command::

    make-translations.py create --experiment="Example Experiment" --locales en,pl

Where ``--experiment`` **must** be equal to ``ExperimentName``. This command
will create translation files for all locales passed to them.


Update translation files
------------------------

When you edit any translatable strings in the source code you'll need to
run ``update`` command to update ``po`` files::

    make-translations.py update --experiment="Example Experiment" --locales en,pl

Write translation files
-----------------------

Now fill in the translation files, or send them to the translators.

Compile the files
-----------------

After you prepare translation files you need to convert them to binary form
by calling::

    make-translations.py compile --experiment="Example Experiment" --locales en,pl

