Sending e-mails with errors
===========================

SILF now has ability to automatically send emails with:

* Uncaught exceptions
* Logged messages with error level

Configuration is described in: :doc:`experiment.config`.

.. warning::

    To log exceptions you must also configure logging properly.

