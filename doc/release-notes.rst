Release notes
=============

Version 1.4.2
-------------

**Enchancements**

* You can now split experiment ini file into several files.
  when calling ``expmain.py experiment.ini``, :ref:`multi-config-files`.
* You can now configure experiments to send e-mails with every uncaught
  exception, and every logging message with ``error`` or ``exception``
  level (latter ones contain stacktrace). See :ref:`experiment-config-smtp`.

  .. warning::

    Please beware that if you use not default logging you'll need to add
    additional configuration: :doc:`experiment.config` .

* You should upgrade requirements (as SleekXMPP was changed)
* Please change ``host`` in ``[Server]`` section to use IP (workaround
  for SleekXMPP bug). 


Version 1.4.1
-------------

**Incompatible changes**

* If any task on the device takes longer than 30 seconds it is considered an
  error. This can be configured using ``single_task_timeout`` parameter in
  a device section in config file.

  Default is to wait for 30 seconds, so I don't expect anyting will be broken. 

Version 1.4.0
-------------

**Incompatible changes**


* Renamed all packages from ``silf_backend_*`` to ``silf.backend.*``, you need
  to change

  * Imports of these packages
  * Experiment ``ini`` files (if they contain references to changed classes).

* ``IntegerControl`` class is now deprecated, please use ``NumberControl``
  instead.
* Diagnostics api has changed (and is *at least!* actually used). ``perform_diagnostics`` method
  is now deprecated. Please use: ``pre_power_up_diagnostics`` and ``post_power_up_diagnostics``.


**Enchancements**

* Experiment now uses randomly assigned XMPP resources.
* You can now sent chatroom messages
* We can reconnect when XMPP server fails.

Version 1.2.0
-------------

**Incompatible changes**

1. You need to change client config, to reflect changes in how we configure XMPP
   client

