
XMPPClient api
==============

For functions descriptions refer to  :doc:`apidoc/silf.backend.client`

Client is low-level component used by experiment that encapsulates XMPP communication,
it's based on SleekXMPP library with minor modifications.

Client allows sending and receiving custom XMPP substanzas (labdata) embedded in basic Message stanzas.

Functionality
-------------

Client can send XMPP groupchat messages to two separate XMPP rooms.
One room is used for data transfer between parts of experiment, second one is chat room for users.

Messages sent to main room must contain labdata substanza. Client provides callbacks fired when specific
labdata namespace or type arrives.

Config options:

- nick - Nick in XMPP room used by client
- jid  - first part of jid (login) on XMPP server
- password - password
- room - XMPP groupchat room to use as main room (for data transfer)
- chatroom - XMPP groupchat room to use as chat room (for user messages)
- server_config_section - Name of config file section that contains XMPP server config

Server config options:

- host - Host used for connection (or IP address)
- domain - Second part of JIDs of users (domain configured in XMPP server)
- groupchat_domain - Domain configured in XMPP server used in room JIDs
- port - Port used for connection

Example config:

.. code-block:: ini

    [Client]
        nick = Echo
        jid = echo
        password = jhfjkkdutgouhitg

        room = testroom
        chatroom = testchatroom
        server_config_section = Server

    [Server]
        host = pip.ilf.edu.pl
        domain = pip.ilf.edu.pl
        groupchat_domain = muc.pip.ilf.edu.pl
        port = 5222

You can specify parsed config file section by passing 'section' parameter to client constructor
([XMPPClient] in default experiment implementation)

Basic usage in experiment
-------------------------

You do not have to touch anything inside client when writing standard experiment,
specify client class in experiment config file
(in [Experiment] section)

.. code-block:: ini

    [Experiment]
        (...)
        ClientClass=silf.backend.client.client.Client
        (...)


Usage as standalone class
-------------------------

.. code-block:: python

    c = Client(config='default.ini', section='Client')
    c.initialize()  # make XMPP connection

    c.send_chatmessage('Hello, XMPP client here')  # send message to chatroom

    l = format_labdata(...)
    c.send_labdata(l)  # Send message with labdata substanza to main room

    c.register_callback(callback_func, namespace=None, type=None) # register callback function for ALL labdata messages
    c.register_callback(callback_get, namespace='silf:mode:get', type=None) # register callback for all labdata messages in 'silf:mode:get' namespace
    c.register_callback(callback_set_query, namespace='silf:mode:set', type='query') # register callback for labdata messages of type query in namespace 'silf:mode:set'

    c.disconnect() # terminate XMPP connection

Refer to protocol documentation for list of available namespaces and labdata types