.. SILF Experiment API documentation master file, created by
   sphinx-quickstart on Sat Apr  6 13:42:08 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _main-page:

Welcome to SILF Experiment API's documentation!
===============================================

Contents:

.. toctree::
   :maxdepth: 1

   device_api
   create-experiment-tutorial
   experiment.config
   XMPPClient
   apidoc/modules
   authors
   release-notes
   mail

Protocol (current previous):

.. toctree::
   :maxdepth: 1

   protocol/v0.5/conversation
   protocol/v0.5/protocol


Protocol (current version):

.. toctree::
   :maxdepth: 1

   protocol/v1.1/conversation
   protocol/v1.1/protocol
   protocol/version-selection.rst





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

