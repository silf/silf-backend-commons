Utilities
=========

Few programs used for debugging/testing labdata related communication.

Sender
------
Connects to XMPP server, sends single message with labdata substanza.

usage:
``python3 -m silf.backend.client.labdata_sender [-h] [--id [ID]] [--body BODY] [--config CONFIG] namespace type``

Required command-line parameters:
 - namespace - labdata namespace, one of :ref:`labdata-namespaces`
 - type - labdata type, one of :ref:`labdata-types`

Optional command-line parameters
 - ``--id`` - id of labdata
 - ``--body`` - body of labdata
 - ``--config`` - config file to use, (only [Sender] section will be parsed), for list of options and format see :ref:`config-client`,
    if not supplied ``config/default.ini`` will be used.

Echo
----
Simple echo bot, bounces back all messages containing labdata substanza.

usage:
``python3 -m silf.backend.client.echo [--config]``

Command-line parameters:
 - ``--config`` - config file (parses [Echo] section), for list of options and format see :ref:`config-client`

.. _listener:

Listener
--------
Listener bot, listens for messages and presence on XMPP groupchat, prints all incoming messages to stdout.

usage:
``python3 -m silf.backend.client.listener [--config]``

Command-line parameters:
 - ``--config`` - config file (parses [Listener] section), for list of options and format see :ref:`config-client`

