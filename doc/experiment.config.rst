
.. _experiment-config-file-document:

Experiment configuration file
=============================

This file contains experiment description.

[Experiment] Section
--------------------

Contains configuration for the whole experiment:

``ExperimentName``
    Name of the experiment
``ExperimentManagerClass``
    Class that manages the experiment (see: :ref:`experiment-manager-tutorial` and
    :class:`BaseExperimentManager`).
``ClientClass``
    XMPPClient to use.

Other entries in this section are optional, and are dependent ``ExperimentManagerClass``.

``LoopTimeout``
    Timeout of main experiment loop [in seconds]
``series_pattern``
    Experiment series will get label according to this pattern (unless
    ``ExperimentManagerClass`` overries it ;).

    This pattern can contain names of any settings sent to the experiment
    in :meth:`str.format` format.
``stop_series_timeout``
    Timeout (in seconds) after which we stop series that was not stopped by the user or the
    experiment (and it does not send any results)
``shutdown_experiment_timeout``
    Timeout (in seconds) after which we stop experiment if there are no series
    active.
``AdditionalConfigFiles``
    See :ref:`multi-config-files`

.. _multi-config-files:

Multi File Config
-----------------

You might want to split experiment configuration into several config files,
valid examples for such setup are, we just wanted to store config files
inside VCS, and have passwords stored elsewhere.

Config entry: ``AdditionalConfigFiles`` in section ``[Config]``
contains semicolon separated
list of paths to config files, these config files will be loaded
during experiment startup and **will override any entries in original
config file**.

If any of specified files is missing exception will be raised.

.. _config-client:

[XMPPClient] Section
--------------------

Contains configuration for the XMPP client.

It is mostly obvious:

``jid``
    JID (part before ``@`` symbol)
``nick``
    nick in XMPP groupchat, any string
``password``
    password for XMPP account
``room``
    XMPP groupchat room name (part before ``@`` symbol)
``server_config_section``
    Name of section that contains XMPP server configuration,
    by default it is ``[Server]``

[Server] Section
----------------

These properties govern connection to tigase server

``host``
    server address **Due to some Sleek bug** you should provide IP rather
    than a hostname
``port``
    server port

These properties are added used to create JID

``domain``
    Domain (added to JID after ``@`` symbol)

``groupchat_domain``
    Domain (added to room name ``@`` symbol)

.. _experiment-config-logging-section:

[Logging] Section
-----------------

Configures logging for this experiment.

``config_type``
    Either ``file`` or ``basic``. ``basic`` calls ``logging.basicConfig(DEBUG``,
    ``file`` allows use ``ini`` based logging configuration.
``logging_file``
    Usable for ``config_type=file``. Specifies the logging configuration to use.
    Defaults to prepared config file.

In both configs a we send e-mails with errors to experiment administrators,
to configure this behaviour in your logging config you might use logging
section that looks like::

    [handler_exception]
    class: silf.backend.commons.util.mail.ExceptionHandler
    formatter: detailed
    args: []

And then add handler configured in this way to root logger.


.. _experiment-config-smtp:

[SMTP] Section
--------------

This section is optional. It configures functionality of sending emails to
administratiors of the experiment.

``smtp_enabled``

    Boolean value is ``false`` or missing altogether thhis functionality is disabled.

``test_smtp``

    Boolean value if ``true`` will use mock smtp (no emails will be sent).

``smtp_server``, ``smtp_port``

    Host to which we connect via SMTP

``smtp_user``, ``smtp_password``

    Login credentials

``smtp_client_class``

    SMTP client class to use. This entry contains path to module and type to use,
    for example: ``smtplib.SMTP_SSL``. This class has to have similar api to ``smtplib.SMTP_SSL``.

``from_email``

    FROM address for emails.

``admin_list``

    List of users to which we send error emails.


Other sections
--------------

This file may also contain other sections that define particular devices.
Device uses configuratuion defined in section named by it's ``deviceId``.

While device can use any property in it's own section this sections also contains:

``single_task_timeout``

    Float value, maximal time for each operation on the remote device in seconds.
    If operation takes longer time error is raised. 

