# coding=utf-8

import sys
from os import path
import inspect

import unittest

from unittest import mock

import time

from silf.backend.client.mock_client import Client
from silf.backend.client import *
from silf.backend.commons.api import *
from silf.backend.commons.experiment import BaseExperimentManager

from silf.backend.commons.util.config import open_configfile

from silf.backend.experiment.experiment import Experiment, ExperimentManagerCallback


def prepend_current_dir_to_config_file(file_name):

    return path.join(path.dirname(inspect.getfile(sys._getframe(1))), file_name)


TIMEOUT = 1


class MockExperimentTests(unittest.TestCase):
    def setUp(self):
        MockManager = mock.Mock(BaseExperimentManager)
        cp = open_configfile(
            prepend_current_dir_to_config_file("experiment-example.ini")
        )
        self.experiment = Experiment(cp, MockManager, Client(cp))
        self.experiment.manager.EXPERIMENT_NAME = "Test"
        self.experiment.manager.get_series_name.return_value = "Label"
        self.experiment.initialize()
        self.experiment._Experiment__new_uuid = lambda: "this is an uuid"
        self.experiment.manager.experiment_callback = mock.MagicMock()

    def tearDown(self):
        self.experiment.tear_down()

    def test_initialized(self):
        m = self.experiment.manager
        self.assertIn(mock.call.initialize(mock.ANY), m.method_calls)
        self.assertNotIn(mock.call.power_down_and_notify(), m.method_calls)

    def test_get_modes(self):
        m = self.experiment.manager
        m.experiment_callback = mock.MagicMock()
        c = self.experiment.client
        modes = ModeSuite(defaut=Mode("Tryb domyślny"))
        m.modes = modes
        c.fire_synthetic_event(SILF_MODE_GET, SILF_TYPE_QUERY)
        time.sleep(TIMEOUT)
        stanzas = c.sent_stanzas
        get_mode_stanzas = stanzas.pop(SILF_MODE_GET, None)
        self.assertTrue(get_mode_stanzas)
        self.assertFalse(stanzas)
        self.assertEqual(len(get_mode_stanzas), 1)
        self.assertEqual(get_mode_stanzas[0].suite, modes)

    def _set_up_for_power_up(self):
        m = self.experiment.manager
        m.get_input_fields.return_value = ControlSuite(NumberControl("foo", "Foo"))
        m.get_output_fields.return_value = OutputFieldSuite(
            foo=OutputField("foo", "foo")
        )

        c = self.experiment.client
        selection = ModeSelection(mode="default")
        c.fire_synthetic_event(SILF_MODE_SET, SILF_TYPE_QUERY, suite=selection)

    def test_set_mode_powers_up(self):
        m = self.experiment.manager
        self._set_up_for_power_up()
        time.sleep(TIMEOUT)
        self.assertIn(mock.call.power_up("default"), m.method_calls)

    def test_set_mode_responds_with_suite(self):
        c = self.experiment.client
        self._set_up_for_power_up()
        time.sleep(TIMEOUT)
        stanzas = c.sent_stanzas
        set_mode_stanzas = stanzas.pop(SILF_MODE_SET, None)
        self.assertFalse(stanzas)
        self.assertEqual(len(set_mode_stanzas), 1)

        expected_suite = ModeSelected(
            mode="default",
            experimentId="this is an uuid",
            resultDescription=OutputFieldSuite(foo=OutputField("foo", "foo")),
            settings=ControlSuite(NumberControl("foo", "Foo")).to_input_field_suite(),
        )

        self.assertEqual(set_mode_stanzas[0].suite, expected_suite)

    def test_set_mode_sets_mode_in_experiment(self):
        self._set_up_for_power_up()
        time.sleep(TIMEOUT)
        self.assertIsNotNone(self.experiment.mode)
        self.assertEqual(self.experiment.mode, "default")

    def __send_settings(self, namespace):
        c = self.experiment.client
        m = self.experiment.manager
        m.get_input_fields.return_value = ControlSuite(NumberControl("foo", "Foo"))
        m.get_output_fields.return_value = OutputFieldSuite(
            foo=OutputField("foo", "foo")
        )
        selection = ModeSelection(mode="default")
        settings = SettingSuite(foo=Setting(10))
        c.fire_synthetic_event(SILF_MODE_SET, SILF_TYPE_QUERY, suite=selection)
        c.fire_synthetic_event(namespace, SILF_TYPE_QUERY, suite=settings)
        return settings

    def test_send_settings_checks_settings(self):
        settings = self.__send_settings(SILF_SERIES_START)
        m = self.experiment.manager
        time.sleep(TIMEOUT)
        m.check_settings.assert_called_with("default", settings)

    def test_send_settings_sets_settings(self):
        settings = self.__send_settings(SILF_SERIES_START)
        m = self.experiment.manager
        time.sleep(TIMEOUT)
        m.apply_settings.assert_called_with(settings)

    def test_send_settigs_sets_series(self):
        self.assertIsNone(self.experiment.seriesId)
        self.__send_settings(SILF_SERIES_START)
        time.sleep(TIMEOUT)
        self.assertIsNotNone(self.experiment.seriesId)

    def test_send_settings_resends_settings(self):
        self.__send_settings(SILF_SERIES_START)
        time.sleep(TIMEOUT)
        c = self.experiment.client
        stanzas = c.sent_stanzas
        set_mode_stanzas = stanzas.pop(SILF_SERIES_START, None)
        stanzas.pop(SILF_MODE_SET, None)
        self.assertFalse(stanzas)
        self.assertEqual(len(set_mode_stanzas), 1)
        self.assertEqual(
            set_mode_stanzas[0].suite,
            SeriesStartedStanza(
                seriesId="this is an uuid",
                initialSettings=SettingSuite(foo=Setting(10)),
                label="Label",
            ),
        )

    def test_error_sending(self):
        m = self.experiment.manager

        def raiseerr(*args, **kwargs):
            raise ValidationError(Error("error", "user", "foo"))

        m.check_settings.side_effect = raiseerr
        m.apply_settings.side_effect = raiseerr
        self.__send_settings(SILF_SETTINGS_CHECK)
        time.sleep(TIMEOUT * 5)
        stanzas = self.experiment.client.sent_stanzas
        errors = stanzas[SILF_SETTINGS_CHECK]
        self.assertEqual(1, len(errors))
        self.assertEqual(errors[0].type, "error")

    def __setup_stop(self):
        self.__send_settings(SILF_SERIES_START)
        c = self.experiment.client
        c.fire_synthetic_event(SILF_SERIES_STOP, SILF_TYPE_QUERY, suite=SeriesStopped())

    def test_series_is_none_after_stop(self):
        self.__setup_stop()
        time.sleep(TIMEOUT)
        self.assertIsNone(self.experiment.seriesId)

    def test_stopping_calls_stop(self):
        self.__setup_stop()
        time.sleep(TIMEOUT)
        m = self.experiment.manager
        self.assertIn(mock.call.stop(), m.method_calls)

    def test_stopping_responds(self):
        self.__setup_stop()
        time.sleep(TIMEOUT)
        c = self.experiment.client
        stanzas = c.sent_stanzas
        stopped_stanzas = stanzas.pop(SILF_SERIES_STOP, None)
        self.assertEqual(len(stopped_stanzas), 1)

    def __send_stop_experiment(self):
        self.__send_settings(SILF_SERIES_START)
        c = self.experiment.client
        c.fire_synthetic_event(SILF_EXPERIMENT_STOP, SILF_TYPE_QUERY)

    def test_experiment_stop_nulls_mode(self):
        self.__send_stop_experiment()
        time.sleep(TIMEOUT)
        self.assertIsNone(self.experiment.mode)

    def test_experiment_stop_nulls_experiment_id(self):
        self.__send_stop_experiment()
        time.sleep(TIMEOUT)
        self.assertIsNone(self.experiment.experimentId)

    def test_experiment_stop_nulls_series_id(self):
        self.__send_stop_experiment()
        time.sleep(TIMEOUT)
        self.assertIsNone(self.experiment.seriesId)

    def test_experiment_stop_powers_down(self):
        self.__send_stop_experiment()
        time.sleep(TIMEOUT)
        m = self.experiment.manager
        self.assertIn(mock.call.power_down(), m.method_calls)

    def test_experiment_sends_stopped(self):
        self.__send_stop_experiment()
        time.sleep(TIMEOUT)
        m = self.experiment.manager
        self.assertIn(mock.call.power_down(), m.method_calls)

    def test_reset_client(self):
        client_id = id(self.experiment.client)
        self.experiment.client.error_handler.fire()

        time.sleep(3)
        new_client_id = id(self.experiment.client)

        self.assertNotEqual(client_id, new_client_id)

    def test_utf_configparser(self):
        try:
            open_configfile(
                prepend_current_dir_to_config_file("experiment-test-utf.ini")
            )
        except UnicodeDecodeError:
            self.fail("Configparser cannot decode UTF-8 config file")


class TestCallback(unittest.TestCase):
    def setUp(self):
        self.client = Client(
            config=prepend_current_dir_to_config_file("experiment-example.ini")
        )
        experiment = mock.MagicMock()
        experiment.seriesId = "series id"
        self.callback = ExperimentManagerCallback(self.client, experiment)

    def test_callback_stop(self):
        cb = self.callback
        cb.send_series_done("Foo")
        c = self.client
        stanzas = c.sent_stanzas
        stop = stanzas.pop(SILF_SERIES_STOP, None)
        self.assertFalse(stanzas)
        self.assertEqual(len(stop), 1)

    def test_callback_done(self):
        cb = self.callback
        cb.send_experiment_done("Foo")
        c = self.client
        stanzas = c.sent_stanzas
        stop = stanzas.pop(SILF_EXPERIMENT_STOP, None)
        self.assertFalse(stanzas)
        self.assertEqual(len(stop), 1)

    def test_callback_results(self):
        rs = ResultSuite(foo=Result(value=[1, 2, 3]))
        cb = self.callback
        cb.send_results(rs)
        c = self.client
        stanzas = c.sent_stanzas
        results = stanzas.pop(SILF_RESULTS, None)
        self.assertFalse(stanzas)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0].suite, rs)
