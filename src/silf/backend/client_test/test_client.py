"""
    Not exactly unit tests ...
"""

import json
import logging
import threading
import unittest
from multiprocessing import Process

import pytest
from sleekxmpp.stanza import Message

from silf.backend.client.api import ClientException
from silf.backend.client.client import Client, CallbackWrapper
from silf.backend.client.const import SILF_MODE_SET, SILF_TYPE_QUERY, SILF_STATE
from silf.backend.client.labdata import LabdataMatcher, format_labdata
from silf.backend.client.labdata_sender import LabdataSender
from silf.backend.commons.api.const import EXPERIMENT_STATE_OFF
from silf.backend.commons.api.stanza_content._error import ErrorSuite
from silf.backend.commons.api.stanza_content._misc import (
    ResultSuite,
    CheckStateStanza,
    SettingSuite,
    ModeSelection,
    SeriesStopped,
)

DEFAULT_TIMEOUT = 10

DISABLE_SLEEK_LOGS = True

if DISABLE_SLEEK_LOGS:
    logging.getLogger("sleekxmpp").setLevel(logging.ERROR)


class ModdedClient(Client):
    """
    Add hooks to few functions
    """

    def __init__(self, config, section):
        super().__init__(config, section)
        self.connected = threading.Event()
        self.callback_fired = threading.Event()
        self.ack_flag = threading.Event()
        self.done_flag = threading.Event()
        self.error_flag = threading.Event()
        self.received = threading.Event()
        self.chat_received = threading.Event()

    def _muc_online(self, presence):
        self.connected.set()
        super()._muc_online(presence)

    def _muc_message(self, msg):
        if msg["mucnick"] != self.nick:
            self.received.set()

    def _chat_muc_message(self, msg):
        if msg["mucnick"] != self.nick:
            self.chat_received.set()


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


class TestUtils(unittest.TestCase):
    def setUp(self):
        """
        Instantiate Clients, open connections to XMPP server
        """
        self.sender = ModdedClient(config="config/test.ini", section="Sender")
        self.receiver = ModdedClient(config="config/test.ini", section="Receiver")
        self.sender.initialize()
        self.receiver.initialize()
        self.sender.connected.wait(DEFAULT_TIMEOUT)
        self.receiver.connected.wait(DEFAULT_TIMEOUT)

    def tearDown(self):
        self.sender.disconnect()
        self.receiver.disconnect()

    def send_labdata_process(self, lnamespace, ltype, lid, lbody):
        """
        Send labdata (fired as separae process)
        """
        args = Struct(
            **{
                "namespace": lnamespace,
                "type": ltype,
                "id": lid,
                "body": lbody,
                "config": "config/test.ini",
            }
        )
        sender = LabdataSender(args, section="Sender2")
        sender.initialize(True)

    def launch_sending_process(self, namespace, type, id=None, suite=None):
        p = Process(
            target=self.send_labdata_process,
            args=(namespace, type, id, json.dumps(suite.__getstate__())),
        )
        p.start()


@unittest.skip(reason="This requires connection to a server that is not available")
class TestLabdataConversations(TestUtils):
    def setUp(self):
        """
        Instantiate Clients, open connections to XMPP server
        """
        self.sender = ModdedClient(config="config/test.ini", section="Sender")
        self.receiver = ModdedClient(config="config/test.ini", section="Receiver")
        self.sender.initialize()
        self.receiver.initialize()
        self.sender.connected.wait(DEFAULT_TIMEOUT)
        self.receiver.connected.wait(DEFAULT_TIMEOUT)

    def tearDown(self):
        self.sender.disconnect()
        self.receiver.disconnect()

    def test_connection(self):
        """
        Test if connection to XMPP server using Client succeeded
        """
        self.assertTrue(
            self.sender.connected.is_set() and self.receiver.connected.is_set()
        )

    def test_data(self):
        """
        Test sending results (without query)
        """
        rs = ResultSuite.from_dict(
            json.loads(
                '{"foo": {"value": "foo", "pragma": "append"}, "bar": {"value": "bar", "pragma": "append"}}'
            )
        )
        self.sender.send_results(rs)
        self.receiver.received.wait(DEFAULT_TIMEOUT)
        self.assertTrue(self.receiver.received.is_set())

    def state_query_modes_callback(self, state):
        """
        Callback function for test below,
        reply to query with mode suite
        """
        self.receiver.callback_fired.set()
        self.receiver.send(state.namespace, None)

    def test_state_query_modes(self):
        """
        Test sending modes after query
        """
        self.receiver.register_callback(
            self.state_query_modes_callback, namespace="silf:mode:get"
        )
        stanza = format_labdata("silf:mode:get", "query")
        self.sender.send_labdata(stanza)
        self.receiver.callback_fired.wait(DEFAULT_TIMEOUT)
        self.sender.received.wait(DEFAULT_TIMEOUT)
        self.assertTrue(
            self.receiver.callback_fired.is_set() and self.sender.received.is_set()
        )

    def state_query_state_callback(self, state):
        """
        Callback function for test below
        Reply with state stanza
        """
        cs = CheckStateStanza.from_dict(
            json.loads(
                '{"seriesId": "eeb23307-69dc-4745-83da-db22f3dfa557", "state": "off", "experimentId": "f5d1762e-f528-4ca1-bee4-97512c22ae6a"}'
            )
        )
        self.receiver.callback_fired.set()
        self.receiver.send(state.namespace, cs)

    def test_state_query_state(self):
        """
        Test sending result (state) after query
        """
        self.receiver.register_callback(
            self.state_query_state_callback, namespace="silf:state"
        )
        stanza = format_labdata(
            "silf:state", "query", suite=CheckStateStanza(state=EXPERIMENT_STATE_OFF)
        )
        self.sender.send_labdata(stanza)
        self.receiver.callback_fired.wait(DEFAULT_TIMEOUT)
        self.sender.received.wait(DEFAULT_TIMEOUT)
        self.assertTrue(
            self.receiver.callback_fired.is_set() and self.sender.received.is_set()
        )

    def state_query_ack_done_callback(self, state):
        """
        Callback function for test below
        Reply with ack an then done
        """
        state.reply("ack", CheckStateStanza(state="off"))
        state.reply("done")
        self.receiver.callback_fired.set()

    def state_query_sender_ack_callback(self, state):
        """
        Callback function for test below
        Dummy callback for sender to set flag when ack labdata arrives
        """
        self.sender.ack_flag.set()

    def state_query_sender_done_callback(self, state):
        """
        Callback function for test below
        Dummy callback for sender to set flag when done labdata arrives
        """
        self.sender.done_flag.set()

    def test_state_query_ack_done(self):
        """
        Test sending ack and then done after query
        """
        self.receiver.register_callback(
            self.state_query_ack_done_callback, namespace="silf:state"
        )
        self.sender.register_callback(self.state_query_sender_ack_callback, type="ack")
        self.sender.register_callback(
            self.state_query_sender_done_callback, type="done"
        )
        stanza = format_labdata(
            "silf:state", "query", suite=CheckStateStanza(state=EXPERIMENT_STATE_OFF)
        )
        self.sender.send_labdata(stanza)
        self.receiver.callback_fired.wait(DEFAULT_TIMEOUT)
        self.sender.ack_flag.wait(DEFAULT_TIMEOUT)
        self.sender.done_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(
            self.receiver.callback_fired.is_set()
            and self.sender.ack_flag.is_set()
            and self.sender.done_flag.is_set()
        )

    def state_query_error_callback(self, state):
        """
        Callback function for test below
        Reply with error
        """
        es = ErrorSuite.from_dict(
            json.loads(
                '{"errors": [{"metadata": {"message": "foo"}, "severity": "warning", "error_type": "device"}, {"metadata": {"message": "bar"}, "severity": "warning", "error_type": "device"}]}'
            )
        )
        self.receiver.callback_fired.set()
        self.receiver.send_error(es, state)

    def test_state_query_error(self):
        """
        Test responding with error to query
        """
        self.receiver.register_callback(
            self.state_query_error_callback, namespace="silf:state"
        )
        stanza = format_labdata(
            "silf:state", "query", suite=CheckStateStanza(state=EXPERIMENT_STATE_OFF)
        )
        self.sender.send_labdata(stanza)
        self.receiver.callback_fired.wait(DEFAULT_TIMEOUT)
        self.sender.received.wait(DEFAULT_TIMEOUT)
        self.assertTrue(
            self.receiver.callback_fired.is_set() and self.sender.received.is_set()
        )

    def state_query_ack_error_callback(self, state):
        """
        Callback function for test below
        Reply with ack an then error
        """
        state.reply("ack", CheckStateStanza(state="off"))
        es = ErrorSuite.from_dict(
            json.loads(
                '{"errors": [{"metadata": {"message": "Oscyloskop sie zjaral"}, "severity": "error", "error_type": "device"}]}'
            )
        )
        self.receiver.send_error(es, state)
        self.receiver.callback_fired.set()

    def state_query_sender_error_callback(self, state):
        """
        Callback function for test below
        Dummy callback for sender to set flag when error labdata arrives
        """
        self.sender.error_flag.set()

    def test_state_query_ack_error(self):
        """
        Test sending errors from 'busy' state (after ack)
        """
        self.receiver.register_callback(
            self.state_query_ack_error_callback, namespace="silf:state"
        )
        self.sender.register_callback(self.state_query_sender_ack_callback, type="ack")
        self.sender.register_callback(
            self.state_query_sender_error_callback, type="error"
        )
        stanza = format_labdata(
            "silf:state", "query", suite=CheckStateStanza(state=EXPERIMENT_STATE_OFF)
        )
        self.sender.send_labdata(stanza)
        self.receiver.callback_fired.wait(DEFAULT_TIMEOUT)
        self.sender.ack_flag.wait(DEFAULT_TIMEOUT)
        self.sender.error_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(
            self.receiver.callback_fired.is_set()
            and self.sender.ack_flag.is_set()
            and self.sender.error_flag.is_set()
        )


@unittest.skip(reason="This requires connection to a server that is not available")
class TestChatMessages(TestUtils):
    def setUp(self):
        """
        Instantiate Clients, open connections to XMPP server
        """
        self.sender = ModdedClient(config="config/test.ini", section="Sender")
        self.receiver = ModdedClient(config="config/test.ini", section="Receiver")
        self.cli = ModdedClient(config="config/test.ini", section="Experiment")
        self.cli.initialize()
        self.sender.initialize()
        self.receiver.initialize()
        self.sender.connected.wait(DEFAULT_TIMEOUT)
        self.receiver.connected.wait(DEFAULT_TIMEOUT)
        self.cli.connected.wait(DEFAULT_TIMEOUT)

    def tearDown(self):
        self.sender.disconnect()
        self.receiver.disconnect()
        self.cli.disconnect()

    def test_chat_message(self):
        """
        Test sending messages to chatroom
        """
        self.sender.send_chatmessage("message body")
        self.receiver.chat_received.wait(DEFAULT_TIMEOUT)
        self.assertTrue(self.receiver.chat_received.is_set())

    def test_no_experiment(self):
        self.assertTrue(self.cli.connected.is_set())


@unittest.skip(reason="This requires connection to a server that is not available")
class TestExperimentPresent(TestUtils):
    def setUp(self):
        """
        Instantiate Clients, open connections to XMPP server
        """
        self.experiment = ModdedClient(config="config/test.ini", section="Experiment")
        self.experiment.initialize()
        self.experiment.connected.wait(DEFAULT_TIMEOUT)
        # try to connect second experiment

        self.cli = ModdedClient(config="config/test.ini", section="Experiment2")

    def tearDown(self):
        self.experiment.disconnect()
        try:
            self.cli.disconnect()
        except AttributeError:
            pass

    def test_experiment_present(self):
        self.assertRaises(ClientException, self.cli.initialize())


@unittest.skip(reason="This requires connection to a server that is not available")
class TestCallbackWrapper(TestUtils):
    def setUp(self):
        self.callback_flag = False

    def tearDown(self):
        pass

    def callback_test(self, state):
        self.callback_flag = True

    def test_run(self):
        """
        Test run function of CallbackWrapper
        """
        cw = CallbackWrapper(
            "testcb", LabdataMatcher("silf:mode:get"), self.callback_test, Client()
        )
        stanza = format_labdata("silf:mode:get", "query")
        msg = Message()
        stanza.insert_into_message(msg)
        cw.run(msg)
        self.assertTrue(self.callback_flag)


@unittest.skip(reason="This requires connection to a server that is not available")
class TestCallbacks(TestUtils):
    def setUp(self):
        super().setUp()
        self.callback_result = False
        self.callback_flag = threading.Event()

    def callback_test(self, suite):
        self.callback_result = suite
        self.callback_flag.set()

    def test_callback_modes_get(self):
        self.receiver.register_callback(self.callback_test, namespace="silf:mode:get")
        p = Process(
            target=self.send_labdata_process,
            args=("silf:mode:get", "query", None, None),
        )
        p.start()
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)

    def test_callback_mode_set(self):
        self.receiver.register_callback(self.callback_test, namespace="silf:mode:set")
        self.launch_sending_process(
            SILF_MODE_SET, SILF_TYPE_QUERY, suite=ModeSelection(mode="foo")
        )
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)

    def test_callback_update_settings(self):
        self.receiver.register_callback(
            self.callback_test, namespace="silf:settings:update"
        )
        p = Process(
            target=self.send_labdata_process,
            args=(
                "silf:settings:update",
                "query",
                None,
                '{"foo": {"current": false, "value": 13}, "bar": {"current": true, "value": "asdf"}}',
            ),
        )
        p.start()
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)

    def test_callback_check_settings(self):
        self.receiver.register_callback(
            self.callback_test, namespace="silf:settings:check"
        )
        p = Process(
            target=self.send_labdata_process,
            args=(
                "silf:settings:check",
                "query",
                None,
                '{"foo": {"current": false, "value": 13}, "bar": {"current": true, "value": "asdf"}}',
            ),
        )
        p.start()
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)

    def test_callback_state(self):
        self.receiver.register_callback(self.callback_test, namespace="silf:state")
        self.launch_sending_process(
            SILF_STATE, SILF_TYPE_QUERY, suite=CheckStateStanza(state="off")
        )
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)

    def test_callback_start_series(self):
        self.receiver.register_callback(
            self.callback_test, namespace="silf:series:start"
        )
        p = Process(
            target=self.send_labdata_process,
            args=(
                "silf:series:start",
                "query",
                None,
                json.dumps(SettingSuite().__getstate__()),
            ),
        )
        p.start()
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)

    def test_callback_stop_series(self):
        self.receiver.register_callback(
            self.callback_test, namespace="silf:series:stop"
        )
        p = Process(
            target=self.send_labdata_process,
            args=(
                "silf:series:stop",
                "query",
                None,
                json.dumps(SeriesStopped().__getstate__()),
            ),
        )
        p.start()
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)

    def test_callback_stop_experiment(self):
        self.receiver.register_callback(
            self.callback_test, namespace="silf:experiment:stop"
        )
        p = Process(
            target=self.send_labdata_process,
            args=("silf:experiment:stop", "query", None, None),
        )
        p.start()
        callback_status = self.callback_flag.wait(DEFAULT_TIMEOUT)
        self.assertTrue(callback_status)
