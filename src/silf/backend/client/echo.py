__author__ = "dk"

import silf.backend.client.labdata as labdata
import argparse
from silf.backend.client.listener import Listener


class Echo(Listener):
    """
    Test XMPP echo bot - bounces back all messages
    Used for labdata/Client debugging
    """

    def __init__(self, config, section="Echo"):
        if config is None:
            config = "config/default.ini"

        super().__init__(config, section)

    def muc_message(self, msg):
        super().muc_message(msg)
        if msg["mucnick"] != self.nick:
            reply = self.client.make_message(
                self.room, mtype="groupchat", mbody=msg["body"], mfrom=self.jid
            )
            stanza = labdata._extract_labdata(msg)
            stanza.insert_into_message(reply)
            reply.send()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="XMPP echo bot (reads labdata).")
    parser.add_argument(
        "--config", type=str, help="client config (default: config/default.ini)"
    )
    args = parser.parse_args()

    client = Echo(config=args.config)
    client.initialize(True)
