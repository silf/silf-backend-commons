import os
from collections import defaultdict
import json
import configparser
import uuid
from silf.backend.commons.util.config import open_configfile

from threading import Condition, RLock

import logging

LOGGER = logging.getLogger(__name__)

import sleekxmpp
from sleekxmpp.xmlstream.handler import Callback
from sleekxmpp.xmlstream.handler.base import BaseHandler

from silf.backend.commons.api import SILFProtocolError, ErrorSuite

import silf.backend.client.labdata as labdata
from silf.backend.client.const import *

from silf.backend.client.api import (
    ClientNoErrorNamespaceException,
    IncomingStanza,
    IClient,
    ClientException,
)

import re

import logging
import threading

LOGGER = logging.getLogger(__name__)


class CallbackWrapper(Callback):
    """
        SleekXMPP does not allow passing additional arguments to _callbacks
        CallbackWrapper adds one layer between sleekxmpp and actual callback function.
    """

    def __init__(self, name, matcher, pointer, client, **kwargs):
        """
        :param str name: Name of callback (any string), derived from sleekxmpp.xmlstream.handler.Callback
        :param matcher: LabdataMatcher object - contains callback running criteria
        :type matcher: labdata.LabdataMatcher
        :param pointer: Callback function to be executed
        :param client: XMPP client
        :param once: Callback should be used only once (defaults to False)
        :param instream: Callback should be executed during stream processing instead of main event loop
        """

        super().__init__(name, matcher, pointer, **kwargs)
        self.client = client

    def run(self, payload, instream=False):
        if not self._instream or instream:
            if self.client is None or payload["mucnick"] != self.client.nick:
                with self.client._callback_condition:
                    BaseHandler.run(self, payload)
                    stanza = labdata._extract_labdata(payload)
                    state = IncomingStanza(self.client, stanza)
                    self._pointer(state)
                    self.client._callback_condition.notify()


class SleekErrorHandler(object):
    """
        SleekXMPP sometimes receives XMPP conflict, XML interpreter (expat?) throws SyntaxError exception,
        and tries to reconnect (XML stream is stuck in some unmanageable condition, and reconnect never succeeds).
        This triggers endless stream of reconnect attempts, experinment server becomes unusable.
        Resetting XML stream from inside of SleekXMPP (abort and then connect) does not work (triggers SocketError).
        XML processing thread must be killed (we must do this from Client context)
    """

    def __init__(self):
        self.callbacks = []
        self.flag = threading.Event()

    def fire(self):
        """
        Called when error is caught,  calls all stored callback functions,
        flag marks client as 'broken' and can be read by other threads
        """
        self.flag.set()
        for callback in self.callbacks:
            callback()

    def check_flag(self):
        return self.flag.is_set()

    def add_callable(self, c):
        """
        Add callback function (called on stream error from main experiment thread)
        avoid using this function directly, use Client.register_error_handler
        """
        self.callbacks.append(c)


class DisconnectedException(Exception):
    """
    Raised when Client couldn't connect during startup. 
    
    We treat all connection errors as fatal errors, that force experiments to be terminated and then resurrected
    by systemd. 
    """


class Client(IClient):
    """
    XMPP client

    Initiates connection to XMPP server, joins multi user chat.

    After initialization use register_*_callback functions to add handlers
    for labdata messages in specific namespaces.
    """

    def __init__(self, config="config/default.ini", section="Client", block=False):
        """

        :param config: ConfigParser to configure client. May be passed as str
                       representing a path to the config file or a live
                       :class:`configparser.ConfigParser` instance.
        :type config: :class:`str` or :class:`configparser.ConfigParser`
        :param  str section:
        :param bool block:
        """

        super().__init__(config, section, block)

        self.online = False

        self.chat_online = False
        self.error_handler = SleekErrorHandler()

        self.__start_condition = Condition()
        self._callback_condition = Condition()

    def initialize(self, time_to_wait_for_connection=0):
        """
        Connect to XMPP server, register necessary plugins

        Create  SleekXMPP client, for sending labdata stanzas.
        Use unencrypted plain authentication, because Tigase has problem with SSL.
        Insert self.error_handler into modified SleekXMPP clinets which will be called when XML perser error occurs.
        """

        self.client = sleekxmpp.ClientXMPP(self.jid, self.password)
        self.client.auto_reconnect = False
        self.client.add_event_handler("session_start", self.start)
        self.client.add_event_handler("groupchat_message", self._muc_message)
        self.client.add_event_handler(
            "muc::{}::got_online".format(self.room), self._muc_online
        )
        self.client["feature_mechanisms"].unencrypted_plain = True
        self.client.use_ipv6 = False

        self.client.register_plugin("xep_0030")  # Service Discovery
        self.client.register_plugin("xep_0045")  # Multi-User Chat
        self.client.register_plugin("xep_0199")  # XMPP Ping

        self.client.error_handler = self.error_handler

        connected = self.client.connect(
            (self.host, self.port), use_ssl=False, use_tls=False, reattempt=False
        )
        if not connected:
            raise DisconnectedException()
        self.client.use_cdata = False
        self.client.plugin["xep_0199"].enable_keepalive(interval=10, timeout=60)
        self.client.process(block=self.block)

    def start(self, event):
        """
        Fired automatically after successful connection to XMPP server
        Join groupchat.
        """
        self.client.get_roster()
        self.client.send_presence()
        self.client.plugin["xep_0045"].joinMUC(self.room, self.nick, wait=True)

    def wait_until_online(self, timeout=15):
        """
        Wait until client gets online
        :param timeout: max time to wait
        """
        with self.__start_condition:
            if self.online:
                return
            self.__start_condition.wait(timeout=timeout)

    def disconnect(self, wait=None):
        """
        Terminate connection to XMPP server.
        Both clients will be disconnected
        """
        self.client.disconnect(wait=wait)

    def _muc_message(self, msg):
        """
        Used mainly for debugging purposes, fired when XMPP groupchat message is received.
        :param msg: received message
        """
        LOGGER.debug("Reclieved message %s", msg)

    def __launch_callbacks(self, stanza):
        self.reclieved_callbacks.fire_callback(stanza)

    def _muc_online(self, presence):
        """
        called after receiving multi user chat presence stanza.
        (somebody got online/offline)
        If client received 409 error (Received when nick in room is already occupied)
        exception is thrown
        """
        if presence["type"] == "error":
            if presence["error"]["code"] == "409":
                raise ClientException(
                    "Unable to enter room {}, maybe experiment is already present?".format(
                        self.room
                    )
                )

        if presence["muc"]["nick"] != self.nick:
            # do something useful here (somebody just got online)
            pass
        else:
            with self.__start_condition:
                self.online = True
                self.client.register_handler(
                    CallbackWrapper(
                        "AnyAny",
                        labdata.LabdataMatcher(lnamespace=None, ltype=None),
                        self.__launch_callbacks,
                        self,
                    )
                )

                self.__start_condition.notify()

    @property
    def Message(self):
        return self.client.Message

    def make_message(
        self,
        mto,
        labdata,
        mbody=None,
        msubject=None,
        mtype=None,
        mhtml=None,
        mfrom=None,
        mnick=None,
    ):
        """
        Create XMPP message stanza and insert labdata into it

        :param labdata: Labdata substanza which will be inserted into message
        :param mbody: Body of message (text)
        :param msubject: Message subject
        :param mtype: Message type (None for user to user messages, 'groupchat' for muc messages)
        :param mhtml: HTML body content (optional)
        :param mfrom: JID of sender (some servers require full client JID)
        :param mnick: Nick of sender (optional)
        """
        msg = self.client.make_message(mto, mbody, msubject, mtype, mhtml, mfrom, mnick)
        labdata.insert_into_message(msg)
        return msg

    def send_labdata(self, stanza):
        """
        Send message with labdata substanza to groupchat
        Add dummy <body> tag, XMPP server (openfire) does not pass groupchat messages without it
        :param stanza: received message.
        """
        msg = self.make_labdata_message(labdata=stanza)
        msg.send()

    def register_error_callback(self, func):
        """
        Register callback function executed before client reset when XML parser error occurs
        """
        self.error_handler.add_callable(func)

    def copy(self) -> "Client":
        """Creates a copy of this client"""
        return type(self)(
            config=self.config, section=self.config_section, block=self.block
        )
