SILF_EXPERIMENT_STOP = "silf:experiment:stop"

SILF_SERIES_STOP = "silf:series:stop"
SILF_SERIES_START = "silf:series:start"

SILF_SETTINGS_UPDATE = "silf:settings:update"
SILF_SETTINGS_CHECK = "silf:settings:check"

SILF_STATE = "silf:state"

SILF_MODE_SET = "silf:mode:set"
SILF_MODE_GET = "silf:mode:get"

SILF_RESULTS = "silf:results"

SILF_MISC_ERROR = "silf:misc:error"

SILF_VERSION_GET = "silf:protocol:version:get"
SILF_VERSION_SET = "silf:protocol:version:set"

SILF_LANG_SET = "silf:lang:set"

SILF_TYPE_QUERY = "query"

SILF_TYPE_ACK = "ack"

SILF_TYPE_RESULT = "result"

SILF_TYPE_DONE = "done"

SILF_TYPE_ERROR = "error"

SILF_TYPE_DATA = "data"

SILF_TYPES_RESPONSE_CONTENT = {SILF_TYPE_ACK, SILF_TYPE_RESULT}
