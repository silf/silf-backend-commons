import abc
from collections import defaultdict
from itertools import chain
import json
import re
from uuid import uuid4
from sleekxmpp.stanza.message import Message

from silf.backend.client.labdata import format_labdata
from silf.backend.commons.api.exceptions import SILFProtocolError
from silf.backend.commons.util.config import open_configfile
from silf.backend.client.const import *

__all__ = ["ClientException", "ClientNoErrorNamespaceException", "IClient"]


class ClientException(Exception):
    """
    Base class for all Client related exceptions
    """

    pass


class ClientNoErrorNamespaceException(ClientException):
    """
    Thrown when send_error is called in a way that does not provide namespace (all args None)
    """

    pass


class IncomingStanza(object):
    """
    Holds 'state' of communication (stanza id and namespace)

    IncomingStanza object is created when client receives query labdata
    that matches criteria for one of registered _callbacks.

    state holds reference to client, namespace of received message and
    content of received query.

    .. warning:: content can be None.
    """

    def __init__(self, client, stanza):
        self.id = stanza["id"]
        self.stanza = stanza
        self.content = stanza.content
        self.client = client

    @property
    def type(self):
        return self.stanza["type"]

    @property
    def namespace(self):
        return self.stanza.namespace

    def reply(self, ltype=SILF_TYPE_RESULT, suite=None):
        """
        Send labdata with id and namespace stored in state
        """

        stanza = format_labdata(self.namespace, ltype, self.id, suite=suite)

        self.client.send_labdata(stanza)

    def __repr__(self):
        return (
            "<IncomingStanza type='{s.type}' namespace='{s.namespace}' "
            "contents={contents} >"
        ).format(
            s=self,
            contents=json.dumps(self.content.__getstate__())
            if self.content
            else "<<empty>>",
        )


class CallbackStore(object):

    """
    >>> def make_callback(message):
    ...     def callback(item):
    ...         print(message, item.type, item.namespace)
    ...     callback.message = message
    ...     return callback
    >>> class MockIncomingStanza(object):
    ...    def __init__(self, type, namespace):
    ...        self.type = type
    ...        self.namespace = namespace
    >>> cs = CallbackStore()

    Store is empty at the begining:

    >>> list(cs.iterate_callbacks())
    []

    Let's add callback for all namespaces and types:

    >>> cs.add_callback(make_callback("first_callback"))
    >>> cs.fire_callback(MockIncomingStanza("TYPE", "NAMESPACE"))
    first_callback TYPE NAMESPACE
    >>> list(cs.iterate_callbacks()) # doctest: +ELLIPSIS
    [(None, None, <function make_callback.<locals>.callback at 0x...>)]

    Callback are evaluated in order they are added

    >>> cs.add_callback(make_callback("second_callback"))

    >>> cs.fire_callback(MockIncomingStanza("TYPE", "NAMESPACE"))
    first_callback TYPE NAMESPACE
    second_callback TYPE NAMESPACE
    >>> cb = list(cs.iterate_callbacks()) # doctest: +ELLIPSIS
    >>> cb[0][2].message == "first_callback"
    True

    Let's add a callback for type

    >>> cs.add_callback(make_callback("for_type"), type="TYPE")

    If we fire event of this type it is launched

    >>> cs.fire_callback(MockIncomingStanza("TYPE", "NAMESPACE"))
    for_type TYPE NAMESPACE
    first_callback TYPE NAMESPACE
    second_callback TYPE NAMESPACE

    If we fire event of other type it is not:

    >>> cs.fire_callback(MockIncomingStanza("OTHER_TYPE", "NAMESPACE"))
    first_callback OTHER_TYPE NAMESPACE
    second_callback OTHER_TYPE NAMESPACE

    """

    def __init__(self):
        super().__init__()
        self._callbacks = defaultdict(lambda: defaultdict(list))

    def add_callback(self, function, *, namespace=None, type=None):
        self._callbacks[type][namespace].append(function)

    def __fire_callbacks(self, type, namespace, object, largs, kwargs):
        for callback in self._callbacks[type][namespace]:
            callback(object, *largs, **kwargs)

    def fire_callback(self, incoming_stanza, *largs, **kwargs):
        type = incoming_stanza.type
        ns = incoming_stanza.namespace
        callbacks = chain(
            self._callbacks[type][ns],
            self._callbacks[type][None],
            self._callbacks[None][ns],
            self._callbacks[None][None],
        )

        callbacks = list(callbacks)  # Makes debugging easier

        for cb in callbacks:
            cb(incoming_stanza, *largs, **kwargs)

    def iterate_callbacks(self):
        for type, map in self._callbacks.items():
            for namespace, callbacks in map.items():
                for callback in callbacks:
                    yield type, namespace, callback


class IClient(object, metaclass=abc.ABCMeta):
    def __init__(self, config="config/default.ini", section="Client", block=False):
        """
        :param config:
        :param section:
        :param block: If set to True will process messages in current thread
        """
        super().__init__()
        self.block = block

        conf = open_configfile(config)
        server_section = conf[section].get("server_config_section", "Server")

        jid = self.random_resource(
            "{}@{}".format(conf[section]["jid"], conf[server_section]["domain"])
        )
        chat_jid = self.random_resource(
            "{}@{}".format(conf[section]["jid"], conf[server_section]["domain"])
        )
        self.config = config
        self.config_section = section

        self._validate_jid(jid)
        self._validate_jid(chat_jid)

        self.jid = jid
        self.chat_jid = chat_jid
        self.password = conf[section]["password"]
        self.nick = conf[section]["nick"]
        self.room = "{}@{}".format(
            conf[section]["room"], conf[server_section]["groupchat_domain"]
        )
        self.host = conf[server_section]["host"]
        self.port = conf[server_section]["port"]
        self.chatroom = "{}@{}".format(
            conf[section]["chatroom"], conf[server_section]["groupchat_domain"]
        )
        self.reclieved_callbacks = CallbackStore()

    @classmethod
    def _validate_jid(cls, jid):
        """
        >>> IClient._validate_jid("foo@bar/baz")
        >>> IClient._validate_jid("attenuation@pip.ilf.edu.pl/experiment")

        >>> IClient._validate_jid("foo@bar")
        Traceback (most recent call last):
        ValueError: Probably invalid jid, did you forgot to provide resource?
        """
        if not re.match(r"""[\w\d\-_\.]+@[\w\d\-_\.]+/[\w\d\-_\.]+""", jid):
            raise ValueError(
                "Probably invalid jid, did you forgot to provide resource?"
            )

    def create_random_id(self):
        return uuid4().urn

    def random_resource(self, jid):
        """
        Add random resource to JID
        """
        return "{}/{}".format(jid, uuid4().hex)

    @abc.abstractmethod
    def initialize(self):
        pass

    @abc.abstractmethod
    def disconnect(self, wait):
        pass

    def register_callback(self, callback, namespace=None, type=None):
        self.reclieved_callbacks.add_callback(callback, namespace=namespace, type=type)

    @property
    def Message(self):
        return Message

    @abc.abstractmethod
    def send_labdata(self, labdata):
        pass

    def send(
        self, namespace, suite, ltype=SILF_TYPE_QUERY, id=None, incoming_stanza=None
    ):
        """
        Send labdata stanza with body containing JSON from suite, take namespace and id from state
        if serializing/sending labdata fails for some reason send error labdata instead

        :param IncomingStanza state: IncomingStanza object (see IncomingStanza class description)
        :param suite: Data to be sent (JSON serializable object - suite/stanza)
        :param str ltype: Labtata type to use (string)
        :return: None
        """
        if id is None:
            id = self.create_random_id()
        try:
            stanza = format_labdata(namespace, ltype, id, suite=suite)
            self.send_labdata(stanza)
        except SILFProtocolError as e:
            self.send_error(e.errors, incoming_stanza)

    def send_standalone_stanza(self, namespace, suite=None):
        """
        Sends standalone stanza (results, stop etc).

        :param str namespace: Namespace to
        :param suite:
        :return:
        """
        try:
            stanza = format_labdata(namespace, SILF_TYPE_DATA, uuid4().urn, suite=suite)
            self.send_labdata(stanza)
        except SILFProtocolError as e:
            self.send_error(e.errors, None)

    def send_results(self, suite):
        """
        Send experiment results in 'data' labdata stanza
        :param suite: DataSuite object (serializable to JSON)
        """
        self.send_standalone_stanza(SILF_RESULTS, suite=suite)

    def send_error(self, suite, incoming_stanza=None, lnamespace=None):
        """
        Send error labdata

        :param suite: ErrorSuite object (contains info about errors - seraializable to JSON)
        :type suite: ErrorSuite
        :param incoming_stanza: IncomingStanza object
        :param lnamespace: Labdata namespace to use
        :return: None

        .. note::
            labdata namespace is taken from state object, if state is None,
            namespace is taken from lnamespace parameter,
            if both are None exception is thrown.

        """
        if not incoming_stanza and not lnamespace:
            raise ClientNoErrorNamespaceException(
                "send_error: You must provide either stream or lnamespace argument"
            )
        lid = None
        namespace = lnamespace
        if incoming_stanza:
            namespace = incoming_stanza.namespace
            lid = incoming_stanza.id
        stanza = format_labdata(namespace, "error", lid, suite=suite)
        self.send_labdata(stanza)

    def make_message(
        self,
        mto,
        labdata,
        mbody=None,
        msubject=None,
        mtype=None,
        mhtml=None,
        mfrom=None,
        mnick=None,
    ):

        message = self.Message(sto=mto, stype=mtype, sfrom=mfrom)
        message["body"] = mbody
        message["subject"] = msubject
        if mnick is not None:
            message["nick"] = mnick
        if mhtml is not None:
            message["html"]["body"] = mhtml

        labdata.insert_into_message(message)

        return message

    def make_labdata_message(self, namespace=None, type=None, suite=None, labdata=None):
        """
        Create XMPP message stanza with labdata substanza using suppliend parameters

        :param namespace: Namespace of labdata substanza
        :param type: Type of labdata substanza
        :param suite: Data suite that will be inserted into labdata substanza
        :param labdata: Labdata substanza

        .. note::
            if labdata parameter is None, new labdata substanza is created using namespace, type and suite parameters,
            otherwise they are ignored.
        """

        if labdata is None:
            labdata = format_labdata(
                namespace, type, self.create_random_id(), suite=suite
            )

        if suite is None and labdata is not None:
            suite = labdata.suite
        if suite:
            body = "{l.namespace}{suite}".format(
                l=labdata, suite=json.dumps(suite.__getstate__())
            )
        else:
            body = labdata.namespace

        return self.make_message(
            self.room, labdata, mbody=body, mtype="groupchat", mfrom=self.jid
        )

    @abc.abstractmethod
    def copy(self) -> "IClient":
        """Creates a copy of this client"""

    @abc.abstractmethod
    def register_error_callback(self, func):
        pass
