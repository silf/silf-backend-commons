__author__ = "dk"

import argparse
import sleekxmpp
import configparser


class Listener:
    """
    Test XMPP client - Sits quietly on room and dumps everything it hears to stdout
    Used oinly for labdata/Client debugging
    """

    def __init__(self, config, section="Listener"):
        if config is None:
            config = "config/default.ini"

        conf = configparser.ConfigParser()
        conf.read(config)
        self.jid = conf[section]["jid"]
        self.password = conf[section]["password"]
        self.nick = conf[section]["nick"]
        self.room = conf[section]["room"]
        self.host = conf[section]["host"]
        self.port = conf[section]["port"]

    def initialize(self, block):
        self.client = sleekxmpp.ClientXMPP(self.jid, self.password)
        # self.client.ssl_version = ssl.PROTOCOL_SSLv3
        self.client["feature_mechanisms"].unencrypted_plain = True

        self.client.add_event_handler("session_start", self.start)
        self.client.add_event_handler("groupchat_message", self.muc_message)
        self.client.add_event_handler(
            "muc::{}::got_online".format(self.room), self.muc_online
        )

        self.client.register_plugin("xep_0030")  # Service Discovery
        self.client.register_plugin("xep_0045")  # Multi-User Chat
        self.client.register_plugin("xep_0199")  # XMPP Ping

        self.client.connect((self.host, self.port), use_ssl=False, use_tls=False)
        self.client.use_cdata = False
        self.client.process(block=block)

    def start(self, event):
        self.client.get_roster()
        self.client.send_presence()
        self.client.plugin["xep_0045"].joinMUC(self.room, self.nick, wait=True)

    def muc_message(self, msg):
        if msg["mucnick"] != self.nick:
            print("got message: {}".format(str(msg)))

    def muc_online(self, presence):
        if presence["muc"]["nick"] != self.nick:
            print("{} got online".format(presence["muc"]["nick"]))

    def disconnect(self):
        self.client.disconnect()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="XMPP listener (prints messages to stdout)."
    )
    parser.add_argument(
        "--config", type=str, help="client config (default: config/default.ini)"
    )
    args = parser.parse_args()

    client = Listener(args.config)
    client.initialize(True)
