""" Labdata XMPP stanza (substanza of Message) """

import abc
from silf.backend.client.const import *
import uuid
from sleekxmpp.xmlstream.stanzabase import ElementBase, register_stanza_plugin
from sleekxmpp.stanza.message import Message
from sleekxmpp.xmlstream.matcher.base import MatcherBase
from silf.backend.commons.api import *
from silf.backend.client import const

import re
import json

from lxml import etree


def format_labdata(lnamespace, ltype="ack", lid=None, lbody=None, suite=None):
    """ Prepare labdata stanza using supplied data """
    if lnamespace not in LABDATA_NAMESPACES:
        raise UnknownLabdataNamespaceException(
            "format_labdata: unknown namespace: {}".format(lnamespace)
        )

    l = LABDATA_NAMESPACES[lnamespace]()

    if lid:
        l["id"] = lid

    l["type"] = ltype

    if lbody:
        l["body"] = lbody

    if suite:
        l.suite = suite

    return l


def reply_labdata(labdata, ltype="ack", lbody=None):
    """ Reply to labdata stanza (create another labdata) """
    if not check_labdata(labdata):
        raise NoLabdataException(
            "reply_labdata: wrong type of argument, must be one of Labdata substanzas"
        )

    rep = LABDATA_NAMESPACES[labdata.namespace]()

    if labdata["type"] == "query":
        rep["id"] = labdata["id"]

    rep["type"] = ltype
    rep["body"] = lbody

    return rep


def check_labdata(stanza):
    """ Check if stanza is valid labdata """
    if type(stanza) in LABDATA_NAMESPACES.values():
        return True

    return False


def check_message_labdata(stanza):
    """ Check if message contains valid labdata """
    # use lxml engine (etree version bundled in sleekxmpp is kinda retarded when it comes to xpath support)
    tree = etree.XML(str(stanza))
    nodes = tree.xpath('./*[local-name() = "labdata"]')

    return len(nodes) > 0


def _check_message_labdata_ns(stanza, namespace):
    """ Check if message contains valid labdata in specified namespace """
    node = stanza.xml.find("{{{}}}labdata".format(namespace))

    return node is not None


def _extract_ns(stanza):
    """ Extract namespace from massage containing labdata stanza """
    tree = etree.XML(str(stanza))
    nodes = tree.xpath('./*[local-name() = "labdata"]')

    if len(nodes) > 0:
        try:
            found = re.search("\{(.+?)\}labdata", nodes[0].tag).group(1)
        except AttributeError:
            raise NoLabdataNamespaceException(
                "LabdataMatcher: message contains labdata without namespace"
            )

        return found

    return None


def _extract_labdata(message):
    """ Extract labdata substanza frm Message (no, message['labdata'] does not work) """
    lns = _extract_ns(message)
    return format_labdata(
        lns, message[lns]["type"], message[lns]["id"], message[lns]["body"]
    )


class LabdataMatcher(MatcherBase):
    def __init__(self, lnamespace=None, ltype=None):
        self.lnamespace = lnamespace
        self.ltype = ltype

    def match(self, stanza):
        """
            Test if message stanza contains proper labdata substanza,
            check if labdata satisfies conditions set in __init__
        """
        namespace = self.lnamespace or _extract_ns(stanza)

        return (
            check_message_labdata(stanza)
            and (namespace is None or _check_message_labdata_ns(stanza, namespace))
            and (self.ltype is None or stanza[namespace]["type"] == self.ltype)
        )


class LabdataException(Exception):
    """
    Base class for all Labdata stanza related exceptions
    """


class UnknownLabdataTypeException(LabdataException):
    """
    Thrown when user tries to create labdata of unknown type
    (not in :data:`LABDATA_TYPES` array)
    """


class NoLabdataException(LabdataException):
    """
    Thrown when :meth:`.reply_labdata` is called on message without labdata
    substanza.
    """


class NoLabdataIdException(LabdataException):
    """
    Thrown when user tries to set labdata type to `ack`,
    `result` or `done` wtithout setting :attr:`id` first
    """


class NoLabdataNamespaceException(LabdataException):
    """
    Thrown when matcher encounters labdata stanza without namespace
    """


class UnknownLabdataNamespaceException(LabdataException):
    """
    Thrown when user tries to format_labdata with namespace not in
    :data:`LABDATA_NAMESPACES`
    """


class InvalidLabdataContentException(LabdataException):
    """
    Thrown when we find labdata with invalid content.
    """


class LabdataBase(ElementBase, metaclass=abc.ABCMeta):

    namespace = "silf"
    name = "labdata"
    plugin_attrib = "labdata"
    interfaces = {"id", "type", "body"}
    sub_interfaces = set(())

    def __init__(self, xml=None, parent=None, type=None, suite=None, id=None):
        super().__init__(xml, parent)

        if id:
            self["id"] = id

        if type:
            self["type"] = type

        if suite:
            self.suite = suite

    def set_type(self, value):
        """ Set type of Labdata substanza, 'result', 'ack' and 'done' need 'id' attribute set before type """
        if not value in LABDATA_TYPES:
            raise UnknownLabdataTypeException(
                "error: Labdata: unknown type - {}".format(value)
            )
        else:
            self._set_attr("type", value)
            if value in ["result", "ack", "done"]:
                if not self["id"]:
                    raise NoLabdataIdException(
                        "error: Labdata: result, ack and done messages must have id attrib set before type"
                    )
            else:
                if not self["id"]:
                    self["id"] = str(uuid.uuid4())

    def set_body(self, value):
        """
            Set body of labdata element in XML:
            <labdata ...> HERE </labdata>
        """
        self.xml.text = value

    def get_body(self):
        return self.xml.text

    def insert_into_message(self, msg):
        """ Insert Labdata substanza into message """
        msg[self.namespace]["id"] = self["id"]
        msg[self.namespace]["type"] = self["type"]
        msg[self.namespace]["body"] = self["body"]

    @property
    def has_body(self):
        return bool(self["body"])

    @property
    def body_as_dict(self):
        if not self["body"]:
            return None
        return json.loads(self["body"])

    @body_as_dict.setter
    def body_as_dict(self, json_data):
        self["body"] = json.dumps(json_data)

    @property
    def type(self):
        return self["type"]

    @property
    def suite_type(self):
        if self.type == SILF_TYPE_DONE:
            return None
        if self.type == SILF_TYPE_ERROR:
            return ErrorSuite
        return self._suite_type

    @abc.abstractmethod
    def _suite_type(self):
        pass

    @property
    def suite(self):
        suite_class = self.suite_type
        if not self.has_body and suite_class is not None:
            raise InvalidLabdataContentException(
                "Labdata with namespace {} and type {} should have content "
                "(instance of {}) but it is empty.".format(
                    self.namespace, self.type, suite_class
                )
            )
        if suite_class is None and self.has_body:
            raise InvalidLabdataContentException(
                "Labdata with namespace {} and type {} should not have any "
                "content but content was {}".format(
                    self.namespace, self.type, self["body"]
                )
            )
        if suite_class is None:
            return None
        return suite_class.from_dict(self.body_as_dict)

    @suite.setter
    def suite(self, new_suite):
        suite_class = self.suite_type
        if suite_class is None and new_suite is not None:
            raise InvalidLabdataContentException(
                "Labdata with namespace {} and type {} should have no content  "
                "but it contains {}.".format(self.namespace, self.type, new_suite)
            )

        if suite_class is not None and new_suite is None:
            raise InvalidLabdataContentException(
                "Labdata with namespace {} and type {} should not have any "
                "content but content was {}".format(
                    self.namespace, self.type, new_suite
                )
            )

        if not isinstance(new_suite, suite_class):
            raise InvalidLabdataContentException(
                "Labdata with namespace {} and type {} should have content "
                "(instance of {}) but it was {}.".format(
                    self.namespace, self.type, suite_class, new_suite
                )
            )

        self.body_as_dict = new_suite.__getstate__()

    @property
    def content(self):
        # TODO: Rename suite to content and remove this accessor
        return self.suite

    @content.setter
    def content(self, suite):
        self.suite = suite

    def __repr__(self):
        # __repr__ from superclass is not good since it uses XML and hence
        # does not specify attribute order which messes with doctests
        return "<labdata {s.type} {s.namespace} {s.content}>".format(s=self)


class LabdataLangSet(LabdataBase):
    namespace = "silf:lang:set"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        if self.type in SILF_TYPES_RESPONSE_CONTENT:
            return Language
        elif self.type == SILF_TYPE_QUERY:
            return Languages
        raise UnknownLabdataTypeException(
            "error: Labdata: unknown type - {}".format(self.type)
        )


class LabdataModeGet(LabdataBase):
    namespace = "silf:mode:get"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        if self.type in SILF_TYPES_RESPONSE_CONTENT:
            return ModeSuite
        return None


class LabdataModeSet(LabdataBase):
    namespace = "silf:mode:set"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        if self.type in SILF_TYPES_RESPONSE_CONTENT:
            return ModeSelected
        return ModeSelection


class LabdataState(LabdataBase):
    namespace = "silf:state"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        return CheckStateStanza


class LabdataSettingsCheck(LabdataBase):
    namespace = "silf:settings:check"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        return SettingSuite


class LabdataSettingsUpdate(LabdataBase):
    namespace = "silf:settings:update"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        return SettingSuite


class LabdataSeriesStart(LabdataBase):
    namespace = "silf:series:start"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        if self.type in SILF_TYPES_RESPONSE_CONTENT:
            return SeriesStartedStanza
        return SettingSuite


class LabdataResults(LabdataBase):
    namespace = "silf:results"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        return ResultSuite


class LabdataSeriesStop(LabdataBase):
    namespace = "silf:series:stop"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        return SeriesStopped


class LabdataExperimentStop(LabdataBase):
    namespace = "silf:experiment:stop"
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        return None


class LabdataVersionGet(LabdataBase):
    namespace = const.SILF_VERSION_GET
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        if self.type == SILF_TYPE_RESULT:
            return ProtocolVersions
        return None


class LabdataVersionSet(LabdataBase):
    namespace = const.SILF_VERSION_SET
    plugin_attrib = namespace

    @property
    def _suite_type(self):
        return ProtocolVersion


class LabdataMiscError(LabdataBase):
    namespace = "silf:misc:error"
    plugin_attrib = namespace

    def _suite_type(self):
        raise ValueError()


LABDATA_TYPES = ("data", "query", "result", "ack", "done", "error")

LABDATA_NAMESPACES = {}

for labdata_subclass in LabdataBase.__subclasses__():
    register_stanza_plugin(Message, labdata_subclass)
    LABDATA_NAMESPACES[labdata_subclass.namespace] = labdata_subclass
