from collections import defaultdict
import copy
import json
import threading
import time
import os

from silf.backend.client.client import SleekErrorHandler

from silf.backend.client.const import (
    SILF_TYPE_ERROR,
    SILF_MODE_SET,
    SILF_TYPE_QUERY,
    SILF_TYPE_ACK,
    SILF_TYPE_DONE,
    SILF_TYPE_RESULT,
    SILF_SERIES_START,
    SILF_RESULTS,
    SILF_SERIES_STOP,
    SILF_EXPERIMENT_STOP,
)

from silf.backend.client.labdata import format_labdata, LabdataState
from silf.backend.client.api import IClient, IncomingStanza, CallbackStore
from silf.backend.client import const, labdata
from silf.backend.commons.api import ModeSelection, Setting
from silf.backend.commons.api.stanza_content._misc import SettingSuite, SeriesStopped


class Client(IClient):

    """
    >>> mock = Client()

    >>> mock.sent_stanzas
    {}

    We are registering callback for all incoming stanzas:

    >>> mock.register_callback(lambda x: print("Reclieved {}".format(x)))
    >>> mock.fire_synthetic_event( "silf:mode:get", "query")
    Reclieved <IncomingStanza type='query' namespace='silf:mode:get' contents=<<empty>> >

    Sent stanzas contains stanzas sent by this client so it is empty:

    >>> mock.sent_stanzas
    {}

    >>> mock.register_send_callback(lambda  x: print("Sent {}".format(x)))

    >>> mock.send_standalone_stanza('silf:experiment:stop')
    Sent <IncomingStanza type='data' namespace='silf:experiment:stop' contents=<<empty>> >

    >>> mock.sent_stanzas
    {'silf:experiment:stop': [<labdata data silf:experiment:stop None>]}

    """

    def __init__(
        self, config="config/default.ini", section="Client", block=False, log_file=None
    ):
        """

        :param config:
        :param section:
        :param block:
        :param log_file:
        """
        super().__init__(config, section, block)
        self.__client_lock = threading.RLock()
        self.send_callbacks = CallbackStore()
        self.error_callbacks = []
        self._sent_stanzas = defaultdict(list)
        self._sent_errors = []
        self.log_file = log_file
        self.error_handler = SleekErrorHandler()

    def get_stanza(self, namespace, types=None):
        if isinstance(types, str):
            types = [types]

        stanzas = self._sent_stanzas.get(namespace, [])
        for s in stanzas:
            if types is None or s.type in types:
                return s
        return None

    def assert_and_get_stanza(self, namespace, types=None):
        """
        Returns stanza with proper ``namespace`` and ``type``
        if it was sent by the remote side (experiment)

        :param string namespace: What namespace to return
        :param list types: Is not None we will return only
            stanzas of specified type. This can be either
            a string or a list.
        :raises AssertionError: If stanza was not found
        :return: Specified labdata
        :rtype: :class:`.LabdataBase`
        """
        s = self.get_stanza(namespace, types)

        if s is not None:
            return s

        raise AssertionError(
            "Stanza {}{} not found in sent stanzas ({})".format(
                namespace, "type {}".format(types) if types is not None else "", stanzas
            )
        )

    @property
    def sent_stanzas(self):
        with self.__client_lock:
            return dict((self._sent_stanzas))

    @property
    def sent_errors(self):
        with self.__client_lock:
            return list(self._sent_errors)

    def clear_client(self):
        with self.__client_lock:
            self._sent_stanzas.clear()
            self._sent_errors = []

    def get_and_clear_errors(self):
        with self.__client_lock:
            try:
                return self._sent_errors
            finally:
                self._sent_errors = []

    def assert_no_errors(self):
        if self._sent_errors:
            for err in self._sent_errors:
                for e in err.content.errors:
                    if "exception_tracebak" in e.metadata:
                        print(e.metadata["exception_tracebak"])
            raise ValueError("Unchecked client errors {}".format(self._sent_errors))

    def register_send_callback(self, callback, *, type=None, namespace=None):
        with self.__client_lock:
            self.send_callbacks.add_callback(callback, type=type, namespace=namespace)

    def __send(self, labdata):

        with self.__client_lock:
            incoming_stanza = IncomingStanza(self, labdata)

            if self.log_file:
                with open(self.log_file, encoding="utf8", mode="a") as f:
                    msg = self.make_labdata_message(labdata=labdata)
                    print(msg, file=f)

            self._sent_stanzas[labdata.namespace].append(labdata)
            self.send_callbacks.fire_callback(incoming_stanza)

            if incoming_stanza.type == SILF_TYPE_ERROR:
                self._sent_errors.append(labdata)
                for cb in self.error_callbacks:
                    cb(incoming_stanza)

    def __to_body(self, suite):
        if suite is not None:
            return json.dumps(suite.__getstate__())
        return ""

    def send_labdata(self, labdata):
        with self.__client_lock:
            self.__send(labdata)

    def initialize(self):
        pass

    def fire_synthetic_event(self, namespace, type, contents=None, suite=None):
        """
        :param LabdataState incoming_stanza: Event to be fired.
        :return:
        """
        with self.__client_lock:
            incoming_stanza = IncomingStanza(
                self,
                labdata.format_labdata(
                    namespace, type, self.create_random_id(), contents, suite=suite
                ),
            )

            self.reclieved_callbacks.fire_callback(incoming_stanza)

    def disconnect(self, wait=None):
        pass

    def register_error_callback(self, func):
        self.error_handler.add_callable(func)

    def copy(self) -> "Client":
        return type(self)(
            config=self.config,
            section=self.config_section,
            block=self.block,
            log_file=self.log_file,
        )


class TestClient(Client):

    __RESULT_TYPES = [SILF_TYPE_ACK, SILF_TYPE_RESULT]

    def send_set_mode(self, mode):
        self.fire_synthetic_event(
            SILF_MODE_SET, SILF_TYPE_QUERY, suite=ModeSelection(mode=mode)
        )

    def send_set_mode_and_wait(self, mode, wait=2):

        self.send_set_mode(mode)
        sleep_time = min(wait, 1.0)
        total_slept = 0
        while total_slept < wait:
            time.sleep(sleep_time)
            self.assert_no_errors()
            st = self.get_stanza(SILF_MODE_SET, types=self.__RESULT_TYPES)
            if st is not None:
                return st

        try:
            return self.assert_and_get_stanza(SILF_MODE_SET, types=self.__RESULT_TYPES)
        finally:
            self.clear_client()

    def __to_settings_suite(self, kwargs):

        settings = {}
        for (k, v) in kwargs.items():
            if not isinstance(v, Setting):
                v = Setting(value=v)
            settings[k] = v

        return SettingSuite(**settings)

    def send_start_series(self, **kwargs):

        suite = self.__to_settings_suite(kwargs)

        self.fire_synthetic_event(SILF_SERIES_START, SILF_TYPE_QUERY, suite=suite)

    def send_start_series_and_wait(self, settings, wait_time=3):

        self.send_start_series(**settings)
        time.sleep(wait_time)
        try:
            return self.assert_and_get_stanza(SILF_SERIES_START, self.__RESULT_TYPES)
        finally:
            self.clear_client()

    def watch_for_results(self, max_time, on_result_callback=None, do_print=False):
        start = time.monotonic()

        to_return = []

        while (time.monotonic() - start) < max_time:
            self.assert_no_errors()
            stanzas = self.sent_stanzas
            self.clear_client()
            if SILF_RESULTS in stanzas:
                results = stanzas[SILF_RESULTS]
                to_return.extend(results)
                for r in results:
                    on_result_callback(r)
                    if do_print:
                        state = r.suite.__getstate__()
                        print(state)

            if SILF_SERIES_STOP in stanzas:
                return True, to_return
            time.sleep(1)

        return False, to_return

    def send_stop_series(self):

        self.fire_synthetic_event(
            SILF_SERIES_STOP, SILF_TYPE_QUERY, suite=SeriesStopped(seriesId="null")
        )

    def send_stop_series_and_wait(self, wait_time=5):

        self.send_stop_series()
        time.sleep(wait_time)
        try:
            return self.assert_and_get_stanza(
                SILF_SERIES_STOP, types=self.__RESULT_TYPES
            )
        finally:
            self.clear_client()

    def send_stop_experiment(self):

        self.fire_synthetic_event(SILF_EXPERIMENT_STOP, SILF_TYPE_QUERY)

    def send_stop_experiment_and_wait(self, wait_time=3):
        self.send_stop_experiment()
        time.sleep(wait_time)
        try:
            return self.assert_and_get_stanza(
                SILF_EXPERIMENT_STOP, types=self.__RESULT_TYPES
            )
        finally:
            self.clear_client()
