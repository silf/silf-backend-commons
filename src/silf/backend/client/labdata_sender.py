__author__ = "dk"

import silf.backend.client.labdata as labdata
from sleekxmpp import ClientXMPP

import configparser
import argparse
import sys
import ssl
import logging


class LabdataSender:
    """
    Connects to XMPP server, joins groupchat, sends single labdata stanza
    with parameters specified by command line args and disconnects.
    Used only for labdata/Client debugging
    """

    def __init__(self, args, config="config/default.ini", section="Sender"):
        if args.config:
            config = args.config

        conf = configparser.ConfigParser()
        conf.read(config)

        server_section = conf[section]["server_config_section"]

        self.jid = "{}@{}/default".format(
            conf[section]["jid"], conf[server_section]["domain"]
        )
        self.password = conf[section]["password"]
        self.nick = conf[section]["nick"]
        self.room = "{}@{}".format(
            conf[section]["room"], conf[server_section]["groupchat_domain"]
        )
        self.host = conf[server_section]["host"]
        self.port = conf[server_section]["port"]

        self.args = args

    def initialize(self, block=True):
        self.client = ClientXMPP(self.jid, self.password)
        # self.client.use_ssl = False
        # self.client.use_tls = False
        # self.client.ssl_version = ssl.PROTOCOL_SSLv3
        self.client["feature_mechanisms"].unencrypted_plain = True
        # self.client.ssl_version = ssl.PROTOCOL_TLSv1
        self.client.use_ipv6 = False
        logging.basicConfig(loglevel=logging.DEBUG)

        self.client.add_event_handler("session_start", self.start)
        self.client.add_event_handler("groupchat_message", self.muc_message)

        self.client.register_plugin("xep_0030")  # Service Discovery
        self.client.register_plugin("xep_0045")  # Multi-User Chat
        self.client.register_plugin("xep_0199")  # XMPP Ping

        self.client.connect((self.host, self.port), use_tls=False, use_ssl=False)
        self.client.use_cdata = False
        self.client.process(block=block)

    def muc_message(self, msg):
        if msg["mucnick"] == self.nick:
            # message came back, exit
            sys.exit(0)

    def start(self, event):
        self.client.get_roster()
        self.client.send_presence()
        self.client.plugin["xep_0045"].joinMUC(self.room, self.nick, wait=True)

        stanza = labdata.format_labdata(
            self.args.namespace, self.args.type, self.args.id, self.args.body
        )
        self.send_labdata(stanza)

    def send_labdata(self, stanza):
        msg = self.client.make_message(self.room, mtype="groupchat", mbody="[labdata]")
        stanza.insert_into_message(msg)
        print(str(msg))
        msg.send()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Send single message with labdata substanza."
    )
    parser.add_argument("namespace", type=str, help="labdata namespace")
    parser.add_argument("type", type=str, help="labdata type")
    parser.add_argument("--id", nargs="?", help="labdata id")
    parser.add_argument("--body", type=str, help="labdata body")
    parser.add_argument(
        "--config", type=str, help="client config (default: config/default.ini)"
    )

    args = parser.parse_args()

    client = LabdataSender(args)
    client.initialize()
