import abc
from abc import abstractmethod
import logging
import time

import warnings


from collections import defaultdict

from sched import scheduler
from silf.backend.commons.api import TRACE

from silf.backend.commons.api import ResultSuite
from silf.backend.commons.experiment import AbstractExperimentManager

__all__ = ["EventListener", "EventExperimentManager"]

LOGGER = logging.getLogger("EventExperimentManager")


class NoListenerForEventTypeWarning(UserWarning):
    pass


warnings.simplefilter("error", category=NoListenerForEventTypeWarning)


class EventObject(object):

    """
        Object passed as argument to :class:`EventListeners:.
    """

    def __init__(self, manager, event_type, args=tuple([]), kwargs=None):
        super().__init__()
        self.manager = manager
        """
        Manager this ecent is launched on :class:`EventExperimentManager`
        """
        self.event_type = event_type
        """
        Type of this event

        :class:`str`
        """
        self.args = args
        """
        Optional arguments passed to the event
        """
        self.kwargs = kwargs if kwargs else {}
        """
        Opational keyword arguments passed to the event
        """

    def __repr__(self):
        return "<EventObject {s.event_type}, (*{s.args}, **{s.kwargs})>".format(s=self)


class EventListener(metaclass=abc.ABCMeta):

    """
    Api of something that listens to event. Really you may pass any callable as
    listener, only reason to use a type is that :meth:`.deregister_listener`
    works better.
    """

    @abstractmethod
    def __call__(self, event):
        """
        :param EventObject event: Current event.
        :return:
        """
        raise NotImplementedError()


class EventExperimentManager(AbstractExperimentManager):

    """
    Event based experiment manager this manager has following properties:

    * Contains internal scheduler that is fired during :meth:`loop_iteration`
      (to serialize access to all internal structores).
    * All lifecycle methods: :meth:`start:, :meth:`stop` and so on just fire
      events (that are not processed by default).
    * Allows to selectively install default listeners to all events.
    """

    def __init__(self, config_parser):
        super().__init__(config_parser)
        self.scheduler = None
        """
        :class:`scheduler`
        """
        self.__event_handler = None

    def install_default_event_managers_for(self, event_type):
        """
        Installs default manager for event `event_type`.

        TODO: Document the meaning of all event types

        :param str event_type:
            Type for which we install default handler.

        :raises ValueError: If there is no default event.
        """
        if event_type in ("start", "power_up", "power_down", "apply_settings"):
            self.register_listener(
                event_type, self.__CallFuncOnPluginListener(self, event_type)
            )

        if "stop" == event_type:
            self.register_listener("stop", self.__stop_event)

        if "tick" == event_type:
            self.register_listener(
                "tick", self.__CallFuncOnPluginListener(self, "query")
            )

        if "pop_results" == event_type:
            self.register_listener("tick", self.__pop_results_on_tick)

    def install_base_managers(self):
        for event in ("stop", "power_up", "power_down", "apply_settings", "tick"):
            self.install_default_event_managers_for(event)

    def install_all_event_managers(self):
        self.install_base_managers()
        for event in ("pop_results", "start"):
            self.install_default_event_managers_for(event)

    def _clear_events(self):
        """
        Removes all events from internal scheduler (will be called before stopping
        or powering_down the experiment.
        """
        self.scheduler._queue.clear()

    def register_listener(self, event_type, listener):
        """
        Registers `listener` for `event_type`.

        :param str event_type: Type for which to register a listener.
        :param EventListener listener:
            An event listener to register, may be any callable taking `event`
            instance
        """
        self.__event_handler.register(event_type, listener)

    def deregister_listener(self, event_type, listener_type):
        """
        Unregisters listener for 'event_type'. Note that this takes a listener
        type.

        :param str event_type: Type for which to deregister a listener.
        :param type listener_type: Type to deregister
        """
        self.__event_handler.deregister_handler(event_type, listener_type)

    def initialize(self, experiment_callback):
        super().initialize(experiment_callback)
        self.scheduler = scheduler(self.__time, self.__delay)
        self.__event_handler = self.__EventHandler()
        self._set_running(False)

    def schedule_event(self, event, delay=None, priority=10):
        """
        Schedules event

        :param float delay_tick:
            Delay in seconds. Even if zero event might not be run instantly:
            it will be ran during next call to :meth:`loop_iteration`.
        :param EventObject event:
            Event to call. Either a :class:`EventObject` or a :class:`str`.
        :param int priority:
            Items scheduled to be ran at the same time instance
            are ordered using priority. Highter the better.
        """
        if delay is None:
            delay = self.LOOP_TIMEOUT
        if isinstance(event, str):
            event = EventObject(self, event)
        if not event.event_type in self.__event_handler:
            warnings.warn(self.__NO_LISTENER_WARNING.format(event.event_type))

        self.scheduler.enter(delay, priority, self.__action, argument=[event])

    def create_event(self, event_type, args=tuple(), kwargs=None):
        """
        :return: Created :class:`EventObject`.
        """
        return EventObject(self, event_type, args, kwargs)

    def stop(self):
        self.schedule_event(self.create_event("stop"), self.LOOP_TIMEOUT)
        super().stop()
        self.scheduler.run(False)

    def start(self):
        super().start()
        self.schedule_event(self.create_event("start"), delay=0)
        self.scheduler.run(False)

    def power_up(self, mode):
        self.schedule_event(
            self.create_event("power_up", args=(mode,)), delay=0, priority=-1
        )
        super().power_up(mode)
        self.scheduler.run(False)

    def power_down(self):
        self._clear_events()
        self.schedule_event(self.create_event("power_down"), delay=0, priority=-5)
        super().power_down()
        self.scheduler.run(False)

    def loop_iteration(self):
        super().loop_iteration()
        self.schedule_event(self.create_event("tick"), 0, priority=10)
        self.scheduler.run(False)

    def apply_settings(self, settings):

        self.schedule_event(
            self.create_event("apply_settings", args=(settings,)), delay=0, priority=0
        )
        self.scheduler.run(False)

    def __stop_event(self, event):
        self._clear_events()
        self._call_on_plugins("stop")

    def __pop_results_on_tick(self, event):
        if self.running:
            self.experiment_callback.send_results(self.current_results)

    class __CallFuncOnPluginListener(EventListener):
        def __init__(self, parent, func):
            super().__init__()
            self.parent = parent
            self.func = func

        def __call__(self, event):
            self.parent._call_on_plugins(self.func, *event.args, **event.kwargs)

    class __DefaultPopResults(EventListener):
        def __init__(self, parent):
            self.parent = parent

        def __call__(self, event, *args, **kwargs):
            return [self.parent._get_suite_from_plugins(ResultSuite, "pop_results")]

    def __action(self, event):
        if event.event_type in ("tick", "ping_results"):
            logging.log(TRACE, "Firing event {}".format(event))
        else:
            logging.debug("Firing event {}".format(event))
        self.__event_handler.fire_event(event)

    @staticmethod
    def __time():
        return time.monotonic()

    @staticmethod
    def __delay(arg):
        if arg != 0:
            raise ValueError()

    class __EventHandler(object):
        def __init__(self):
            super().__init__()
            self.listeners = defaultdict(set)

        def register(self, type, listener):
            self.listeners[type].add(listener)

        def __contains__(self, item):
            collection = self.listeners[item]
            return bool(collection)

        def deregister_handler(self, event_type, listener_type):
            to_remove = set()
            listeners = self.listeners[event_type]
            for l in listeners:
                if isinstance(l, listener_type):
                    to_remove.add(l)
            self.listeners[event_type] = listeners.difference(to_remove)

        def fire_event(self, event):
            results = []
            for l in self.listeners[event.event_type]:
                results.append(l(event))
            return results

    __NO_LISTENER_WARNING = (
        "No listener registered for event type '{}', hovever this event is just "
        "launched."
    )
