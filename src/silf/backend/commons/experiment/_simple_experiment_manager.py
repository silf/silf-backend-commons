# coding=utf-8
import abc
from copy import deepcopy, copy
import collections
import warnings
from silf.backend.commons.api import SILFProtocolError
from silf.backend.commons.device import (
    RUNNING,
    READY,
    DEVICE_STATES,
    DIAGNOSTICS_SHORT,
    DIAGNOSTICS_LEVEL,
)
from silf.backend.commons.device_manager import IDeviceManager
from silf.backend.commons.experiment._experiment_manager import IExperimentManager
from silf.backend.commons.util.config import open_configfile

from silf.backend.commons.util.reflect_utils import load_item_from_module

from silf.backend.commons.experiment import (
    EventExperimentManager,
    EventListener,
    BaseExperimentManager,
)


__all__ = ["SimpleExperimentManager", "CompositeExperimentManager"]


class PopResultsTickListener(EventListener):

    # TODO: Remove this as the same functionality is in EventExperimentManager

    def __call__(self, event):
        if event.manager.running:
            resuls = event.manager["default"].pop_results()
            event.manager.experiment_callback.send_results(resuls)


class SimpleExperimentManager(EventExperimentManager):

    """
    Simple experiment manager usefull when there is only one :class:`IDeviceManager`.
    """

    def get_experiment_name(self) -> str:
        """
        This is machine readable unique name for the experiment.
        """
        return self.config["Experiment"]["ExperimentName"]

    def __init__(self, config_parser, DeviceManagerClass=None):

        config_parser = open_configfile(config_parser)

        cp_section = config_parser["Experiment"]

        if "LoopTimeout" in cp_section:
            self.LOOP_TIMEOUT = float(cp_section["LoopTimeout"])

        if DeviceManagerClass is not None:
            self.DEVICE_MANAGER_CLASS = DeviceManagerClass
            self.DEVICE_MANAGERS["default"] = self.DEVICE_MANAGER_CLASS
        else:
            self.DEVICE_MANAGER_CLASS = cp_section["DeviceManagerClass"]
            self.DEVICE_MANAGERS["default"] = load_item_from_module(
                self.DEVICE_MANAGER_CLASS
            )

        self.__started = True

        self.__stop_requested = False

        self.available_languages = ["pl"]
        if "AvailableLanguages" in cp_section:
            self.available_languages = [
                l.strip() for l in cp_section["AvailableLanguages"].split(",")
            ]

        super().__init__(config_parser)

    def __install_default_event_managers_for_all_events(self):
        for et in ("start", "stop", "power_up", "power_down", "tick", "apply_settings"):
            self.install_default_event_managers_for(et)

    def __on_device_stop(self, *args, **kwargs):

        results = self["default"].pop_results()

        self.experiment_callback.send_results(results)

        if self.__stop_requested is False:
            self._set_running(False)
            self.experiment_callback.send_series_done()
        self["default"].clear_current_results()
        self.__stop_requested = False

    def initialize(self, experiment_callback):
        super().initialize(experiment_callback)
        self.__install_default_event_managers_for_all_events()
        self.register_listener("tick", PopResultsTickListener())
        self["default"].register_state_transition_listener(
            self.__on_device_stop,
            DEVICE_STATES[READY],
            from_state=DEVICE_STATES[RUNNING],
        )

    def start(self):
        super().start()
        self.__stop_requested = False
        self.__started = True

    def stop(self):
        self.__stop_requested = True
        self._set_running(False)
        super().stop()

    def get_available_languages(self):
        return self.available_languages


class CompositeExperimentManager(IExperimentManager):

    """
    Experiment manager that supports multimode experiment, by suppying different
    single mode experiment managers.
    """

    def get_experiment_name(self) -> str:
        return self.config["Experiment"]["ExperimentName"]

    def __init__(self, config_parser):
        super().__init__(config_parser)
        self.__managers = {}
        self.__manager = None
        self.__config = config_parser
        self.config = config_parser
        self.experiment_callback = None

    def initialize(self, experiment_callback):
        self.experiment_callback = experiment_callback

    def _register_experiment_manager(
        self, ManagerClass, experiment_name, modes, config_file=tuple()
    ):
        """

        :ManagerClass: Class used to perform an experiment if user chooses one
            `modes`.
        :type ManagerClass: Either a :class:`IDeviceManager` or a :class:`BaseExperimentManager`.
        :param str experiment_name: Name of the experiment
        :param list modes: List of modes for which this `ManagerClass`
            is registered.
        :param list config_file: Optional iterable of config file names.
        :return:
        """
        if isinstance(modes, str):
            modes = [modes]
        for mode in modes:
            self.__managers[mode] = (ManagerClass, experiment_name, config_file)

    @abc.abstractmethod
    def modes(self):
        raise ValueError()

    @abc.abstractmethod
    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        pass

    @abc.abstractmethod
    def perform_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        """
        Performs diagnostics on all the devices
        """
        warnings.warn(
            "perform_diagnostics is deprecated, use pre_power_up_diagnostics and post_power_up_diagnostics",
            DeprecationWarning,
        )
        self.post_power_up_diagnostics(diagnostics_level)

    @abc.abstractmethod
    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        pass

    def _load_manager(self, mode):
        ManagerClass, experiment_name, config_files = self.__managers[mode]
        manager_config = copy(self.__config)
        for cf in config_files:
            with open(cf) as f:
                manager_config.read(f)

        if issubclass(ManagerClass, IDeviceManager):
            return SimpleExperimentManager(
                manager_config, DeviceManagerClass=ManagerClass
            )

        return ManagerClass(manager_config)

    def __get_manager(self):
        if self.__manager is None:
            raise SILFProtocolError.from_args("error", "user", "Need to set mode first")
        return self.__manager

    def power_down(self):
        if self.__manager is not None:
            self.__manager.power_down()
            self.__manager.tear_down()
            self.__manager = None

    def power_up(self, mode):
        self.__manager = self._load_manager(mode)
        self.__manager.initialize(self.experiment_callback)
        self.__manager.power_up(mode)

    def tear_down(self):
        self.power_down()

    def loop_iteration(self):
        if self.__manager:
            self.__manager.loop_iteration()

    def __getattr__(self, item):
        return getattr(self.__get_manager(), item)
