import configparser
import logging
import sys
import time
import types
import typing
import warnings
import sys
import re
import gettext
from abc import abstractmethod, ABCMeta
from gettext import GNUTranslations

from silf.backend.commons.api.exceptions import *
from silf.backend.commons.api.stanza_content import (
    OutputFieldSuite,
    SettingSuite,
    ModeSuite,
    Mode,
    ResultSuite,
    ControlSuite,
)
from silf.backend.commons.device import DIAGNOSTICS_SHORT, DIAGNOSTICS_LEVEL
from silf.backend.commons.util.exception_utils import (
    current_exception_to_silf_error_stanza,
)
from silf.backend.commons.api.translations.trans import set_translation

__all__ = [
    "ExperimentCallback",
    "BaseExperimentManager",
    "AbstractExperimentManager",
    "IExperimentManager",
    "TranslationMixin",
]


class ExperimentCallback(metaclass=ABCMeta):

    """
    Callback passed to the :class:`.BaseExperimentManager` that allows it
    to asynchronously send messages to clients.
    """

    @abstractmethod
    def send_results(self, results: ResultSuite):
        """
        Sends results to clients.

        :param results: Results to send
        :type results: :class:`.ResultSuite`

        """

    @abstractmethod
    def send_series_done(self, message: str = None):
        """
        Called when experiment wants to stop the series on it's own.

        :param str message:
            Optional message to sent to users, telling why we close this series.
        """

    @abstractmethod
    def send_experiment_done(self, message: str = None):
        """
        Called when experiment wants to end the experiment on it's own.

        :param str message:
            Optional message to sent to users, telling why we close this experiment.
        """


class IExperimentManager(metaclass=ABCMeta):

    __DEFAULT_EXPERIMENT_NAME = "Override self.Experiment name to set name"

    EXPERIMENT_NAME = __DEFAULT_EXPERIMENT_NAME
    """
    Name of this experiment.

    This is deprecated, please override cls.get_experiment_name.
    """

    SUPPORTS_LIVE_SETTIGS = False

    def __init__(self, config_parser: configparser.ConfigParser):
        super().__init__()
        self.experiment_callback = None
        """
        :class:`ExperimentCallback` set during :meth:`initialize`.
        """

        self.config = config_parser
        """
        :class:`ConfigParser`
        """
        self.__series_no = 0

        if hasattr(self, "on_miscallenous_error"):
            raise ValueError(
                "Method on_miscallenous_error has a spelling error and is not used now, "
                "please rename it to on_miscellaneous_error."
            )

    def get_experiment_name(self) -> str:
        """
        Return experiment name
        """
        if self.EXPERIMENT_NAME == self.__DEFAULT_EXPERIMENT_NAME:
            if not self.config.has_option("Experiment", "ExperimentName"):
                warnings.warn(
                    "Please set ExperimentName in Experiment section of the config file"
                )
        else:
            warnings.warn(
                "Instead of overriding EXPERIMENT_NAME please override get_experiment_name method.",
                category=DeprecationWarning,
            )
        return self.config.get(
            "Experiment", "ExperimentName", fallback=self.EXPERIMENT_NAME
        )

    @property
    def logger(self):
        """
        Logger associated with this instance, it has name `experiment.<<EXPERIMENT_NAME>>`.
        """
        return logging.getLogger("experiment." + self.get_experiment_name())

    def initialize(self, experiment_callback: ExperimentCallback):
        self.experiment_callback = experiment_callback

    @property
    def modes(self) -> ModeSuite:
        """
        Returns modes this experiment can support, it is called in response
        to: :ref:`proto-silf-mode-get`.

        :return: Modes
        :rtype: :class:`.ModeSuite`.
        """
        return ModeSuite(defaut=Mode("Tryb domyślny"))

    @abstractmethod
    def get_series_name(self, settings: dict) -> str:
        """
        This method returns name of result series, this name will be presented
        to users.

        See also default implementation at:
        :meth:`.AbstractExperimentManager.get_series_name`

        :return: Name of result series
        :rtype: str
        """
        pass

    @abstractmethod
    def get_input_fields(self, mode: str) -> ControlSuite:
        """
        Returns control suite for `current_mode`

        :param str mode: current_mode for which we get controls
        :return: Input fields
        :rtype: :class:`.ControlSuite`
        """
        pass

    @abstractmethod
    def get_output_fields(self, mode: str) -> OutputFieldSuite:
        """
        Returns output fields for `current_mode`
        :param str mode: current_mode for which we get output fields
        :rtype: :class:`.OutputFieldSuite`
        """
        pass

    @abstractmethod
    def check_settings(self, mode: str, settings: SettingSuite) -> None:
        """
        Checks settings for mode. This method should raise exception on
        any errors in settings.
        Used when client sends :ref:`proto-silf-settings-check`
        or :ref:`proto-silf-series-start`.
        :param str mode: Mode for which we check settings for validity
        :param settings: Settings to check
        :type settings: :class:`.SettingsSuite`
        :raises: :class:`silf.backend.commons.api.exceptions.SILFProtocolError`
        if there are any errors in settings, or if there is any error during setting
        the equipment up.
        """
        pass

    @abstractmethod
    def apply_settings(self, settings: SettingSuite) -> None:
        """
        Applies settings to the experiment.
        This is called when user sends :ref:`proto-silf-series-start`.
        :type settings: :class:`.SettingsSuite`
        :raises: :class:`silf.backend.commons.api.exceptions.SILFProtocolError`
        if there are any errors in settings, or if there is any error during setting
        the equipment up.
        """
        pass

    def apply_settings_live(self, settings: SettingSuite) -> None:
        """
        Applies settings to the experiment.
        This is called when user sends :ref:`proto-silf-series-start`.
        :type settings: :class:`.SettingsSuite`
        :raises: :class:`silf.backend.commons.api.exceptions.SILFProtocolError`
        if there are any errors in settings, or if there is any error during setting
        the equipement up.
        """

        raise SILFProtocolError.from_args(
            "error",
            "user",
            "We are not supporting silf:settings:update message, sorry.",
        )

    @abstractmethod
    def start(self) -> None:
        """
        Starts acquisition in response to :ref:`proto-silf-series-start`. Settings
        are already validated and applied to all device managers.
        """
        pass

    @abstractmethod
    def stop(self) -> None:
        """
        Stops data acquisition for the experiment. It does not notify users.
        Called in response to: :ref:`proto-silf-series-stop`.
        """
        pass

    def stop_and_notify(self, message=None):
        """
        Stops current series and notifies the users.

        Called in response to errors.

        :param str message: Optional message to be sent to users
        """
        self.stop()
        self.experiment_callback.send_series_done(message)

    @abstractmethod
    def tear_down(self):
        """
        Cleans up this instance and releases all attached resources.


        """
        pass

    @abstractmethod
    def power_up(self, mode: str):
        """
        Powers up the device and sets it's mode (if applicable).
        Called when experiment receives message with namespace :ref:`proto-silf-mode-set`.
        """
        pass

    @abstractmethod
    def power_down(self):
        """
        Powers down the device, if suspend is false it also cleans up the remote
        device kills and remote process.
        Called in response to: :ref:`proto-silf-experiment-stop`
        """
        pass

    @abstractmethod
    def loop_iteration(self):
        """
        Subclasses should override that so this method needed background tasks.

        Called repeatedly (even when experiment is stopped or powered down).

        Approximate timeout is configured by the config file (``LoopTimeout``).
        """
        pass

    def on_miscellaneous_error(self):
        """
        Called from catch block when this Manager raises a miscellaneous error
        (that is not an instance of SilfProtocolError).

        Default implementation just raises SILFProtocolError.

        If error needs to be propagated to users please raise s
        :type:`SILFProtocolError`.
        """
        self.logger.exception("Miscellaneous error")
        __, exc, __ = sys.exc_info()
        raise SILFProtocolError(current_exception_to_silf_error_stanza()) from exc

    @abstractmethod
    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        """
        Performs diagnostics on all the devices before power up.
        Executed once on first mode set.

        :param str diagnostics_level: Level of the diagnostics.
        :raises .SILFProtocolError: When there are errors in any device.

        """

    @abstractmethod
    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        """
        Performs diagnostics on all the devices after power up.
        Executed once on first mode set.

        :param str diagnostics_level: Level of the diagnostics.
        :raises .SILFProtocolError: When there are errors in any device.

        """

    @property
    @abstractmethod
    def translations(self) -> typing.Mapping[str, GNUTranslations]:
        """
        Maps from language to GNUTranslations object.
        """

    @abstractmethod
    def get_available_languages(self):
        """
        Returns list of languages in which it is possible for experiment server to return messages presented to user.
        :return: List of available languages
        """
        raise NotImplementedError

    @abstractmethod
    def set_current_language(self, lang_code: str):
        """
        Method allows to set language in which messages used by experiment server will be translated.
        Must be one of the languages available in method get_available_languages
        :param lang_code Language code to set the experiment messages
        :raise ValueError when language code is unknown
        """
        raise NotImplementedError


class TranslationMixin(IExperimentManager):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.has_translations = False
        self.current_lang = None
        """
        :class:`string` lang code for currently selected language. This lang code
        indicates language in which are send messages for GUI
        """
        self.__translations = self.__load_translations()
        self.has_translations = len(self.__translations) > 0
        self.set_current_language(self.get_available_languages()[0])

    @property
    def translations(self) -> typing.Mapping[str, GNUTranslations]:
        return self.__translations

    def __load_translations(self):
        translations = {}
        translation_domain = re.sub(r"\s+", "-", self.get_experiment_name().lower())
        if self.config.has_option("Experiment", "TranslationsRootDir"):
            translation_dir = self.config["Experiment"]["TranslationsRootDir"]
            language_config_option = self.config["Experiment"]["AvailableLanguages"]
            languages_from_config = [
                s.strip() for s in language_config_option.split(",")
            ]
            for lang in languages_from_config:
                tr = gettext.translation(
                    translation_domain, translation_dir, [lang], codeset="utf-8"
                )
                translations[lang] = tr
        else:
            self.logger.warning(
                "Missing TranslationsRootDir config option. "
                "Experiment will use only default translation"
            )
        return translations

    def get_available_languages(self):
        """
        Returns list of languages in which it is possible for experiment server to return messages presented to user.
        :return: List of available languages
        """
        if not self.has_translations:
            return ["pl"]
        return sorted(self.translations.keys())

    def set_current_language(self, lang_code: str):
        """
        Method allows to set language in which messages used by experiment server will be translated.
        Must be one of the languages available in method get_available_languages
        :param lang_code Language code to set the experiment messages
        :raise ValueError when language code is unknown
        """
        if not self.has_translations:
            return
        if lang_code not in self.translations:
            raise ValueError("Unknown language code: " + lang_code)
        set_translation(self.translations[lang_code])
        self.logger.info("Set language: " + lang_code)


class BaseExperimentManager(TranslationMixin, IExperimentManager, metaclass=ABCMeta):
    """
    Instances of this class are representing the experiment. They:

    * Contain a contain a dict of :class:`.IDeviceManager`, particular device
      managers are can be accessed using :meth:`.__getitem__`
    * Manage lifecycle of these devices
    * Take and respond to lo lifecycle methods using protocol objects.
    * Handle translations
    """

    LOOP_TIMEOUT = 0.1
    """
    Preferred time between calls to :class:`.loop_iteration`.
    """

    def __init__(self, config_parser):
        super().__init__(config_parser)
        self.__running = False

    def _set_running(self, value):
        self.__running = value

    def get_series_name(self, settings):
        """
        This method returns name of result series, this name will be presented
        to users. Additionally this method gets settings that will be applied for
        this series.

        See also default implementation at:
        :meth:`.AbstractExperimentManager.get_series_name`

        :return: Name of result series
        :rtype: str
        """
        return "Seria {}".format(self.__series_no)

    @property
    def running(self):
        """
        True if the device is performing acquisition now :class:`bool`
        """
        return self.__running

    @abstractmethod
    def initialize(self, experiment_callback):
        """
        Initializes this experiment manager. Called after XMPP connection is
        established, but before any input from users is processed.

        :param experiment_callback: Callback this experiment uses.
        :type experiment_callback: :class:`ExperimentCallback`
        """
        self.experiment_callback = experiment_callback

    @abstractmethod
    def __getitem__(self, item):
        """
        Returns device manager by name

        :param str item: Name oe the experiment manager
        :rtype: :class:`.IDeviceManager`
        """

    @property
    def modes(self):
        """
        Returns modes this experiment can supports, it is called in response
        to: :ref:`proto-silf-mode-get`.

        :return: Modes
        :rtype: :class:`.ModeSuite`.
        """
        return ModeSuite(defaut=Mode("Tryb domyślny"))

    @abstractmethod
    def get_input_fields(self, mode):
        """
        Returns control suite for `current_mode`

        :param str mode: current_mode for which we get controls
        :return: Input fields
        :rtype: :class:`.ControlSuite`
        """
        return ControlSuite()

    @abstractmethod
    def get_output_fields(self, mode):
        """
        Returns output fields for `current_mode`

        :param str mode: current_mode for which we get output fields
        :rtype: :class:`.OutputFieldSuite`
        """
        return OutputFieldSuite()

    @abstractmethod
    def check_settings(self, mode, settings):
        """
        Checks settings for mode. This method should raise exception on
        any errors in settings.

        Used when client sends :ref:`proto-silf-settings-check`
        or :ref:`proto-silf-series-start`.

        :param str mode: Mode for which we check settings for validity
        :param settings: Settings to check
        :type settings: :class:`.SettingsSuite`
        :raises: :class:`silf.backend.commons.api.exceptions.SILFProtocolError`
            if there are any errors in settings, or if there is any error during setting
            the equipments up.

        """

    @abstractmethod
    def apply_settings(self, settings):
        """
        Applies settings to the experiment.

        This is called when user sends :ref:`proto-silf-series-start`.

        :type settings: :class:`.SettingsSuite`
        :raises: :class:`silf.backend.commons.api.exceptions.SILFProtocolError`
            if there are any errors in settings, or if there is any error during setting
            the equipments up.
        """

    @abstractmethod
    def start(self):
        """
        Starts acquisition in response to :ref:`proto-silf-series-start`. Settings
        are already validated and applied to all device managers.

        :return: Name of started series
        :rtype:`str`
        """
        self.__series_no += 1

    def power_down_and_notify(self, message=None):
        """

        Powers down the experiment and notifies the users.

        :param str message: Optional message to be sent to users
        :return:
        """
        self.power_down()
        self.experiment_callback.send_experiment_done(message)

    @abstractmethod
    def stop(self):
        """
        Stops data acquisition for the experiment. It does not notify users.

        Called in response to: :ref:`proto-silf-series-stop`.

        """

    @abstractmethod
    def tear_down(self):
        """
        Cleans up this instance and releases all attached resources.

        """

    @abstractmethod
    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        """
        Performs diagnostics on all the devices before power up.
        Executed once on first mode set.

        :param str diagnostics_level: Level of the diagnostics.
        :raises .SILFProtocolError: When there are errors in any device.

        """

    @abstractmethod
    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        """
        Performs diagnostics on all the devices after power up.
        Executed once on first mode set.

        :param str diagnostics_level: Level of the diagnostics.
        :raises .SILFProtocolError: When there are errors in any device.

        """

    @abstractmethod
    def power_up(self, mode):
        """
        Powers up the device and sets it's mode (if applicable).
        Called when experiment receives message with namespace :ref:`proto-silf-mode-set`.
        """
        self.__series_no = 0

    @abstractmethod
    def power_down(self):
        """
        Powers down the device, if suspend is false it also cleans up the remote
        device kills and remote process.

        Called in response to: :ref:`proto-silf-experiment-stop`

        """
        pass

    def loop_iteration(self):
        """
        Subclasses should override that so this method needed background tasks.
        """
        pass

    @abstractmethod
    def __getitem__(self, item):
        """
        Returns device managers by name

        :param item:
        :return: :class:`.IDeviceManager`
        """

    def __del__(self):
        self.tear_down()


class AbstractExperimentManager(BaseExperimentManager):

    """
    Abstract implementation of :class:`BaseExperimentManager`, it contains a
    dictionary of :class:`IDeviceManagers` and provides following
    functionaries:

    * Lifecycle of all devices is tied to lifecycle to this instance:
      that is: all devices are started when :meth:`start` is called, and so on.
    * It gathers all metadata from :class:`IDeviceManagers`
    * It provides Auto power down functionality, that is series is stopped after
      10 minutes. And if idle (no current series) experiment powers down itself
      after next 10 minutes. This is configurable in experiment settings file:
      :ref:`experiment-config-file-document`.
    """

    DEVICE_MANAGERS = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.powered_up = False
        self.settings = self.__setting_proxy(self)
        self._managers = {}

        self.__shutdown_experiment_timeout = None
        self.__stop_series_timeout = None

        self.__last_time_running = None
        self.__session_start = None

        self.__shutdown_experiment_timeout = -1
        self.__stop_series_timeout = -1

        if self.config is not None:
            self.__shutdown_experiment_timeout = self.config.getint(
                "Experiment", "shutdown_experiment_timeout", fallback=1200
            )
            self.__stop_series_timeout = self.config.getint(
                "Experiment", "stop_series_timeout", fallback=600
            )
            self.__series_pattern = self.config.get(
                "Experiment", "series_pattern", fallback=None
            )

    @property
    def managers(self):
        return self._managers

    @property
    def current_settings(self):
        """
        Settings currently applied to all devices.
        """
        suite = self._get_suite_from_plugins(
            SettingSuite, "current_settings", do_call=False
        )
        return types.MappingProxyType({k: v.value for k, v in suite.internal_dict})

    def get_additional_input_fields(self, mode):
        """
        Method that allows to inject additional input fields. Normally subclasses
        of this class return only fields gathered from :class:`IDeviceManagers`,
        this allows it to add additional fields.
        :param str mode: Mode for which to get fields
        :return: Fields to add.
        """
        return ControlSuite()

    def get_additional_output_fields(self, mode):
        """
        Method that allows to inject additional output fields. Normally subclasses
        of this class return only fields gathered from :class:`IDeviceManagers`,
        this allows it to add additional fields.
        :param str mode: Mode for which to get fields
        :return: Fields to add.
        """
        return OutputFieldSuite()

    @property
    def current_results(self):
        """
        All results from start of series till now.
        """
        return self._get_suite_from_plugins(
            ResultSuite, "current_results", do_call=False
        )

    def get_series_name(self, settings):
        if self.__series_pattern is None:
            return super().get_series_name(settings)
        series_dict = {
            name: str(setting.value) for name, setting in settings.settings.items()
        }
        try:
            return self.__series_pattern.format(**series_dict)
        except KeyError as e:
            raise ConfigurationException(
                "Invalid key {} in series pattern '{}', probably you just need "
                "to fix `series_pattern` entry in experiment.ini".format(
                    e.args[0], self.__series_pattern
                )
            )

    def initialize(self, experiment_callback):
        super().initialize(experiment_callback)
        self._managers = {}
        for k, v in self.DEVICE_MANAGERS.items():
            try:
                self._managers[k] = v(self.get_experiment_name(), self.config)
            except Exception as e:
                raise ExperimentCreationError(
                    "Error creating manager {}:{}".format(k, v)
                ) from e

    def __getitem__(self, item):
        """
        Returns device managers by name
        :param item:
        :return:
        """
        return self.managers[item]

    def get_input_fields(self, mode):
        cs = self._get_suite_from_plugins(ControlSuite, "get_input_fields", mode)
        return ControlSuite.join(cs, self.get_additional_input_fields(mode))

    def get_output_fields(self, mode):
        os = self._get_suite_from_plugins(OutputFieldSuite, "get_output_fields", mode)
        return OutputFieldSuite.join(os, self.get_additional_output_fields(mode))

    def stop(self):
        self._set_running(False)

    def start(self):
        self._set_running(True)
        self.__session_start = time.monotonic()

    def power_down(self):
        self._set_running(False)
        self.powered_up = False

    def power_up(self, mode):
        super(AbstractExperimentManager, self).power_up(mode)
        self.powered_up = True
        if self.running:
            self.stop()
        self.__last_time_running = time.monotonic()

    def perform_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        warnings.warn(
            "perform_diagnostics is deprecated, use pre_power_up diagnostics and "
            "post_power_up_diagnostics",
            category=DeprecationWarning,
        )
        self.post_power_up_diagnostics(diagnostics_level)

    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        for dm in self.managers.values():
            print(dm)
            dm.pre_power_up_diagnostics(diagnostics_level)
        self._gather_exceptions("pre_power_up_diagnostics", diagnostics_level)

    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        for dm in self.managers.values():
            dm.post_power_up_diagnostics(diagnostics_level)
        self._gather_exceptions("post_power_up_diagnostics", diagnostics_level)

    def check_settings(self, mode, settings):
        self._gather_exceptions(
            "check_settings", mode, settings, error_type=ValidationError
        )

    def tear_down(self):
        self._call_on_plugins("tearDown")

    def loop_iteration(self):
        super().loop_iteration()
        self._update_experiment_timeout()
        self._update_session_timeout()

    # PRIVATE STUFF

    def _call_on_plugins(self, method_name, *args, **kwargs):
        do_call = kwargs.pop("do_call", True)
        results = []
        for p in self.managers.values():
            # print(method_name, args, kwargs)
            getattr_result = getattr(p, method_name)
            if do_call:
                results.append(getattr_result(*args, **kwargs))
            else:
                results.append(getattr_result)
        return results

    def _get_suite_from_plugins(self, SuiteClass, method_name, *args, **kwargs):
        return SuiteClass.join(*self._call_on_plugins(method_name, *args, **kwargs))

    def _gather_exceptions(self, method_name, *args, **kwargs):
        error_type = kwargs.pop("error_type", SILFProtocolError)
        results = []
        for p in self.managers.values():
            method = getattr(p, method_name)
            try:
                method(*args, **kwargs)
            except SILFProtocolError as e:
                results.append(e)
        if results:
            raise error_type.join(*results)

    class __setting_proxy(object):
        def __init__(self, parent):
            super().__init__()
            self.parent = parent

        def __getitem__(self, item):
            return self.parent.managers[item].current_settings

    def _update_session_timeout(self):
        if self.running:

            self.__last_time_running = time.monotonic()

            if self.__stop_series_timeout < 0:
                return

            time_from_session_start = time.monotonic() - self.__session_start

            if time_from_session_start > self.__stop_series_timeout:
                self.logger.error(
                    "Experiment series finished prematurely because series took too long"
                )
                self.stop_and_notify(
                    "Koniec serii pomiarowej ze względu na przekroczenie maksymalnego czasu"
                )

    def _update_experiment_timeout(self):

        if not self.powered_up:
            return

        if self.running:
            return

        if self.__shutdown_experiment_timeout < 0:
            return

        time_from_session_end = time.monotonic() - self.__last_time_running

        if time_from_session_end > self.__shutdown_experiment_timeout:
            self.logger.error(
                "Experiment shut down because experiment was idle for too long"
            )
            self.power_down_and_notify(
                "Koniec eksperymentu ze względu na przekroczenie maksymalnego czasu"
            )
