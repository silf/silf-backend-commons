"""
Contains classes that cope with merging results taken from the device prior
to sending them to the client.

"""
from operator import itemgetter
from collections.abc import Sized

from silf.backend.commons.api.stanza_content._misc import Result
from silf.backend.commons.util.uniqueify import uniquefy_last

RESULTS_UNSET = object()

__all__ = [
    "ResultCreator",
    "AppendResultCreator",
    "OverrideResultsCreator",
    "XYChartResultCreator",
    "ReturnLastElementResultCreator",
]


class ResultCreator(object):

    """
    Instances of this class get results from the device and produce
    appropriate :class:`Result` instances.
    """

    def __init__(self, result_name, pragma=Result.DEFAULT_PRAGMA, *, read_from=None):
        super().__init__()
        self.result_name = result_name
        """
        Name of interesting results
        """
        self.pragma = pragma
        if read_from is None:
            read_from = result_name

        self.read_from = read_from

    @property
    def has_results(self):
        raise NotImplementedError()

    def pop_results(self) -> Result:
        """
        Returns current set of results and possibly removes
        them from results cache.

        :return: Experiment results or :ref:`RESULTS_UNSET` if there are no
            new results to return.
        :rtype: :class:`Result` or :class:`None`
        """

    def aggregate_results(self, results):
        """
        Adds `results` to results cache.
        :param dict results: dictionary containing many results values.
        """

    def clear(self):
        """
        Clears the results cache.
        """


class AppendResultCreator(ResultCreator):

    """
    Class that aggregates results from source, and empties cache on each
    call to :meth:`pop_results`.

    It is useful if device provides series of points, each of these points is
    unique, and should not be resent to client.
    >>> agg = AppendResultCreator("foo")
    >>> agg.has_results
    False
    >>> agg.pop_results()
    <Result pragma=append value=[] >
    >>> agg.aggregate_results({"foo" : [1, 2], "bar": [3, 4]})
    >>> agg.pop_results()
    <Result pragma=append value=[1, 2] >
    >>> agg.pop_results()
    <Result pragma=append value=[] >
    >>> agg.aggregate_results({"foo" : [1, 2], "bar": [3, 4]})
    >>> agg.aggregate_results({"foo" : [2, 3], "bar": [3, 4]})
    >>> agg.has_results
    True
    >>> agg.pop_results()
    <Result pragma=append value=[1, 2, 2, 3] >
    >>> agg.pop_results()
    <Result pragma=append value=[] >
    >>> agg.has_results
    False
    """

    def __init__(self, result_name, pragma="append", **kwargs):
        super().__init__(result_name, pragma, **kwargs)
        self.cache = []

    @property
    def has_results(self):
        return bool(self.cache)

    def aggregate_results(self, results):
        res = results.get(self.read_from, RESULTS_UNSET)

        if res is not RESULTS_UNSET and res is not None:
            try:
                self.cache.extend(res)
            except TypeError:
                #  Probably an element list
                self.cache.append(res)

    def clear_results(self, results_to_clear=0):
        self.cache = self.cache[results_to_clear:]

    def pop_results(self):
        if len(self.cache) > 0:
            results = self.cache
            self.cache = []
            return Result(value=results, pragma=self.pragma)
        return Result(value=[], pragma=self.pragma)


class OverrideResultsCreator(ResultCreator):

    """
    Class that always returns last result. Useful for returning device state
    to user, and when single result compromises whole result set.

    >>> agg = OverrideResultsCreator("foo")
    >>> agg.has_results
    False
    >>> agg.pop_results()
    <Result pragma=replace value=[] >
    >>> agg.aggregate_results({"foo": [1, 2, 3, 4, 5, 6], "bar": "ignored"})
    >>> agg.has_results
    True
    >>> agg.pop_results()
    <Result pragma=replace value=[1, 2, 3, 4, 5, 6] >
    >>> agg.pop_results()
    <Result pragma=replace value=[1, 2, 3, 4, 5, 6] >
    >>> agg.aggregate_results({"foo":  [7, 8, 9, 10, 11, 12], "bar": "ignored"})
    >>> agg.pop_results()
    <Result pragma=replace value=[7, 8, 9, 10, 11, 12] >
    >>> agg.aggregate_results({"bar": "ignored"})
    >>> agg.pop_results()
    <Result pragma=replace value=[7, 8, 9, 10, 11, 12] >
    >>> agg = OverrideResultsCreator("bar", read_from="foo")
    >>> agg.has_results
    False
    >>> agg.pop_results()
    <Result pragma=replace value=[] >
    >>> agg.aggregate_results({"foo": [1]})
    >>> agg.pop_results()
    <Result pragma=replace value=[1] >
    """

    def __init__(self, result_name, pragma="replace", *, read_from=None):
        super().__init__(result_name, pragma, read_from=read_from)
        self.cache = Result(value=[], pragma=self.pragma)
        if read_from is None:
            read_from = result_name
        self.read_from = read_from

    @property
    def has_results(self):
        v = self.cache.value
        if isinstance(v, Sized):
            return len(v) > 0
        return v is not None

    def pop_results(self):
        return self.cache

    def aggregate_results(self, results):
        res = results.get(self.read_from, RESULTS_UNSET)
        if res is not RESULTS_UNSET:
            self.cache = Result(value=res, pragma=self.pragma)

    def clear(self):
        self.cache = Result(value=None)


class ReturnLastElementResultCreator(ResultCreator):

    """
    >>> agg = ReturnLastElementResultCreator("foo")
    >>> agg.has_results()
    False
    >>> agg.pop_results()
    <Result pragma=append value=None >
    >>> agg.aggregate_results({'foo' : [1, 2, 3]})
    >>> agg.pop_results()
    <Result pragma=append value=3 >
    >>> agg.pop_results()
    <Result pragma=append value=3 >
    """

    def __init__(self, result_name, pragma=Result.DEFAULT_PRAGMA, *, read_from=None):
        super().__init__(result_name, pragma, read_from=read_from)

        self.value = None

    def pop_results(self):
        return Result(value=self.value, pragma=self.pragma)

    def has_results(self):
        return self.value is not None

    def clear(self):
        self.value = None

    def aggregate_results(self, results):
        res = results.get(self.read_from, RESULTS_UNSET)
        if isinstance(res, Sized):
            res = res[-1]
        if res is not RESULTS_UNSET:
            self.value = res


class XYChartResultCreator(ResultCreator):

    """

    Create this result creator

    >>> creator = XYChartResultCreator(result_name="baz", read_from=['foo', 'bar'])

    At the beginning it is empty

    >>> creator.pop_results()
    <Result pragma=replace value=[] >
    >>> creator.has_results
    False

    Now let's append some results

    >>> creator.aggregate_results({"foo": [1, 2, 3], "bar": [-1, -2, -3]})
    >>> creator.has_results
    True
    >>> creator.pop_results()
    <Result pragma=replace value=[[1, -1], [2, -2], [3, -3]] >

    Results are stored

    >>> creator.has_results
    True
    >>> creator.pop_results()
    <Result pragma=replace value=[[1, -1], [2, -2], [3, -3]] >

    Results are cleared after call to clear:

    >>> creator.clear()
    >>> creator.has_results
    False
    >>> creator.pop_results()
    <Result pragma=replace value=[] >

    After appending only one coordinate we don't produce results

    >>> creator.aggregate_results({"foo": [1, 2, 3]})
    >>> creator.has_results
    False
    >>> creator.pop_results()
    <Result pragma=replace value=[] >


    After adding second coordinate we produce them:

    >>> creator.aggregate_results({"bar": [1]})
    >>> creator.has_results
    True
    >>> creator.pop_results()
    <Result pragma=replace value=[[1, 1]] >


    >>> creator = XYChartResultCreator(result_name="baz", read_from=['foo', 'bar'])
    >>> creator.aggregate_results({"foo": [1, 1, 1], "bar": [-1, -2, -3]})
    >>> creator.pop_results()
    <Result pragma=replace value=[[1, -3]] >

    """

    def __init__(self, result_name, pragma="replace", *, read_from=tuple()):

        if read_from is None or len(read_from) != 2:
            raise ValueError()

        super().__init__(result_name, pragma, read_from=read_from)

        self.__clear_cache()

    def __clear_cache(self):
        self.cache = [
            AppendResultCreator(self.read_from[0]),
            AppendResultCreator(self.read_from[1]),
        ]

        self.point_cache = []

    @property
    def has_results(self):
        return bool(self.point_cache)

    def aggregate_results(self, results):
        [rc.aggregate_results(results) for rc in self.cache]
        resuls = []

        for x, y in zip(self.cache[0].cache, self.cache[1].cache):
            resuls.append([x, y])

        [rc.clear_results(len(resuls)) for rc in self.cache]

        self.point_cache.extend(resuls)

        self.point_cache = uniquefy_last(self.point_cache, idfun=itemgetter(0))

        pass

    def pop_results(self):

        return Result(value=self.point_cache, pragma=self.pragma)

    def clear(self):
        self.__clear_cache()
