from collections import defaultdict
import contextlib
from functools import partial
from abc import ABCMeta, abstractmethod
from copy import deepcopy
from itertools import chain
import logging
import types
import numbers
import warnings
from silf.backend.commons.api.stanza_content import _misc

from silf.backend.commons.api.exceptions import SILFProtocolError
from silf.backend.commons.device_manager._worker import DeviceWorkerWrapper

from silf.backend.commons.api import *
from silf.backend.commons.device import *


__all__ = [
    "IDeviceManager",
    "DefaultDeviceManager",
    "SingleModeDeviceManager",
    "exception_translator",
]


@contextlib.contextmanager
def exception_translator():
    """
    Wrapper that catches all :class:`DeviceException` and raises them again as
    :class:`SilfProtocolError`.

    >>> with exception_translator():
    ...     raise DeviceException("Bar")
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.SILFProtocolError: Bar

    >>> with exception_translator():
    ...     raise TypeError("Foo")
    Traceback (most recent call last):
    TypeError: Foo

    :rtype None:
    """
    try:
        yield
    except DeviceException as e:
        if e.fixable_by_user:
            severity = "warning"
            error_type = "user"
        else:
            severity = "error"
            error_type = "device"
        raise SILFProtocolError.from_args(
            severity=severity, error_type=error_type, message=e.message
        ) from e


class StateTranstionListener(object):

    """
    >>> stl = StateTranstionListener("source")
    >>> cb= lambda *args, **kwargs: print("callback:", args, kwargs)
    >>> stl.register_transition(cb, "new_state")
    >>> stl.fire_callbacks("new_state", "old_state")
    callback: ('new_state', 'old_state') {'source': 'source'}
    >>> stl.fire_callbacks("another_new_state", "old_state")
    >>> stl.fire_callbacks("new_state", "another_old_state")
    callback: ('new_state', 'another_old_state') {'source': 'source'}
    >>> stl.register_transition(cb, "another_new_state", from_state="old_state")
    >>> stl.fire_callbacks("another_new_state", "old_state")
    callback: ('another_new_state', 'old_state') {'source': 'source'}
    >>> stl.fire_callbacks("another_new_state", "another_old_state")


    """

    def __init__(self, source=None):
        super().__init__()
        self.__source = source
        self.__callbacks = defaultdict(lambda: defaultdict(list))

    def register_transition(self, callback, to_state, *, from_state=None):
        self.__callbacks[to_state][from_state].append(callback)

    def fire_callbacks(self, current_state, old_state):

        partial = self.__callbacks[current_state]
        for callback in chain(partial[old_state], partial[None]):
            callback(current_state, old_state, source=self.__source)


class IDeviceManager(metaclass=ABCMeta):

    """
    Instances of this class serve as proxy and translator between main experiment
    :class:`.Experiment` and
    :class:`.Device`.

    TODO: add link do the experiment

    It translated unfiltered input from user to input recognizable to device,
    it also contains metadata needed by the experiment.

    """

    device_name = "default"
    """
    Name of the device. Should be unique in the experiment.
    """

    config_section = None
    """
    Section that customizes this device in config file
    """

    def __init__(self, experiment_name, config):
        """
        :param str experiment_name: Name of experiment this device is attached
                                    to.
        :param configparser.ConfigParser config: Configuration for whole
            experiment.
        :return:
        """

        self.config = config
        self.experiment_name = experiment_name
        self.state_transition_listener = StateTranstionListener(source=self)
        self._state = DEVICE_STATES[OFF]

    def _update_state(self, new_state):
        if self.state == new_state:
            return
        old_state = self._state
        self._state = new_state
        self.state_transition_listener.fire_callbacks(new_state, old_state)

    def register_state_transition_listener(
        self, callback, to_state, *, from_state=None
    ):
        self.state_transition_listener.register_transition(
            callback, to_state, from_state=from_state
        )

    @property
    @abstractmethod
    def current_mode(self):
        """
        Current mode this device is in.
        :return: Current mode
        :rtype: str
        """
        return None

    @property
    def applicable_modes(self):
        """
        List of modes this device can be in. It is used purely for validation
        purposes, if we try to :meth:`.power_up` this device in a mode
        that is outside of :attr:`.applicable_modes` exception should be raised.

        If value returned is :data:`None` it means that this device is mode
        agnostic,

        :return: Modes applicable for this device
        :rtype: list of strings or None.
        """
        return None

    def query(self):
        """
        This method will be called periodically it should perform any neccesary
        communication with the device to refresh state of this instane.

        It should exit in in less than 50ms.
        """
        pass

    @property
    @abstractmethod
    def device_state(self):
        """
        State of device managed by this manager.
        """
        raise NotImplementedError()

    @property
    def state(self):
        """
        State of this manager, may different from the :attr:`.device_state`

        :return: State of this manager
        :rtype: :class:`str`
        """
        return self._state

    @property
    @abstractmethod
    def running(self):
        """

        If this device is running it means:

        * That :meth:`start` was called
        * That calls to :meth:`perform_diagnostics`, :meth:`power_up`,
          :meth:`apply_settings`, :meth:`start` should fail.
        * Results will be updated periodically.

        If this device is not running it means:

        * Results are None

        :return: Whether this device is running.
        :rtype: :class:`bool`
        """
        return False

    @property
    @abstractmethod
    def current_results(self):
        """
        Current results, that is: all results produces in current series.
        This returns class that is serializable and ready to be sent. For
        developer friendly version use :meth:`current_result_map`.

        This property is resetted during :meth:`stop` method.

        :return: Current results
        :rtype: :class:`ResultSuite`
        """
        return ResultSuite()

    @property
    @abstractmethod
    def current_settings(self):
        """
        Currenty applied settings.

        This property is resetted during :meth:`power_down` method.

        :return: Currently applied settings. Mapping from setting name to setting value.
        :rtype: :class:`dict`
        """
        return {}

    @property
    def current_result_map(self):
        """
        Current results, that is: all results produces in current series.
        This method returns a plain dictionary.

        :return: Current results
        :rtype: immutable :class:`dict`
        """
        return types.MappingProxyType(
            {k: v.value for k, v in self.current_results.internal_dict.items()}
        )

    def has_result(self, name):
        """
        :return: Returns true if this instance has result named `name`
            in it's current results. Result is considered valid if it's value
            resolves to :data:`True` (not `None`, if collection is must be not
            empty, etc).
        :rtype: bool
        """
        result = self.current_result_map.get(name, None)
        if isinstance(result, numbers.Number):
            return True  # If result is zero it is a still valid result
        return bool(result)

    @abstractmethod
    def pop_results(self):
        """
        Return new results that could be sent to user.
        This method mutates the state of this instance, immidiately after a call
        to :meth:`pop_results` next call will return empty
        :class:`.ResultSuite`.

        :return: Recently acquired results
        :rtype: :class:`.ResultSuite`
        """
        return ResultSuite()

    @abstractmethod
    def get_input_fields(self, mode):
        """
        Returns _control suite for `mode`
        :param str mode: mode for which we get controls
        :rtype: :class:`.ControlSuite`
        """
        return ControlSuite()

    @abstractmethod
    def get_output_fields(self, mode):
        """
        Returns output fields for `mode`
        :param str mode: mode for which we get output fields

        :return: Returns output fields for `mode`
        :rtype: :class:`.OutputFieldSuite`
        """
        return OutputFieldSuite()

    def check_settings(self, mode, settings):
        """
        Checks settings for a particular mode. This method is implemented,
        using native validation of class:`.ControlSuite`,
        using :meth:`get_input_fields`.

        :param str mode: Mode for which settings are checked.
        :param settings: Settings to be checked.
        :type settings: :class:`.SettingSuite`

        :raises .SILFProtocolError: When settings are invalid
        """
        controls = self.get_input_fields(mode)
        controls.check_settings(settings)

    @abstractmethod
    def apply_settings(self, settings):
        """
        Applies settings to the device.

        :param dict settings: settings to be applied. Can contain settings that
                              are not interesting for this device, these should
                              be filtered out. It contains raw input from
                              json parser, values might need to be converted
                              to python objects.
        :raises error.SILFProtocolError: if there are any errors in settings,
                                     or if there is any error during setting
                                     the equipement up.
        """

        self.check_settings(self.current_mode, settings)

    @abstractmethod
    def start(self, callback=None):
        """
        Starts data acquisition for this device.

        :param callback: Optional callable to be called when devie is started.
        """

    @abstractmethod
    def stop(self):
        """
        Stops data acquisition for the device.
        """

    @abstractmethod
    def tearDown(self):
        """
        Cleans up this instance and releases all attached resources.
        """
        warnings.warn(
            "Used deprecated function tearDown, You should rename it to tear_down",
            DeprecationWarning,
        )
        self.tear_down()

    @abstractmethod
    def tear_down(self):
        """
        Cleans up this instance and releases all attached resources.
        """

    @abstractmethod
    def perform_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        warnings.warn(
            "perform_diagnostics is deprecated, use pre_power_up diagnostics and post_power_up_diagnostics",
            DeprecationWarning,
        )
        self.post_power_up_diagnostics(diagnostics_level)

    @abstractmethod
    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        pass

    @abstractmethod
    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        pass

    @abstractmethod
    def power_up(self, mode):
        """
        Powers up the device and sets it's mode (if applicable).

        :param str mode: Mode to initialize device in.
        """

    @abstractmethod
    def power_down(self, suspend=False):
        """

        Powers down the devie, if suspend is false it also cleans up the remote
        device kills and remote proces.

        :param bool suspend: If :data:`False` will kill the remote process.
        """

    def __del__(self):
        self.tear_down()


class ResultMixin(IDeviceManager):

    """
    This mixin handles getting results from the experiment,

    Each entry in :attr:`RESULT_CREATORS` define single result that will be sent
    to the client. So class in example will send only two results:
    `foo` and `bar`, even if device itself will prowide other results.

    Contents of particular result fields (`foo` and `bar`) are totally dependent
    on result creators.

    Additionaly you can override :meth:`.ResultMixin._convert_result` that allows for
    for more extennsive costumistaion.

    >>> from silf.backend.commons.device_manager import *
    >>> from silf.backend.commons.device_manager._result_creator import *
    >>> from silf.backend.commons.util.abc_utils import patch_abc

    >>> p = patch_abc(ResultMixin)

    >>> class TestResultMixin(ResultMixin):
    ...     RESULT_CREATORS = [
    ...         AppendResultCreator("foo"),
    ...         OverrideResultsCreator("bar")
    ...     ]
    ...
    ...     RESULTS = []
    ...
    ...     def _pop_results_internal(self):
    ...         try:
    ...             return self.RESULTS
    ...         finally:
    ...             self.RESULTS = []

    >>> test = TestResultMixin(None, None)

    >>> test.RESULTS = [{"foo": [1, 2, 3], "bar": 1.15}, {"foo": [4, 5], "bar" : 3.12}]

    >>> test.current_results
    <ResultSuite bar=<Result pragma=replace value=3.12 > foo=<Result pragma=append value=[1, 2, 3, 4, 5] > >

    >>> test.current_results
    <ResultSuite bar=<Result pragma=replace value=3.12 > foo=<Result pragma=append value=[1, 2, 3, 4, 5] > >

    >>> test.pop_results()
    <ResultSuite bar=<Result pragma=replace value=3.12 > foo=<Result pragma=append value=[1, 2, 3, 4, 5] > >

    >>> test.pop_results()
    <ResultSuite bar=<Result pragma=replace value=3.12 > >

    >>> test.clear_current_results()

    >>> test.pop_results()
    <ResultSuite >

    >>> test.current_results
    <ResultSuite >

    Clean up:
    >>> p.stop()
    """

    RESULT_CREATORS = []
    """
    A list that maps str (name of the result recieved from the device) to
    instance of :class:'result_appender.ResultAggregator`.
    """

    @abstractmethod
    def _pop_results_internal(self):
        """
        Returns raw results from the device.
        :return: list of dicts.
        """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__clear_results()

    @property
    def current_results(self):
        """
        :rtype: :class:`misc.ResultSuite`
        """
        self._update_results()
        return self.__current_results

    def clear_current_results(self):
        """
        Clears current results (that is after this call it is guaranteed that
        both :attr:`current_results` and :attr:`pop_results` will be empty.
        :return:
        """
        self.pop_results()  # After this we have pulled all remaining data
        self.__clear_results()

    def __clear_results(self):
        self.__diff_result_creators = deepcopy(self.RESULT_CREATORS)
        self.__current_results_creators = deepcopy(self.RESULT_CREATORS)
        self.result_names = set([c.result_name for c in self.__diff_result_creators])
        self.__current_results = ResultSuite()

    def pop_results(self):
        """

        :return:
        """
        self._update_results()
        diff_results = {}
        for diff in self.__diff_result_creators:
            if diff.has_results:
                diff_results[diff.result_name] = diff.pop_results()

        if hasattr(self, "logger"):
            self.logger.debug(
                "Popping results {}".format(diff_results), stack_info=False
            )

        return ResultSuite(**diff_results)

    def _convert_result(self, dict):
        """
            Enables to translate between device and experiment results.
        """
        return dict

    def _update_results(self):
        results = self._pop_results_internal()

        results = [self._convert_result(dict) for dict in results]

        for curr in self.__current_results_creators:
            curr.aggregate_results(
                {k: v.value for k, v in self.__current_results.internal_dict.items()}
            )

        for result_set in results:
            for curr, diff in zip(
                self.__current_results_creators, self.__diff_result_creators
            ):
                curr.aggregate_results(result_set)
                diff.aggregate_results(result_set)

        current = {}
        for curr in self.__current_results_creators:
            if curr.has_results:
                current[curr.result_name] = curr.pop_results()
        self.__current_results = ResultSuite(**current)


class SingleModeDeviceMixin(IDeviceManager):

    """
    Mixin for single current_mode device.
    """

    CONTROLS = None

    OUTPUT_FIELDS = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.CONTROLS is None:
            self.CONTROLS = ControlSuite()
        if self.OUTPUT_FIELDS is None:
            self.OUTPUT_FIELDS = _misc.OutputFieldSuite()

    def get_input_fields(self, mode):
        """
        Returns _control suite for `current_mode`
        :param str mode: current_mode for which we get controls (Ignored)
        :return:
        :rtype: misc.ControlSuite
        """
        return self.CONTROLS

    def get_output_fields(self, mode):
        """
        Returns output fields for `current_mode`
        :param str mode: current_mode for which we get output fields (Ignored)
        :return:
        :rtype: :class:`_control.OutputFieldSuite`
        """
        return self.OUTPUT_FIELDS


class DeclarativeDeviceMixin(IDeviceManager):

    """
    >>> from silf.backend.commons.api import *
    >>> from silf.backend.commons.util.abc_utils import patch_abc

    >>> foo_mode = ControlSuite(
    ...   NumberControl("foo", "Insert foo", max_value=10),
    ...   NumberControl("bar", "Insert bar")
    ... )

    >>> bar_mode = ControlSuite(
    ...   NumberControl("foo", "Insert foo", max_value=10),
    ...   NumberControl("bar", "Insert bar")
    ... )

    >>> output_fields = _misc.OutputFieldSuite(
    ...     bar=_misc.OutputField("foo", "bar")
    ... )


    Enable instantiating `DeclarativeDeviceMixin` (it is an ABC so you
    shouldn't be able to do this normallu
    >>> p = patch_abc(DeclarativeDeviceMixin)

    >>> class DeclarativeDeviceMixinTest(DeclarativeDeviceMixin):
    ...     CONTROL_MAP = {'foo' : foo_mode, 'bar' : bar_mode}
    ...     OUTPUT_FIELD_MAP = {
    ...         'foo' : output_fields,
    ...         'bar' : output_fields,
    ...     }

    >>> manager = DeclarativeDeviceMixinTest(None, None)

    **Basic operatoons**

    >>> manager.get_input_fields('foo')
    <ControlSuite <Control name=bar type=number live=False label='Insert bar'> <Control name=foo type=number live=False label='Insert foo'>>

    >>> manager.get_input_fields('bar')
    <ControlSuite <Control name=bar type=number live=False label='Insert bar'> <Control name=foo type=number live=False label='Insert foo'>>

    >>> manager.get_input_fields("baz") # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
    KeyError: 'baz'

    >>> manager.get_output_fields("foo")
    <OutputFieldSuite bar=<Result class=foo name=['bar'] type=array metadata={} settings={}> >

    Clean up:
    >>> p.stop()

    """

    CONTROL_MAP = {}

    OUTPUT_FIELD_MAP = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_input_fields(self, mode):
        """
        Returns _control suite for `current_mode`
        :param str mode: current_mode for which we get controls
        :return:
        :rtype: misc.ControlSuite
        """
        warnings.warn(
            "DeclarativeDeviceMixin is deprecated - implement this method and "
            "dynamically create output field dictionary",
            DeprecationWarning,
        )
        return self.CONTROL_MAP[mode]

    def get_output_fields(self, mode):
        """
        Returns output fields for `current_mode`
        :param str mode: current_mode for which we get output fields
        :return:
        :rtype: :class:`_control.OutputFieldSuite`
        """
        warnings.warn(
            "DeclarativeDeviceMixin is deprecated - implement this method and "
            "dynamically create output field dictionary",
            DeprecationWarning,
        )
        return self.OUTPUT_FIELD_MAP[mode]

    @property
    def applicable_modes(self):
        return self.CONTROL_MAP.keys()

    ___INVALID_MODE_ERROR = """
Mode '{}' is invalid for device '{}', this device supports {}.
    """.strip()

    __SANITY_CHECK_ERROR = """
DeclarativeDevice should contains {map_name} for all MODES, for now it supports modes {modes} and has {map_name} for {map_modes}
""".strip()


class DeviceWrapperMixin(ResultMixin):
    """
    Device manager that wraps a device.
    """

    DEVICE_CONSTRUCTOR = None
    """
    Type of the device, or callable that takes single argument: the device id.
    """

    DEVICE_ID = ""
    """
    Id of the device
    """

    AUTOMATICALLY_POOL_RESULTS = True
    """
    Boolean value. If true device will automatically pool results on each
    iteration.
    """

    BIND_STATE_TO_DEVICE_STATE = True
    """
    If true state of this device will be synchronized with state of underlying
    device. State is updated on every call to :meth:`query`.
    """

    USE_WORKER = True
    """
    If this is set to true this instance will spawn device in dofferent process,
    and all methods will be executed asynchroneusly. If set to False methods
    will be executed in current thread.

    .. warning::

        This is sometimes usefull when debugging stuff, but may break stuff in
        weird ways. Never use it in production.


    """

    __current_settings = types.MappingProxyType({})

    def __init__(self, experiment_name, config):
        super().__init__(experiment_name, config)

        self.device = None

        self.__running = False
        self.__current_settings = {}
        self.__current_mode = None
        self.__device_state = DEVICE_STATES[OFF]
        self.logger = logging.getLogger(".".join(("manager", self.DEVICE_ID)))
        self.device_state_transition_manager = StateTranstionListener()

    def _update_device_state(self, new_state):
        if self.__device_state != new_state:
            old_state = self.__device_state
            self.__device_state = new_state
            self.logger.info(
                "Device state changed from {} to {}".format(old_state, new_state)
            )
            self.device_state_transition_manager.fire_callbacks(
                self.__device_state, old_state
            )

    def _construct_device(self):
        """
        Protected method that is used to construct the internal device.
        :return: `WorkerWrapper`
        """
        constructor = partial(
            self.DEVICE_CONSTRUCTOR, self.DEVICE_ID, config_file=self.config
        )
        if self.USE_WORKER:
            device = DeviceWorkerWrapper(
                self.DEVICE_ID, constructor, config_file=self.config
            )
            device.auto_pull_results = self.AUTOMATICALLY_POOL_RESULTS
            device.start_subprocess()
        else:
            device = constructor()
        return device

    def register_state_transition_listener(
        self, callback, to_state, *, from_state=None, for_device=False
    ):
        if not for_device:
            super().register_state_transition_listener(
                callback, to_state, from_state=from_state
            )
        else:
            self.device_state_transition_manager.register_transition(
                callback, to_state, from_state=from_state
            )

    @property
    def current_settings(self):
        return types.MappingProxyType(self.__current_settings)

    @property
    def current_mode(self):
        return self.__current_mode

    def power_up(self, mode):
        """
        Creates and powers up the device.

        See :meth:`IDeviceManager.power_up`.
        """
        self.clear_current_results()
        self.__current_mode = mode

        with self._exception_translator():
            if self.device is None:
                try:
                    self.device = self._construct_device()
                    self.device.power_up()
                except Exception:
                    self.device = None
                    raise

    @property
    def running(self):
        """
        See :meth:`IDeviceManager.running`.
        """
        return self.state == DEVICE_STATES[RUNNING]

    @property
    def device_state(self):
        if self.device is None:
            return DEVICE_STATES[OFF]
        return self.device.state

    def _convert_settings_to_device_format(self, converted_settings):
        """
        Converts settings from format readable to user, to format readable
        to device.
        :param dict converted_settings: Parsed, validated, settings from the user.
        :return: Converted settings
        :rtype: :class:`dict`
        """
        return converted_settings

    def _apply_settings_to_device(self, converted_settings):
        """
        Applies settings to the device.
        :param dict converted_settings: Result of settings conversion.
        :return:
        """
        with self._exception_translator():
            settings = self._convert_settings_to_device_format(converted_settings)
            self.logger.debug("Applying setings {}".format(settings))
            self.device.apply_settings(settings)

    def _load_settings_without_applying_to_device(self, settings, live):
        """
        Loads settings (updates self.current_settings)
        :param SetingsSuite settings:
        :return:
        """
        converted = self.get_input_fields(self.current_mode).check_and_convert_settings(
            settings
        )
        self.__current_settings.update(converted)
        return converted

    def apply_settings(self, settings):
        """
        See :meth:`IDeviceManager.apply_settings`.
        """
        converted = self._load_settings_without_applying_to_device(settings, False)
        self._apply_settings_to_device(converted)

    def start(self, callback=None):
        """
        See :meth:`IDeviceManager.start`.
        """
        self.clear_current_results()
        with self._exception_translator():
            if self.USE_WORKER:
                self.device.pop_results(sync=True)
                self.device.start(callback=callback, sync=True)
                self.clear_current_results()
            else:
                self.device.start()
                if callback is not None:
                    callback()
        self.__running = True

    def stop(self):
        """
        See :meth:`IDeviceManager.stop`.
        """
        with self._exception_translator():

            if self.device:
                self.device.stop(sync=True)
                self.device.pop_results(sync=True)
                assert self.device.state in (
                    DEVICE_STATES[READY],
                    DEVICE_STATES[STAND_BY],
                )
        self.clear_current_results()

        self.__running = False

    def perform_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        """
        See :meth:`IDeviceManager.perform_diagnostics`.
        """
        warnings.warn(
            "perform_diagnostics is deprecated, use pre_power_up diagnostics and post_power_up_diagnostics",
            DeprecationWarning,
        )
        self.post_power_up_diagnostics(diagnostics_level)

    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        with self._exception_translator():
            try:
                self.device.post_power_up_diagnostics(diagnostics_level)
            except Exception as e:
                raise DiagnosticsException() from e

    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        with self._exception_translator():
            if self.device is None:
                try:
                    self.device = self._construct_device()
                    self.device.pre_power_up_diagnostics(diagnostics_level)
                except Exception as e:
                    raise DiagnosticsException() from e
                finally:
                    self.power_down(suspend=False)
                    self.device = None

    def __kill_device(self, wait_time):
        if self.USE_WORKER:
            self.device.kill(wait_time)
            self.device = None
        else:
            self.device.power_down()

    def power_down(self, suspend=False, wait_time=10):
        """

        Powers down the device, and optionally kills it.

        See :meth:`IDeviceManager.power_down`.
        """
        self.__current_settings = {}
        self.clear_current_results()
        with self._exception_translator():
            if self.device is not None:
                self.device.power_down()
                if not suspend:
                    self.__kill_device(wait_time)
                    self.device = None

    def tear_down(self, wait_time=2):
        """
        Kills the device if device is present.

        See :meth:`IDeviceManager.tearDown`.
        """
        with self._exception_translator():
            if self.device is not None:
                self.__kill_device(wait_time)

    def tearDown(self, wait_time=2):
        warnings.warn(
            "Used deprecated function tearDown, You should rename it to tear_down",
            DeprecationWarning,
        )
        self.tear_down()

    def query(self):
        with self._exception_translator():
            if self.device:
                self._update_results()
                self.device.loop_iteration()
                self._update_device_state(self.device.state)
                if self.BIND_STATE_TO_DEVICE_STATE:
                    self._update_state(self.device.state)

    def _pop_results_internal(self):
        with self._exception_translator():
            if not self.device:
                return []
            return self.device.pop_results()

    _exception_translator = staticmethod(exception_translator)


class DefaultDeviceManager(DeclarativeDeviceMixin, DeviceWrapperMixin):

    """
    To create instance of this class you need to specify following attributes:

    >>> from silf.backend.commons_test.device.mock_voltimeter import MockVoltimeter
    >>> class TestDeviceManager(DefaultDeviceManager):
    ...     DEVICE_CONSTRUCTOR = MockVoltimeter
    ...     DEVICE_ID = "voltimeter1"
    ...     CONTROL_MAP = {
    ...         "mode1" : ControlSuite(NumberControl("foo", "Foo Control")),
    ...         "mode2" : ControlSuite(NumberControl("foo", "Foo Control"))
    ...     }
    ...     OUTPUT_FIELD_MAP = {
    ...         "mode2" : OutputFieldSuite(),
    ...         "mode1" : OutputFieldSuite(bar = OutputField("foo", "bar"))
    ...     }
    >>> mngr = TestDeviceManager("FooExperiment", None)
    >>> sorted(mngr.applicable_modes)
    ['mode1', 'mode2']
    >>> mngr.get_output_fields('mode1')
    <OutputFieldSuite bar=<Result class=foo name=['bar'] type=array metadata={} settings={}> >
    >>> mngr.get_input_fields('mode2')
    <ControlSuite <Control name=foo type=number live=False label='Foo Control'>>
    """


class SingleModeDeviceManager(SingleModeDeviceMixin, DeviceWrapperMixin):

    # TODO: Dodaj sprawdzanie czy wszystkie stałe są w typie zdefiniowane. Na przykład kotś nie poda DEVICE_CONSTRUCTOR to i tak powinno działać.

    """
    To create instance of this class you need to specify following attributes:

    >>> from silf.backend.commons_test.device.mock_voltimeter import MockVoltimeter
    >>> class TestDeviceManager(SingleModeDeviceManager):
    ...     DEVICE_CONSTRUCTOR = MockVoltimeter
    ...     DEVICE_ID = "voltimeter1"
    ...     CONTROLS = ControlSuite(NumberControl("foo", "Foo Control"))
    ...     OUTPUT_FIELDS = OutputFieldSuite(bar = OutputField("foo", "bar"))
    >>> mngr = TestDeviceManager("FooExperiment", None)
    >>> mngr.applicable_modes
    >>> mngr.get_output_fields('any-mode')
    <OutputFieldSuite bar=<Result class=foo name=['bar'] type=array metadata={} settings={}> >
    >>> mngr.get_input_fields('any-mode')
    <ControlSuite <Control name=foo type=number live=False label='Foo Control'>>
    """
