# coding=utf-8
from abc import abstractmethod
import time
from silf.backend.commons.device import RUNNING, READY
from silf.backend.commons.device._const import DEVICE_STATES, STAND_BY

from silf.backend.commons.device_manager._device_manager import (
    SingleModeDeviceManager,
    ResultMixin,
    DeviceWrapperMixin,
    SingleModeDeviceMixin,
    IDeviceManager,
    DeclarativeDeviceMixin,
)

__all__ = [
    "ISeriesManager",
    "SingleModeSeriesManager",
    "MultiModeSeriesManager",
    "OnFinalSeriesPoint",
]


class OnFinalSeriesPoint(Exception):
    pass


class AbstractSeriesManager(DeviceWrapperMixin):
    """
    Device manager that gathers series of datapoints, while underlying device
    can capture only single point.

    It has following abstractions:

    * Takes N measurements from 0 to :attr:`end_of_series`. Currently acquired point
      is stored as :attr:`current_point_id`.
    * Adds :meth:`move_to_next_series_point` mehod that increments current point,
      applies settings the device and starts it.

    To implement this you need to:

    * Implement :meth:`get_last_point_id` that returns number of points in
      current series.
    * Implement :meth:`_convert_settings_to_device_format` that should return
      settings for current point.
    """

    BIND_STATE_TO_DEVICE_STATE = False
    """
    This is not a configuration parameter --- it is mant to be false (it
    configures superclass).

    .. note::

        TODO: This might not be accurate (check it).
    """

    def __init__(self, experiment_name, config):
        super().__init__(experiment_name, config)
        self.work_automatically = True
        """
        If true this instance will take whole series auomatically, taking next
        point as soon as last poin has been acquired. After whole series is
        acquired device changes it's state to `ready`.
        """
        self.current_point_id = -1
        """
        Id of current point.
        """
        self.__finished_current_point = False

        self.register_state_transition_listener(
            self.__on_point_finished,
            DEVICE_STATES[READY],
            from_state=DEVICE_STATES[RUNNING],
            for_device=True,
        )

        self.register_state_transition_listener(
            self.__on_series_finished_listener,
            DEVICE_STATES[READY],
            from_state=DEVICE_STATES[RUNNING],
        )

    @property
    @abstractmethod
    def is_series_finished(self):
        pass

    @abstractmethod
    def _convert_settings_to_device_format(self, converted_settings):
        return converted_settings

    @property
    def finished_current_point(self):
        return self.__finished_current_point

    def stop(self):
        self.logger.debug("Stop")
        self.current_point_id = -1
        super().stop()
        self._update_state(DEVICE_STATES[READY])

    def move_to_next_series_point(self):
        self.current_point_id += 1
        assert (
            self.device_state != DEVICE_STATES[RUNNING]
        ), "Invalid device state: {}".format(self.device_state)
        if self.is_series_finished:
            self._update_state(DEVICE_STATES[READY])
            raise OnFinalSeriesPoint()
        self.__finished_current_point = False
        self.__started = False
        self._apply_settings_to_device(self.current_settings)
        self.device.start(sync=True)
        # assert self.device_state in (DEVICE_STATES[RUNNING], )

    def power_down(self, suspend=False, wait_time=10):
        self.current_point_id = -1
        super().power_down(suspend, wait_time)

    def power_up(self, mode):
        self.current_point_id = -1
        return super().power_up(mode)

    def start(self, callback=None):
        # No call to super is intended
        self.logger.debug("Start")
        if self.device.state == DEVICE_STATES[RUNNING]:
            self.device.stop(sync=True)
        self.current_point_id = -1
        self.clear_current_results()
        if callback is not None:
            callback()
        self._update_state(DEVICE_STATES[RUNNING])
        if self.work_automatically:
            self.move_to_next_series_point()

    def query(self):
        super().query()
        self.__query()

    def apply_settings(self, settings):
        """
        See :meth:`IDeviceManager.apply_settings`.
        """
        self.current_point_id = -1
        self._load_settings_without_applying_to_device(settings, False)

    @staticmethod
    def _calculate_point_position(point_id, num_points, start, end):
        """
        >>> ISeriesManager._calculate_point_position(0, 10, 0, 100)
        0
        >>> ISeriesManager._calculate_point_position(9, 10, 0, 100)
        100
        >>> ISeriesManager._calculate_point_position(3, 10, 0, 100) # doctest: +ELLIPSIS
        33.33...
        >>> [ISeriesManager._calculate_point_position(ii, 10, 0, 100) for ii in range(10)]
        [0, 11.11111111111111, 22.22222222222222, 33.33333333333333, 44.44444444444444, 55.55555555555556, 66.66666666666666, 77.77777777777777, 88.88888888888889, 100]
        """
        if point_id == 0:
            return start
        if point_id == num_points - 1:
            return end
        interval = (end - start) / (num_points - 1)
        return start + interval * point_id

    # def _calculate_point_position_pawel(self, point_id, ):

    def __query(self):
        if self.work_automatically and self.state == DEVICE_STATES[RUNNING]:
            if self.finished_current_point:
                try:
                    self.move_to_next_series_point()
                except OnFinalSeriesPoint:
                    self._update_results()
                    self.device.loop_iteration()
                    self._update_state(DEVICE_STATES[READY])

    def __on_point_finished(self, *args, **kwargs):
        self.logger.debug("Series managed detected finish of curren point")
        self.__finished_current_point = True

    def _on_series_finished(self):
        pass

    def __on_series_finished_listener(self, *args, **kwargs):
        self._on_series_finished()


class ISeriesManager(AbstractSeriesManager):
    def __init__(self, experiment_name, config):
        super().__init__(experiment_name, config)
        self.end_of_series = -1

    @property
    def is_series_finished(self):
        return self.current_point_id >= self.end_of_series

    def apply_settings(self, settings):
        super().apply_settings(settings)
        self.end_of_series = int(
            self.get_number_of_points_in_series(self.current_settings)
        )

    @abstractmethod
    def get_number_of_points_in_series(self, settings):
        """
        Calculates number of points in series

        :param dict settings: Mapping from setting name to setting value.
        :return: Number of points in series.
        :rtype: :class:`int`
        """
        pass


class SingleModeSeriesManager(ISeriesManager, SingleModeDeviceMixin):

    pass


class MultiModeSeriesManager(ISeriesManager, DeclarativeDeviceMixin):

    pass
