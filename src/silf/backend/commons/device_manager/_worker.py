"""
Here we have defined a wrapper that creates device on separate process
and allows to all methods of this device
"""
from configparser import ConfigParser
from distutils.log import INFO
from logging import WARNING
import pickle
from pickle import PicklingError
from sched import scheduler
import time
from silf.backend.commons.device._device import Device
from silf.backend.commons.util.config import SilfConfigParser


__all__ = [
    "DeviceWorkerWrapper",
    "set_multiprocessing",
    "reset_multiprocessing",
    "start_worker_interactive",
]

import traceback
import uuid
from multiprocessing import Queue, Process
from multiprocessing.dummy import Queue as DummyQueue, Process as DummyProcess
from queue import Empty

import sys
import logging
import io

from silf.backend.commons.device import DEVICE_STATES, RUNNING, OFF
from silf.backend.commons.api import TRACE, ExperimentBackendUnresponsive

DEVICE_REMOTE_LOGGER = logging.root

_MULTIPROCESS_DEFAULT_PARAMETER = object()

__INITIAL_MULTIPROCESSING = True

DEFAULT_MULTIPROCESS = __INITIAL_MULTIPROCESSING


def set_multiprocessing(do_multiprocess):
    global DEFAULT_MULTIPROCESS
    DEFAULT_MULTIPROCESS = do_multiprocess


def reset_multiprocessing():
    global DEFAULT_MULTIPROCESS
    DEFAULT_MULTIPROCESS = __INITIAL_MULTIPROCESSING


class TaskResponse(object):
    """
    Response for QueueTask
    """

    @classmethod
    def from_exception(cls, task_id, task_type, exception):
        file = io.StringIO()
        file.write("Error while performing task:\n")
        traceback.print_exc(file=file)
        file.seek(0)

        tb = file.read()

        DEVICE_REMOTE_LOGGER.error(tb)

        return TaskResponse(
            task_id=task_id,
            task_type=task_type,
            exception=exception,
            exception_tracebak=tb,
        )

    @classmethod
    def from_response(cls, task_id, state, result, task_type=None):
        return TaskResponse(
            task_id=task_id, state=state, result=result, task_type=task_type
        )

    def __init__(
        self,
        task_id,
        state=None,
        result=None,
        task_type=None,
        exception=None,
        exception_tracebak=None,
    ):
        self.id = task_id
        """
        ID of associated task task
        """
        self.state = state
        """
        State of device after performing this task
        """
        self.result = result
        """
        Result of performed task
        """
        self.task_type = task_type
        """
        Type of performed task
        """
        self.exception = exception
        """
        Exception raised by this task (if any)
        """
        self.exception_tracebak = exception_tracebak

        try:
            pickle.dumps(self)
        except PicklingError as e:
            raise ValueError(
                "This TaskResponse is not pickleable, and will be pickled. We raise early so you know where to look"
            ) from e

    def __repr__(self):
        return """
        <TaskResponse state={s.state} result={s.result} task_type={s.task_type} exception={s.exception} id={s.id}
        """.strip().format(
            s=self
        )


class QueueTask(object):
    def __init__(self, task_id=None, task_type=None, suppres_response_if_empty=False):
        """
        Construct task.

        Will be called on local process.
        :param task_id:
        :param task_type:
        :return:
        """

        if task_id is None:
            task_id = uuid.uuid4()
        self.id = task_id
        """
        Id of this task
        """
        self.task_type = task_type
        """
        Type of this task.
        """
        self.suppres_response_if_empty = suppres_response_if_empty
        """
        If true and Result is none response will not be send
        """

        try:
            pickle.dumps(self)
        except PicklingError as e:
            raise ValueError(
                "This QueueTask is not pickleable, and will be pickled. We raise early so you know where to look"
            ) from e

    def __call__(self, device):
        """
        Execute task on remote process.
        :param device: Device to be acted upon.
        :return: result of call to device
        """
        try:
            result = self.call(device)
            return TaskResponse.from_response(
                self.id, device.state, result, self.task_type
            )
        except Exception as e:
            return TaskResponse.from_exception(self.id, self.task_type, e)

    def __repr__(self):
        return "<QueueTask {s.id} {s.task_type}>".format(s=self)

    def call(self, device):
        raise NotImplementedError()


class ExitTask(QueueTask):
    def __init__(self, task_id=None):
        super().__init__(task_id, "exit")

    def __call__(self, device):
        if device.state == DEVICE_STATES[RUNNING]:
            device.stop()
        device.tear_down()
        sys.exit(0)


class CallFunctionOnPluginTask(QueueTask):
    def __init__(self, function, *args, **kwargs):
        super().__init__()
        if callable(function):
            self.function_name = function.__name__
        else:
            self.function_name = function
        self.args = args
        self.kwargs = kwargs
        self.task_type = "call_function_on_plugin"

    def call(self, device):
        function = getattr(device, self.function_name)
        return function(*self.args, **self.kwargs)

    def __repr__(self):
        return u"<CallFunctionOnPluginTask {s.function_name}({s.args}, {s.kwargs}) {s.id}>".format(
            s=self
        )


class GetResults(QueueTask):
    def __init__(self, task_id=None):
        super().__init__(task_id, "ping_results")

    def call(self, device):
        # logging.getLogger(device.device_id + "").debug("pop_results")
        return device.pop_results()


class MainLoopTask(QueueTask):
    def __init__(self, task_id=None):
        super().__init__(task_id, "main_loop")

    def call(self, device):
        device.loop_iteration()


class DynamicTask(object):
    def __init__(self, wrapper, function_name):
        self.wrapper = wrapper
        self.function_name = function_name

    def __call__(self, *args, **kwargs):
        callback = kwargs.pop("callback", None)
        return self.wrapper.execute_task(
            CallFunctionOnPluginTask(self.function_name, *args, **kwargs),
            callback=callback,
        )


class StopExperiment(QueueTask):
    def __init__(self, task_id=None):
        super().__init__(task_id, "stop")

    def call(self, device):
        device.stop()


class ExperimentDeviceWorker(object):
    def __init__(
        self,
        device_id,
        device_constructor,
        auto_pull_results=True,
        multiprocess=_MULTIPROCESS_DEFAULT_PARAMETER,
    ):
        super().__init__()

        self.auto_pull_results = auto_pull_results

        if multiprocess == _MULTIPROCESS_DEFAULT_PARAMETER:
            multiprocess = DEFAULT_MULTIPROCESS

        self.multiprocess = multiprocess

        self.device_id = device_id
        self.device_constructor = device_constructor

    def start_wrapper(self, in_queue, out_queue):
        if self.multiprocess:
            process = Process(target=self, args=(in_queue, out_queue))
            process.daemon = True
        else:
            process = DummyProcess(target=self, args=(in_queue, out_queue))
            process.pid = "dummy_process"
        process.start()
        return process

    def start_no_subprocess(self):
        self.initialize_on_remote_side()

    def construct_device(self):
        """
            MAGIC!
        :return: Constructed plugin
        """
        return self.device_constructor()

    def initialize_on_remote_side(self):
        global DEVICE_REMOTE_LOGGER
        self.plugin = self.construct_device()
        DEVICE_REMOTE_LOGGER = logging.getLogger(self.plugin.device_id)

    def _process_task(self, in_queue, out_queue, task):
        logging.getLogger("device." + self.device_id).log(
            TRACE, "doing task {}".format(task)
        )
        state_before = self.plugin.state
        response = task(self.plugin)
        state_after = self.plugin.state
        # self.plugin.logger.warning("Device state: {}".format(state_after))
        # If task response is empty, and state did not change, we might
        # suppress the response
        can_suppress_response = response.result is None and state_after == state_before
        if not (task.suppres_response_if_empty and can_suppress_response):
            out_queue.put(response)

    def poll_queue(self, in_queue, out_queue):
        try:
            while True:
                task = in_queue.get_nowait()
                self._process_task(in_queue, out_queue, task)
        except Empty:
            return

    def internal_loop(self, in_queue, out_queue):
        loop_interval = self.plugin.MAIN_LOOP_INTERVAL

        running_in_last_loop = False

        while True:

            self._process_task(
                in_queue, out_queue, CallFunctionOnPluginTask(Device.loop_iteration)
            )

            for ii in range(500):
                try:
                    task = in_queue.get_nowait()
                    self._process_task(in_queue, out_queue, task)
                except Empty:
                    pass

            # If this device is configured to pull results auomatically,
            # and it makes sense to pull results we'll do it.
            # It makes sense when either when device is running, or was running
            # in last iteration (in this case we assume that there could be
            # come stale results generated from the function call that ended
            # the run.
            if self.auto_pull_results and (
                self.plugin.state == DEVICE_STATES[RUNNING] or running_in_last_loop
            ):
                task = GetResults(uuid.uuid4())
                self._process_task(in_queue, out_queue, task)

            running_in_last_loop = self.plugin.state == DEVICE_STATES[RUNNING]

            time.sleep(loop_interval)

    def __call__(self, in_queue, out_queue):
        logging.root.setLevel(logging.DEBUG)
        if self.multiprocess:
            logging.root.addHandler(QueueHandler(out_queue))
        self.initialize_on_remote_side()

        try:
            self.internal_loop(in_queue, out_queue)
        except SystemExit:
            try:
                out_queue.cancel_join_thread()
                if self.multiprocess:
                    out_queue.close()
            except:
                pass
            try:
                if self.multiprocess:
                    in_queue.cancel_join_thread()
            except:
                pass


class QueueHandler(logging.Handler):
    def __init__(self, out_queue, level=logging.DEBUG):
        super().__init__(level)
        self.out_queue = out_queue

    def emit(self, record):
        self.out_queue.put(
            TaskResponse.from_response(
                task_id=uuid.uuid4(), state=None, result=record, task_type="log"
            )
        )


class DeviceWorkerWrapper(object):
    """
    This a wrapper for the device it launched it works that way:

    * Creates a process on this machine
    * Constructs the device inside this process
    * Enables more-or-less transparent operations on the remote device.
    * Enables us to kill the remote process when really needed

    Support for remote process should be transparent, with one important
    exception: all mehods are asynchroneous, for example when making a
    :meth:`get_results` call you actually end up doing:

    * Sheduling a request to refresh results from remote side
    * Reading all messages from input queue
    * Returning last used results

    """

    def __init__(
        self,
        device_id,
        device_constructor,
        multiprocess=_MULTIPROCESS_DEFAULT_PARAMETER,
        config_file=None,
    ):
        """
        Initializes this device wrapper

        :param str device_id: Name of initialized device
        :param device_constructor: Callable that constructs this device, this
            will be serialized, so please use non-local function.
        :class: picklable callable
        """

        if multiprocess == _MULTIPROCESS_DEFAULT_PARAMETER:
            multiprocess = DEFAULT_MULTIPROCESS

        self.multiprocess = multiprocess

        self.in_queue = None
        """
            Queue that contains responses for tasks from remote process.
            Contains instances of :class:`TaskResponse`.
        """

        if self.multiprocess:
            self.in_queue = Queue()
        else:
            self.in_queue = DummyQueue()

        self.out_queue = None
        """
            Queue that contains tasks to be executed on remote process,
            Should contain instances of :class:`QueueTask`, though any callable
            with proper signature will do.
        """

        if self.multiprocess:
            self.out_queue = Queue()
        else:
            self.out_queue = DummyQueue()

        self.device_id = device_id

        self.device_constructor = device_constructor

        """
            Remote process.
        """
        self.results = []
        """
            Current results, :class:`list`
        """
        self._state = DEVICE_STATES[OFF]
        """
            Current state
        """
        self._callbacks = {}
        """
        Contains _callbacks for tasks with scheduled _callbacks.
        """

        self.auto_pull_results = False
        """
        If true the experiment will automatically pull results.
        """

        if config_file is None:

            config_file = SilfConfigParser()

        self.config_file = config_file
        """
        Congig parser instance for this experiment.
        """

        self.task_delay = self.config_file.getfloat(
            self.device_id, "single_task_timeout", fallback=30
        )

        self.__hanging_operation_scheduler = scheduler()
        """
        Scheduler that raises exceptions if task takes
        too long,

        This is used to detect tasks that take to long to process by the worker. This
        functionality works as follows: when a task is added to output queue we schedule
        event that raises exception. When response to this event is sent by the client
        we cacel that event.
        """

        self.__task_id_event_mapping = {}
        """
        Map that maps id of task to scheduler event --- it
        """

    def __raise_timeout_exception(self, task):
        raise ExperimentBackendUnresponsive()

    @property
    def logger(self):
        return logging.getLogger("worker." + self.device_id)

    @property
    def _queues_logger(self):
        return logging.getLogger("worker.queuee." + self.device_id)

    def start_subprocess(self):
        """
        Starts subprocess attached to this instance.
        """
        self.worker = ExperimentDeviceWorker(
            self.device_id,
            self.device_constructor,
            self.auto_pull_results,
            multiprocess=self.multiprocess,
        )
        self.process = self.worker.start_wrapper(self.out_queue, self.in_queue)
        self.logger.info(
            "Starting subprocess {} for device {}".format(
                self.process.pid, self.device_id
            )
        )

    @property
    def state(self):
        """
        Returns most recent state of remote process.
        :return: State
        :rtype: str
        """
        self.loop_iteration()
        self.ping_results()
        return self._state

    def pop_results(self, sync=False):
        """
        Schedules getting of new results and then returns cached results.
        :return:
        """
        self.ping_results(sync=sync)
        results = self.results
        self.results = []
        return results

    def append_results(self, results):
        self.results.extend(results)

    def ping_results(self, callback=None, sync=False):
        """
        Schedules getting of new results/
        :param callback: See :meth:`execute_task`
        """
        id = self.execute_task(GetResults(), callback=callback)

        if sync:
            self.wait_for_response(id)

        return id

    @property
    def process_alive(self):
        """
        Checks if remote process is alive.

        :return: :data:`True` if remote process is alive, :data:`False`
                 otherwise

        :rtype: :class:`bool`
        """
        return self.process.is_alive()

    def __read_tasks(self, max_read):
        for ii in range(max_read):
            try:
                response = self.in_queue.get_nowait()
                self.__process_response(response)
            except Empty:
                return

    def loop_iteration(self, max_read=None):
        """
        Reads all messages from the input queue, updating `self.state` and
        `self.results`.

        :return: None
        """
        self._queues_logger.log(
            TRACE,
            "Input queue {}, out_queue {}".format(
                self.in_queue.qsize(), self.out_queue.qsize()
            ),
        )
        if max_read is None:
            max_read = int(1e6)
        self.__read_tasks(max_read)
        self.__hanging_operation_scheduler.run(False)

    def wait_for_response(self, task_id):
        """
        Gets responses from queue until response for task with id equal to
        `task_id` param.

        .. note:: This method will block until we get response for `task_id`

        .. warning::
            It may introduce deadlock if response for `task_id` has
            already been processed. To be sure it will not cause deadlock
            please execute this function just after sheduling the task.
            Much safer solution is to attach a `callback` for particular task,
            please see :meth:`execute_task`.

        :param task_id: Task id we are looking for.

        :return: Response for task with id equal to `task_id`.
        """
        while True:
            response = self.in_queue.get()
            self.__process_response(response)
            if response.id == task_id:
                return response

    def purge_queue(self, queue):
        """
        Removes all pending operations from a queue.
        :return: None`
        """
        while True:
            try:
                queue.get_nowait()
            except Empty:
                return

    def execute_task(self, task, callback=None, sync=False):
        """
        Schedules task execution on remote process.

        :param task: Task to be executed
        :param callback: A callable that will be called upon completion of this
                         task. It should accept single argument: instance of
                         :class:`TaskResponse` that contains the results for
                         this task.

        :return: Task id
        """
        self.loop_iteration()
        self.out_queue.put(task)
        event = self.__hanging_operation_scheduler.enter(
            self.task_delay, 0, self.__raise_timeout_exception, argument=[task]
        )
        # logging.debug("Scheduled task id:{}, task: {}".format(str(task.id), task))
        self.__task_id_event_mapping[task.id] = event
        if callback is not None:
            if not callable(callback):
                raise ValueError("Callback should be callable")
            self._callbacks[task.id] = callback

        if sync:
            self.wait_for_response(task.id)
        return task.id

    def start(self, callback=None, sync=False):
        id = DynamicTask(self, "start")(callback=callback)

        if sync:
            self.wait_for_response(id)

        return id

    def stop(self, callback=None, sync=True):
        """
        Schedules a task to stop the device.
        :param callback: See :meth:`execute_task`
        :return: Returns task id
        """
        id = self.execute_task(StopExperiment(), callback=callback)

        if sync:
            self.wait_for_response(id)

        return id

    def cleanup(self, callback=None):
        """
        Schedules a task to turn off the device kill the remote thread.
        :return: task_id
        """
        self.purge_queue(self.out_queue)

        task_id = self.execute_task(ExitTask(), callback=callback)

        if self.multiprocess:
            self.out_queue.close()

        return task_id

    def __getattr__(self, name):
        """
        Returns a method that calls method named `name` on the device.
        :param name: Method to call on the device.
        :return:
        """
        if name in ("trait_names", "_getAttributeNames"):
            raise AttributeError()
        return DynamicTask(self, name)

    def kill(self, wait_time=2):
        """
        Kills remote process, optionally waiting. This method will block until
        remote process is killed.

        :param float wait_time: Wait time in seconds. If `wait_time` is None,
            then terminate process without additional waiting.

        :return: None
        """
        if wait_time is not None:
            self.cleanup()
            self.process.join(wait_time)

        if self.multiprocess:
            self.process.terminate()
            self.logger.info(
                "Killing process {} for device {}".format(
                    self.process.pid, self.device_id
                )
            )

    def __process_response(self, response):
        callback = self._callbacks.pop(response.id, None)

        event = self.__task_id_event_mapping.pop(response.id, None)
        # logging.debug("Scheduled task id:{} finished execution. Following IDS remain in queue {}".format(str(response.id), list(self.__task_id_event_mapping.keys())))
        if event is not None:
            self.__hanging_operation_scheduler.cancel(event)
        if callback is not None:
            try:
                callback(response)
            except Exception as e:
                self.logger.exception("While executing callback")
        if response.exception is not None:
            raise response.exception
        if response.task_type == "log":
            logging.getLogger(self.device_id).handle(response.result)
            return
        if response.state:
            self._state = response.state
        if response.task_type == "ping_results":
            if response.result is not None:
                self.append_results(response.result)


def start_worker_interactive(
    device_id,
    device_class,
    auto_pull_results=True,
    configure_logging=True,
    config_file=None,
):
    worker = DeviceWorkerWrapper(device_id, device_class)
    worker.auto_pull_results = auto_pull_results
    worker.start_subprocess()
    if configure_logging:
        logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    return worker
