from circuits import Component

from silf.backend.commons.api import Result
from .event import results


class ResultsManager(Component):

    """
    Manager for results creators.
    Handles event ``result_values``, fills result creators with new data from devices and current settings, and finally
     sends results produced by creators to client (via ``results`` event).
    """

    def init(self, result_creators):
        self.result_creators = result_creators
        self.current_settings = {}

    def reset(self):
        for creator in self.result_creators:
            creator.reset()

    def result_values(self, series_id, values, step=None):
        collected = {}
        values.update(self.current_settings)
        for creator in self.result_creators:
            creator.apply_values(values, step)
            if creator.has_result():
                collected[creator.name] = {
                    "value": creator.pop_result(),
                    "pragma": creator.pragma,
                }
        self.fire(results(series_id, collected), "*")

    def apply_settings(self, settings):
        self.current_settings = settings


class ResultsCreator:
    """
    Basic results creator. Resets object on init. Do nothing more.
    """

    def __init__(self, name, pragma=Result.DEFAULT_PRAGMA, read_from=None):
        """
        :param name: result code name
        :param pragma: result pragma
        :param read_from: input data name (key in device data values dictionary)
        """
        self.name = name
        self.pragma = pragma
        self.read_from = self.name if read_from is None else read_from
        if getattr(self, "reset", None) is not None:
            self.reset()

    def reset(self):
        """
        Prepare for new measurement, reset current values.
        """
        raise NotImplementedError()

    def apply_values(self, values, step):
        """
        Apply new (current) values from all devices.
        :param values: dictionary of all new values
        :param step: current step number
        """
        raise NotImplementedError()

    def has_result(self):
        """
        New result is ready to pop.
        :return: True if new result wasn't popped earlier, False otherwise
        """
        raise NotImplementedError()

    def pop_result(self):
        """
        Returns last calculated result.
        :return: current result, which should be serialized to json
        """
        raise NotImplementedError()


class LiveResultsCreator(ResultsCreator):
    """
    Simple results creator which returns result just after obtain new data.
    Result is calculated from device data in :meth:`calculate`.
    """

    def reset(self):
        """
        Prepare for new measurement, reset current values.
        """
        self.value = None
        self.ready = False

    def apply_values(self, values, step):
        """
        Apply new (current) values from all devices.
        :param values: dictionary of all new values
        :param step: current step number
        """
        if self.read_from in values:
            self.value = self.calculate(values[self.read_from])
            self.ready = True

    def calculate(self, value):
        """
        Calculates result value form input data.
        :param value: input data to process
        :return: current result, which should be serialized to json
        """
        return value

    def has_result(self):
        """
        New result is ready to pop.
        :return: True if new result wasn't popped earlier, False otherwise
        """
        return self.ready

    def pop_result(self):
        """
        Returns last calculated result.
        :return: current result, which should be serialized to json
        """
        self.ready = False
        return self.value


class AggregatedResultsCreator(ResultsCreator):
    """
    Result creator witch aggregates all results from beginning.
    Current result is calculated in :meth:`calculate` for each data item earlier processed in :meth:`convert`.
    """

    def reset(self):
        """
        Prepare for new measurement, reset current values.
        """
        self.aggregated_values = []
        self.current_value = None
        self.ready = False

    def apply_values(self, values, step):
        """
        Apply new (current) values from all devices.
        :param values: dictionary of all new values
        :param step: current step number
        """
        if self.read_from in values:
            self.aggregated_values.append(self.convert(values[self.read_from]))
            self.current_value = self.calculate(self.aggregated_values)
            self.ready = True

    def convert(self, value):
        """
        Converts single value for current measurement.
        :param value: single value to convert before aggregation
        :return: converted value
        """
        return value

    def calculate(self, values):
        """
        Calculates result value form input data.
        :param value: list of input data to process
        :return: current result values
        """
        return values

    def has_result(self):
        """
        New result is ready to pop.
        :return: True if new result wasn't popped earlier, False otherwise
        """
        return self.ready

    def pop_result(self):
        """
        Returns last calculated result.
        :return: current result, which should be serialized to json
        """
        self.ready = False
        return self.current_value


class ChartXYCreator(ResultsCreator):
    """
    Simple results creator which returns result as [X,Y] pair just after obtain new data.
    Result is calculated from device data in :meth:`calculate`.
    """

    def __init__(self, name, x=None, y=None):
        self.x = x
        self.y = y
        super().__init__(name, pragma="append")

    def reset(self):
        """
        Prepare for new measurement, reset current values.
        """
        self.value = None
        self.ready = False

    def apply_values(self, values, step):
        """
        Apply new (current) values from all devices.
        :param values: dictionary of all new values
        :param step: current step number
        """
        if self.x and self.y in values:
            self.value = self.calculate(values[self.x], values[self.y])
            self.ready = True

    def calculate(self, valueX, valueY):
        """
        Calculates result value form input data.
        :param value: input data to process
        :return: current result, which should be serialized to json
        """
        return [[valueX, valueY]]

    def has_result(self):
        """
        New result is ready to pop.
        :return: True if new result wasn't popped earlier, False otherwise
        """
        return self.ready

    def pop_result(self):
        """
        Returns last calculated result.
        :return: current result, which should be serialized to json
        """
        self.ready = False
        return self.value
