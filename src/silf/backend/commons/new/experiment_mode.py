import logging
import uuid

from circuits import Component, handler, Timer

from silf.backend.commons.api import (
    SeriesStartedStanza,
    SeriesStopped,
    SILFProtocolError,
    ValidationError,
)
from .event import *
from .device import DevicesManager
from .result import ResultsManager


class next_step(Event):
    success = True


class next_step_settings(Event):
    success = True


class result_values(Event):
    success = True


class ExperimentMode(Component):
    """
    This class representing one mode of experiment. The subclass should contain:

    * :attr:`name` - code name for this mode
    * :attr:`label` - visible and meaningful name for user
    * :attr:`controls` - list of controls for mode settings wrapped in :class:`ControlSuite`
    * :attr:`output_fields` - list of output fields showing some experiment data wrapped in :class:`OutputFieldSuite`
    * :attr:`result_creators` - list of result creators (instances of :class:`ResultsCreator`), i.e. objects translating
         experiment data into output fields defined in :attr:`output_fields`
    * devices - list of devices, bare classes used for device management
    """

    channel = "mode"

    device_query_timeout = 1.0
    """
    If this device has ``live`` class attribute that is Truish its
    :meth:`Device.read_state` method will be called repeatedly with interval od
    :attr:`device_query_timeout` in seconds.
    """

    def init(self, config=None):
        self.timer = None
        self.__clear_overriden_handlers()
        self.__updated_settings = {}
        self.logger = logging.getLogger("ExperimentMode")
        self.config = config
        self.id = uuid.uuid4().urn
        self.series_no = 0
        self.series_id = None
        self.series_running = False
        self.results_for_step = False
        DevicesManager(getattr(self, "devices", []), config=self.config).register(self)
        self.results_manager = ResultsManager(
            getattr(self, "result_creators", [])
        ).register(self)

    def __clear_overriden_handlers(self):
        for func_name in ["next_step_settings"]:
            original_method = getattr(self.__class__.__base__, func_name, None)
            if original_method is not None:
                original_method = original_method.__get__(self)
                overriding_method = getattr(self, func_name, None)
                if original_method != overriding_method:
                    try:
                        self.removeHandler(original_method)
                    except KeyError:
                        pass

    def check_settings(self, settings):
        self.controls.check_settings(settings)
        return settings

    def update_settings(self, settings):
        if self.series_running:
            translated_settings = self.__retrive_settings_from_stanza(settings)
            self.__updated_settings.update(translated_settings)
            evt = apply_settings(translated_settings)
            # evt.live = True
            evt.update = True
            evt.propagate_error = True
            self.fire(evt, "device")
            return settings
        else:
            self.logger.warn("CANNOT UPDATE SETTINGS DUE TO NOT RUNNING SERIES")

    def get_series_label(self, settings):
        """
        Returns label for current measurement series generated for input settings.
        :param settings: input settings for current series
        :return: label for current series
        """
        return "serie %d" % self.series_no

    def start_series(self, settings):
        if not self.series_running:
            self.check_settings(settings)
            self.series_label = self.get_series_label(settings)
            self.series_no += 1
            self.series_id = uuid.uuid4().urn
            self.settings = self.__retrive_settings_from_stanza(settings)
            self.series_running = True
            self.fire(start_measurements(self.series_id, self.settings))
            return SeriesStartedStanza(
                seriesId=self.series_id,
                initialSettings=settings,
                label=self.series_label,
            )
        else:
            self.logger.warn("SERIES IS ALREADY RUNNING")

    @staticmethod
    def __retrive_settings_from_stanza(settings):
        return {key: value.value for key, value in settings.settings.items()}

    def stop_series(self, series_id, internal=False):
        if self.series_running:
            self.series_id = None
            self.series_running = False
            self.fire(stop_measurements())
            return SeriesStopped(seriesId=series_id)
        else:
            self.logger.warn("CANNOT STOP NOT RUNNING SERIES")

    def start_measurements(self, series_id, settings):
        self.results_manager.reset()
        yield self.call(power_up(), "device")
        yield self.call(start_measurements(settings), "device")
        evt = read_state()
        evt.live = True
        self.timer = Timer(
            self.device_query_timeout, evt, "device-timer", persist=True
        ).register(self)
        self.step = 0
        self.fire(next_step_settings(settings, None))

    # @handler("read_state_changed", channel="*")
    # def read_state_notify(self, *args, **kwargs):
    #    print("READ_STATE_NOTIFY", args, kwargs)

    # @handler("read_state_for_step_changed", channel="*")
    # def read_state_for_step_notify(self, value):
    #    print("READ_STATE_FOR_STEP_NOTIFY", value, 'for step', value.event.step)

    def stop_measurements(self):
        if self.timer is not None:
            self.timer.unregister()
        self.fire(stop_measurements(), "device")

    def next_step(self, settings):
        if self.series_running:
            self.step += 1
            settings["step"] = self.step
            self.__check_settings_to_update(settings)
            self.fire(apply_settings(settings))  # for results creators
            yield self.call(apply_settings(settings), "device")
            evt = read_state()
            evt.step = self.step
            evt.notify = "read_state_for_step_changed"
            self.fire(evt, "device")
            notified_value = yield self.wait("read_state_for_step_changed")
            results = self.__process_results(notified_value.event.args[0].value)
            self.fire(next_step_settings(settings, results))

    def __check_settings_to_update(self, settings):
        settings.update(self.__updated_settings)
        self.__updated_settings.clear()

    @handler("read_state_success", channel="device")
    def _on_read_state_success(self, evt, value):
        if value:
            result = self.__process_results(value)
            step = getattr(evt, "step", None)
            if step is not None:
                result["step"] = step
            self.fire(result_values(self.series_id, result, step), "*")

    def __process_results(self, value):
        iterator = (value,) if not isinstance(value, (tuple, list)) else value
        results = {}
        for parial_results in iterator:
            if isinstance(parial_results, dict):
                results.update({k: v for k, v in parial_results.items()})
            else:
                self.logger.warn(
                    "unknown type of partial results: %s with value %s"
                    % (str(type(parial_results)), str(parial_results))
                )
        return results

    def result_values(self, series_id, values, step=None):
        """
        If generated results are needed for something this method
        is the right place to collect them and use.
        :param str series_id: Id of current series
        :param dict values: Aggregated results from current step for all devices
        :param step: current step or None if results is obtained from live device
                     outside of step processing
        """
        self.logger.debug("%s for step %s", values, step)

    def next_step_settings(self, settings, results):
        """
        Returns settings for next step using settings from previous step.
        For the first step the initial settings for current series is passed as input parameter.
        Default implementation returns the same settings for all steps, i.e. series never ends.
        :param settings: previous step settings or initial series settings (for the first step)
        :param results: results obtained in previous step or None (for the first step)
        :return: next step settings or None if experiment series should stop (there is no new step)
        """
        return settings

    def next_step_settings_success(self, event, evt, value):
        """
        Callback called after next_step event. It stops current series or invokes next next_step event.
        :param event: current callback event
        :param evt: last next_step event
        :param value: settings returned by last next_step event
        """
        if value is None:
            evt = stop_series(self.series_id, True)
            yield self.call(evt)
            self.fire(notify(evt), "*")
        else:
            self.fire(next_step(value))
