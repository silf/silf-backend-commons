import logging
from circuits import Component, task, handler, Bridge, Worker, Debugger, Event

import os
from silf.backend.commons.api import DeviceException


_original_bridge_on_event = Bridge._on_event


@handler(channel="*", priority=100.0)
def _modified_bridge_on_event(self, event, *args, **kwargs):
    # print('modified bridge processing', event)
    if "device" in event.channels and not event.name.startswith("task"):
        # print('modified bridge passed')
        return _original_bridge_on_event(self, event, *args, **kwargs)


class IllegalStateError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class DevicesManager(Component):
    """
    Manager for set of devices.
    Prevents form calling method on all devices before previous call of the same method is not finished.
    Translates timer events into normal device events.
    """

    channel = "device"

    def init(self, devices, config=None):
        self.__running_events = {}
        Bridge._on_event = _modified_bridge_on_event
        for device in devices:
            DeviceProxy(device, config=config).start(process=True, link=self)
        Bridge._on_event = _original_bridge_on_event

    def __is_running(self, event):
        return self.__running_events.get(event.__class__, None) is not None

    def __start_running(self, event):
        self.__running_events[event.__class__] = event

    def __stop_running(self, event):
        self.__running_events[event.__class__] = None

    @handler(channel="device-timer", priority=100.3)
    def _on_timer_event(self, event, *eargs, **ekwargs):
        if (
            event.name in ["generate_events", "registered"]
            or event.name.endswith("_done")
            or event.name.endswith("_complete")
            or event.name.endswith("_success")
            or event.name.startswith("task")
            or not "device-timer" in event.channels
        ):
            # or self.__is_running(event):
            return
        real_event = event.__class__(*eargs, **ekwargs)
        real_event.notify = "read_state_changed"
        real_event.live = getattr(event, "live", False)
        self.fire(real_event, "device")

    @handler(channel="device", priority=100.2)
    def _on_event(self, event, *eargs, **ekwargs):
        # print('processing on device', event)
        if (
            event.name in ["generate_events", "registered"]
            or event.name.endswith("_done")
            or event.name.endswith("_complete")
            or event.name.endswith("_success")
            or event.name.startswith("task")
            or not self.channel in event.channels
        ):  # very weird :/
            return
        else:
            if getattr(event, "live", False):
                return

            if self.__is_running(event):
                event.cancel()
                event.stop()
                error = IllegalStateError("event %s already is running" % event.name)
                error.inform_user = getattr(event, "propagate_error", False)
                raise error
            else:
                # print('good', event)
                event.alert_done = True
                event.locked = True
                self.__start_running(event)

                def _unlock_event(self, event, *args, **kwargs):
                    if getattr(event.parent, "locked", False):
                        self.removeHandler(_unlock_handler, event.name)
                        self.__stop_running(event.parent)

                _unlock_handler = self.addHandler(
                    handler("%s_done" % event.name, channel="device")(_unlock_event)
                )
                # print('ok, let\'s run', event)
                # if not self.running_events.get(event.__class__, None):
                # event_for_device = event.__class__(*eargs, **ekwargs)
                # self.running_events[event.__class__] = event  # event_for_device
                # print('ok, let\'s run', event)
                # result = yield self.call(event, 'device-proxy')
                # print('and result is', result)
                # event.value = result
                # self.running_events[event.__class__] = None
                # return result.value


class task_finished(Event):
    pass


class DeviceProxy(Component):
    """
    Proxy for devices run in another process.
    All method calls are passed to process in which real device class is instantiated.
    If class doesn't have specific method nothing is passed. Otherwise, method execution is added to worker queue
    and executed in worker single thread as soon as all earlier calls are processed.
    """

    channel = "device"

    def init(self, device, config=None):
        self.logger = logging.getLogger("DeviceProxy[%s]" % str(device))
        self.device = device()
        init_method = getattr(self.device, "init", None)
        if init_method:
            DeviceProxy._wrap_call(init_method, config=config)

    def started(self, _):
        self.logger.debug("registering worker in %d", os.getpid())
        Worker(workers=1, channel="*", process=False).register(self)

    @handler(channel="device", priority=100.1)
    def _on_event(self, event, *eargs, **ekwargs):
        if event.name not in [
            "generate_events",
            "registered",
        ] and not event.name.startswith("task"):
            try:
                method = getattr(self.device, event.name)
            except AttributeError:
                pass
            else:
                if getattr(event, "live", False):
                    if not getattr(self.device, "live", False):
                        self.logger.info("returning from live event in non live device")
                        return
                t = task(method, *eargs, **ekwargs)
                t.notify = "task_notify"

                # def _task_finished(self, event, *args, **kwargs):
                #     print(event)
                #
                # _task_done_handler = self.addHandler(
                #     handler('task_done', channel='device')(_task_finished)
                # )
                # _task_failure_handler = self.addHandler(
                #     handler('task_failure', channel='device')(_task_finished)
                # )
                #

                # self.fire(t)
                # while True:
                #     result = yield self.wait('task_finished')
                #     if result.event.parent == t:
                #         break
                # result = self._process_result(result)
                # yield result

                # try:
                #     result = yield self.call(t)
                #     print('result is', result)
                #     result = self._process_result(result)
                #     print('processed result is', result)
                #     yield result
                # except object as e:
                #     print('error is', e)
                #     yield {'result':'ok'}

                self.fire(t)
                result = yield self.wait("task_notify")
                result = self._process_result(result)
                yield result

    @staticmethod
    def _process_result(result):
        value = result.event.args[0]  # result.event.parent.value
        if value.errors:
            ex = value.value[1]
            dex = DeviceProxy._wrap_error(ex)
            raise dex.with_traceback(ex.__traceback__) from ex
        return value.value

    # @handler('task_success', channel='device', priority=100.1)
    # def _on_task_success(self, event, *args, **kwargs):
    #     print('task success', event.parent)
    #     t = task_finished()
    #     t.parent = event.parent
    #     self.fire(t)
    #
    # @handler('task_failure', channel='device', priority=100.1)
    # def _on_task_failure(self, event, *args, **kwargs):
    #     print('task failure', event.parent)
    #     t = task_finished()
    #     t.parent = event.parent
    #     self.fire(t)
    #
    # @handler('task_done', channel='device', priority=100.1)
    # def _on_task_done(self, event, *args, **kwargs):
    #     print('task done', event.parent)
    #
    # @handler('task_complete', channel='device', priority=100.1)
    # def _on_task_complete(self, event, *args, **kwargs):
    #     print('task complete', event.parent)
    #
    # @handler('task_notify', channel='device', priority=100.1)
    # def _on_task_notify(self, event, *args, **kwargs):
    #     print('task notify', event, args, kwargs)
    #
    # @handler('exception', channel='device', priority=100.1)
    # def _on_exception(self, event, *args, **kwargs):
    #     print('exception', event)
    #     if isinstance(kwargs['fevent'], task):
    #         event.cancel()
    #         event.stop()

    @staticmethod
    def _wrap_error(ex):
        if not isinstance(ex, DeviceException):
            msg = getattr(ex, "message", None) or str(ex)
            nex = DeviceException(msg)
            ex = nex
        return ex

    @staticmethod
    def _wrap_call(fun, *args, **kwargs):
        try:
            return fun(*args, **kwargs)
        except Exception as ex:
            raise DeviceProxy._wrap_error(ex).with_traceback(ex.__traceback__) from ex


class DeviceApi:
    """
    Example device class to show what methods, if present, are invoked by experiment.
    The order of method invocation is more or less the same as order of methods described below.
    """

    live = True
    """
    flag to tell that device can collect data in a live way, i.e. not only for step but all the time
    """

    def power_up(self):
        """
        device should be powered up and initial calibration should be done
        :return: None
        """
        pass

    def start_measurements(self, settings):
        """
        new measurement series is starting, device should prepare itself for that fact
        :param dict settings: dictionary contains initial settings for all devices
        :return: None
        """
        pass

    def apply_settings(self, settings):
        """
        next measurement step is comming and new settings for that step should be earlier applied
        device should retrieve related settings and change it's state
        :param dict settings: dictionary contains settings for all devices for the next step
        :return: None
        """
        pass

    def read_state(self):
        """
        reads current data from device
        :return: dictonary of results
        """
        pass

    def stop_measurements(self):
        """
        current series is finished and device should clean after itself (e.g. perform calibration)
        :return: None
        """
        pass

    def power_down(self):
        """
        device should be powered down, no new measurements will be performed during this session (on this instance)
        :return: None
        """
        pass
