import logging
from circuits import Component, Event

from silf.backend.commons.api import Mode, ModeSuite, ModeSelected, SILFProtocolError
from silf.backend.commons.util.config import open_configfile

from .connection import XMPPConnection
from .event import power_down, stop_series


class unregister_mode(Event):
    pass


class Experiment(Component):
    """
    Instances of this class are representing the experiment. They should contain:

    * :attr:`config` - path to configuration file with xmpp settings
    * :attr:`modes` - list of experiment modes of :class:`.ExperimentMode`
    """

    channel = "experiment"

    def __init__(self, config, client=None):
        super().__init__()
        self.logger = logging.getLogger("Experiment")
        self.config = config
        self.connection = XMPPConnection(self.config, client=client).register(self)
        self.config = open_configfile(self.config)
        # Worker(channel='*', process=False).register(self)
        self.mode = None

    def get_modes(self):
        suite = {
            mode.name: Mode(mode.name, mode.label, index)
            for index, mode in enumerate(self.modes, start=1)
        }
        return ModeSuite(**suite)

    def get_mode(self, name):
        for mode in self.modes:
            if mode.name == name:
                return mode

    def set_mode(self, mode):
        yield self.call(unregister_mode())
        self.mode = self.get_mode(mode)(config=self.config)
        self.mode.register(self)
        yield ModeSelected(
            mode=mode,
            experimentId=self.mode.id,
            settings=self.mode.controls.to_input_field_suite(),
            resultDescription=self.mode.output_fields,
        )

    def check_settings(self, settings):
        if self.mode is None:
            raise SILFProtocolError.from_args(
                severity="error",
                error_type="user",
                message="Can't check settings without settings mode",
            )

    def stop_experiment(self):
        yield self.call(unregister_mode())
        self.fire(power_down(), self.channel)

    def unregister_mode(self):
        if self.mode is not None:
            yield self.call(stop_series(self.mode.series_id))
            yield self.call(power_down(), "device")
            self.mode.unregister()
