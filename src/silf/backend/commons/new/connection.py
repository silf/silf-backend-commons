import logging
import traceback
from queue import Queue, Empty

from circuits import Component, handler
from circuits.core.manager import TIMEOUT

from silf.backend.client.client import Client
from silf.backend.commons.version import PROTOCOL_VERSIONS, ProtocolVersionUtil
from silf.backend.client.const import *
from silf.backend.commons.api import (
    ExperimentBackendUnresponsive,
    ValidationError,
    SILFProtocolError,
    Error,
    ErrorSuite,
    Result,
    ResultSuite,
    ProtocolVersions,
)

from .event import *


class XMPPConnection(Component):

    channel = "connection"

    def __init__(self, config="config.ini", client=None):
        self.config = config or "config.ini"
        if client is None:
            client = Client(self.config, section="XMPPClient")
        self.client = client
        super().__init__()

    def init(self):
        self.logger = logging.getLogger("XMPPConnection")
        self.incomming_stanzas = Queue()
        self.client.register_callback(self.__process_stanza)
        self.client.initialize()

    def __process_stanza(self, state):
        if state.type == "query":
            self.incomming_stanzas.put(stanza(state=state), True)

    @handler("generate_events")
    def _on_generate_events(self, event):
        try:
            stanza = self.incomming_stanzas.get_nowait()
            self.fire(stanza)
            event.reduce_time_left(0)
            event.stop()
        except Empty:
            event.reduce_time_left(TIMEOUT)

    def stanza(self, state):
        ns = state.namespace
        channel = "*"
        evt = None
        if ns == SILF_MODE_GET:
            evt = get_modes()
            channel = "experiment"
        elif ns == SILF_MODE_SET:
            evt = set_mode(state.content.mode)
            channel = "experiment"
        elif ns == SILF_SETTINGS_CHECK:
            evt = check_settings(state.content)
            channel = "mode"
        elif ns == SILF_SERIES_START:
            evt = start_series(state.content)
            channel = "mode"
        elif ns == SILF_SERIES_STOP:
            evt = stop_series(state.content.seriesId)
            channel = "mode"
        elif ns == SILF_EXPERIMENT_STOP:
            evt = stop_experiment()
            channel = "experiment"
        elif ns == SILF_SETTINGS_UPDATE:
            evt = update_settings(state.content)
            channel = "mode"
        elif ns == SILF_VERSION_GET:
            evt = get_version()
            channel = self.channel
        elif ns == SILF_VERSION_SET:
            evt = set_version(state.content)
            channel = self.channel
        else:
            self.logger.error("unknown ns %s", ns)
        if evt:
            evt.stanza_state = state
            evt.notify = "after_stanza_processing"
            self.fire(evt, channel)

    @handler("after_stanza_processing", channel="*")
    def _after_stanza_processing(self, value):
        result = value.value
        state = value.event.stanza_state
        if value.errors:
            self.__process_error(result[1], result[2], state)
        elif value.result:
            state.reply(suite=result)
        else:
            state.reply()

    def get_version(self):
        return ProtocolVersions(PROTOCOL_VERSIONS)

    def set_version(self, version):
        if not ProtocolVersionUtil.is_version_in_list(
            version.version, PROTOCOL_VERSIONS
        ):
            raise SILFProtocolError.from_args(
                severity="error",
                error_type="user",
                message="No supported protocol version",
            )
        return version

    def notify(self, evt):
        evt_type = type(evt)
        if evt_type is stop_series:
            ns = SILF_SERIES_STOP
        elif evt_type is results:
            ns = SILF_RESULTS
        self.client.send_standalone_stanza(ns, suite=evt.value.value)

    def results(self, series_id, values):
        collected = {
            name: Result(value=item["value"], pragma=item["pragma"])
            for name, item in values.items()
        }
        self.logger.info("results sent: %s", collected)
        suite = ResultSuite(**collected)
        # stanza = ResultsStanza(seriesId=series_id, results=suite)
        # self.client.send_standalone_stanza(SILF_RESULTS, suite=stanza)
        self.client.send_standalone_stanza(SILF_RESULTS, suite=suite)

    @handler("exception", channel="*")
    def _on_exception(self, error_type, value, traceback, handler=None, fevent=None):
        self.__process_error(value, traceback, None)

    def __process_error(self, error, tb, state):
        if getattr(error, "processed", False):
            return
        self.logger.error(
            "Error occurred: %s\n%s", error, XMPPConnection.__format_traceback(tb)
        )
        error.processed = True
        if not getattr(error, "inform_user", True):
            self.logger.debug("Error '%s' won't be propagated to the user" % error)
            return
        if not isinstance(tb, list):
            tb = traceback.extract_tb(tb)
        try:
            raise error
        except ExperimentBackendUnresponsive:
            self.__on_silf_error(error, state)
            self.fire(stop_experiment("Experiment backend unresponsive"), "*")
        except ValidationError:
            self.__on_validation_error(error, state)
        except SILFProtocolError:
            self.__on_silf_error(error, state)
        except Exception:
            self.__on_other_error(error, tb)

    @staticmethod
    def __format_traceback(tb):
        if isinstance(tb, list):
            return "".join(tb)
        else:
            return str(tb)

    def __on_validation_error(self, error, state):
        self.client.send_error(error.errors, state, SILF_MISC_ERROR)
        self.logger.warn("VALIDATION:ERROR,", error.errors)

    def __on_silf_error(self, error, state):
        self.client.send_error(error.errors, state, SILF_MISC_ERROR)
        self.logger.exception("PROTOCOL:ERROR, errors follow")
        for error in error.errors.errors:
            self.logger.warn("%s, %s", error.message, error.field)

    def __on_other_error(self, error, traceback):
        error_stanza = Error(
            severity="error",
            error_type="system",
            message="Miscalleneous exception:\n%s" % str(error),
        )
        error_stanza.metadata["exception_message"] = str(error)
        error_stanza.metadata["exception_tracebak"] = traceback
        self.client.send_error(
            ErrorSuite(errors=[error_stanza]), lnamespace=SILF_MISC_ERROR
        )
        self.logger.exception("MISC:ERROR")
