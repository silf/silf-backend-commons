from circuits import Event


##### common events #####


class error(Event):
    pass


##### protocol events #####


class get_modes(Event):
    pass


class set_mode(Event):
    pass


class check_settings(Event):
    pass


class start_series(Event):
    pass


class results(Event):
    pass


class stop_series(Event):
    pass


class stop_experiment(Event):
    pass


class stanza(Event):
    pass


class notify(Event):
    pass


class update_settings(Event):
    pass


class get_version(Event):
    pass


class set_version(Event):
    pass


##### mode events #####


class start_measurements(Event):
    """ start measurements """


class stop_measurements(Event):
    """ stop measurements """


##### device events #####


class read_state(Event):
    """ read status """

    success = True
    complete = True


class power_up(Event):
    """ power up """


class power_down(Event):
    """ power down """


class apply_settings(Event):
    """ set settings """

    success = True
