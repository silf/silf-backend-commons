# -*- coding: utf-8 -*-
from io import StringIO
import sys
import traceback
from silf.backend.client import SILF_MISC_ERROR
from silf.backend.commons.api import ErrorSuite, Error


def current_exception_to_string() -> str:
    tb = StringIO()
    traceback.print_exc(file=tb)
    tb.seek(0)
    return tb.read()


def current_exception_to_silf_error_stanza() -> ErrorSuite:
    __, exc, __ = sys.exc_info()
    if exc is None:
        raise ValueError("This function may only be called from try block")

    error = Error(
        severity="error", error_type="system", message="Miscalleneous exception"
    )

    error.metadata["exception_message"] = str(exc)

    error.metadata["exception_tracebak"] = current_exception_to_string()

    return ErrorSuite(errors=[error])
