# coding=utf-8
"""
    >>> import abc
    >>> class A(metaclass = abc.ABCMeta):
    ...     @abc.abstractmethod
    ...     def foo(self): pass


    >>> A()
    Traceback (most recent call last):
    TypeError: Can't instantiate abstract class A with abstract methods foo
    >>> A.__abstractmethods__=set()
    >>> A() #doctest: +ELLIPSIS
    <....A object at 0x...>

    >>> class B(object): pass
    >>> B() #doctest: +ELLIPSIS
    <....B object at 0x...>

    >>> B.__abstractmethods__={"foo"}
    >>> B()
    Traceback (most recent call last):
    TypeError: Can't instantiate abstract class B with abstract methods foo

    >>> class A(metaclass = abc.ABCMeta):
    ...     @abc.abstractmethod
    ...     def foo(self): pass
    >>> from unittest.mock import patch
    >>> p = patch.multiple(A, __abstractmethods__=set())
    >>> p.start()
    {}
    >>> A() #doctest: +ELLIPSIS
    <....A object at 0x...>
    >>> p.stop()
    >>> A()
    Traceback (most recent call last):
    TypeError: Can't instantiate abstract class A with abstract methods foo
"""


def patch_abc(to_patch):

    from unittest.mock import patch

    p = patch.multiple(to_patch, __abstractmethods__=set())
    p.start()
    return p
