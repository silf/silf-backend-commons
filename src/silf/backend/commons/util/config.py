# coding=utf-8
import os

import sys
import configparser
import inspect
import logging
from os import path
import warnings

__all__ = [
    "prepend_current_dir_to_config_file",
    "open_configfile",
    "SilfConfigParser",
    "ConfigValidationError",
]


class ConfigValidationError(configparser.Error):
    pass


class SilfConfigParser(configparser.ConfigParser):
    def __init__(self, *args, **kwargs):
        if "interpolation" not in kwargs:
            kwargs["interpolation"] = configparser.ExtendedInterpolation()
        super().__init__(*args, **kwargs)
        self._read_files = []

    def _read(self, fp, fpname):
        result = super()._read(fp, fpname)

        self._read_files.append(fpname)

        return result

    def validate_mandatory_config_keys(self, section, required_cofig_keys):
        warnings.warn(
            "SilfConfigParser.validate_mandatory_config_files is deprecated use silf.backend.util.config.validate_mandatory_config_keys",
            DeprecationWarning,
        )
        validate_mandatory_config_keys(self, section, required_cofig_keys)


def prepend_current_dir_to_config_file(file_name, caller_frame_offset=1):
    """

    .. todo::
        Use proper packaging tool, and get rid of assumption that files are
        laying on the fs directories.


    Prepends directory name of caller `file_name`

    :param str file_name: File name ot be appended
    :param int caller_frame_offset: Frame from which to take the dirname :default:1
    :return: Absolute path to file
    """
    return path.join(
        path.dirname(inspect.getfile(sys._getframe(caller_frame_offset))), file_name
    )


def create_config_parser(configfile):
    """
    Creates configparser loading additional config files
    :param str configfile:
    :return: :py:class:`configparser.ConfigParser`
    """

    if not os.path.exists(configfile):
        raise ValueError("ConfigFile {} does not exist".format(configfile))

    cp = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    cp.read(configfile)

    additional_config_files = cp.get("Config", "AdditionalConfigFiles", fallback=None)

    if additional_config_files is not None:
        for file in additional_config_files.split(";"):
            if not os.path.exists(file):
                raise ValueError("Config file {} does not exist".format(file))
            cp.read(file)

    return cp


def open_configfile(*config_files):

    """
    Opens configfile.

    :param str config_file: Absolute path to open
    :return: :class:SilfConfigParser
    """

    config_file = config_files[0]

    if config_file is None:
        return None

    if isinstance(config_file, configparser.RawConfigParser):
        return config_file

    cp = SilfConfigParser()

    for cf in config_files:

        if not isinstance(cf, str):
            raise TypeError()

        if not path.exists(cf):
            raise ValueError("Config file {} does not exist".format(config_file))

        with open(cf, encoding="utf8") as f:
            cp.read_file(f)

    return cp


def validate_mandatory_config_keys(parser, section, required_cofig_keys):
    """

    >>> cp = SilfConfigParser()
    >>> cp.read_dict({'foo' : {'foo': '1.23', 'bar': 'bar'}})

    >>> validate_mandatory_config_keys(cp, 'foo', ('foo', 'bar'))

    >>> validate_mandatory_config_keys(cp, 'bar', ('foo', 'bar')) # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
    config.ConfigValidationError: Required section bar was missing from configfile. We read following files []

    >>> validate_mandatory_config_keys(cp, 'foo', ('foo', 'baz')) # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
    config.ConfigValidationError: Required values {'baz'} in section foo were not found in configfile. We read following files into the file []

    >>> cp.getfloat('foo', 'foo')
    1.23

    >>> cp.getfloat('foo', 'baz', fallback=32.1)
    32.1

    :param str section:
    :param list required_cofig_keys:

    :raises ConfigValidationError: If any mandatory values are missiong.

    """

    read_files = getattr(parser, "_read_files", [])

    if not section in parser:
        raise ConfigValidationError(
            "Required section {} was missing from configfile. "
            "We read following files {}".format(section, read_files)
        )

    required_cofig_keys = {ck.lower() for ck in required_cofig_keys}
    avilable_keys = set(parser[section].keys())

    diffenence = required_cofig_keys - avilable_keys

    if diffenence:
        raise ConfigValidationError(
            "Required values {} in section '{}' were not found in "
            "configfile. We read following files into the file {}".format(
                diffenence, section, read_files
            )
        )


def configure_logging(config, logging_section="Logging"):
    """
    expected are two config values for logging:
        config_type - can be basic or file, if file then next config value is required
        logging_file - path to file which contains logging configuration
    """
    logging_type = config.get(logging_section, "config_type", fallback="basic")

    if logging_type == "basic":
        logging.basicConfig(level=logging.DEBUG)

        return

    if logging_type == "file":
        default = prepend_current_dir_to_config_file("logging.ini")
        logging.config.fileConfig(
            config.get(logging_section, "logging_file", fallback=default),
            disable_existing_loggers=False,
        )
        return
