from math import log10, floor


def round_sig(x, sig=2):
    """
    round float to sig significant digits

    params:
        x   -- float to round
        sig -- number of significant digits
    return: rounded float
    """

    if x == 0:
        return 0
    return round(x, sig - int(floor(log10(abs(x)))) - 1)
