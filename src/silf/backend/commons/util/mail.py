# -*- coding: utf-8 -*-

from configparser import ConfigParser, NoOptionError
from io import StringIO
import smtplib
from email.mime.text import MIMEText
import traceback
import sys
import logging
import time

from silf.backend.commons.api.exceptions import ConfigurationException
from silf.backend.commons.util.reflect_utils import load_item_from_module


class BorgSender(object):
    """
    This is a borg class containing e-mail sender for currenly tunning expeirment
    we have to have global state containing e-mail sender because without
    it confguiring logging by ini file would be hard.
    """

    borg = {}

    def __init__(self):
        super().__init__()
        self.__dict__ = self.borg

    def set_sender(self, sender):
        self.sender = sender


class ExceptionHandler(logging.Handler):
    """
    Sends e-mail with logging messahes
    """

    def __init__(self, level=logging.ERROR):
        super().__init__(level)

    def emit(self, record):
        try:
            message = self.format(record)
            sender = BorgSender().sender
            if sys.exc_info()[0]:
                sender.send_current_exception_to_admins(message=message)
            else:
                sender.send_email_to_admins(body=message)
        except Exception:
            print("Couldn't send error message")
            # logging.error("Couldn't send error message")


RESEND_EXCEPTION_TIMEOUT_SEC = 300.0
"""
How long we weit between sending emails with exceptions with the same stacktrace.
"""


class EmailSender(object):

    """
    Class that sends e-mails with errors to experiment admins.
    """

    def __init__(self, config):
        assert isinstance(config, ConfigParser)

        self.email_throttling_dict = {}
        """
        A dictionary that maps tracebak to time when exception with this
        traceback was sent last time.
        """

        self.enabled = config.getboolean("SMTP", "smtp_enabled", fallback=False)
        self.mock = False

        if not self.enabled:
            BorgSender().sender = self
            return

        if config.getboolean("SMTP", "smtp_mock", fallback=False):
            BorgSender().sender = self
            self.mock = True
            self.mock_messages = []
            return

        self.experiment_name = config.get("Experiment", "ExperimentName")
        self.smtp_user = config.get("SMTP", "smtp_user")
        self.smtp_port = config.getint("SMTP", "smtp_port")

        self.smtp_password = config.get("SMTP", "smtp_password")
        self.client_path = config.get("SMTP", "smtp_client_class")
        self.SMTP = load_item_from_module(self.client_path)
        self.from_email = config.get("SMTP", "from_email")
        try:
            self.smtp_server_domain = config.get("SMTP", "smtp_server_domain")
        except NoOptionError as e:
            e.message = (
                u"smtp_server option has been deleted in favour of two other options: "
                u"smtp_server_domain (domain used for ehlo command, its value should be the same as it was for smtp_server) "
                u"smtp_server_ip (ip of the smtp server). "
                u"Please update the experiment configuration"
            )
            raise

        self.smtp_server_ip = config.get(
            "SMTP", "smtp_server_ip", fallback=self.smtp_server_domain
        )

        if "@" not in self.from_email:
            raise ConfigurationException(
                "'from_email' configuration field should be an e-mail address"
            )

        self.admins = config.get("SMTP", "admin_list").split(",")

        try:
            self._log_in()
        except smtplib.SMTPResponseException as e:
            raise ConfigurationException(
                "Couldn't login using supplied parameters, please check configuration"
            ) from e
        BorgSender().sender = self

    def should_send_exception(self, traceback):

        keys_to_remove = set()

        now = time.monotonic()

        for k, v in self.email_throttling_dict.items():
            if (now - v) > RESEND_EXCEPTION_TIMEOUT_SEC:
                keys_to_remove.add(k)

        for k in keys_to_remove:
            del self.email_throttling_dict[k]

        if traceback in self.email_throttling_dict:
            return False

        self.email_throttling_dict[traceback] = now
        return True

    def _log_in(self):
        client = self.SMTP(
            host=self.smtp_server_ip,
            port=self.smtp_port,
            local_hostname=self.smtp_server_domain,
        )
        client.ehlo()
        client.login(self.smtp_user, self.smtp_password)
        return client

    def install_error_function(self):
        oldhook = sys.excepthook

        def new_hook(*args):
            oldhook(*args)
            try:
                self.send_current_exception_to_admins(
                    message="Uncaught exception for " + self.experiment_name,
                    excinfo=args,
                )
            except Exception:
                pass

        sys.excepthook = new_hook

    def send_email_to_admins(self, body, subject=None):
        if self.mock:
            self.mock_messages.append((subject, body))
            return
        if not self.enabled:
            return

        if subject is None:
            subject = self.__DEFAULT_MESSAGE_SUBJECT.format(s=self)

        with self._log_in() as client:

            msg = MIMEText(body)
            msg["From"] = self.from_email
            msg["Subject"] = "[SILF] {}".format(subject)

            client.send_message(msg, self.from_email, self.admins)

    def send_current_exception_to_admins(
        self, message=None, subject=None, excinfo=None
    ):
        if not self.enabled:
            return
        file = StringIO()
        tb_file = StringIO()
        if message is None:
            message = self.__DEFAULT_EXCEPTION_MESSAGE.format(s=self)
        file.write(message)
        file.write("\n")
        if excinfo is None:
            excinfo = sys.exc_info()
        traceback.print_exception(*excinfo, file=tb_file)
        if self.should_send_exception(tb_file.getvalue()):
            file.write(tb_file.getvalue())
            self.send_email_to_admins(file.getvalue(), subject=subject)

    __DEFAULT_MESSAGE_SUBJECT = """
    Error in {s.experiment_name}
    """

    __DEFAULT_EXCEPTION_MESSAGE = """
        Following exception occoured in {s.experiment_name}:
    """.strip()
