import importlib


def pythonpath_entries(pythonpath_string):

    entries = pythonpath_string.split(":")

    return [e.strip() for e in entries if len(e.strip()) > 0]


def load_item_from_module(path):
    mod_name, object_name = path.rsplit(".", 1)
    module = importlib.import_module(mod_name)
    clazz = getattr(module, object_name)
    return clazz
