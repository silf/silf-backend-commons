# coding=utf-8
from silf.backend.commons.util.config import open_configfile
import configparser


def create_config(base_config, key_overrides):
    """
    Opens config file overriding some values

    :param str base_config: Filename of config file
    :param dict key_overrides: :class:dict of :class:`dict`'s containing overrides
    :return: :class:`.ConfigParser`
    """

    config_file = open_configfile(base_config)

    for section_name, sect_overrides in key_overrides.items():
        if not config_file.has_section(section_name):
            config_file.add_section(section_name)
        for name, value in sect_overrides.items():
            config_file[section_name][name] = value

    return config_file


def convert_dict_to_config(dict_config: dict):
    conf_parser = configparser.ConfigParser()
    for section_name, sect_values in dict_config.items():
        conf_parser.add_section(section_name)
        for name, value in sect_values.items():
            conf_parser[section_name][name] = value

    return conf_parser
