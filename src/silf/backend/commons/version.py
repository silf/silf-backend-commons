# coding=utf-8

import logging
import re
from os import path
from configparser import ConfigParser


class ProtocolVersionUtil:

    version_pattern = re.compile("\d+\.\d+\.\d+")
    version_range_pattern = re.compile("\d+\.\d+\.\d+-\d+\.\d+\.\d+")

    @classmethod
    def validate_protocol_versions(cls, version_list):
        for v in version_list:
            cls.validate_protocol_version(v)

    @classmethod
    def validate_protocol_version(cls, ver):
        """
        >>> ProtocolVersionUtil.validate_protocol_version("0.0.0")
        True

        >>> ProtocolVersionUtil.validate_protocol_version("2.1.0")
        True

        >>> ProtocolVersionUtil.validate_protocol_version("2.1.0-2.2.0")
        True

        >>> ProtocolVersionUtil.validate_protocol_version("1.0")
        Traceback (most recent call last):
        ValueError: Wrong syntax for protocol version: 1.0
        >>> ProtocolVersionUtil.validate_protocol_version("1.0.a")
        Traceback (most recent call last):
        ValueError: Wrong syntax for protocol version: 1.0.a
        >>> ProtocolVersionUtil.validate_protocol_version("ala")
        Traceback (most recent call last):
        ValueError: Wrong syntax for protocol version: ala
        >>> ProtocolVersionUtil.validate_protocol_version("2.a.0-2.2.0")
        Traceback (most recent call last):
        ValueError: Wrong syntax for protocol range: 2.a.0-2.2.0
        >>> ProtocolVersionUtil.validate_protocol_version("2.1.0-2.2.bb")
        Traceback (most recent call last):
        ValueError: Wrong syntax for protocol range: 2.1.0-2.2.bb
        """
        # TODO przerob na zwraceanie true false lub wyrzej lap wyjatki
        if "-" in ver:
            if not cls.version_range_pattern.match(ver):
                raise ValueError("Wrong syntax for protocol range: " + ver)
        else:
            if not cls.version_pattern.match(ver):
                raise ValueError("Wrong syntax for protocol version: " + ver)

        return True

    @classmethod
    def is_version_in_list(cls, ver, version_list):
        """
        >>> ProtocolVersionUtil.is_version_in_list("1.0.0", [])
        False
        >>> ProtocolVersionUtil.is_version_in_list("1.0.0", ["0.0.1"])
        False
        >>> ProtocolVersionUtil.is_version_in_list("1.0.0", ["0.0.1","0.5.0-0.9.0"])
        False
        >>> ProtocolVersionUtil.is_version_in_list("1.0.0", ["1.0.0","0.5.0-0.9.0"])
        True
        >>> ProtocolVersionUtil.is_version_in_list("1.0.0", ["0.7.0","0.9.0-1.9.0"])
        True
        """
        version_is_supported = False
        for v in version_list:
            if "-" in v:
                version_is_supported |= cls._is_version_in_range(ver, v)
            else:
                version_is_supported |= v == ver

        return version_is_supported

    @classmethod
    def _is_version_in_range(cls, ver, version_range):
        """
        >>> ProtocolVersionUtil._is_version_in_range("2.0.0", "1.7.5-2.1.0")
        True
        >>> ProtocolVersionUtil._is_version_in_range("1.7.5", "1.7.5-2.1.0")
        True
        >>> ProtocolVersionUtil._is_version_in_range("2.1.0", "1.7.5-2.1.0")
        False
        >>> ProtocolVersionUtil._is_version_in_range("2.0.999", "1.7.5-2.1.0")
        True
        >>> ProtocolVersionUtil._is_version_in_range("2.1.1", "1.7.5-2.1.0")
        False
        """
        vt = version_range.split("-")
        return ver == vt[0] or (
            cls._is_version_lower(vt[0], ver) and cls._is_version_lower(ver, vt[1])
        )

    @classmethod
    def _is_version_lower(cls, left, right):
        """
         >>> ProtocolVersionUtil._is_version_lower("1.0.0", "2.0.0")
         True
         >>> ProtocolVersionUtil._is_version_lower("1.10.0", "1.29.0")
         True
         >>> ProtocolVersionUtil._is_version_lower("1.10.17", "1.10.1001")
         True
         >>> ProtocolVersionUtil._is_version_lower("1.10.7", "1.10.7")
         False
         >>> ProtocolVersionUtil._is_version_lower("1.10.7", "1.10.1")
         False
        """
        l_tab = [int(subV) for subV in left.split(".")]
        r_tab = [int(subV) for subV in right.split(".")]
        return (
            l_tab[0] < r_tab[0]
            or (l_tab[0] == r_tab[0] and l_tab[1] < r_tab[1])
            or (l_tab[0] == r_tab[0] and l_tab[1] == r_tab[1] and l_tab[2] < r_tab[2])
        )


cp = ConfigParser()
file = path.join(path.dirname(__file__), "version.ini")
with open(file, encoding="utf-8") as f:
    cp.read_file(f)

__VERSION__ = cp["VERSION"]["version"].split(".")
PROTOCOL_VERSIONS = cp["VERSION"]["PROTOCOL_VERSIONS"].split(",")
ProtocolVersionUtil.validate_protocol_versions(PROTOCOL_VERSIONS)

logging.getLogger("silf.backend.commons").info(
    "Running silf.backend.commons version".format(".".join(__VERSION__))
)
