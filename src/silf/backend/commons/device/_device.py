# coding=utf-8
"""

This is a api for a device.

"""

import sys
import warnings
import configparser
import contextlib

import logging

import abc
from silf.backend.commons.util.abc_utils import patch_abc

from silf.backend.commons.util.config import open_configfile

from silf.backend.commons.api.exceptions import *
from silf.backend.commons.device._const import *


class InvalidCallToAssertState(Warning):
    pass


warnings.simplefilter("error", InvalidCallToAssertState)


class Device(metaclass=abc.ABCMeta):

    """
    Defines plugin for a particular device in the experiment.

    All methods are blocking, that is should block the current thread until
    finished.

    .. note:: Instances of this object don't need to use any synchronization,
        they will always be called from single thread, This instance will be
        constructed used and destroyed on single process.

    .. warning:: All methods should exit relatively fast.

    .. warning:: Both method parameters and responses should be pickleable,
        these will be travelling between process boundaries.

    """

    MAIN_LOOP_INTERVAL = 0.1
    """
    Interval between invocations of main loop.
    Represents number of seconds as :class:`float`.
    """

    def __init__(self, device_id="default", config_file=None):

        """
        :param str device_id: Name of the device
        :param str config_file: Name of `ini` file used to configure this device.
        """

        self.state = DEVICE_STATES[OFF]
        """
            State of this device should be in :data:`DEVICE_STATES`, full
            state chart is avilable in: :ref:`device-state-chart`.
        """
        self.device_id = device_id

        self.series_id = None

        self.config_file_name = config_file

        self.config = open_configfile(config_file)

    def _assert_state(self, state_to_be_asserted, msg=""):

        if msg in DEVICE_STATES:
            warnings.warn(
                "Message argument to Device._assert_state was '{}', "
                "which is a name of state. Probably a "
                "programming error".format(msg),
                InvalidCallToAssertState,
            )

        if isinstance(state_to_be_asserted, str):
            state_to_be_asserted = (state_to_be_asserted,)

        caller_frame = sys._getframe(1)
        caller_name = caller_frame.f_code.co_name

        if not self.state in state_to_be_asserted:
            raise InvalidStateException(
                message="Device {} should be in state '{}', and is in state '{}'. Custom "
                "message was {}. Method called was: {}".format(
                    self.device_id, state_to_be_asserted, self.state, msg, caller_name
                )
            )

    @property
    def logger(self):
        """
        :return: Logger instance attached to this device. Utility method, you
                 may use whatsoever logger you want
        """
        return logging.getLogger("device." + self.device_id)

    def perform_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        """
        Performs diagnostics on the device. Can be ran if this device is
        :data:`OFF`. or :data:`STAND_BY`.

        :param str diagnostics_level: Whether diagnostisc should be thororough or not, must be in :data:`DEVICE_STATES`

        :raises DiagnosticsException: If there is error in diagnostics.
        :raises InvalidStateException: If device is in invalid state (that is not :data:`OFF`)
        :raises DeviceRuntimeException: If any exception occours.
        """
        warnings.warn(
            "perform_diagnostics is deprecated, use pre_power_up diagnostics and post_power_up_diagnostics",
            DeprecationWarning,
        )
        self.post_power_up_diagnostics(diagnostics_level)

    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        self._assert_state((DEVICE_STATES[OFF], DEVICE_STATES[STAND_BY]))

    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        self._assert_state((DEVICE_STATES[OFF], DEVICE_STATES[STAND_BY]))

    def apply_settings(self, settings):
        """
        Applies some set of settings to this device.

        :param dict settings: Settings to be applied, it is already validated
            by the :class:`IDeviceManager`.

        :raises InvalidStateException: If device is in invalid state (that is
                                       not :data:`STAND_BY` or :data:`RUNNING`)

        :raises DeviceRuntimeException: If any exception occours

        :return: None
        :rtype: None
        """
        self._assert_state(
            (DEVICE_STATES[STAND_BY], DEVICE_STATES[RUNNING], DEVICE_STATES[READY])
        )
        if self.state == DEVICE_STATES[STAND_BY]:
            self.state = DEVICE_STATES[READY]

    def power_up(self):
        """
        Call to this method enables consecutive :meth:`apply_settings`.

        It also should power up the device (if this action makes any sense
        for this particular device see also: :ref:`device-power-management`).

        :raises InvalidStateException: If device is in invalid state (that is
                                       not :data:`OFF`
        :raises DeviceRuntimeException: If any exception occours
        """
        self._assert_state((DEVICE_STATES[OFF]))
        self.state = DEVICE_STATES[STAND_BY]
        self.logger.debug("Powering up")

    def power_down(self):
        """
        Call to this method moves this class to :data:`OFF` state.

        :raises InvalidStateException: If device is in invalid state (that is
                                       not :data:`STAND_BY`
        :raises DeviceRuntimeException: If any exception occours
        """
        self.state = DEVICE_STATES[OFF]
        self.logger.debug("Powering down")

    def pop_results(self):
        """
        This method returns list of recently acquired points, it should clear
        this list so next calls won't return the same result points.

        :raises InvalidStateException: If device is in invalid state (that is
                                       not :data:`RUNNING`

        :return: Returns results for (possibly) many points.
        :rtype: :class:`list` (or any other iterable) of `dict`.

        :raises InvalidStateException: If device is in invalid state (that is
                                       not :data:`READY`
        :raises DeviceRuntimeException: If any exception occours


        """
        return []

    def start(self):
        """
        Starts the acquisituon on the device (that is starts the measurements).

        Blocks until this device is stared.

        :raises InvalidStateException: If device is in invalid state (that is
                                       not :data:`READY`

        :return: None
        :rtype: None
        """
        self._assert_state((DEVICE_STATES[READY], DEVICE_STATES[RUNNING]))
        self.state = DEVICE_STATES[RUNNING]

    def stop(self):
        """
        Stops the acquisituon on the device (that is stops the measurements).

        Blocks until this device is stared.

        :raises InvalidStateException: If device is in invalid state (that is not :data:`RUNNING`)
        :raises DeviceRuntimeException: If any exception occours

        :return: None
        :rtype: None
        """
        self._assert_state((DEVICE_STATES[READY], DEVICE_STATES[RUNNING]))
        self.state = DEVICE_STATES[READY]

    def _tear_down(self):
        """
        You should override this method when doing tear_down, it will
        get called only once per device.

        .. note:: You should override this method.

        :raises DeviceRuntimeException: If any exception occours

        :return: None
        :rtype; None
        """

        self.logger.debug("Tearing down")

    def tearDown(self):
        warnings.warn(
            "Used deprecated function tearDown, You should rename it to tear_down",
            DeprecationWarning,
        )
        self.tear_down()

    def tear_down(self):
        """
        Called when current process is being disabled.

        This method can be called multiple times.

        .. note:: do not override this method, override :meth:`_tear_down`.

        :raises DeviceRuntimeException: If any exception occours

        :return: None
        :rtype: None
        """

        if self.state != DEVICE_STATES[CLEANED_UP]:
            try:
                self._tear_down()
            finally:
                self.state = DEVICE_STATES[CLEANED_UP]

    def loop_iteration(self):
        """
        Perform an iteration of main experiment loop. Should terminate quickly,

        :raises DeviceRuntimeException: If any exception occours

        :return: If returned value is `False`
                 or `None` next iteration of this method will be scheduled after
                 :attr:`MAIN_LOOP_INTERVAL` seconds, it result is true it will
                 be sheduled earlier (after at most one command from controller
                 was performed);
        """

    @contextlib.contextmanager
    def _config_exception_wrapper(self):
        """
        Contextmanager that translates :class:`configparser.Error`
        to :class:`DeviceException`.

        >>> p = patch_abc(Device)
        >>> d = Device()
        >>> with d._config_exception_wrapper():
        ...    raise configparser.NoSectionError('foo') # doctest: +IGNORE_EXCEPTION_DETAIL, +ELLIPSIS
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.DeviceException: No section: 'foo'
        >>> p.stop()


        """
        try:
            yield
        except configparser.Error as e:
            raise DeviceException(e.message) from e
