"""
    It packages api for single device.
"""

from silf.backend.commons.device._const import *
from silf.backend.commons.device._device import Device
from silf.backend.commons.api.exceptions import *
