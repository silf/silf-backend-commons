OFF = 0
STAND_BY = 1
READY = 2
RUNNING = 3
CLEANED_UP = 4

DEVICE_STATES = ("off", "stand-by", "ready", "running", "cleaned_up")
"""
Touple containing all allowable device states.
"""

DIAGNOSTICS_SHORT = 0
DIAGNOSTICS_LONG = 1

DIAGNOSTICS_LEVEL = ("short", "long")
