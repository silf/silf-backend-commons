import os
import time

# TODO mozna zrobic konstruktor ktory przyjmuje jako argument nazwe urzadzenia ktora potem mozna przeczytac
# z /dev/usbtmc0 i otworzyc odpwoedni plik - nr pliku jest w kolumnie Minor, kolumny oddzielone przez \t


class UsbTmcDriver(object):
    def __init__(self, deviceFilePath):
        # TODO obsluzyc wyjatki - sprawdzic czy plik istnieje
        self.devFile = os.open(deviceFilePath, os.O_RDWR)
        self.lastCmd = None

    def close(self):
        # TODO ten ponizszy przy wielokrotnym zamknieciu rzuca wyjatek, popraw
        os.close(self.devFile)

    def runCmd(self, cmdObj, *parameters):
        """
        Run given usbtmc command

        :param cmdObj: Command to be executed
        :type cmdObj: :class:`UsbTmcCommand`
        :return:
        """
        # TODO sprawdzic czy wpisana ilosc zgadza sie z wejsciowa komenda
        count = os.write(self.devFile, bytes(cmdObj.get_command(*parameters), "UTF-8"))
        self.lastCmd = cmdObj
        time.sleep(cmdObj.sleepTime)

    def getResult(self):
        # TODO ustal czy zawsze jest pusty wiersz po wyniku
        # TODO czy konwertowac wyniki do obiektow - bo mozna :)

        if self.lastCmd is None:
            raise ValueError("No command executed")
        if not self.lastCmd.isResultExpected:
            # TODO czy rzucac wyjatkiem??
            return ""
        res = os.read(self.devFile, self.lastCmd.resultSize)
        result_str = res.decode("utf-8")
        return result_str[0:-1] if result_str[-1] == "\n" else result_str


class UsbTmcCommand(object):
    def __init__(
        self,
        cmdString,
        sleepTime=0.2,
        resultSize=256,
        isResultExpected=True,
        param_separator=",",
    ):
        """

        :param cmdString:
        :param sleepTime: Time in seconds to sleep after executing command
        :param resultSize: Expected maximum result size for given command in bytes
        :param isResultExpected: Set to true if after executing command result is expected
        :param param_separator Separator used for parameters for command
        """

        # TODO z jakiejs dokuemntacji mamy:
        # Note that echo will also add a \n (newline) character (which is required by most instruments).
        # wiec trzeba bedzie dodawac \n jesli nie ma na koncu komendy
        self.cmdString = cmdString
        self.sleepTime = sleepTime
        self.isResultExpected = isResultExpected
        self.resultSize = resultSize
        self.param_separator = param_separator

    def get_command(self, *cmdParams):
        param_string = ""
        if cmdParams:
            strParams = [str(x) for x in cmdParams]
            param_string = " " + self.param_separator.join(strParams)
        return self.cmdString + param_string + "\n"


class BaseUsbTmcCommands(object):
    # Queries the equipment ID
    IDN = UsbTmcCommand("*IDN?")
    # Resets the instrument and restores it into factory defaults.
    RESET = UsbTmcCommand("*RST", sleepTime=2, isResultExpected=False)
    # Clears values from all of the Event Registers and the Error Queue.
    CLEAR = UsbTmcCommand("*CLS", isResultExpected=False)
