from silf.backend.commons.io.serialbase import *
import zlib
import time
import struct

from functools import wraps


class CRCError(Exception):
    pass


__LONG_TRANSACTION_ERROR = (
    "Error when calling '{f.__name__}' with parameters {args} {kwargs}. "
    "Call was retried {tries}, after each tried we slept {timeout}secs."
    "Last error was attached as cause for this exception."
)


def long_transaction(tries=5, timeout=0.5, caught_exceptions=(CRCError,)):
    """
    Executes decorated function. If this function raises CRCError it will wait
    timeout and retry. It will retry `tries` time.
    :param int tries: Maximal number of retries
    :param float timeout: Timeout between retries in seconds.
    :return:
    """

    if isinstance(caught_exceptions, list):
        caught_exceptions = tuple(caught_exceptions)

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            last_error = None
            for __ in range(tries):
                try:
                    return f(*args, **kwargs)
                except caught_exceptions as e:
                    time.sleep(timeout)
                    last_error = e
            raise SerialError(
                __LONG_TRANSACTION_ERROR.format(
                    f=f, args=args, kwargs=kwargs, tries=tries, timeout=timeout
                )
            ) from last_error

        return wrapper

    return decorator


class BasicDriver(SerialBase):
    """
    Basic serial port driver

    Implements protocol described here:
    https://bitbucket.org/silf/silf-backend-photoelectric-hardware
    """

    def __init__(self, port, baud, databits, stopbits, parity, timeout):
        """
        :param string port: Serial port device (eg. '/dev/ttyUSB0')
        :param int baud: Baud rate (eg. 9600 or 115200)
        :param int databits: Number of data bits in serial frame
        :param int stopbits: Number of stop bits in serial frame
        :param const parity: Parity checking (one of PARITY_NONE, PARITY_EVEN, PARITY_ODD, PARITY_MARK, PARITY_SPACE)
        :param float timeout: Read timeout (in seconds), 0 = no timeout
        """
        super().__init__(port, baud, databits, stopbits, parity, timeout)

    def send_command(self, cmd, arg=0):
        """
        Send command with argument through serial port
        Append CRC32 checksum and newline to sent data

        :param char cmd: Command to be sent
        :param int16_t arg: Command argument (16 bit integer)
        """
        data = struct.pack("<2BH", ord("C"), cmd, arg)
        buf = bytearray(9)
        buf[0:4] = data[0:4]
        buf[4:8] = struct.pack("<I", zlib.crc32(data))
        buf[8] = ord("\n")

        self.write_bytes(buf)

        return self.parse_reply(self.read_bytes(9))

    def parse_reply(self, reply):
        """
        Parse reply from hardware: Check CRC-32 checksum appended to reply,
        check if hardware returned error, unpack data and return it

        :param string reply: Reply from hardware to be parsed
        """
        data = bytearray(4)

        if reply[0] == 0xFF:
            raise CRCError(
                "Hardware returned CRC error (command sent to board got corrupted), check cabling"
            )

        data[0:4] = reply[0:4]
        crc = struct.unpack("<I", reply[4:8])
        crc_calc = zlib.crc32(data)

        if crc[0] != crc_calc:
            raise CRCError(
                "CRC checksum of reply does not match (data sent from board got corrupted), check cabling"
            )

        data[0:3] = data[1:4]
        data[3] = 0

        result = struct.unpack("<I", bytes(data[0:4]))[0]

        return result
