# -*- coding: utf-8 -*-
from silf.backend.commons.io.text_serial import TextSerial
import re


class ATException(Exception):
    pass


class ATTimeoutException(ATException):
    pass


class ATErrorException(ATException):
    pass


class ATDriver(TextSerial):
    def at_command(self, command=None, argument=None):
        if command:
            if argument is not None:
                result = self.send_command("AT+{}={}\n".format(command, argument), 1)
            else:
                result = self.send_command("AT+{}\n".format(command), 1)
        else:
            result = self.send_command("AT\n", 1)

        result = result[0].strip(" \0")

        if result == "ERROR":
            raise ATErrorException("AT+{} command returned error".format(command))
        if not result:
            raise ATTimeoutException(
                "Timeout during AT+{} command handling (no response received)".format(
                    command
                )
            )

        if result != "OK":
            if re.match(r"^\+{}: ".format(command), result):
                return result.replace("+{}: ".format(command), "", 1)
            else:
                raise ATException(
                    "Hardware returned malformed reply for AT+{} command ({}), check cabling".format(
                        command, result
                    )
                )

        return result
