from configparser import ConfigParser
from contextlib import contextmanager
import serial

PARITY_NONE, PARITY_EVEN, PARITY_ODD, PARITY_MARK, PARITY_SPACE = (
    "N",
    "E",
    "O",
    "M",
    "S",
)


class SerialError(Exception):
    pass


class SerialBase:
    """
    Base class for Serial port drivers

    Can open, close, read from and write to serial port.
    If You wish to create new serial driver (protocol) use this class as base.
    """

    @classmethod
    def create_from_config_parser(cls, cp, section):
        assert isinstance(cp, ConfigParser)
        return cls(
            cp.get(section, "port"),
            baud=cp.getint(section, "baud", fallback=9600),
            databits=cp.getint(section, "databits", fallback=8),
            stopbits=cp.getint(section, "stopbits", fallback=1),
            parity=cp.get(section, "parity", fallback=PARITY_NONE),
            timeout=cp.getfloat(section, "timeout", fallback=1),
        )

    def __init__(self, port, baud, databits, stopbits, parity, timeout):
        """
        Only save serial connection parameters, in order to use serial port You have to call open_port first.

        :param string port: Serial port device (eg. '/dev/ttyUSB0')
        :param int baud: Baud rate (eg. 9600 or 115200)
        :param int databits: Number of data bits in serial frame
        :param int stopbits: Number of stop bits in serial frame
        :param const parity: Parity checking (one of PARITY_NONE, PARITY_EVEN, PARITY_ODD, PARITY_MARK, PARITY_SPACE)
        :param float timeout: Read timeout (in seconds), 0 = no timeout
        """
        self.port = port
        self.baud = baud
        self.databits = databits
        self.stopbits = stopbits
        self.parity = parity
        self.timeout = timeout
        self.tty = None

    def open_port(self):
        """
        Open serial port
        """
        if self.tty and self.tty.isOpen():
            raise SerialError(
                "Port {} already open (something else uses it)!".format(self.port)
            )
        self.tty = serial.Serial(
            self.port,
            self.baud,
            self.databits,
            self.parity,
            self.stopbits,
            timeout=self.timeout,
        )

    def close_port(self):
        """
        Close serial port
        """
        if self.tty and self.tty.isOpen():
            self.tty.close()

    @contextmanager
    def with_opened_port(self):
        port_opened = False
        try:
            if not (self.tty and self.tty.isOpen()):
                self.open_port()
                port_opened = True
            yield
        finally:
            if port_opened:
                self.close_port()

    def write_bytes(self, data):
        """
        Write data to serial port

        :param data: Data to be written
        """
        self.tty.write(data)

    def read_bytes(self, bytes):
        """
        Read bytes from serial port

        :param int bytes: Number of bytes to read from serial port
        """
        return self.tty.read(bytes)
