# -*- coding: utf-8 -*-
from contextlib import contextmanager
import io
from silf.backend.commons.io.serialbase import SerialBase


class TextSerial(SerialBase):
    def __init__(self, port, baud, databits, stopbits, parity, timeout):
        super().__init__(port, baud, databits, stopbits, parity, timeout)
        self.__text_io_wrapper = None

    def readline(self):
        return self.tty.readline().decode("ASCII").strip()

    def write_str(self, message):
        # print("MSG={}".format(message))
        self.write_bytes(message.encode("ASCII"))

    # @profile
    def send_command(self, message, lines_of_response=0):
        with self.with_opened_port():
            self.write_str(message)
            return [self.readline() for ii in range(lines_of_response)]
