# coding=utf-8
import logging

import struct
import subprocess
from subprocess import CalledProcessError

import time
from functools import wraps
from contextlib import contextmanager

import quick2wire.i2c as i2c
from silf.backend.commons.api import DiagnosticsException
from silf.backend.commons.api.const import MAX_DEVICE_WAIT_TIME


class I2CError(Exception):
    pass


__LONG_TRANSACTION_ERROR = (
    "Error when calling '{f.__name__}' with parameters {args} {kwargs}. "
    "Call was retried {tries}, after each tried we slept {timeout}secs."
    "Last error was attached as cause for this exception."
)


def long_transaction(tries=5, timeout=0.5, caught_exceptions=(OSError, I2CError)):
    """
    Executes decorated function. If this function raises OSError with
    errno == 5 (raised by quick2wire when i2c error occours) it will wait
    timeout and retry. It will retry `tries` time.
    :param int tries: Maximal number of retrues
    :param float timeout: Timeout between retries in seconds.
    :return:
    """

    if isinstance(caught_exceptions, list):
        caught_exceptions = tuple(caught_exceptions)

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            last_error = None
            for __ in range(tries):
                try:
                    return f(*args, **kwargs)
                except caught_exceptions as e:
                    time.sleep(timeout)
                    last_error = e
            raise I2CError(
                __LONG_TRANSACTION_ERROR.format(
                    f=f, args=args, kwargs=kwargs, tries=tries, timeout=timeout
                )
            ) from last_error

        return wrapper

    return decorator


class I2CDriver(object):
    def __init__(self, addr):
        super().__init__()
        self.addr = addr
        self.user_readable_name = None
        self.__bus = None
        self.register_mapper = {}
        self.logger = logging.root

    @contextmanager
    def _get_bus(self):

        if self.__bus is None:
            with i2c.I2CMaster() as bus:
                self.__bus = bus
                try:
                    yield
                finally:
                    self.__bus = None
        else:
            yield

    def __convert_register(self, register):
        if isinstance(register, str):
            return self.register_mapper[register]
        return register

    @classmethod
    def __find_address(cls, output, addr):
        """

        Finds address in ``i2cdetect`` output.

        :param bytes output: Output of i2cdetect subprocess
        :param int addr: Address to look for.

        :return: Whether ``i2cdetect`` detected ``address``
        :rtype: :class:`bool`

        >>> TEST_DATA = b'''
        ...     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
        ...    00:          -- -- -- -- -- -- -- -- -- -- -- -- --
        ...    10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
        ...    20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
        ...    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
        ...    40: 40 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
        ...    50: -- -- -- 54 -- -- -- -- -- -- -- -- -- 5e -- --
        ...    60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
        ...    70: -- -- -- -- -- -- -- --'''

        >>> I2CDriver._I2CDriver__find_address(TEST_DATA, 0x40)
        True
        >>> I2CDriver._I2CDriver__find_address(TEST_DATA, 0x54)
        True
        >>> I2CDriver._I2CDriver__find_address(TEST_DATA, 0x5e)
        True
        >>> I2CDriver._I2CDriver__find_address(TEST_DATA, 0x04)
        False
        >>> I2CDriver._I2CDriver__find_address(TEST_DATA, 0x3f)
        False
        """
        output = output.decode("ascii").strip()
        lines = output.split("\n")
        lines = lines[1:]  # Ignore header line
        addr = hex(addr)[2:]  # Cut off 0x
        if len(addr) == 1:
            addr = "0" + addr
        for l in lines:
            parts = l.split(":")
            address_part = parts[1]
            if addr in address_part:
                return True
        return False

    def perform_diagnostics(self, diagnostics_level=None):
        """

        Performs diangostics for this device.

        This method checks whether ``i2cdetect`` detects address of this device.

        .. note::
            Should call superclass method.

        """
        base_err = "Nie udało się nawiązać połączenia i2c z urządzeniem '{}' o adresie: '{}'.".format(
            self.user_readable_name, self.addr
        )
        try:
            stdout = subprocess.check_output(
                ["i2cdetect", "-y", "1"],
                stderr=subprocess.STDOUT,
                timeout=MAX_DEVICE_WAIT_TIME.seconds,
            )
        except TimeoutError as e:
            raise DiagnosticsException(
                base_err + "Odpytywanie magistrali trwa zbyt długo."
            ) from e
        except CalledProcessError as e:
            raise DiagnosticsException(
                base_err + "Polecenie i2cdetect zwróciło błąd."
            ) from e

        if not self.__find_address(stdout, self.addr):
            raise DiagnosticsException(base_err + "Urządzenia nie ma na magistrali.")

    def write_byte(self, register, value):
        register = self.__convert_register(register)
        with self._get_bus():
            self.__bus.transaction(i2c.writing_bytes(self.addr, register, value))

    def read_raw(self, register, num_bytes=1):
        register = self.__convert_register(register)
        # self.logger.debug("Reading register {}, num bytes {}".format(register, num_bytes))
        with self._get_bus():
            raw_str = self.__bus.transaction(
                i2c.writing_bytes(self.addr, register),
                i2c.reading(self.addr, num_bytes),
            )[0]
            # self.logger.debug("Value: {}".format(raw_str))
            return raw_str

    def __validate_value(self, value, register, min_value, max_value, type):
        if min_value is not None and int(value) < min_value:
            raise ValueError(
                "Value in register {} (type {}) is {} which is less than minumum set at {}".format(
                    register, type, value, min_value
                )
            )
        if max_value is not None and int(value) > max_value:
            raise ValueError(
                "Value in register {} (type {}) is {} which is highter than maximum set at {}".format(
                    register, type, value, max_value
                )
            )

    def read_byte(self, register, min_value=None, max_value=None):
        raw_str = self.read_raw(register)
        byte = raw_str[0]
        self.__validate_value(byte, register, min_value, max_value, "byte")
        return byte

    def read_int(self, register, num_bytes=4, fmt="<I", min_value=None, max_value=None):
        raw_str = self.read_raw(register, num_bytes)
        value = struct.unpack(fmt, raw_str)[0]
        self.__validate_value(value, register, min_value, max_value, "int")
        return value

    def read_bool(self, register):
        return bool(self.read_byte(register))

    def wait_until_byte(self, register, desired_value, max_time=3, iter_time=0.1):

        with self._get_bus():

            start = time.monotonic()

            while (time.monotonic() - start) < max_time:

                byte = self.read_byte(register)
                # print("Got {}".format(byte))
                if byte == desired_value:
                    return
                time.sleep(iter_time)

        raise TimeoutError()


class DeclarativeDriver(I2CDriver):

    ADDR = None
    REGISTER_MAPPER = {}
    USER_READABLE_NAME = ""

    def __init__(self):
        super().__init__(self.ADDR)
        self.register_mapper = dict(self.REGISTER_MAPPER)
        self.user_readable_name = self.USER_READABLE_NAME
