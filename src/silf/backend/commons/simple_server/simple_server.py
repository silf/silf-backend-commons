# -*- coding: utf-8 -*-

import abc
import copy
import typing
from abc import ABCMeta
from abc import abstractmethod
import configparser
import logging
from silf.backend.commons.api.stanza_content import (
    ModeSuite,
    Mode,
    ResultSuite,
    SettingSuite,
    OutputFieldSuite,
)
from silf.backend.commons.api.stanza_content.control import ControlSuite
from silf.backend.commons.device_manager import ResultCreator
from silf.backend.commons.experiment import IExperimentManager, ExperimentCallback
from silf.backend.commons.experiment import TranslationMixin

Result = typing.Mapping[str, typing.Any]


class MultiModeManager(TranslationMixin, IExperimentManager):
    """
    A simple experiment manager that supports many modes.
    """

    @classmethod
    @abc.abstractmethod
    def get_mode_managers(
        cls, config_parser: configparser.ConfigParser
    ) -> typing.List["ExperimentMode"]:
        raise NotImplementedError

    def __init__(self, config_parser):
        super().__init__(config_parser)
        self.mode_name = None
        self._mode_manager = None
        self.__started = False
        mode_list = self.get_mode_managers(self.config)
        assert len(mode_list) > 0, "Please supply some modes"
        self._mode_manager_map = {mode.get_mode_name(): mode for mode in mode_list}
        assert len(self._mode_manager_map) == len(
            mode_list
        ), "Duplicate mode name in get_mode_managers"

    def get_mode_manager_type(self, mode_name: str) -> "ExperimentMode":
        """Returns mode manager for selected mode_name."""
        return self._mode_manager_map[mode_name]

    @property
    def mode_manager(self) -> "ExperimentMode":
        """
        Returns manager for currently selected mode. If mode wasn't selected
        raises AssertionError.

        Mode is selected after call to power_up and until call to power_down.
        """
        assert self.has_mode, "Please set mode first"
        return self._mode_manager

    @property
    def is_started(self) -> bool:
        """Returns true if start() was called but stop() wasn't called yet."""
        return self.__started

    @property
    def has_mode(self):
        """
        True if mode is selected.

        Mode is selected after call to power_up and until call to power_down.
        """
        return self.mode_name is not None

    @property
    def modes(self) -> ModeSuite:
        return ModeSuite(
            **{
                m.get_mode_name(): Mode(
                    m.get_mode_label(), m.get_description(), order=ii
                )
                for ii, m in enumerate(self.get_mode_managers(self.config))
            }
        )

    def tear_down(self):
        if self.has_mode:
            self._mode_manager.tear_down()

    def stop(self):
        if self.has_mode:
            self._mode_manager.stop()
            self.__started = False

    def start(self):
        self.mode_manager.start()
        self.__started = True

    def power_up(self, mode_name: str):
        self.__started = False
        if self.has_mode:
            self.power_down()
        mode_type = self.get_mode_manager_type(mode_name)
        self._mode_manager = mode_type(self.config, self.experiment_callback)
        self._mode_manager.power_up()
        self.mode_name = mode_name

    def power_down(self):
        self.__started = False
        if self._mode_manager is not None:
            self._mode_manager.power_down()
            self._mode_manager = None
        self.mode_name = None

    def loop_iteration(self):
        if self.has_mode:
            self._mode_manager.loop_iteration()

    def get_series_name(self, settings: dict) -> str:
        cs = self.get_input_fields(self.mode_name)
        converted = cs.check_and_convert_settings(settings)
        return self.mode_manager.get_series_name(converted)

    def get_output_fields(self, mode_name: str):
        mode_manager_type = self.get_mode_manager_type(mode_name)
        mode_manager = mode_manager_type(self.config, self.experiment_callback)
        return mode_manager.get_output_fields()

    def get_input_fields(self, mode_name: str):
        mode_manager_type = self.get_mode_manager_type(mode_name)
        mode_manager = mode_manager_type(self.config, self.experiment_callback)
        return mode_manager.get_input_fields()

    def check_settings(self, mode_name, settings):
        mode_manager_type = self.get_mode_manager_type(mode_name)
        mode_manager = mode_manager_type(self.config, self.experiment_callback)
        return mode_manager.check_settings(settings)

    def apply_settings(self, settings: SettingSuite):
        self.mode_manager.apply_settings(settings)

    def apply_settings_live(self, settings: SettingSuite):
        self.mode_manager.apply_settings_live(settings)

    def on_miscellaneous_error(self):

        try:
            # First do default implementation (send message) then stop the experiment.
            super().on_miscellaneous_error()
        finally:
            try:
                if self.__started:
                    self.experiment_callback.send_series_done("Exception raised")
                    self.stop()
            except Exception as e:
                logging.exception(
                    "Exception raised when tried to stop the experiment because of earlier error"
                )


class ExperimentMode(metaclass=ABCMeta):
    def __init__(
        self,
        config_parser: configparser.ConfigParser,
        experiment_callback: ExperimentCallback,
    ):
        super().__init__()
        self.experiment_callback = experiment_callback

        self.config = config_parser
        """
        :type:`configparser.ConfigParser`
        """
        self.__settings = {}

        assert not hasattr(self, "DESCRIPTION"), (
            "DESCRIPTION attribute from ExperimentMode has been deprecated in favour of "
            "get_description and get_mode_name methods. "
        )

    @classmethod
    @abstractmethod
    def get_description(cls) -> str:
        """
        Return human-readable mode description.

        It will be translated by the ModeStanza.
        """
        raise NotImplementedError

    @property
    def settings(self):
        return self.__settings

    @classmethod
    @abstractmethod
    def get_mode_name(cls) -> str:
        """
        Technical mode name. Not human readable, should not be translated.
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def get_mode_label(cls):
        return cls.get_mode_name()

    @classmethod
    @abstractmethod
    def get_series_name(cls, settings: dict) -> str:
        """
        This method returns name of result series, this name will be presented
        to users. Additionally this method gets settings that will be applied for
        this series.
        See also default implementation at:
        :meth:`.AbstractExperimentManager.get_series_name`
        :return: Name of result series
        :rtype: str
        """
        raise NotImplementedError

    @abstractmethod
    def get_input_fields(self) -> ControlSuite:
        """
        Returns control suite for this mode.

        You may safely change this to instance method if you need to do so.
        """
        raise NotImplementedError

    @abstractmethod
    def get_output_fields(self) -> OutputFieldSuite:
        """
        Returns control suite for this mode.

        You may safely change this to instance method if you need to do so.
        """
        raise NotImplementedError

    def apply_settings_live(self, settings: SettingSuite):
        cs = self.get_input_fields()
        self.__settings = cs.check_and_convert_live_settings(settings, self.__settings)

    def check_settings(self, settings: SettingSuite) -> None:
        cs = self.get_input_fields()
        cs.check_settings(settings)

    def apply_settings(self, settings: SettingSuite) -> None:
        """
        Applies settings to the experiment.
        This is called when user sends :ref:`proto-silf-series-start`.
        :type settings: :class:`.SettingsSuite`
        :raises: :class:`silf.backend.commons.api.exceptions.SILFProtocolError`
        if there are any errors in settings, or if there is any error during setting
        the equipment up.
        """
        cs = self.get_input_fields()
        self.__settings = cs.check_and_convert_settings(settings)

    @abstractmethod
    def start(self):
        """
        Starts acquisition in response to :ref:`proto-silf-series-start`. Settings
        are already validated and applied to all device managers.
        :return: Name of started series
        :rtype:`str`
        """

    @abstractmethod
    def stop(self):
        """
        Stops data acquisition for the experiment. It does not notify users.
        Called in response to: :ref:`proto-silf-series-stop`.
        """
        self.__settings = {}

    def tear_down(self):
        """
        Cleans up this instance and releases all attached resources.
        """
        self.power_down()

    @abstractmethod
    def power_up(self):
        """
        Powers up the device and sets it's mode (if applicable).
        Called when experiment receives message with namespace :ref:`proto-silf-mode-set`.
        """
        raise NotImplementedError

    @abstractmethod
    def power_down(self):
        """
        Powers down the device, if suspend is false it also cleans up the remote
        device kills and remote process.
        Called in response to: :ref:`proto-silf-experiment-stop`
        """
        raise NotImplementedError

    @abstractmethod
    def loop_iteration(self):
        """
        Subclasses should override that so this method needed background tasks.
        """
        raise NotImplementedError


class ResultManager(object):
    def __init__(
        self, creators: typing.Sequence[ResultCreator], callback: ExperimentCallback
    ):
        self.creators = copy.deepcopy(creators)
        self.callback = callback

    def clear(self):
        """
        Clears all results stored in this manager.

        Should be called eg. on stop.
        """
        for creator in self.creators:
            creator.clear()

    @classmethod
    def _prepare_results(
        self, results: typing.Union[typing.List[Result], Result]
    ) -> typing.List[Result]:
        if not isinstance(results, typing.Sequence):
            results = [results]
        return results

    def _feed_result_aggregators(self, results: typing.List[Result]):
        for r in results:
            for agg in self.creators:
                assert isinstance(agg, ResultCreator)
                agg.aggregate_results(r)

    def _make_results_suite(self) -> ResultSuite:
        return ResultSuite(
            **{
                agg.result_name: agg.pop_results()
                for agg in self.creators
                if agg.has_results
            }
        )

    def push_results(self, results: typing.Union[typing.Sequence[dict], dict]):
        """

        :param results: A dict or a list of dicts with results
        :return:
        """

        results = self._prepare_results(results)
        self._feed_result_aggregators(results)
        result_suite = self._make_results_suite()
        self.callback.send_results(result_suite)
