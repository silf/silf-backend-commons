from datetime import timedelta

MAX_DEVICE_WAIT_TIME = timedelta(seconds=3)

FLOAT_ARRAY = 1

OUTPUT_TYPES = ("int-array", "array", "string", "boolean")

OUTPUT_FIELD_LABEL = "label"

AXIS_X_LABEL = "chart.axis.x.label"
AXIS_Y_LABEL = "chart.axis.y.label"

PRAGMA_APPEND = 1

PRAGMAS = ("transient", "append", "replace")

EXPERIMENT_STATE_OFF = "off"

EXPERIMENT_STATE = (EXPERIMENT_STATE_OFF, "stand-by" "acquiring")

ERROR_SEVERITY = ("warning", "error", "info")

ERROR_TYPE = ("device", "user", "system")
