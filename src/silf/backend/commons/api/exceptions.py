class SilfException(Exception):
    pass


class SILFProtocolError(SilfException):
    """
    Exception that should be propagated to the enduser, it should be caught by
    the experiment class and trensformed to error stanza.
    """

    @classmethod
    def from_args(cls, severity, error_type, message, field=None, **kwargs):
        """

        >>> raise SILFProtocolError.from_args("error", "system", "Test error") # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        exceptions.SILFProtocolError: Test error

        Constructs :class:`SILFProtocolError` from  :class:`.Error`
        constructed  from `args` and `kwargs`.
        :rtype: :class:`SILFProtocolError`
        """
        from silf.backend.commons.api.stanza_content._error import Error

        error = Error(severity, error_type, message, field, **kwargs)
        return cls(error)

    @classmethod
    def join(cls, *args):
        """
        Constructs :class:`SILFProtocolError` from other :class:`SILFProtocolError`
        newly created exception will contain all errors from exception it
        was created from.
        :param args: Iterable of :class:`SILFProtocolError`
        :rtype: :class:`SILFProtocolError`
        """
        from silf.backend.commons.api.stanza_content._error import ErrorSuite

        return cls(ErrorSuite.join(*[e.errors for e in args]))

    def __init__(self, errors):
        """
        :param errors:
        :type errors: :class:`ErrorSuite` or list or :class:`Error`
        """

        from silf.backend.commons.api.stanza_content._error import ErrorSuite

        if not isinstance(errors, ErrorSuite):
            errors = ErrorSuite(errors)

        self.errors = errors

        messages = [err.message.strip() for err in errors.errors]

        super(SILFProtocolError, self).__init__("\n".join(messages))


class ValidationError(SILFProtocolError):
    pass


class SerializationException(SilfException):
    """
    Exception raised during serialization to or from json. It normally signifies
    programming error.
    """

    pass


class MissingRequiredControlException(ValidationError):
    pass


class SettingNonLiveControlException(ValidationError):
    pass


class DeviceException(SilfException):
    """
    Exception raised by the device.
    """

    def __init__(self, message, fixable_by_user=False, **kwargs):
        """
        Exception associated with the device.

        :param str message: Message to pass to user
        :param bool fixable_by_user:
            If true user can fix this exception, is False (default)
            it is the device fault and it must be fixed manually.

        """
        super().__init__(message, **kwargs)
        self.message = message
        self.fixable_by_user = fixable_by_user
        """
        :class:`bool`. True if this erroc can be fixed by user.
        """


class DeviceRuntimeException(SilfException):
    """
    Wrapper for any exception raised by the device.
    """


class InvalidStateException(DeviceException):
    """
    Raised when device is in invalid state
    """

    pass


class DiagnosticsException(DeviceRuntimeException):
    """
    Thrown if intialization of this device encounters an error.
    """

    pass


class InvalidModeException(SilfException):
    """
    Raised when experiment or device wrapper is in invalid mode.
    """


class ConfigurationException(SilfException):
    pass


class ExperimentCreationError(ConfigurationException):
    """
    Raised when we can't create an experiment
    """

    pass


class ExperimentBackendUnresponsive(SILFProtocolError):
    def __init__(self):
        from silf.backend.commons.api.stanza_content._error import Error

        super().__init__(
            [
                Error(
                    "error",
                    "system",
                    "Operacja na urządzeniu trwa zbyt długo. Prawdopdobnie jest to błąd w eksperymencie, "
                    "eksperyment zostanie wyłączony, możesz spróbować uruchomić go ponownie. "
                    "Administratorzy zostali powiadomeni o błędzie.",
                )
            ]
        )
