import itertools

from silf.backend.commons.api.stanza_content._json_mapper import (
    JsonMapped,
    SerializationException,
)

from silf.backend.commons.api.const import ERROR_SEVERITY, ERROR_TYPE

__all__ = ["Error", "ErrorSuite", "ERROR_SEVERITY", "ERROR_TYPE"]


class Error(JsonMapped):

    """
    Represents error message.

    >>> err = Error("warning", "device", "foo")
    >>> err
    <Error severity='warning' error_type='device' metadata='[('message', 'foo')]'>
    >>> err.field = "Foo"
    >>> err
    <Error severity='warning' error_type='device' metadata='[('field_name', 'Foo'), ('message', 'foo')]'>
    >>> err.__getstate__() == {'metadata': {'field_name': 'Foo', 'message': 'foo'}, 'severity': 'warning', 'error_type': 'device'}
    True

    >>> Error.from_dict(err.__getstate__()) == err
    True

    """

    DICT_CONTENTS = {"severity", "error_type", "metadata"}

    DEFAULTS = {"metadata": {}}

    def __init__(self, severity, error_type, message, field=None, **kwargs):
        super().__init__(**kwargs)
        self.severity = severity
        self.error_type = error_type
        self.message = message
        self._validate_state()
        if field is not None:
            self.field = field

    @property
    def message(self):
        return self.metadata.get("message", None)

    @message.setter
    def message(self, val):
        if not isinstance(self.metadata, dict):
            raise SerializationException()
        self.metadata["message"] = val

    @message.deleter
    def message(self):
        del self.metadata["message"]

    @property
    def field(self):
        """
        Field to which this error is attached
        """
        return self.metadata.get("field_name", None)

    @field.setter
    def field(self, val):
        self.metadata["field_name"] = val

    def _validate_state(self):
        """
        Validates this instance.
        >>> Error("warning", "device", "foo") #doctest: +IGNORE_EXCEPTION_DETAIL
        <Error severity='warning' error_type='device' metadata='[('message', 'foo')]'>
        >>> Error("Foo", "device", "foo") #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Severity attribute is Foo and should be in ('warning', 'error', 'info')
        >>> Error("warning", "bar", "foo") #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Error attribute is bar and should be ('device', 'user')
        >>> Error(None, "device", "foo") #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'severity' is None, and it should not be.
        >>> Error("warning", None, "foo") #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'error_type' is None, and it should not be.
        >>> Error("warning", "device", None) #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'message' is None, and it should not be.
        >>> Error(1, "bar", "foo") #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'severity' is not instance of '<class 'str'>' (and is should be)
        >>> Error("warning", 1, "foo") #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'error_type' is not instance of '<class 'str'>' (and is should be)
        >>> Error("warning", "device", 1) #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'message' is not instance of '<class 'str'>' (and is should be)
        >>> Error("warning", "device", "foo", metadata = 1) #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException
        """
        self._assert_is_not_none("severity")
        self._assert_is_not_none("error_type")
        self._assert_is_not_none("metadata")
        self._assert_field(self.message is not None, "Message should not be none")
        self._assert_instance("severity", str)
        self._assert_instance("error_type", str)
        self._assert_instance("message", str)
        self._assert_instance("metadata", dict)
        self._assert_field(
            self.severity in ERROR_SEVERITY,
            "Severity attribute is {} and should be in {}".format(
                self.severity, ERROR_SEVERITY
            ),
        )
        self._assert_field(
            self.error_type in ERROR_TYPE,
            "Error attribute is {} and should be {}".format(
                self.error_type, ERROR_TYPE
            ),
        )

    def __str__(self):
        metadata = sorted(self.metadata.items())
        return (
            "<Error severity='{s.severity}' error_type='{s.error_type}' "
            "metadata='{metadata}'>".format(s=self, metadata=metadata)
        )

    __repr__ = __str__


class ErrorSuite(JsonMapped):

    """
    >>> err = ErrorSuite(errors=[
    ... Error("warning", "device", "foo"), Error("warning", "device", "bar")
    ... ])

    >>> err.__getstate__() == {'errors': [{'metadata': {'message': 'foo'}, 'severity': 'warning', 'error_type': 'device'}, {'metadata': {'message': 'bar'}, 'severity': 'warning', 'error_type': 'device'}]}
    True
    >>> err.setstate({'errors': []})
    >>> err.errors
    []
    >>> err.__setstate__({'errors': [{'metadata': {'message': 'foo'}, 'severity': 'warning', 'error_type': 'device'}, {'metadata': {'message': 'bar'}, 'severity': 'warning', 'error_type': 'device'}]})
    >>> err.errors
    [<Error severity='warning' error_type='device' metadata='[('message', 'foo')]'>, <Error severity='warning' error_type='device' metadata='[('message', 'bar')]'>]

    >>> ErrorSuite.from_dict(err.__getstate__()) == err
    True

    """

    DICT_CONTENTS = {"errors"}

    @classmethod
    def join(cls, *args):
        """
        Returns :class:`ErrorSuite` constructed from all errors in ``*args``.

        >>> error1 = Error("warning", "device", "foo")
        >>> error2 = Error("warning", "device", "bar")
        >>> error3 = Error("warning", "device", "baz")
        >>> ErrorSuite.join(ErrorSuite(errors=[error1, error2]), ErrorSuite(errors=[error3]))
        <ErrorSuite errors=[<Error severity='warning' error_type='device' metadata='[('message', 'foo')]'>, <Error severity='warning' error_type='device' metadata='[('message', 'bar')]'>, <Error severity='warning' error_type='device' metadata='[('message', 'baz')]'>] >

        :param args: List of :class:`ErrorSuite`.
        :rtype: :class:`ErrorSuite`
        """
        errors = list(itertools.chain(*[s.errors for s in args]))
        return ErrorSuite(errors=errors)

    def __init__(self, errors=None):
        """
        :param errors: Errors in this suite. :default:`None`
        :type errors: :class:`list` or :class:`Error`.
        """
        super().__init__()

        if isinstance(errors, Error):
            errors = [errors]

        self.errors = errors
        self._validate_state()

    def _validate_state(self):
        """
        >>> ErrorSuite(errors = None)  #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'errors' is None, and it should not be.
        >>> ErrorSuite(errors = {}) #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'errors' is not instance of '<class 'list'>' (and is should be)
        >>> ErrorSuite(errors = [1, 2, 3]) #doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Items in 'errors' list must be an instanceof Error. Value at index 0 is not: 1
        """
        self._assert_is_not_none("errors")
        self._assert_instance("errors", list)
        for ii, item in enumerate(self.errors):
            if not isinstance(item, Error):
                raise SerializationException(self.__VALIDATE_MESSAGE.format(ii, item))

    def getstate(self):
        return {"errors": [e.__getstate__() for e in self.errors]}

    def setstate(self, state):
        self.errors = []
        for err in state["errors"]:
            self.errors.append(Error.from_dict(err))

    __VALIDATE_MESSAGE = (
        "Items in 'errors' list must be an instance"
        "of Error. Value at index {} is not: {}"
    )
