HTML_INPUT_TYPES = {
    "button",
    "checkbox",
    "color",
    "date",
    "datetime",
    "datetime-local",
    "email",
    "file",
    "hidden",
    "image",
    "month",
    "number",
    "password",
    "radio",
    "range",
    "reset",
    "search",
    "submit",
    "tel",
    "text",
    "time",
    "url",
    "week",
}

SILF_IMPUT_TYPES = {"timedelta-min"}

INPUT_TYPES = HTML_INPUT_TYPES.union(SILF_IMPUT_TYPES)
