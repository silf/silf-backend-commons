from silf.backend.commons.api.exceptions import (
    SILFProtocolError,
    ValidationError,
    MissingRequiredControlException,
)

import abc
import re

from silf.backend.commons.api.stanza_content.control._const import *
from silf.backend.commons.api.stanza_content._error import Error
from silf.backend.commons.api.translations.trans import _


__all__ = ["Control"]


class Control(object):

    """
    Class representing a input field in WEB UI.

    >>> c = Control("foo", "Foo label", "Foo type")
    >>> c
    <Control name=foo type=Foo type live=False label='Foo label'>
    >>> c.to_json_dict() == {'type': 'Foo type', 'metadata': {'label': 'Foo label'}, 'live': False, 'name': 'foo', 'order': None}
    True
    >>> c.default_value=3
    >>> c.to_json_dict() ==  {'default_value': 3, 'name': 'foo', 'live': False, 'metadata': {'label': 'Foo label'}, 'type': 'Foo type', 'order': None}
    True
    >>> c.style = "indicator"
    >>> c.description = "Foo Bar"
    >>> c.to_json_dict() == {'name': 'foo', 'default_value': 3, 'type': 'Foo type', 'live': False, 'metadata': {'style': 'indicator', 'label': 'Foo label', 'description': 'Foo Bar'}, 'order': None}
    True
    """

    def __init__(
        self,
        name,
        label,
        type,
        style=None,
        default_value=None,
        live=False,
        validators=None,
        required=True,
        description=None,
        order=None,
    ):

        super().__init__()

        if validators is None:
            validators = []
        self.validators = validators
        """
        List of validators attached to this field. Validators are python
        callables that should raise
        :class:`silf.backend.commons.api.error.SILFProtocolError`.
        """

        self.name = name
        """
        Html name of the field. Required.
        """
        self.type = type

        self.label = label

        self.id = id
        """
        Html id of the field.  Defaults to ``"id_{}".format(self.name)``
        """
        self.style = style
        """
        HTML class of the field. Defaults to ``None``.
        """

        self.live = live
        """
        Value of `data-live` attribute. This value governs when settings from
        this field are sent. Defaults to ``False``
        """

        self.description = description

        self.default_value = default_value

        self.required = required

        self.order = order

    def post_construct_validate(self):
        """
        Overriden to perform any post-construction validation.
        """

    def _validation_error(self, message, level="error", system_error=False):
        return Error(
            level, "user" if not system_error else "system", message, field=self.name
        )

    def _check_type(self, type):
        if not type:
            raise ValueError("Control type must be a nonempty string")
        if not type in INPUT_TYPES:
            raise ValueError(self.__TYPE_ERROR_MSG.format(type))

    def value_is_empty(self, raw_value):
        """
        Checks whether ``raw_value`` shoule be considered empty.
        :param object raw_value: Unparsed value reclieved from json schema.
        :return:
        """
        if raw_value is None:
            return True
        if isinstance(raw_value, str):
            return len(raw_value) == 0
        return False

    def _validate_required(self, raw_value):

        """
        This method raises error if this control is required and was not set
        by the user.

        :param object raw_value: Unparsed value reclieved from json schema.
        """
        if self.value_is_empty(raw_value):
            if self.required:
                raise MissingRequiredControlException(
                    self._validation_error(self.__CONTROL_REQUIRED.format(self=self))
                )

    def _base_validator(self, value):
        """
        Validator attached to this instance, behaves like one it was a first
        element of :attr:`validators` list
        :param value: value to be validated.
        :raises silf.backend.commons.api.error.SILFProtocolError: if validation
            fails.

        .. note::
            Subclasses should call super method as it does some checks.
        """

    def get_value_type(self):
        return object

    def _validate(self, value):
        """
        Launches all valudators agnist `value`.

        :param value: Value to validate (converted to python object).
        """

        for v in self.validators:
            v(value)
        self._base_validator(value)

    def _python_to_json(self, value):
        """
        Converts python value to json representation of this value.

        :param value: Python value to be converted.
        :return: value converted and ready to be parsed by `json` library.
        """
        return value

    def _json_to_python(self, value):
        """
        Converts json value to python object.
        :param value: Value read from client.
        :return: converted value
        """
        return value

    def convert_json_to_python(self, value):
        """
        Converts python value to json representation of this value.

        :param object value: Unparsed value reclieved from json schema.

        .. note::
            Do not override this method, rather override :meth:`_json_to_python`
            as this method performs additional checks,
        """

        self._validate_required(value)

        if self.value_is_empty(value):
            value = self.default_value

        if self.value_is_empty(value):
            return None

        pyth_value = self._json_to_python(value)

        self._validate(pyth_value)

        return pyth_value

    def convert_python_to_json(self, value, omit_validation=False):
        """
        Converts json value to python object.

        .. note::
            Do not override this method, rather override :meth:`_python_to_json`
            as this method performs additional checks,
        """
        if not omit_validation:
            self._validate(value)
        return self._python_to_json(value)

    ### PROPERTIES

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, val):
        if not val:
            raise ValueError("Name of control must be a nonempty string")
        self._name = val

    @name.deleter
    def name(self):
        del self._name

    @property
    def html_class(self):
        return self._html_class

    @html_class.setter
    def html_class(self, val):
        if val is None:
            val = ""
        self._html_class = val

    @html_class.deleter
    def html_class(self):
        del self._html_class

    def to_json_dict(self):
        base_dict = {
            "name": self.name,
            "type": self.type,
            "live": self.live,
            "order": self.order,
            "metadata": {"label": _(self.label)},
        }

        if self.description:
            base_dict["metadata"]["description"] = _(self.description)

        if self.style:
            base_dict["metadata"]["style"] = self.style

        if self.default_value is not None:
            base_dict["default_value"] = self._python_to_json(self.default_value)

        return base_dict

    def __repr__(self):

        return "<Control name={s.name} type={s.type} live={s.live} label='{s.label}'>".format(
            s=self
        )

    __TYPE_ERROR_MSG = """
    Control type "{{}}" is unsupported. We support following types {}
    """.format(
        sorted(INPUT_TYPES)
    ).strip()

    __CONTROL_REQUIRED = """
    Pole {self.label} jest wymagane, a nie zostało wypełnione
    """
