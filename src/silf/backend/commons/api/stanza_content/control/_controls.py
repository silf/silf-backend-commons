# coding=utf-8
from datetime import timedelta

import math
import re
import datetime
import warnings
from silf.backend.commons.api.exceptions import SILFProtocolError, ValidationError

from silf.backend.commons.api.stanza_content.control import *
from silf.backend.commons.api.translations.trans import _


__all__ = [
    "IntegerControl",
    "NumberControl",
    "TimeControlMinSec",
    "BooleanControl",
    "ComboBoxControl",
]


class NumberControl(Control):

    """
    Control that take integer input, rendered as "number" field.

    >>> control = NumberControl("foo", "Enter a number")
    >>> control.to_json_dict() == {'type': 'number', 'name': 'foo', 'metadata': {'label': 'Enter a number'}, 'live': False, 'validations': {}, 'order': None}
    True
    >>> control.min_value = 1
    >>> control.max_value = 10
    >>> control.description = "Foo"
    >>> control.style = "gauge"

    >>> control.to_json_dict() ==  {
    ...     'type': 'number',
    ...     'validations': {'min_value': 1, 'max_value': 10},
    ...     'live': False,
    ...     'metadata': {'label': 'Enter a number', 'description': 'Foo', 'style': 'gauge'},
    ...     'name': 'foo',
    ...     'order': None}
    True

    >>> control.convert_json_to_python("5")
    5.0

    >>> control.convert_json_to_python("5.5")
    5.5

    >>> control.convert_json_to_python("aaa")
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.ValidationError: Pole "Enter a number" powinno przyjmować wartość liczbową. Nie udało się skonwerować: aaa
    do wartości liczbowej.

    >>> control.convert_json_to_python(5)
    5.0
    >>> control.convert_json_to_python(11)
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.ValidationError: Wartość w polu "Enter a number" powinna być mniejsza niż 10 a wynosi 11.0.

    >>> control.convert_json_to_python(-1)
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.ValidationError: Wartość w polu "Enter a number" powinna być większa niż 1 a wynosi -1.0.

    """

    def __init__(
        self,
        name,
        label,
        type="number",
        style=None,
        default_value=None,
        live=False,
        validators=None,
        required=True,
        description=None,
        min_value=None,
        max_value=None,
        step=None,
    ):
        super().__init__(
            name,
            label,
            type,
            style,
            default_value,
            live,
            validators,
            required,
            description,
        )

        self.min_value = min_value
        self.max_value = max_value
        self.step = step

    __MIN_ERROR_MSG = """
Wartość w polu "{self.label}" powinna być większa niż {self.min_value} a wynosi {value}.
    """.strip()

    __MAX_ERROR_MSG = """
Wartość w polu "{self.label}" powinna być mniejsza niż {self.max_value} a wynosi {value}.
    """.strip()

    def _json_to_python(self, value):
        try:
            if isinstance(value, str):
                value = value.replace(",", ".")
            return self.get_value_type()(value)
        except ValueError as e:
            raise ValidationError(
                self._validation_error(
                    self.__CONVERSION_ERROR.format(self=self, curr_val=value)
                )
            ) from e

    def _base_validator(self, value, omit_required=False):
        messages = []
        super()._base_validator(value)
        if self.min_value is not None and value < self.min_value:
            messages.append(self.__MIN_ERROR_MSG)
        if self.max_value is not None and value > self.max_value:
            messages.append(self.__MAX_ERROR_MSG)
        if messages:
            errors = [
                self._validation_error(msg.format(self=self, value=value), "error")
                for msg in messages
            ]
            raise ValidationError(errors)

    def to_json_dict(self):
        dct = super().to_json_dict()
        dct["validations"] = {}
        if self.min_value is not None:
            dct["validations"]["min_value"] = self.min_value
        if self.max_value is not None:
            dct["validations"]["max_value"] = self.max_value
        if self.step is not None:
            dct["validations"]["step"] = self.step
        return dct

    def get_value_type(self):
        return float

    __CONVERSION_ERROR = """
Pole "{self.label}" powinno przyjmować wartość liczbową. Nie udało się skonwerować: {curr_val}
do wartości liczbowej.
    """.strip()


class IntegerControl(NumberControl):
    def __init__(
        self,
        name,
        label,
        type="number",
        style=None,
        default_value=None,
        live=False,
        validators=None,
        required=True,
        description=None,
        min_value=None,
        max_value=None,
        step=None,
    ):
        super().__init__(
            name,
            label,
            type,
            style,
            default_value,
            live,
            validators,
            required,
            description,
            min_value,
            max_value,
            step,
        )

        warnings.warn(
            "Class IntegerControl is deprecated, please change all instances to number control instead.",
            DeprecationWarning,
        )


class TimeControlMinSec(Control):

    """
    Control that renders to silf-proprietary input field that allows input
    time deltas in format `MM:SS`.

    >>> control = TimeControlMinSec("foo", "Insert time",
    ...    min_time=datetime.timedelta(seconds=30),
    ...    max_time=datetime.timedelta(days=60))

    >>> control.to_json_dict() ==  {'type': 'interval', 'validations': {'max_value': 5184000.0, 'min_value': 30.0}, 'name': 'foo', 'metadata': {'label': 'Insert time'}, 'live': False, 'order': None}
    True

    >>> control.convert_json_to_python(75)
    datetime.timedelta(seconds=75)

    >>> control.convert_python_to_json(datetime.timedelta(days = 10))
    864000.0

    >>> control.convert_json_to_python("bar")
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.ValidationError: Podaj ilość sekund jako "Insert time". Nie udało się przekształcić tej wartości: "bar" na stałą ilczbową.

    >>> control.convert_json_to_python(0)
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.ValidationError: Minimalny czas w polu "Insert time" wynosi 0:00:30. Ustawiono mniejszą wartość: 0:00:00.

    >>> control.convert_python_to_json(datetime.timedelta(days = 60, seconds=1))
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.ValidationError: Maksymalny czas w polu "Insert time" wynosi 60 days, 0:00:00. Ustawiono większą wartość: 60 days, 0:00:01.
    """

    def __init__(
        self,
        name,
        label,
        type="interval",
        style=None,
        default_value=None,
        live=False,
        validators=None,
        required=True,
        description=None,
        max_time=None,
        min_time=datetime.timedelta(),
    ):
        super().__init__(
            name,
            label,
            type,
            style,
            default_value,
            live,
            validators,
            required,
            description,
        )
        self.min_time = min_time
        self.max_time = max_time

    def get_value_type(self):
        return datetime.timedelta

    def to_json_dict(self):
        dct = super().to_json_dict()
        if self.min_time or self.max_time:
            dct["validations"] = {}
            if self.min_time:
                dct["validations"]["min_value"] = self.min_time.total_seconds()
            if self.max_time:
                dct["validations"]["max_value"] = self.max_time.total_seconds()
        if self.default_value:
            if isinstance(self.default_value, timedelta):
                dct["default_value"] = self.default_value.total_seconds()
        return dct

    def _python_to_json(self, value):
        if value is None:
            return ""
        return value.total_seconds()

    def _json_to_python(self, value):
        if value is None:
            return datetime.timedelta()
        if isinstance(value, str):
            value = value.replace(",", ".")
            try:
                value = int(value)
            except ValueError as e:
                raise ValidationError(
                    self._validation_error(
                        self.__INVALID_TYPE.format(self=self, curr_val=value)
                    )
                ) from e

        if value > 31 * 24 * 3600:
            raise ValidationError(
                self._validation_error(
                    self.__TOO_LONG_TIME_ERROR.format(self=self, curr_val=value)
                )
            )

        return datetime.timedelta(seconds=value)

    def _base_validator(self, value):

        super()._base_validator(value)

        dic = {"self": self, "curr_val": value}

        if value is not None and self.min_time and value < self.min_time:
            raise ValidationError(
                self._validation_error(self.__MIN_TIME_ERROR.format(**dic))
            )
        if value is not None and self.max_time and value > self.max_time:
            raise ValidationError(
                self._validation_error(self.__MAX_TIME_ERROR.format(**dic))
            )

    __TOO_LONG_TIME_ERROR = """
Poddano zbyt dużą wartość w "{self.label}".
    """.strip()

    __MIN_TIME_ERROR = """
Minimalny czas w polu "{self.label}" wynosi {self.min_time}. Ustawiono mniejszą wartość: {curr_val}.
    """.strip()

    __MAX_TIME_ERROR = """
Maksymalny czas w polu "{self.label}" wynosi {self.max_time}. Ustawiono większą wartość: {curr_val}.
    """.strip()

    __INVALID_TYPE = """
Podaj ilość sekund jako "{self.label}". Nie udało się przekształcić tej wartości: "{curr_val}" na stałą ilczbową.
    """.strip()


class BooleanControl(Control):
    def __init__(
        self,
        name,
        label,
        type="boolean",
        style=None,
        default_value=None,
        live=False,
        validators=None,
        required=True,
        description=None,
    ):
        super().__init__(
            name,
            label,
            type,
            style,
            default_value,
            live,
            validators,
            required,
            description,
        )

    def _python_to_json(self, value):
        return bool(value)

    def _json_to_python(self, value):
        try:
            return bool(value)
        except ValueError:
            raise ValidationError(
                self._validation_error(
                    "Please supply something convertible to bool for {s.name}".format(
                        s=self
                    ),
                    system_error=True,
                )
            )

    def get_value_type(self):
        return bool


class LightControl(BooleanControl):
    def __init__(
        self,
        name,
        label,
        type="light",
        style=None,
        default_value=None,
        live=False,
        validators=None,
        required=True,
        description=None,
    ):
        super().__init__(
            name,
            label,
            type,
            style,
            default_value,
            live,
            validators,
            required,
            description,
        )


class ComboBoxControl(Control):

    __ITEM_TYPES = [str, int]

    def __init__(self, name, label, item_type=str, choices=None, **kwargs):
        """
        >>> material_widths = {
        ...     3: "3 mm",
        ...     4: "4 mm"
        ... }
        >>> cbc = ComboBoxControl("width", "select width", choices=material_widths, item_type=int)
        >>> cbc.default_value = 3
        >>> cbc.to_json_dict() == {
        ...    'name': 'width',
        ...    'type': 'combo-box',
        ...    'default_value': '3',
        ...    'metadata': {
        ...         'choices': {
        ...             '3': '3 mm', '4': '4 mm'
        ...         },
        ...         'label': 'select width'},
        ...    'live': False,
        ...    'order': None
        ...  }
        True

        >>> labels = {
        ...     "lead" : "Use lead aperture",
        ...     "cu" : "Use cooper aperture",
        ... }
        >>> cbc = ComboBoxControl("material", "Select material", choices=labels, item_type=str)
        >>> cbc.default_value = "cu"
        >>> cbc.to_json_dict() ==  {
        ...     'type': 'combo-box',
        ...     'live': False,
        ...     'metadata': {
        ...         'label': 'Select material',
        ...         'choices': {
        ...             'lead': 'Use lead aperture',
        ...             'cu': 'Use cooper aperture'
        ...         }
        ...     },
        ...     'name': 'material',
        ...     'default_value': 'cu',
        ...     'order': None
        ... }
        True

        :param type item_type: Either an :class:`int` or :class:`str`
        :param dict choices: Dictionary mapping from instances of `item_type` to
            strings.
        """
        super().__init__(name, label, "combo-box", **kwargs)
        self.item_type = item_type

        if choices is None:
            choices = {}
        self.choices = choices

    @property
    def choices(self):
        return self._choices

    @choices.setter
    def choices(self, val):
        if not isinstance(val, dict):
            raise ValueError("Coices for ComboBoxControl should be a dictionary")
        self._choices = val

    def post_construct_validate(self):
        super().post_construct_validate()
        if self.default_value is not None:
            self.default_value = self.item_type(self.default_value)
        for k, v in self.choices:
            if not isinstance(k, self.item_type):
                raise ValueError(
                    "Key im choices dict should be of type {}, key {} is "
                    "not.".format(self.item_type, k)
                )
            if not isinstance(v, str):
                raise ValueError("Values in choices dict should be a string")

    def _base_validator(self, value):
        super()._base_validator(value)
        self._validate_required(value)
        if value is not None:
            if not value in self.choices.keys():
                raise SILFProtocolError(
                    self._validation_error(
                        "Invalid value submittd to ComboBox, Acceptable values are"
                        "'{}', while '{}' was submitted".format(
                            sorted(self.choices.keys()), value
                        ),
                        system_error=True,
                    )
                )

    @property
    def __json_choices(self):
        return {str(k): _(v) for k, v in self.choices.items()}

    def _python_to_json(self, value):
        return str(value)

    def _json_to_python(self, value):
        try:
            return self.item_type(value)
        except ValueError as e:
            raise SILFProtocolError(
                self._validation_error(
                    "Can't convert value submitted to ComboBox {s.name}".format(s=self),
                    system_error=True,
                )
            ) from e

    def to_json_dict(self):
        dict = super().to_json_dict()
        dict["metadata"]["choices"] = self.__json_choices
        return dict
