# coding=utf-8

import itertools

from copy import deepcopy
from collections import defaultdict
from operator import attrgetter
from silf.backend.commons.api.exceptions import (
    SILFProtocolError,
    ValidationError,
    MissingRequiredControlException,
    SettingNonLiveControlException,
)
from silf.backend.commons.api.stanza_content._misc import SettingSuite
from silf.backend.commons.api.stanza_content.control._controls import NumberControl
from silf.backend.commons.api.stanza_content._misc import InputFieldSuite, Setting

from silf.backend.commons.util.uniqueify import uniqueify


__all__ = ["ControlSuite"]


class ControlSuite(object):

    controls = []

    @classmethod
    def join(cls, *args):
        """

        :param args:
        :return:
        """
        return ControlSuite(
            *uniqueify(itertools.chain(*[o.controls for o in args]), attrgetter("name"))
        )

    def __getitem__(self, item):
        for c in self.controls:
            if c.name == item:
                return c
        return KeyError(item)

    def __init__(self, *args):
        """
        :param *args: Controols to add to this insatance
        """
        self.controls = []
        self.controls.extend(args)
        self.control_map = {}
        for order, c in enumerate(self.controls):
            self.control_map[c.name] = c
            if c.order is None:
                c.order = order

    def to_input_field_suite(self):
        """
        Converts this instance to :class:`misc.InputFieldSuite`.
        """
        return InputFieldSuite(self.controls)

    def check_settings(self, setting_suite, ignore_unset_settings=True) -> dict:
        """
        Checks settings received from json and converts them to python domain.

        >>> c = ControlSuite(
        ...   NumberControl("foo", "Insert foo", max_value=10),
        ...   NumberControl("bar", "Insert bar")
        ... )

        >>> from silf.backend.commons.api import SettingSuite, Setting

        >>> i = SettingSuite(
        ...    foo = Setting(1, False),
        ...    bar = Setting(2, False)
        ... )

        >>> c.check_settings(i)

        >>> i = SettingSuite(
        ...    foo = Setting(11, True),
        ...    bar = Setting(2, False)
        ... )
        >>> c.check_settings(i)
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.ValidationError: Wartość w polu "Insert foo" powinna być mniejsza niż 10 a wynosi 11.0.

        >>> i = SettingSuite(
        ...    foo = Setting(9, True),
        ... )
        >>> c.check_settings(i)

        :param SettingSuite setting_suite: Settings read from json.
        :param bool ignore_current_parameter: Whether this is called before applying
            settings (``check_only``=False) or only to check them and display errors to user.
            If ``check_only`` is true this method might
            omit specific validations for fields that are not currently edited.

        :raises ValidationError: If there is a validation error.

        :return: None
        """

        self.__check_and_convert_settings(setting_suite, ignore_unset_settings)

    def check_and_convert_live_settings(
        self, setting_suite: SettingSuite, old_settings: dict
    ) -> dict:

        """
         Checks settings and returns parsed values as a dictionaty.

        >>> from silf.backend.commons.api import SettingSuite, Setting

        >>> c = ControlSuite(
        ...   NumberControl("foo", "Insert foo", max_value=10, live=True),
        ...   NumberControl("bar", "Insert bar", required=False)
        ... )

        >>> old_settings = {'foo': 1.0, "bar":2.0}

        >>> converted = c.check_and_convert_live_settings(SettingSuite(foo=Setting(5, False)), old_settings)

        >>> converted == {'foo': 5.0, 'bar': 2.0}
        True

        >>> c.check_and_convert_live_settings(SettingSuite(bar=Setting(1, False)), old_settings)
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.ValidationError: Pole Insert bar zostało ustawione przez settings:update, ale nie zostało oznaczone jako live.

        :param setting_suite:
        :param old_settings:
        :return:
        """

        errors = []

        for name, setting in setting_suite.internal_dict.items():
            c = self.control_map[name]
            if not c.live:
                errors.append(
                    SettingNonLiveControlException.from_args(
                        "error",
                        "user",
                        "Pole {c.label} zostało ustawione przez settings:update, ale nie "
                        "zostało oznaczone jako live.".format(c=c),
                    )
                )

        merged_settings = SettingSuite()
        merged_settings.internal_dict.update(
            {k: Setting(v) for (k, v) in old_settings.items()}
        )
        merged_settings.internal_dict.update(setting_suite.internal_dict)

        return self.__check_and_convert_settings(
            merged_settings, ignore_unset_settings=False, errors=errors
        )

    def check_and_convert_settings(self, setting_suite):
        """
        Checks settings and returns parsed values as a dictionaty.

        >>> from silf.backend.commons.api import SettingSuite, Setting

        >>> c = ControlSuite(
        ...   NumberControl("foo", "Insert foo", max_value=10),
        ...   NumberControl("bar", "Insert bar", required=False)
        ... )

        >>> i = SettingSuite(
        ...    foo = Setting(1, False),
        ...    bar = Setting(2, False)
        ... )

        >>> c.check_and_convert_settings(i) == {'bar': 2, 'foo': 1}
        True

        >>> i = SettingSuite(
        ...    foo = Setting(11, False),
        ...    bar = Setting(2, False)
        ... )
        >>> c.check_and_convert_settings(i)
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.ValidationError: Wartość w polu "Insert foo" powinna być mniejsza niż 10 a wynosi 11.0.

        >>> i = SettingSuite(
        ...    bar = Setting(9, True)
        ... )
        >>> c.check_settings(i)

        """
        return self.__check_and_convert_settings(
            setting_suite, ignore_unset_settings=True
        )

    def __check_and_convert_settings(
        self, setting_suite, ignore_unset_settings=False, errors=None
    ) -> dict:

        NO_VALUE = Setting(value=None)

        if errors is None:
            errors = []

        cleared_settings = {}

        for control in self.controls:
            name = control.name
            setting = setting_suite.get(name, NO_VALUE)
            try:
                cleared_settings[name] = control.convert_json_to_python(setting.value)
            except MissingRequiredControlException as e:
                if ignore_unset_settings and setting is NO_VALUE:
                    continue
                errors.append(e)
            except ValidationError as e:
                errors.append(e)

        if errors:
            raise ValidationError.join(*errors)

        return cleared_settings

    def __getstate__(self):
        return self.to_input_field_suite().__getstate__()

    def __setstate__(self, state):
        raise ValueError()

    def __copy__(self):
        return ControlSuite(*self.controls)

    def __deepcopy__(self, memo):
        return ControlSuite(*deepcopy(self.controls, memo))

    def __repr__(self):

        return """
            <ControlSuite {}>
        """.format(
            " ".join([repr(c) for c in sorted(self.controls, key=attrgetter("name"))])
        ).strip()
