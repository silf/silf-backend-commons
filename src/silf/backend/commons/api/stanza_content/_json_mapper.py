"""

Does serialization to json.


**Serialization example**

>>> from silf.backend.commons.api import *
>>> import json
>>> selection = ModeSelection(mode = "foo")
>>> json.dumps(selection.__getstate__())
'{"mode": "foo"}'

**Deserialization example**

>>> ModeSelection.from_dict(json.loads('{"mode": "foo"}'))
<ModeSelection mode=foo>
"""

from copy import deepcopy
import json
from operator import itemgetter

import collections

from silf.backend.commons.api.exceptions import SerializationException

__all__ = [
    "SerializationException",
    "FromStateMixin",
    "JsonMapped",
    "FlatDictJsonMapped",
]


class FromStateMixin(object):
    @classmethod
    def from_dict(cls, state):
        """
        Creates instance of this class using provided state dictionatry
        :param state: state from which to create instance
        :return: Created instance
        :rtype:cls
        """
        # Create new instance without calling `__init__` method (yes it is MAGIC)
        item = object.__new__(cls)
        item.__setstate__(state)
        return item


class JsonMapped(FromStateMixin):

    """
    Json class that has well-known set of properties. Each property has known
    type (that can optionally be checked).

    This class should be subclassed like that:

    >>> class TmpTestJsonMapped(JsonMapped):
    ...
    ...   DICT_CONTENTS = {'foo'}
    ...
    ...   def __init__(self, foo):
    ...      super().__init__()
    ...      self.foo = foo
    >>> foo = TmpTestJsonMapped(foo="bar")
    >>> foo.__getstate__()
    {'foo': 'bar'}


    This class can automagically set atributes in __init__ method

    >>> class TmpTestJsonMapped(JsonMapped):
    ...
    ...   DICT_CONTENTS = {'foo'}
    ...
    ...   def __init__(self, **kwargs):
    ...      super().__init__(**kwargs)
    >>> foo = TmpTestJsonMapped(foo="bar")
    >>> foo.__getstate__()
    {'foo': 'bar'}
    >>> foo = TmpTestJsonMapped(bar="bar")
    Traceback (most recent call last):
    ValueError: Invalid __init__ parameter bar
    """

    DICT_CONTENTS = set()
    """
        Defines set of properties that need to be in state dicts (for both
        serialization and deserialization)
    """

    DEFAULTS = {}
    """
    Dictionaty containing default values for some of the properties.
    """

    def __init__(self, **kwargs):
        super().__init__()
        for k, v in deepcopy(self.DEFAULTS).items():
            setattr(self, k, v)
        for k, v in kwargs.items():
            if k not in self.DICT_CONTENTS:
                raise ValueError("Invalid __init__ parameter {}".format(k))
            setattr(self, k, v)

    @classmethod
    def _assert_field(cls, assertion, message):
        """
        Utility method, raises exception if assertion is false

        :param bool assertion: Whether to raise error
        :param str message: Error message

        :raises ValueError: If assertion is true.

        >>> TestJsonMapped._assert_field(True, "foo")
        >>> TestJsonMapped._assert_field(False, "bar") # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: bar
        """
        if not assertion:
            raise SerializationException(message)

    def _assert_instance(self, field_name, cls):
        """
        Checks whether `getattr(self, field_name)` returns instance of `cls`. If
        `field_name` is in `DEFAULTS` it allows it to be None.

        >>> foo = TestJsonMapped("bar")
        >>> foo._assert_instance("foo", str)
        >>> foo._assert_instance("foo", dict) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'foo' is not instance of '<class 'dict'>' (and is should be)

        :param field_name: Name of the
        :param cls: class
        :raises ValueError: if field is of inproper class
        """
        value = getattr(self, field_name)

        if value is None and field_name in self.DEFAULTS:
            return

        message = "Field '{}' is not instance of '{}' (and is '{}', type '{}')".format(
            field_name, cls, value, type(value)
        )

        self._assert_field(isinstance(value, cls), message)

    def _assert_is_not_none(self, field_name):
        self._assert_field(
            getattr(self, field_name, None) is not None,
            "Field '{}' is None, and it should not be.".format(field_name),
        )

    def _assert_is_bool(self, field_name):
        """

        :param field_name:
        :return:
        """
        self._assert_instance(field_name, bool)

    def _assert_in(self, field_name, collection):
        self._assert_is_not_none(field_name)
        self._assert_field(
            getattr(self, field_name) in collection,
            "Field {} is not in {} (and it should be)".format(field_name, collection),
        )

    def _validate_state(self):
        """
        Checks state of this instance after deserialiation.
        :raises ValueError: If this instace is in invalid state
        """

    def getstate(self):
        """
        Iternal function that should be overriden, it returns state dictionary,
        that will be transformed to json:

        >>> foo = TestJsonMapped(foo="bar")
        >>> foo.getstate() == {'foo': 'bar'}
        True

        Default implementation just filters the `__dict__` property, so any
        additional items are removed:

        >>> foo.bar = 'baz'
        >>> foo.getstate() == {'foo': 'bar'}
        True
        """
        return dict([(k, getattr(self, k)) for k in self.DICT_CONTENTS])

    def setstate(self, state):
        """
        Iternal function that should be overriden by subclasses.

        .. note::
            This function should not be called directly, rather use
            :meth:`__setstate__` that checks state dicitonary for invalid keys.

        Default implementation just does::

            for k, v in state.items():
                setattr(self, k, v)

        >>> foo = TestJsonMapped("foo")

        This function performs no checks whatsoever!

        >>> foo.setstate({'foo': 'bar', 'bar': 'baz'})
        >>> foo.bar
        'baz'

        """
        for k, v in state.items():
            setattr(self, k, v)

    def _check_dict_contents(self, dict, error_msg, cause=None):
        dict_keys = dict.keys()
        if not dict_keys == self.DICT_CONTENTS:
            se = SerializationException(
                error_msg.format(sorted(self.DICT_CONTENTS), sorted(dict_keys))
            )
            if cause:
                raise se from cause
            raise se

    def __getstate__(self):
        """
        Serializes this instance to dictionary.

        .. note::
            This method should not be overriden, override :meth:`getstate`
            instead, this function just checks for invalid keys in result of
            :meth:`getstate` and returns it.

        >>> foo = TestJsonMapped(foo = 'bar')
        >>> foo.__getstate__() == {'foo': 'bar'}
        True

        Rig `foo` so it's getstate returns bogus contents:

        >>> foo.__dict__ = {}
        >>> foo.__getstate__() # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: This object should serialize to dictionary containing following keys: ['foo'],
            however it contained following keys: []
        """
        self._validate_state()
        defaults = deepcopy(self.DEFAULTS)
        try:
            state = self.getstate()
        except Exception as e:
            self._check_dict_contents({}, self.__ERROR_MESAGE_GET, e)
        defaults.update(state)
        self._check_dict_contents(defaults, self.__ERROR_MESAGE_GET)
        return defaults

    def __setstate__(self, state):
        """
        Deserializes this instance from dictionary.

        .. note::
            This method should not be overriden, override :meth:`setstate`
            instead, this function just checks for invalid keys in passed
            dictionary. and then calls :meth:`setstate`

        >>> foo = TestJsonMapped(None)
        >>> foo.__setstate__({'foo': 'bar'})
        >>> foo.foo
        'bar'

        It raises errors if state dict has invalid content:

        >>> foo.__setstate__({}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: This object should deserialize from dictionary that contains
            following keys: ['foo'], however this dictionary contained: []
        >>> foo.__setstate__({'foo': 'bar', 'bar': 'baz'}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: This object should deserialize from dictionary that contains
            following keys: ['foo'], however this dictionary contained: ['bar', 'foo']

        Or if :meth:_validate_state raises an exception.

        >>> foo.__setstate__({'foo': 'barfoobar'})
        Traceback (most recent call last):
        ValueError: foo can contain only 'foo', 'bar' or 'baz'
        """
        try:
            defaults = deepcopy(self.DEFAULTS)
            defaults.update(state)
            self._check_dict_contents(defaults, self.__ERROR_MESSAGE_SET)
            self.setstate(defaults)
        except SerializationException:
            raise
        except Exception as e:
            raise SerializationException() from e
        self._validate_state()

    def __eq__(self, y):
        return all([getattr(self, k) == getattr(y, k) for k in self.DICT_CONTENTS])

    def __repr__(self):
        result = ["<" + type(self).__name__]
        for key in sorted(self.DICT_CONTENTS):
            result.append("{}={}".format(key, getattr(self, key)))
        result.append(">")
        return " ".join(result)

    # Private parts:

    __ERROR_MESAGE_GET = """
    This object should serialize to dictionary containing following keys: {},
    however it contained following keys: {}
    """.strip().replace(
        r"\s*\n\s*", " "
    )

    __ERROR_MESSAGE_SET = """
    This object should deserialize from dictionary that contains
    following keys: {}, however this dictionary contained: {}
    """.strip().replace(
        r"\s*\n\s*", " "
    )


class TestJsonMapped(JsonMapped):

    """
    .. note::
        Class used in doctests, move along.
    """

    DICT_CONTENTS = {"foo"}

    def __init__(self, foo):
        super().__init__()
        self.foo = foo

    def _validate_state(self):
        if hasattr(self, "foo"):
            if not self.foo in ("foo", "bar", "baz"):
                raise ValueError("foo can contain only 'foo', 'bar' or 'baz'")

    def __eq__(self, y):
        return self.foo == y.foo

    def __repr__(self):
        return "<Foo foo={self.foo}>".format(self=self)


class FlatDictJsonMapped(FromStateMixin):

    """
    Represents type that serializes to json object that can contain any property,
    but all properties are of the same type.

    >>> class TmpTestFlatDictJsonMapped(FlatDictJsonMapped):
    ...    INTERNAL_DICT_NAME = "foos"
    ...    INTERNAL_VALUE_CLASS = TestJsonMapped

    Init method can take optional keyword named as :attr:`INTERNAL_DICT_NAME`
    >>> flat = TmpTestFlatDictJsonMapped(
    ...    foo = TestJsonMapped(foo='bar'),
    ...    bar = TestJsonMapped(foo='baz'))
    >>> sorted(flat.foos.items()) # doctest: +ELLIPSIS
    [('bar', <Foo foo=baz>), ('foo', <Foo foo=bar>)]

    It implements get state:

    >>> flat.__getstate__() == {'foo': {'foo': 'bar'}, 'bar': {'foo': 'baz'}}
    True
    >>> flat.foos = {}
    >>> flat.foos
    {}

    And set state:

    >>> flat.__setstate__({'foo': {'foo': 'bar'}, 'bar': {'foo': 'baz'}})
    >>> sorted(flat.foos.items()) #doctest: +ELLIPSIS
    [('bar', <Foo foo=baz>), ('foo', <Foo foo=bar>)]
    """

    INTERNAL_DICT_NAME = None
    """
    Name of property that contains internal dictionary that will hold mapping.
    """

    INTERNAL_VALUE_CLASS = None
    """
    Class to which all properties will be deserialized
    """

    @classmethod
    def join(cls, *args):
        """
        Joins series of instances of this class into single instance, checking
        for eventual duplicate keys;

        >>> from silf.backend.commons.api import ResultSuite, Result

        >>> a = ResultSuite(foo=Result(value=[5]), foobar=Result(value=[1, 2, 3]))
        >>> b = ResultSuite(bar=Result(value=[10]), barbar=Result(value=[1, 2, 3]))

        >>> ResultSuite.join(a, b)
        <ResultSuite bar=<Result pragma=append value=[10] > barbar=<Result pragma=append value=[1, 2, 3] > foo=<Result pragma=append value=[5] > foobar=<Result pragma=append value=[1, 2, 3] > >

        >>> ResultSuite.join(a, b, a) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        ValueError: We are joining instances of <class 'silf.backend.commons.api.misc.ResultSuite'>, these instances should not contain     duplicate data, however they did. Duplicate keys were ['foo', 'foobar'].     Debug data is: [{"foo": {"pragma": "append", "value": [5]}, "foobar": {"pragma": "append", "value": [1, 2, 3]}}, {"bar": {"pragma": "append", "value": [10]}, "barbar": {"pragma": "append", "value": [1, 2, 3]}}, {"foo": {"pragma": "append", "value": [5]}, "foobar": {"pragma": "append", "value": [1, 2, 3]}}].
        """
        keys = []
        result = {}

        for item in args:
            d = item.internal_dict
            keys.extend(d.keys())
            result.update(d)

        cnter = collections.Counter(keys)

        duplicated_keys = []

        for k, v in cnter.items():
            if v > 1:
                duplicated_keys.append(k)

        if len(duplicated_keys) > 0:

            raise ValueError(
                cls.__DUPLICATE_KEYS_ERROR.format(
                    type=cls,
                    duplicates=duplicated_keys,
                    debug=json.dumps([a.__getstate__() for a in args]),
                )
            )

        return cls(**result)

    def __init__(self, **kwargs):
        """
        Constructor.

        :param kwargs: All kwargs will be passed to internal dict.

        """
        super().__init__()
        setattr(self, self.INTERNAL_DICT_NAME, kwargs)

    def __getstate__(self):
        return dict(
            [
                (k, self.serialize_internal_item(v))
                for k, v in self.internal_dict.items()
            ]
        )

    def __setstate__(self, state):
        internal_dict = dict(
            [(k, self.deserialize_internal_item(v)) for k, v in state.items()]
        )
        setattr(self, self.INTERNAL_DICT_NAME, internal_dict)

    def serialize_internal_item(self, item):
        """
        Serializes :attr:INTERNAL_VALUE_CLASS. by default it calls
        `item.__getstate__`.
        """
        return item.__getstate__()

    def deserialize_internal_item(self, item):
        """
        Deserializes :attr:INTERNAL_VALUE_CLASS. by default it calls
        `item.from_dict`.
        """
        return self.INTERNAL_VALUE_CLASS.from_dict(item)

    @property
    def internal_dict(self):
        return getattr(self, self.INTERNAL_DICT_NAME)

    def __getitem__(self, item):
        return self.internal_dict[item]

    def __setitem__(self, key, value):
        self.internal_dict[key] = value

    def __delitem__(self, key):
        del self.internal_dict[key]

    def __contains__(self, item):
        return item in self.internal_dict

    def __eq__(self, y):
        return self.internal_dict == y.internal_dict

    def get(self, *args, **kwargs):
        return self.internal_dict.get(*args, **kwargs)

    def __repr__(self):
        result = ["<" + type(self).__name__]
        for k, v in sorted(
            getattr(self, self.INTERNAL_DICT_NAME).items(), key=itemgetter(0)
        ):
            result.append("{}={}".format(k, v))
        result.append(">")
        return " ".join(result)

    __DUPLICATE_KEYS_ERROR = """
    We are joining instances of {type}, these instances should not contain
    duplicate data, however they did. Duplicate keys were {duplicates}.
    Debug data is: {debug}.
    """.replace(
        "\n", " "
    ).strip()


class TestFlatDictJsonMapped(FlatDictJsonMapped):

    """
    .. note::
        Test class, move along.

    """

    INTERNAL_DICT_NAME = "foos"

    INTERNAL_VALUE_CLASS = TestJsonMapped

    def __eq__(self, y):
        return self.foos == y.foos
