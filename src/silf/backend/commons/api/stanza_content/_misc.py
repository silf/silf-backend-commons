# coding=utf-8
from silf.backend.commons.api.exceptions import ConfigurationException

from silf.backend.commons.api.stanza_content._json_mapper import (
    JsonMapped,
    FlatDictJsonMapped,
)

from silf.backend.commons.api.const import *
from silf.backend.commons import version
from silf.backend.commons.version import ProtocolVersionUtil
from silf.backend.commons.api.translations.trans import _


__all__ = [
    "Setting",
    "SettingSuite",
    "InputFieldSuite",
    "OutputField",
    "ChartField",
    "OutputFieldSuite",
    "Mode",
    "ModeSuite",
    "ModeSelection",
    "ModeSelected",
    "SeriesStartedStanza",
    "Result",
    "ResultSuite",
    "ResultsStanza",
    "CheckStateStanza",
    "SeriesStopped",
    "VideoElement",
    "ProtocolVersions",
    "ProtocolVersion",
    "Language",
    "Languages",
]


class Setting(JsonMapped):
    """
    Single setting object. It serializes to following JSON:

    >>> setting = Setting(value=15.4, current=True)

    **Serialization format**

    >>> setting.__getstate__() == {'current': True, 'value': 15.4}
    True
    >>> setting.__setstate__({'current': True, 'value': 15.4})
    >>> setting.current
    True
    >>> setting.value
    15.4

    It can deserialize from dictionary without `current` item:
    >>> setting.__setstate__({'value': 15.4})
    >>> setting.current
    False
    >>> setting.value
    15.4

    Check equality:

    >>> setting2 = Setting.from_dict(setting.__getstate__())
    >>> setting2 == setting
    True

    Setting incorrect type for current raises an exception
    >>> setting = Setting(value=15.4, current="dupa")
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.SerializationException: Current property must be either True or False
    >>> setting.__setstate__({"value" :5, 'current':"dupa"})
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.SerializationException: Current property must be either True or False
    """

    DICT_CONTENTS = {"value", "current"}

    DEFAULTS = {"current": False}

    def __init__(self, value=None, current=False):
        super().__init__()
        self.value = value
        self.current = current
        self._validate_state()

    def _validate_state(self):
        self._assert_field(
            isinstance(self.current, bool),
            "Current property must be either True or False",
        )


class SettingSuite(FlatDictJsonMapped):

    """
    Contains a set of settings.

    Serializes to following JSON:

    >>> settings_set = SettingSuite(
    ...    foo = Setting(value = 13),
    ...    bar = Setting(value= "Kotek!", current=True)
    ... )

    <SettingSuite bar=<Setting current=True value=Kotek! > foo=<Setting current=False value=13 > >

      **Serialization format**

    >>> settings_set.__getstate__() == {'foo': {'current': False, 'value': 13}, 'bar': {'current': True, 'value': 'Kotek!'}}
    True

    Test equality:

    >>> setting_set2 = SettingSuite.from_dict(settings_set.__getstate__())
    >>> setting_set2 == settings_set
    True
    """

    settings = {}

    INTERNAL_DICT_NAME = "settings"

    INTERNAL_VALUE_CLASS = Setting


class InputFieldSuite(FlatDictJsonMapped):

    """
    Contains a set of controls.

    >>> from silf.backend.commons.api import *

    >>> control_set = InputFieldSuite(
    ...     controls = [NumberControl("foo", "Insert foo"),
    ...                 NumberControl("bar", "Insert bar")]
    ... )
    >>> control_set.__getstate__() == {
    ...     'foo': {'validations': {}, 'live': False, 'type': 'number', 'name': 'foo', 'metadata': {'label': 'Insert foo'}, "order": 0},
    ...     'bar': {'live': False, 'type': 'number', 'name': 'bar', 'metadata': {'label': 'Insert bar'}, 'validations': {}, "order": 1}
    ... }
    True

    Test equality:

    >>> InputFieldSuite.from_dict(control_set.__getstate__()) == control_set
    True

    >>> control_set.__setstate__({"foo" : 15})
    Traceback (most recent call last):
    ValueError: Can deserialize only dict, got 15 (type: <class 'int'>)
    """

    controls = {}

    INTERNAL_DICT_NAME = "controls"

    INTERNAL_VALUE_CLASS = None

    def __init__(self, controls=tuple(), **kwargs):
        super(InputFieldSuite, self).__init__(**kwargs)
        for order, c in enumerate(controls):
            if c.order is None:
                c.order = order
        for c in controls:
            self.controls[c.name] = c.to_json_dict()

    def serialize_internal_item(self, item):
        return item

    def deserialize_internal_item(self, item):
        if not isinstance(item, dict):
            raise ValueError(
                "Can deserialize only dict, got {} (type: {})".format(item, type(item))
            )
        return item


class OutputField(JsonMapped):

    """
    Represents a Result field.

    Default values are applied when constructing, and while using __setstate__

    >>> result = OutputField("chart", "betaArray")
    >>> result
    <Result class=chart name=['betaArray'] type=array metadata={} settings={}>

    >>> result.__setstate__({"class": "chart", "name": ["betaArray"]})
    >>> result
    <Result class=chart name=['betaArray'] type=array metadata={} settings={}>
    >>> result.settings
    {}

    Some metadata properties can ve accessed as properties:

    >>> result.label = "foo"
    >>> result
    <Result class=chart name=['betaArray'] type=array metadata={'label': 'foo'} settings={}>

    Json property named `class` is avilable as `html_class` property

    >>> result.html_class = "integer-indicator"

    **Serialization format**

    >>> result.__getstate__() == {'metadata': {'label': 'foo'}, 'class': 'integer-indicator', 'settings': {}, 'type': 'array', 'name': ['betaArray']}
    True
    >>> result.html_class
    'integer-indicator'

    Check equality:

    >>> OutputField.from_dict(result.__getstate__()) == result
    True

    >>> result = OutputField("chart", "betaArray")
    >>> result.__getstate__() == {'class': 'chart', 'name': ['betaArray'], 'metadata': {}, 'type': 'array', 'settings': {}}
    True
    >>> result.setstate( {'class': 'chart', 'name': ['gammaArray'], 'metadata': {}, 'type': 'array', 'settings': {}})
    >>> result
    <Result class=chart name=['gammaArray'] type=array metadata={} settings={}>

    """

    DICT_CONTENTS = {"class", "name", "metadata", "settings", "type"}

    DEFAULTS = {"type": OUTPUT_TYPES[FLOAT_ARRAY], "metadata": {}, "settings": {}}

    def __init__(self, html_class, name, label=None, **kwargs):
        super().__init__(**kwargs)
        self.html_class = html_class
        if isinstance(name, str):
            name = [name]
        self.name = name
        if label is not None:
            self.label = label
        self._validate_state()

    @property
    def html_class(self):
        return getattr(self, "class")

    @html_class.setter
    def html_class(self, val):
        setattr(self, "class", val)

    @html_class.deleter
    def html_class(self):
        delattr(self, "class")

    @property
    def label(self):
        return self.metadata[OUTPUT_FIELD_LABEL]

    @label.setter
    def label(self, value):
        self.metadata[OUTPUT_FIELD_LABEL] = value

    def _validate_state(self):
        """
        >>> result = OutputField("chart", "betaArray")
        >>> result.__setstate__({"class": "chart", "name": "betaArray", "append": 16}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException:
        >>> result = OutputField("chart", "betaArray")
        >>> result.__setstate__({"class": "chart", "name": "betaArray", "metadata": 16})  # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'metadata' is not instance of '<class 'dict'>' (and is should be)
        >>> result = OutputField("chart", "betaArray")
        >>> result.__setstate__({"class": "chart", "name": "betaArray", "settings": 16}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException:
        >>> result = OutputField("chart", "betaArray")
        >>> result.__setstate__({"class": "chart", "name": "betaArray", "type": 16}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException:
        >>> result = OutputField("chart", "betaArray")
        >>> result.__setstate__({"name": "betaArray", "class": 16}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException:
        >>> result = OutputField("chart", "name")
        >>> result.__setstate__({"class": "chart", "name": "betaArray", "append": 16}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException:
        :return:
        """
        self._assert_instance("metadata", dict)
        self._assert_instance("settings", dict)
        self._assert_instance("type", str)
        self._assert_instance("class", str)
        self._assert_instance("name", list)
        self._assert_field(
            self.type in OUTPUT_TYPES,
            "Output Field type must be in {}, current value is {}".format(
                OUTPUT_TYPES, self.type
            ),
        )

    def getstate(self):
        """
        Override parent implementation to dynamically translate labels inside of metadata dict
        :return:
        """
        state = super().getstate()
        self._translate_metadata_field(state["metadata"], OUTPUT_FIELD_LABEL)
        return state

    @staticmethod
    def _translate_metadata_field(metadata, field):
        if field in metadata:
            metadata[field] = _(metadata[field])

    def __str__(self):
        return (
            "<Result class={s.class} name={s.name} "
            "type={s.type} metadata={s.metadata} settings={s.settings}>".format(s=self)
        )

    __repr__ = __str__


AXIS_X_LABEL = "chart.axis.x.label"
AXIS_Y_LABEL = "chart.axis.y.label"


class ChartField(OutputField):
    def __init__(
        self,
        html_class,
        name,
        label=None,
        axis_x_label=None,
        axis_y_label=None,
        **kwargs
    ):
        super().__init__(html_class, name, label, **kwargs)
        self.axis_x_label = axis_x_label
        self.axis_y_label = axis_y_label

    @property
    def axis_x_label(self):
        return self.metadata[AXIS_X_LABEL]

    @axis_x_label.setter
    def axis_x_label(self, value):
        self.metadata[AXIS_X_LABEL] = value

    @property
    def axis_y_label(self):
        return self.metadata[AXIS_Y_LABEL]

    @axis_y_label.setter
    def axis_y_label(self, value):
        self.metadata[AXIS_Y_LABEL] = value

    def getstate(self):
        """
        Override parent implementation to dynamically translate labels inside of metadata dict
        :return:
        """
        state = super().getstate()
        self._translate_metadata_field(state["metadata"], AXIS_X_LABEL)
        self._translate_metadata_field(state["metadata"], AXIS_Y_LABEL)
        return state


class VideoElement(OutputField):

    """
    >>> video = VideoElement("image", "http://someserver/path")
    >>> video.__getstate__() == {'name': [], 'type': 'array', 'metadata': {}, 'settings': {'camera_type': 'image', 'camera_url': 'http://someserver/path'}, 'class': 'camera'}
    True
    """

    def __init__(self, video_type=None, camera_url=None, label=None, **kwargs):
        super().__init__("camera", [], label, **kwargs)
        self.camera_type = video_type
        self.camera_url = camera_url

    @property
    def camera_type(self):
        return self.settings.get("camera_type")

    @camera_type.setter
    def camera_type(self, val):
        if val is None:
            del self.camera_type
            return
        self.settings["camera_type"] = val

    @camera_type.deleter
    def camera_type(self):
        self.settings.pop("camera_type", None)

    @property
    def camera_url(self):
        return self.settings.get("camera_url")

    @camera_url.setter
    def camera_url(self, val):
        if val is None:
            del self.camera_url
            return
        self.settings["camera_url"] = val

    @camera_url.deleter
    def camera_url(self):
        self.settings.pop("camera_url", None)


class OutputFieldSuite(FlatDictJsonMapped):

    """
    Holds suite of output results:

    >>> suite = OutputFieldSuite(foo = OutputField("foo", ["foo"]))
    >>> suite.output_fields
    {'foo': <Result class=foo name=['foo'] type=array metadata={} settings={}>}
    >>> suite.__getstate__() ==  {'foo': {'class': 'foo', 'metadata': {}, 'name': ['foo'], 'settings': {}, 'type': 'array'}}
    True

    Check equality:

    >>> suite == OutputFieldSuite.from_dict(suite.__getstate__())
    True

    >>> suite.__setstate__({'bar': {'class': 'foo', 'metadata': {}, 'name': ['bar'], 'settings': {}, 'type': 'array'}})
    >>> suite.output_fields
    {'bar': <Result class=foo name=['bar'] type=array metadata={} settings={}>}

    **TEST ERRORS**

    >>> suite = OutputFieldSuite(foo = OutputField("foo", ["foo", "baz"]))
    Traceback (most recent call last):
    silf.backend.commons.api.exceptions.ConfigurationException: Following errors encountered: ["Error --- control foo takes input from more than one result (that is: ['foo', 'baz']) this is disallowed"]
    """

    output_fields = None

    INTERNAL_DICT_NAME = "output_fields"

    INTERNAL_VALUE_CLASS = OutputField

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__check_internal_dict()

    def __setstate__(self, state):
        super().__setstate__(state)
        self.__check_internal_dict()

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self.__check_internal_dict()

    def __check_internal_dict(self):
        errors = []
        for k, item in self.internal_dict.items():
            names = item.name
            if len(names) != 1:
                errors.append(
                    "Error --- control {} takes input from more than one result "
                    "(that is: {}) this is disallowed".format(k, names)
                )
                continue
            if names[0] != k:
                errors.append(
                    "Error --- control {} takes input from result field named {}"
                    " this is not allowed (see protocol description in docs to "
                    " see why)".format(k, names[0])
                )
        if errors:
            raise ConfigurationException(
                "Following errors encountered: {}".format(errors)
            )


class Languages(JsonMapped):
    """
    Represents stanza with list of possible languages used by user
    """

    DICT_CONTENTS = {"langs"}

    def __init__(self, langs):
        super().__init__()
        self.langs = langs

    def _validate_state(self):
        self._assert_is_not_none("langs")
        self._assert_instance("langs", list)


class Language(JsonMapped):
    """
    Represents single language chosen by experiment in which user data is translated
    """

    DICT_CONTENTS = {"lang"}

    def __init__(self, lang):
        super().__init__()
        self.lang = lang

    def _validate_state(self):
        self._assert_is_not_none("lang")
        self._assert_instance("lang", str)


class Mode(JsonMapped):

    """

    Encapsulates single mode.

    >>> mode = Mode("foo", description="Foo mode", order=1)
    >>> mode.__getstate__() == {'description': 'Foo mode', 'label': 'foo', 'order': 1}
    True
    >>> mode.setstate({'description': 'Bar mode', 'label': 'bar'})
    >>> mode
    <Mode 'bar' description='Bar mode'>


    Check Equality:

    >>> mode == Mode.from_dict(mode.__getstate__())
    True
    """

    DICT_CONTENTS = {"label", "description", "order"}

    DEFAULTS = {"description": None, "order": None}

    def __init__(self, label, description=None, order=None, **kwargs):
        super().__init__()
        self.label = label
        self.description = description
        self.order = order
        self._validate_state()

    def getstate(self):
        state = super().getstate()
        OutputField._translate_metadata_field(state, "label")
        OutputField._translate_metadata_field(state, "description")
        return state

    def _validate_state(self):
        self._assert_is_not_none("label")
        self._assert_instance("label", str)

    def __str__(self):
        return "<Mode '{s.label}' description='{s.description}'>".format(s=self)

    __repr__ = __str__


class ModeSuite(FlatDictJsonMapped):
    """
    Encapsulates suite of modes.

    >>> suite = ModeSuite(foo= Mode("Foo Mode", "descr"), bar = Mode("Bar Mode", "descr"))
    >>> suite.__getstate__() == {
    ...     'foo': {'label': 'Foo Mode', 'description': 'descr', 'order': None},
    ...     'bar': {'label': 'Bar Mode', 'description': 'descr', 'order': None}
    ... }
    True
    >>> suite.__setstate__({'bar': {'label': 'Bar Mode', 'description': 'descr'}})
    >>> suite.modes
    {'bar': <Mode 'Bar Mode' description='descr'>}

    Check equality:

    >>> suite == ModeSuite.from_dict(suite.__getstate__())
    True

    Nice example:

    >>> suite = ModeSuite(time = Mode("Pomiar czasu", "Mierz zliczenia, i kończ pomiar po upływie ustalonego czasu.", order=0), counts = Mode("Pomiar zliczeń", "Mierz czas potrzebny do uzyskania ustalonej liczby zliczeń", order=1))
    >>> suite.__getstate__() == {
    ...       'counts': {
    ...               'description': 'Mierz czas potrzebny do uzyskania ustalonej liczby zliczeń',
    ...               'label': 'Pomiar zliczeń',
    ...               'order' : 1
    ...       },
    ...       'time': {
    ...               'description': 'Mierz zliczenia, i kończ pomiar po upływie ustalonego czasu.',
    ...               'label': 'Pomiar czasu',
    ...               'order': 0
    ...             }
    ...       }
    True
    """

    INTERNAL_DICT_NAME = "modes"
    INTERNAL_VALUE_CLASS = Mode

    modes = {}


class ModeSelection(JsonMapped):

    """

    Encapsulates mode selection:

    >>> mode = ModeSelection(mode = "foo")
    >>> mode.__getstate__()
    {'mode': 'foo'}
    >>> mode.setstate( {'mode': 'bar'})
    >>> mode.mode
    'bar'

    Check Equality:

    >>> mode == ModeSelection.from_dict(mode.__getstate__())
    True

    """

    DICT_CONTENTS = {"mode"}

    def __repr__(self):
        return "<ModeSelection mode={s.mode}>".format(s=self)


class ModeSelected(JsonMapped):

    """
    Response after user selects mode:


    >>> from silf.backend.commons.api import ControlSuite, NumberControl
    >>> from silf.backend.commons.api.stanza_content._misc import *

    >>> c = ControlSuite(
    ...   NumberControl("foo", "Insert foo", max_value=10),
    ...   NumberControl("bar", "Insert bar")
    ... )

    >>> resp = ModeSelected("foo", 'dfae1382-1746-4b5e-851e-93dff62b01ba',
    ...  settings=c.to_input_field_suite(),
    ...  resultDescription=OutputFieldSuite(betaArray = OutputField("array", name=["betaArray"]))
    ... )

    >>> resp.resultDescription
    <OutputFieldSuite betaArray=<Result class=array name=['betaArray'] type=array metadata={} settings={}> >

    >>> resp.__getstate__() == {
    ...     'settings': {
    ...         'foo': {'validations': {'max_value': 10}, 'live': False, 'metadata': {'label': 'Insert foo'}, 'name': 'foo', 'type': 'number', "order": 0},
    ...         'bar': {'live': False, 'metadata': {'label': 'Insert bar'}, 'name': 'bar', 'type': 'number', 'validations': {}, "order": 1}
    ...          },
    ...      'experimentId': 'dfae1382-1746-4b5e-851e-93dff62b01ba',
    ...      'mode': 'foo',
    ...      'resultDescription': {
    ...         'betaArray': {'metadata': {}, 'class': 'array', 'settings': {}, 'name': ['betaArray'], 'type': 'array'}}}
    True

    Check Equality:

    >>> resp == ModeSelected.from_dict(resp.__getstate__())
    True

    >>> resp = ModeSelected("foo", 'dfae1382-1746-4b5e-851e-93dff62b01ba',
    ...  settings=c,
    ...  resultDescription=OutputFieldSuite(betaArray = OutputField("array", name=["betaArray"]))
    ... )


    """

    DICT_CONTENTS = {"mode", "experimentId", "settings", "resultDescription"}

    def __init__(
        self, mode, experimentId=None, settings=None, resultDescription=None, **kwargs
    ):
        super().__init__(**kwargs)

        self.mode = mode
        self.experimentId = experimentId

        if settings is None:
            settings = InputFieldSuite()

        if resultDescription is None:
            resultDescription = OutputFieldSuite()

        from silf.backend.commons.api.stanza_content import ControlSuite

        if isinstance(settings, ControlSuite):
            settings = settings.to_input_field_suite()

        self.settings = settings
        self.resultDescription = resultDescription

    def _validate_state(self):
        self._assert_is_not_none("mode")
        self._assert_instance("mode", str)
        self._assert_is_not_none("experimentId")
        self._assert_instance("experimentId", str)
        self._assert_is_not_none("settings")
        self._assert_instance("settings", InputFieldSuite)
        self._assert_is_not_none("resultDescription")
        self._assert_instance("resultDescription", OutputFieldSuite)

    def getstate(self):
        return {
            "mode": self.mode,
            "experimentId": self.experimentId,
            "settings": self.settings.__getstate__(),
            "resultDescription": self.resultDescription.__getstate__(),
        }

    def setstate(self, state):
        self.mode = state["mode"]
        self.experimentId = state["experimentId"]
        self.settings = InputFieldSuite.from_dict(state["settings"])
        self.resultDescription = OutputFieldSuite.from_dict(state["resultDescription"])


class SeriesStartedStanza(JsonMapped):

    """

    Message sent when we start the single measurement session..

    >>> settings_set = SettingSuite(
    ...    foo = Setting(value = 13),
    ...    bar = Setting(value= "Kotek!", current=True)
    ... )
    >>> series_started = SeriesStartedStanza(seriesId = "fac96014-e88f-4bab-af2c-32fd0cf14d20", initialSettings=settings_set, label="Seria z foo=12")
    >>> series_started
    <SeriesStartedStanza initialSettings=<SettingSuite bar=<Setting current=True value=Kotek! > foo=<Setting current=False value=13 > > metadata={'label': 'Seria z foo=12'} seriesId=fac96014-e88f-4bab-af2c-32fd0cf14d20 >
    >>> series_started.__getstate__() == {'initialSettings': {'foo': {'current': False, 'value': 13}, 'bar': {'current': True, 'value': 'Kotek!'}}, 'metadata': {'label': 'Seria z foo=12'}, 'seriesId': 'fac96014-e88f-4bab-af2c-32fd0cf14d20'}
    True

    Test equality:

    >>> series_started == SeriesStartedStanza.from_dict(series_started.__getstate__())
    True

    """

    DICT_CONTENTS = {"seriesId", "initialSettings", "metadata"}

    DEFAULTS = {"metadata": {}}

    seriesId = ""

    initialSettings = SettingSuite()

    def __init__(self, **kwargs):
        label = kwargs.pop("label", None)
        super().__init__(**kwargs)
        self.label = label

    def _validate_state(self):
        super()._validate_state()
        self._assert_instance("seriesId", str)
        self._assert_instance("initialSettings", SettingSuite)
        self._assert_instance("metadata", dict)

    def getstate(self):
        return {
            "seriesId": self.seriesId,
            "initialSettings": self.initialSettings.__getstate__(),
            "metadata": self.metadata,
        }

    def setstate(self, state):
        self.seriesId = state["seriesId"]
        self.initialSettings = SettingSuite.from_dict(state["initialSettings"])
        self.metadata = state["metadata"]

    @property
    def label(self):
        return self.metadata.get("label")

    @label.setter
    def label(self, val):
        if val is None:
            del self.label
            return
        self.metadata["label"] = val

    @label.deleter
    def label(self):
        self.metadata.pop("label", None)


class Result(JsonMapped):

    """

    Single result type.

    >>> res = Result(value = [1, 2, 3, 4], pragma = "transient")
    >>> res.__getstate__() == {'pragma': 'transient', 'value': [1, 2, 3, 4]}
    True
    >>> res.__setstate__({"value" : "foobar"})
    >>> res
    <Result pragma=append value=foobar >
    >>> res.__getstate__() ==  {'pragma': 'append', 'value': 'foobar'}
    True
    >>> res = Result(value = [[1, 1], [2, 2]], pragma="transient")
    >>> res.__getstate__() == {'pragma': 'transient', 'value': [[1, 1], [2, 2]]}
    True
    """

    DICT_CONTENTS = {"value", "pragma"}

    DEFAULT_PRAGMA = PRAGMAS[PRAGMA_APPEND]

    DEFAULTS = {"pragma": DEFAULT_PRAGMA}

    def _validate_state(self):
        """
        >>> res = Result(value = [1, 2, 3, 4], pragma = "transient")
        >>> res.__setstate__({'value' : [1, 2, 3, 4], "pragma" :  12})  # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'pragma' is not instance of '<class 'str'>' (and is should be)
        >>> res.__setstate__({'value' : [1, 2, 3, 4], "pragma" :  "foo"}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Pragma must be in ('transient', 'append', 'replace'), currently it is 'foo'.
        >>> res.__setstate__({'value' : {"foo" : "bar"}, "pragma" :  "transient"}) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'value' is not instance of '(<class 'str'>, <class 'bool'>, <class 'list'>)' (and is should be)
        """
        super()._validate_state()
        self._assert_instance("pragma", str)
        self._assert_field(
            self.pragma in PRAGMAS, self.__PRAGMA_ERROR_MSG.format(self.pragma)
        )
        self._assert_instance("value", (str, bool, list, type(None), int, float))

    __PRAGMA_ERROR_MSG = """
    Pragma must be in {}, currently it is '{{}}'.
    """.format(
        PRAGMAS
    ).strip()


class ResultSuite(FlatDictJsonMapped):

    """

   Set of results.

    >>> suite = ResultSuite(foo = Result(value = [1, 2, 3, 4]), bar = Result(value = True))
    >>> suite.__getstate__() == {'foo': {'value': [1, 2, 3, 4], 'pragma': 'append'}, 'bar': {'value': True, 'pragma': 'append'}}
    True
    """

    INTERNAL_DICT_NAME = "results"
    INTERNAL_VALUE_CLASS = Result


class ResultsStanza(JsonMapped):

    """

    Message containing a set of results sent to user.

    >>> results_stanza = ResultsStanza(seriesId = "96beeb7e-3209-400a-892a-bf3201aa4658", results = ResultSuite(foo = Result(value = [1, 2, 3, 4]), bar = Result(value = True)))
    >>> results_stanza.__getstate__() == {'seriesId': '96beeb7e-3209-400a-892a-bf3201aa4658', 'results': {'foo': {'value': [1, 2, 3, 4], 'pragma': 'append'}, 'bar': {'value': True, 'pragma': 'append'}}}
    True
    >>> results_stanza2 = ResultsStanza.from_dict({'seriesId': '96beeb7e-3209-400a-892a-bf3201aa4658', 'results': {'foo': {'value': [1, 2, 3, 4], 'pragma': 'append'}, 'bar': {'value': True, 'pragma': 'append'}}})
    >>> results_stanza == results_stanza2
    True
    """

    DICT_CONTENTS = {"results", "seriesId"}

    def _validate_state(self):
        """
        >>> results_stanza = ResultsStanza(seriesId = 13, results = ResultSuite(foo = Result(value = [1, 2, 3, 4]), bar = Result(value = True)))
        >>> results_stanza.__getstate__() # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'seriesId' is not instance of '<class 'str'>' (and is should be)
        >>> results_stanza = ResultsStanza(seriesId = 'foo', results = {'foo' :Result(value = [1, 2, 3, 4]), 'bar': Result(value = True)})
        >>> results_stanza.__getstate__() # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        silf.backend.commons.api.exceptions.SerializationException: Field 'results' is not instance of '<class 'silf.backend.commons.api.misc.ResultSuite'>' (and is should be)
        """
        super()._validate_state()
        self._assert_instance("seriesId", str)
        self._assert_instance("results", ResultSuite)

    def getstate(self):
        return {"seriesId": self.seriesId, "results": self.results.__getstate__()}

    def setstate(self, state):
        self.seriesId = state["seriesId"]
        self.results = ResultSuite.from_dict(state["results"])


class SeriesStopped(JsonMapped):

    """
    Sent when someone stops series

    >>> state = SeriesStopped(seriesId = "f5d1762e-f528-4ca1-bee4-97512c22ae6a")
    >>> state.__getstate__() == {'seriesId': 'f5d1762e-f528-4ca1-bee4-97512c22ae6a'}
    True
    """

    DICT_CONTENTS = {"seriesId"}

    DEFAULTS = {"seriesId": None}


class CheckStateStanza(JsonMapped):

    """
    Response for checking of experiment state.

    >>> state = CheckStateStanza(state = "off")
    >>> state.__getstate__() == {'seriesId': None, 'state': 'off', 'experimentId': None}
    True
    >>> state = CheckStateStanza(state = "off", experimentId = 'f5d1762e-f528-4ca1-bee4-97512c22ae6a', seriesId = 'eeb23307-69dc-4745-83da-db22f3dfa557')
    >>> state.__getstate__() == {'seriesId': 'eeb23307-69dc-4745-83da-db22f3dfa557', 'state': 'off', 'experimentId': 'f5d1762e-f528-4ca1-bee4-97512c22ae6a'}
    True

    Check equality:

    >>> state == CheckStateStanza.from_dict(state.__getstate__())
    True

    """

    DICT_CONTENTS = {"state", "experimentId", "seriesId"}

    DEFAULTS = {"experimentId": None, "seriesId": None}

    def _validate_state(self):
        super()._validate_state()
        self._assert_in("state", EXPERIMENT_STATE)


class ProtocolVersions(JsonMapped):

    DICT_CONTENTS = {"versions"}

    DEFAULTS = {"versions": None}

    def __init__(self, version_list):
        self.versions = version_list


class ProtocolVersion(JsonMapped):

    DICT_CONTENTS = {"version"}

    DEFAULTS = {"version": None}

    def _validate_state(self):
        self._assert_is_not_none("version")
        self._assert_field(
            ProtocolVersionUtil.validate_protocol_version(self.version),
            "Unexpected protocol version {} - dose not match allowed schema".format(
                self.version
            ),
        )
