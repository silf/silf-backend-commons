import gettext

# use NullTranslations to have any translation running in the beginning
TRANSLATION = gettext.NullTranslations()


def set_translation(translation):
    """
    New translation to be set
    :param translation: Current translation to be set
    :return:
    """
    global TRANSLATION
    TRANSLATION = translation


def _(message):
    return TRANSLATION.gettext(message)
