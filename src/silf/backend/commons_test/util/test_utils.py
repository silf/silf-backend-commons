# coding=utf-8
import unittest

from silf.backend.commons.util.config import prepend_current_dir_to_config_file
from silf.backend.commons.util.test_util.config_test_utils import create_config


class CreateConfigTests(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.test_file = prepend_current_dir_to_config_file("conf.ini")

    def test_file_contents(self):
        config = create_config(self.test_file, {})
        self.assertIsNotNone(
            config, "Configuration not read, check path to config file"
        )
        self.assertEqual(config["DEFAULT"]["JakJest"], "zajebiscie")
        self.assertEqual(config["silf"]["JakJest"], "python a FUJ!")

    def test_overrides_values(self):
        config = create_config(self.test_file, {"silf": {"JakJest": "Zajebiście!"}})
        self.assertEqual(config["silf"]["JakJest"], "Zajebiście!")

    def test_other_values_unchanged_when_overriding(self):
        config = create_config(self.test_file, {"silf": {"JakJest": "Zajebiście!"}})
        self.assertEqual(config["DEFAULT"]["JakJest"], "zajebiscie")

    def test_adds_new_section(self):
        config = create_config(
            self.test_file, {"better-silf": {"JakJest": "Zajebiście!"}}
        )
        self.assertEqual(config["better-silf"]["JakJest"], "Zajebiście!")

    def test_other_values_unchanged_when_overriding_new_section(self):
        config = create_config(
            self.test_file, {"better-silf": {"JakJest": "Zajebiście!"}}
        )
        self.assertEqual(config["DEFAULT"]["JakJest"], "zajebiscie")
        self.assertEqual(config["silf"]["JakJest"], "python a FUJ!")
