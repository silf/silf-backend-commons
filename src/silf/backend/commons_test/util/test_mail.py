# -*- coding: utf-8 -*-
from smtplib import SMTPAuthenticationError, SMTP
import unittest
from configparser import ConfigParser
from unittest import mock

from silf.backend.commons.api.exceptions import ConfigurationException
from silf.backend.commons.util.config import (
    open_configfile,
    prepend_current_dir_to_config_file,
)
from silf.backend.commons.util.mail import EmailSender, RESEND_EXCEPTION_TIMEOUT_SEC


class ErrorSmtp(SMTP):
    def __init__(self, *args, **kwargs):
        pass

    def login(self, user, password):
        raise SMTPAuthenticationError(500, "")

    def ehlo(self, name=""):
        pass


class MailTests(unittest.TestCase):
    @classmethod
    def load_cp(cls):
        cp = open_configfile(prepend_current_dir_to_config_file("mail.ini"))
        return cp

    def test_disabled_cp(self):
        cp = ConfigParser()
        cp.add_section("SMTP")
        cp.set("SMTP", "smtp_enabled", "false")
        mail = EmailSender(cp)
        self.assertFalse(mail.enabled)

    def test_login_error(self):
        client = mock.MagicMock()
        client.login.side_effect = SMTPAuthenticationError(300, "Error")
        mock.return_value = client
        cp = self.load_cp()
        cp.set(
            "SMTP",
            "smtp_client_class",
            "silf.backend.commons_test.util.test_mail.ErrorSmtp",
        )
        with self.assertRaises(ConfigurationException):
            mail = EmailSender(cp)

    @mock.patch("time.monotonic")
    def test_email_with_is_sent(self, now):
        now.return_value = 0
        cp = self.load_cp()
        cp.set("SMTP", "smtp_mock", "true")
        mail = EmailSender(cp)
        try:
            raise ValueError()
        except ValueError:
            mail.send_current_exception_to_admins("E", "S")
        self.assertEqual(len(mail.mock_messages), 1)

    @mock.patch("time.monotonic")
    def test_email_is_sent_once(self, now):
        now.return_value = 0
        cp = self.load_cp()
        cp.set("SMTP", "smtp_mock", "true")
        mail = EmailSender(cp)
        try:
            raise ValueError()
        except ValueError:
            mail.send_current_exception_to_admins("E", "S")
            mail.send_current_exception_to_admins("E", "S")
        self.assertEqual(len(mail.mock_messages), 1)

    @mock.patch("time.monotonic")
    def test_the_the_same_error_is_send_twice_if_timeout_is_esceeded(self, now):

        cp = self.load_cp()
        cp.set("SMTP", "smtp_mock", "true")
        mail = EmailSender(cp)
        try:
            raise ValueError()
        except ValueError:
            now.return_value = 0
            mail.send_current_exception_to_admins("E", "S")
            now.return_value = RESEND_EXCEPTION_TIMEOUT_SEC + 1
            mail.send_current_exception_to_admins("E", "S")
        self.assertEqual(len(mail.mock_messages), 2)

    @mock.patch("time.monotonic")
    def test_different_exceptions_are_sent_regardless_timeout(self, now):
        now.return_value = 0
        cp = self.load_cp()
        cp.set("SMTP", "smtp_mock", "true")
        mail = EmailSender(cp)
        try:
            raise ValueError()
        except ValueError:
            now.return_value = 0
            mail.send_current_exception_to_admins("E", "S")
        try:
            raise ValueError()
        except ValueError:
            now.return_value = 0
            mail.send_current_exception_to_admins("E", "S")
        self.assertEqual(len(mail.mock_messages), 2)
