# coding=utf-8


import unittest

from silf.backend.commons.util.config import (
    open_configfile,
    prepend_current_dir_to_config_file,
)


class TestOpenConfigFile(unittest.TestCase):
    def test_config_file(self):
        # When we pass file it should be opened
        config = open_configfile(prepend_current_dir_to_config_file("conf.ini"))

        self.assertIsNotNone(
            config, "Configuration not read, check path to config file"
        )
        self.assertEqual(config["DEFAULT"]["JakJest"], "zajebiscie")
        self.assertEqual(config["silf"]["JakJest"], "python a FUJ!")

    def test_config_parser(self):
        # When we pass config parser to open_configfile it should pass unchanged
        cp = open_configfile(prepend_current_dir_to_config_file("conf.ini"))
        config = open_configfile(cp)

        self.assertIs(config, cp)

    def test_invalid_file_config(self):
        self.assertRaises(
            ValueError,
            open_configfile,
            prepend_current_dir_to_config_file("no-such-file.ini"),
        )

    def test_returns_none_on_none_input(self):
        config = open_configfile(None)
        self.assertIsNone(config, "Should return none on none input")
