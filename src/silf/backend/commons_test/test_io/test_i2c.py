# coding=utf-8

import unittest
import time
from unittest.mock import patch
from silf.backend.commons.io.i2c import long_transaction, I2CError

import silf.backend.commons.io.i2c


class TestI2C(unittest.TestCase):
    def test_ok_function(self):
        """
        If wrapped function does not raise an exception we should call it once.
        """

        called_times = 0

        def func():
            nonlocal called_times
            called_times += 1
            return "foobar"

        wrapped = long_transaction()(func)

        result = wrapped()

        self.assertEqual(called_times, 1)

        self.assertEqual(result, "foobar")

    def test_allways_raise_function(self):
        """
        If wrapped function allways raises OsError we call it predefined amount of times, and then raise own exception.
        """

        called_times = 0

        def func():
            nonlocal called_times
            called_times += 1
            raise OSError()

        wrapped = long_transaction(tries=5, timeout=0.5)(func)

        started_at = time.monotonic()

        self.assertRaises(I2CError, wrapped)
        self.assertEqual(called_times, 5)
        self.assertGreater(time.monotonic() - started_at, 2.5)

    def test_function_that_raises_exception_then_returns(self):
        """
        If wrapped function raises OsError, and on second try returns we ignore
        exception and return new value
        """

        called_times = 0

        def func():
            nonlocal called_times
            called_times += 1
            if called_times == 1:
                raise OSError()
            return "foobar"

        started_at = time.monotonic()

        wrapped = long_transaction(tries=5, timeout=0.5)(func)

        result = wrapped()
        self.assertEqual(result, "foobar")
        self.assertEqual(called_times, 2)
        self.assertGreater(time.monotonic() - started_at, 0.5)

    def test_other_exceptions_are_propagated(self):
        """
        Exceptions other than OsError are propagated freely
        """

        called_times = 0

        def func():
            nonlocal called_times
            called_times += 1
            raise ValueError()

        wrapped = long_transaction()(func)

        self.assertRaises(ValueError, wrapped)
        self.assertEqual(called_times, 1)

    def test_os_error_propagated_if_not_in_errors_tuple(self):

        called_times = 0

        def func():
            nonlocal called_times
            called_times += 1
            raise OSError()

        wrapped = long_transaction(caught_exceptions=[])(func)
        self.assertRaises(OSError, wrapped)
        self.assertEqual(called_times, 1)

    def test_value_error_caugth_if_in_errors_touple(self):

        called_times = 0

        def func():
            nonlocal called_times
            called_times += 1
            raise ValueError()

        wrapped = long_transaction(
            tries=5, timeout=0.5, caught_exceptions=[ValueError]
        )(func)

        started_at = time.monotonic()

        self.assertRaises(I2CError, wrapped)
        self.assertEqual(called_times, 5)
        self.assertGreater(time.monotonic() - started_at, 2.5)

    @patch("silf.backend.commons.io.i2c.DeclarativeDriver.read_raw")
    def test_i2c_validation_min_value(self, read_raw):
        read_raw.return_value = bytearray([1, 0, 0, 0])

        driver = silf.backend.commons.io.i2c.DeclarativeDriver()

        self.assertRaises(ValueError, driver.read_int, "", min_value=5)

    @patch("silf.backend.commons.io.i2c.DeclarativeDriver.read_raw")
    def test_i2c_validation_max_value(self, read_raw):
        read_raw.return_value = bytearray([0, 0, 0, 1])

        driver = silf.backend.commons.io.i2c.DeclarativeDriver()

        self.assertRaises(ValueError, driver.read_int, "", max_value=5)

    @patch("silf.backend.commons.io.i2c.DeclarativeDriver.read_raw")
    def test_i2c_validation_min_ok(self, read_raw):
        read_raw.return_value = bytearray([1, 0, 0, 0])

        driver = silf.backend.commons.io.i2c.DeclarativeDriver()

        self.assertEqual(driver.read_int("", min_value=0), 1)

    @patch("silf.backend.commons.io.i2c.DeclarativeDriver.read_raw")
    def test_i2c_validation_max_ok(self, read_raw):
        read_raw.return_value = bytearray([1, 0, 0, 0])

        driver = silf.backend.commons.io.i2c.DeclarativeDriver()

        self.assertEqual(driver.read_int("", max_value=5), 1)
