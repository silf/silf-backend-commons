import configparser
import contextlib
import typing
import unittest
from unittest import mock

from silf.backend.commons.api.exceptions import SILFProtocolError
from silf.backend.commons.api.stanza_content import (
    ControlSuite,
    SettingSuite,
    ResultSuite,
    Result,
    ModeSuite,
    Mode,
    Setting,
)
from silf.backend.commons.device_manager import (
    ResultCreator,
    AppendResultCreator,
    ReturnLastElementResultCreator,
)
from silf.backend.commons.experiment import ExperimentCallback
from silf.backend.commons.simple_server.simple_server import (
    ExperimentMode,
    ResultManager,
    MultiModeManager,
)
from silf.backend.commons.util.abc_utils import patch_abc


class TestExperimentMode(unittest.TestCase):
    def setUp(self):
        self.patch = patch_abc(ExperimentMode)
        self.tested_mode = ExperimentMode(
            config_parser=mock.MagicMock(), experiment_callback=mock.MagicMock()
        )

    def test_constructor(self):
        config_parser = mock.MagicMock()
        experiment_callback = mock.MagicMock()
        mode = ExperimentMode(
            config_parser=config_parser, experiment_callback=experiment_callback
        )
        self.assertIs(config_parser, mode.config)
        self.assertIs(experiment_callback, mode.experiment_callback)

    def test_description_assertion(self):
        class ModeWithDescription(ExperimentMode):
            DESCRIPTION = "Something"

        with self.assertRaises(AssertionError):
            ModeWithDescription(
                config_parser=mock.MagicMock(), experiment_callback=mock.MagicMock()
            )

    def test_get_check_settings(self):
        control_mock = mock.MagicMock()
        with mock.patch.object(
            ExperimentMode, "get_input_fields", return_value=control_mock
        ):
            settings = SettingSuite()
            self.tested_mode.check_settings(settings)
            control_mock.check_settings.assert_called_once_with(settings)

    def test_get_apply_settings(self):
        control_mock = mock.MagicMock()
        cleaned_settings = {}
        control_mock.check_and_convert_settings.return_value = cleaned_settings
        with mock.patch.object(
            ExperimentMode, "get_input_fields", return_value=control_mock
        ):
            settings = SettingSuite()
            self.tested_mode.apply_settings(settings)
            control_mock.check_and_convert_settings.assert_called_once_with(settings)
            self.assertIs(self.tested_mode.settings, cleaned_settings)

    def tearDown(self):
        self.patch.stop()


class TestResultManager(unittest.TestCase):
    def setUp(self):
        self.callback = mock.Mock()
        self.callback.send_results = mock.Mock(return_value=None)
        self.result_manager = ResultManager([], self.callback)

    def test_prepare_results(self):
        results = {}
        prepared = self.result_manager._prepare_results(results)
        self.assertIsInstance(prepared, list)
        self.assertIs(prepared[0], results)

    def test_feed_results(self):
        creators = [
            mock.MagicMock(spec=ResultCreator),
            mock.MagicMock(spec=ResultCreator),
        ]
        self.result_manager.creators = creators
        results = [{"foo": 1}, {"foo": 2}]
        self.result_manager._feed_result_aggregators(results)
        self.assertEqual(
            creators[0].aggregate_results.call_args_list,
            [mock.call(results[0]), mock.call(results[1])],
        )
        self.assertEqual(
            creators[1].aggregate_results.call_args_list,
            [mock.call(results[0]), mock.call(results[1])],
        )

    def test_make_result_suites(self):
        creator_a = mock.MagicMock(spec=ResultCreator)
        creator_a.result_name = "foo"
        creator_a.has_results = True
        creator_a.pop_results.return_value = [1, 2, 3]
        creator_b = mock.MagicMock(spec=ResultCreator)
        creator_b.result_name = "bar"
        creator_b.has_results = True
        creator_b.pop_results.return_value = [3, 2, 1]
        creator_c = mock.MagicMock(spec=ResultCreator)
        creator_c.result_name = "baz"
        creator_c.has_results = False
        creator_c.pop_results.return_value = [3, 2, 1]
        result_creators = [creator_a, creator_b, creator_c]
        self.result_manager.creators = result_creators
        self.assertEqual(
            self.result_manager._make_results_suite(),
            ResultSuite(foo=[1, 2, 3], bar=[3, 2, 1]),
        )


class TestResultManagerIntegrationTests(unittest.TestCase):
    def setUp(self):
        self.creators = [
            AppendResultCreator("foo"),
            ReturnLastElementResultCreator("bar"),
        ]
        self.callback = mock.Mock()
        self.callback.send_results = mock.Mock(return_value=None)
        self.result_manager = ResultManager(self.creators, self.callback)

    def test_prepare_results(self):
        self.result_manager._feed_result_aggregators(
            [{"foo": [1, 2, 3], "bar": [1, 2, 3]}]
        )
        self.result_manager._feed_result_aggregators(
            [{"foo": [4, 5, 6], "bar": [4, 5, 6]}]
        )

        self.assertEqual(
            self.result_manager._make_results_suite(),
            ResultSuite(
                bar=Result(value=6, pragma="append"),
                foo=Result(value=[1, 2, 3, 4, 5, 6], pragma="append"),
            ),
        )

    def test_push_results(self):
        self.result_manager.push_results(
            [{"foo": [1, 2, 3], "bar": [1, 2, 3]}, {"foo": [4, 5, 6], "bar": [4, 5, 6]}]
        )
        self.callback.send_results.assert_called_once_with(
            ResultSuite(
                bar=Result(value=6, pragma="append"),
                foo=Result(value=[1, 2, 3, 4, 5, 6], pragma="append"),
            )
        )


class TestSimpleServer(unittest.TestCase):
    def create_mode_mock(self, name: str, description: str):
        mode = mock.Mock(spec=ExperimentMode)
        mode.get_mode_name.return_value = name
        mode.get_mode_label.return_value = name
        mode.get_description.return_value = description
        return mode

    def create_simple_server(
        self,
        modes: typing.List[ExperimentMode],
        config_parser: configparser.ConfigParser = configparser.ConfigParser(),
    ):
        class Manager(MultiModeManager):
            def post_power_up_diagnostics(self, *args):
                pass

            def pre_power_up_diagnostics(self, *args):
                pass

            def get_mode_managers(
                cls, config_parser: configparser.ConfigParser
            ) -> typing.List["ExperimentMode"]:
                return modes

        manager = Manager(config_parser)
        callback = mock.Mock(spec=ExperimentCallback)
        manager.initialize(callback)
        return manager

    def create_default_server(self):
        return self.create_simple_server(
            [
                self.create_mode_mock("foo", "Foo description"),
                self.create_mode_mock("bar", "Bar description"),
            ]
        )

    def test_modes_are_necessary(self):
        """If there are no modes AssertionError should be raised"""
        with self.assertRaises(AssertionError):
            self.create_simple_server([])

    def test_duplicate_modes_raise_error(self):
        with self.assertRaises(AssertionError):
            self.create_simple_server(
                [
                    self.create_mode_mock("foo", "Foo description"),
                    self.create_mode_mock("foo", "Foo description"),
                ]
            )

    def test_constructor(self):
        foo = self.create_mode_mock("foo", "Foo description")
        bar = self.create_mode_mock("bar", "Bar description")
        server = self.create_simple_server([foo, bar])
        self.assertEqual(server._mode_manager_map, {"foo": foo, "bar": bar})
        self.assertFalse(server.has_mode)
        self.assertFalse(server.is_started)

    def test_get_mode_manager(self):
        foo = self.create_mode_mock("foo", "Foo description")
        bar = self.create_mode_mock("bar", "Bar description")
        server = self.create_simple_server([foo, bar])
        self.assertIs(server.get_mode_manager_type("foo"), foo)
        self.assertIs(server.get_mode_manager_type("bar"), bar)

    def test_modes(self):
        server = self.create_default_server()
        self.assertEqual(
            server.modes,
            ModeSuite(
                foo=Mode("foo", "Foo description", order=0),
                bar=Mode("bar", "Bar description", order=1),
            ),
        )

    def test_power_up(self):
        server = self.create_default_server()
        self.assertFalse(server.has_mode)
        server.power_up("foo")
        self.assertTrue(server.has_mode)
        self.assertEqual(server.mode_name, "foo")
        self.assertIsNotNone(server._mode_manager)

    def test_power_up_wrong_mode(self):
        server = self.create_default_server()
        self.assertFalse(server.has_mode)
        with self.assertRaises(KeyError):
            server.power_up("no such mode")

    def test_power_up_selects_manager(self):
        foo = self.create_mode_mock("foo", "Foo description")
        bar = self.create_mode_mock("bar", "Bar description")
        created_manager = mock.Mock(spec=ExperimentMode)
        foo.return_value = created_manager
        server = self.create_simple_server([foo, bar])
        server.power_up("foo")
        self.assertIs(server.mode_manager, created_manager)
        foo.assert_called_once_with(server.config, server.experiment_callback)
        created_manager.power_up.assert_called_once_with()

    def test_power_up_stops_previous_mode(self):

        server = self.create_default_server()
        server.power_up("bar")
        server.power_down = mock.Mock()
        server.power_up("foo")
        # Power down is called to stop previous mode.
        server.power_down.assert_called_once_with()

    def test_tear_down_when_there_is_no_mode(self):
        server = self.create_default_server()
        server.tear_down()  # It's a noop

    def test_tear_down_with_mode(self):
        server = self.create_default_server()
        server.power_up("foo")
        server.tear_down()
        server.mode_manager.tear_down.assert_called_once_with()

    def test_power_down(self):
        server = self.create_default_server()
        # Now its is a noop
        server.power_down()
        self.assertFalse(server.has_mode)
        self.assertIsNone(server._mode_manager)

    def test_power_up_with_previous_mode(self):
        server = self.create_default_server()
        server.power_up("foo")
        mode_manager = server._mode_manager = mock.MagicMock()
        server.power_down()
        mode_manager.power_down.assert_called_once_with()

    def test_power_down_stops_experiment(self):
        server = self.create_default_server()
        server.power_up("foo")
        server.start()
        self.assertTrue(server.is_started)
        server.power_down()
        self.assertFalse(server.is_started)

    def test_stop_when_powered_down(self):
        server = self.create_default_server()
        server.stop()  # It's a noop.

    def test_start(self):
        server = self.create_default_server()
        server.power_up("foo")
        server.start()
        self.assertTrue(server.is_started)
        server.mode_manager.start.assert_called_once_with()

    def test_start_stop(self):
        server = self.create_default_server()
        server.power_up("foo")
        server.start()
        mode_manager = server._mode_manager = mock.MagicMock()
        server.stop()
        self.assertFalse(server.is_started)
        mode_manager.stop.assert_called_once_with()

    def test_start_no_mode(self):
        server = self.create_default_server()
        with self.assertRaises(AssertionError):
            server.start()

    def test_loop_iteration_no_mode(self):
        server = self.create_default_server()
        server.loop_iteration()  # It's a noop

    def test_loop_iteration(self):
        server = self.create_default_server()
        server.power_up("foo")
        server.loop_iteration()
        server.mode_manager.loop_iteration.assert_called_once_with()

    def test_apply_settings_no_mode(self):
        server = self.create_default_server()
        with self.assertRaises(AssertionError):
            server.apply_settings(SettingSuite())

    def test_apply_settings(self):
        server = self.create_default_server()
        server.power_up("foo")
        suite = SettingSuite(foo=Setting(value=5))
        server.apply_settings(suite)
        server.mode_manager.apply_settings.assert_called_once_with(suite)

    def test_check_settings(self):
        foo = self.create_mode_mock("foo", "Foo description")
        bar = self.create_mode_mock("bar", "Bar description")
        server = self.create_simple_server([foo, bar])
        suite = SettingSuite(foo=Setting(value=5))
        server.check_settings("foo", suite)
        foo(
            server.config, server.experiment_callback
        ).check_settings.assert_called_once_with(suite)
        self.assertEqual(bar.check_settings.call_count, 0)

    def test_get_output_fields(self):
        foo = self.create_mode_mock("foo", "Foo description")
        bar = self.create_mode_mock("bar", "Bar description")
        marker = object()
        server = self.create_simple_server([foo, bar])
        foo(
            server.config, server.experiment_callback
        ).get_output_fields.return_value = marker
        self.assertEqual(server.get_output_fields("foo"), marker)

    def test_get_input_fields(self):
        foo = self.create_mode_mock("foo", "Foo description")
        bar = self.create_mode_mock("bar", "Bar description")
        marker = object()
        server = self.create_simple_server([foo, bar])
        foo(
            server.config, server.experiment_callback
        ).get_input_fields.return_value = marker
        self.assertEqual(server.get_input_fields("foo"), marker)

    def test_on_misc_error(self):
        server = self.create_default_server()
        server.power_up("foo")
        server.start()
        with self.assertRaises(SILFProtocolError):
            try:
                raise ValueError
            except:
                # This method should be ran from catch context
                server.on_miscellaneous_error()
        self.assertFalse(server.is_started)
        server.experiment_callback.send_series_done.assert_called_once_with(
            "Exception raised"
        )

    def test_get_series_name(self):
        # Monstrosity below is replayed get_series_name function
        input_fields = mock.Mock(spec=ControlSuite)
        converted_settings = {"foo": 1, "bar": 2}
        input_fields.check_and_convert_settings.return_value = converted_settings
        foo = self.create_mode_mock("foo", "Foo description")
        bar = self.create_mode_mock("bar", "Bar description")
        marker = object()
        server = self.create_simple_server([foo, bar])
        foo(
            server.config, server.experiment_callback
        ).get_input_fields.return_value = input_fields
        server.power_up("foo")
        server._mode_manager = mock.MagicMock()
        server._mode_manager.get_series_name.return_value = marker
        settings = SettingSuite()
        result = server.get_series_name(settings)
        self.assertEqual(marker, result)
        foo(
            server.config, server.experiment_callback
        ).get_input_fields.assert_called_once_with()
        server._mode_manager.get_series_name.assert_called_once_with(converted_settings)
        input_fields.check_and_convert_settings.assert_called_once_with(settings)
