# -*- coding: utf-8 -*-
import unittest
from silf.backend.commons.api import (
    IntegerControl,
    ControlSuite,
    NumberControl,
    ValidationError,
    SILFProtocolError,
)
from silf.backend.commons.api.stanza_content._misc import SettingSuite, Setting


class TestNumberControl(unittest.TestCase):

    OMIT_VALIDATION = False

    def setUp(self):
        super().setUp()
        self.contol = NumberControl("foo", "Foo", min_value=10, max_value=100)

    def test_validate_ok(self):
        result = self.contol.convert_json_to_python("11")
        self.assertEqual(result, 11)

    def test_validate_invalid_number(self):
        with self.assertRaises(ValidationError):
            self.contol.convert_json_to_python("5")

    def test_validate_invalid_number_2(self):
        with self.assertRaises(ValidationError):
            self.contol.convert_json_to_python("101")

    def test_validate_invalid_number_3(self):
        with self.assertRaises(ValidationError):
            self.contol.convert_json_to_python("dsadasdad")


class TestNumberControl2(TestNumberControl):

    OMIT_VALIDATION = True


class TestControlValidationWithCurrentControls(unittest.TestCase):
    def setUp(self):

        self.control_suite = ControlSuite(
            NumberControl("foo", "Insert foo", max_value=10),
            NumberControl("bar", "Insert bar", required=False),
        )

    def test_missing_not_required_control(self):
        self.control_suite.check_settings(
            SettingSuite(
                foo=Setting(value=1, current=True), bar=Setting(value="", current=True)
            )
        )

    def test_missing_not_required_control_another_format(self):
        self.control_suite.check_settings(
            SettingSuite(
                foo=Setting(value=1, current=True),
                bar=Setting(value=None, current=True),
            )
        )

    def test_missing_not_required_control_yet_another_format(self):
        self.control_suite.check_settings(
            SettingSuite(foo=Setting(value=1, current=True))
        )

    def test_missing_required_control(self):

        with self.assertRaises(SILFProtocolError):
            self.control_suite.check_settings(
                SettingSuite(
                    foo=Setting(value="", current=True),
                    bar=Setting(value=3, current=False),
                )
            )
