import unittest
from unittest import mock

from silf.backend.commons.api import *
from silf.backend.commons.device_manager import (
    set_multiprocessing,
    reset_multiprocessing,
)

from silf.backend.commons_test.experiment.mock_experiment_manager import (
    MockExperimentManager,
)
from silf.backend.commons.util.test_util.config_test_utils import convert_dict_to_config


class MockExperimentManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        set_multiprocessing(True)

    @classmethod
    def tearDownClass(cls):
        reset_multiprocessing()

    def setUp(self):
        self.manager = MockExperimentManager(convert_dict_to_config({"Experiment": {}}))
        experiment_callback = mock.Mock()
        experiment_callback.send_results = mock.MagicMock()
        self.manager.initialize(experiment_callback)
        self.manager.power_up("default")
        self.manager.loop_iteration()

    def test_experiment_manager(self):

        self.manager.apply_settings(
            SettingSuite(
                begin_position=Setting(value=0),
                end_position=Setting(value=100),
                step_count=Setting(value=3),
            )
        )

        self.manager.loop_iteration()

        self.manager.start()

        for ii in range(250):
            self.manager.loop_iteration()
            if not self.manager.running:
                break
        self.assertLess(ii, 250)

        last_pos = None

        for call in self.manager.experiment_callback.send_results.call_args_list:
            args, kwargs = call
            result_suite = args[0]
            position_results = result_suite["position_result"]
            voltage_result = result_suite["voltage_result"]

            if last_pos is not None:
                self.assertGreaterEqual(position_results.value, last_pos)

            last_pos = position_results.value

        self.assertEqual(last_pos, 100)
