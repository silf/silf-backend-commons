# coding=utf-8
import unittest
from silf.backend.commons.api import SettingSuite, Setting
from silf.backend.commons.experiment import AbstractExperimentManager
from silf.backend.commons.util.abc_utils import patch_abc
from silf.backend.commons.util.config import (
    open_configfile,
    prepend_current_dir_to_config_file,
)
from silf.backend.commons.util.test_util.config_test_utils import create_config


class TestSeriesName(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.__patcher = patch_abc(AbstractExperimentManager)

    @classmethod
    def tearDownClass(cls):
        cls.__patcher.stop()

    def test_series_name_is_none_by_default(self):
        cp = create_config(prepend_current_dir_to_config_file("test_config.ini"), {})
        self.manager = AbstractExperimentManager(cp)
        self.manager.power_up("default")
        self.assertEquals(self.manager.get_series_name(SettingSuite()), "Seria 0")

    def test_series_name_from_settings(self):
        cp = create_config(
            prepend_current_dir_to_config_file("test_config.ini"),
            {"Experiment": {"series_pattern": "foo"}},
        )
        self.manager = AbstractExperimentManager(cp)
        self.assertEqual(self.manager.get_series_name(SettingSuite()), "foo")

    def test_interpolation(self):
        cp = create_config(
            prepend_current_dir_to_config_file("test_config.ini"),
            {"Experiment": {"series_pattern": "Series for foo = {foo}"}},
        )
        self.manager = AbstractExperimentManager(cp)
        self.assertEqual(
            self.manager.get_series_name(SettingSuite(foo=Setting(value=12))),
            "Series for foo = 12",
        )
