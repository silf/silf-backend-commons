import time

from silf.backend.commons.api.stanza_content import ResultSuite, Result
from silf.backend.commons.api.exceptions import ValidationError
from silf.backend.commons.experiment import *
from silf.backend.commons_test.device_manager.mock_engine_manager import (
    ScanningEngineManager,
)
from silf.backend.commons_test.device_manager.mock_voltimeter_manager import (
    MockVoltimeterManager,
)

from silf.backend.commons.util.test_util.config_test_utils import convert_dict_to_config


class PositionListener(EventListener):
    def __call__(self, event, *args, **kwargs):

        try:
            if event.manager["eng"].on_position:

                def _voltage_callback(*args):
                    event.manager.schedule_event("get_voltage", 1)

                event.manager["volt"].start(callback=_voltage_callback)
                # event.manager.schedule_event(10, 'get_voltage')
                return
            event.manager.schedule_event("check_position", 1)
        except Exception as e:
            event.manager.schedule_event("check_position", 1)
            raise


class NextPositionListener(EventListener):
    def __call__(self, event, *args, **kwargs):

        event.manager._running = True

        if event.manager["eng"].on_final_position:
            event.manager.stop_and_notify()
            event.manager._running = False
            return

        event.manager["eng"].go_to_next_position()

        event.manager.schedule_event("check_position", 1)


class VoltageListener(EventListener):
    def __call__(self, event, *args, **kwargs):

        volt = event.manager["volt"]
        eng = event.manager["eng"]

        if "current_voltage" in volt.current_results:

            results = ResultSuite(
                voltage_result=volt.current_results["current_voltage"],
                position_result=Result(value=eng.desired_position),
            )

            event.manager.experiment_callback.send_results(results)

            if not volt.running:
                event.manager.schedule_event("next_position", 1)
                return

        event.manager.schedule_event("get_voltage", 1)
        return


class SendCurrentPosition(EventListener):
    def __call__(self, event, *args, **kwargs):

        event.manager.experiment_callback.send_results(ResultSuite())


class MockExperimentManager(EventExperimentManager):

    """
    >>> config = convert_dict_to_config({'Experiment': {}})
    >>> man = MockExperimentManager(config)

    >>> from unittest import mock

    >>> man.initialize(mock.MagicMock())

    >>> man.get_input_fields("default")
    <ControlSuite <Control name=begin_position type=number live=False label='Begin position'> <Control name=end_position type=number live=False label='End position'> <Control name=step_count type=number live=False label='Number of steps'>>

    >>> man.get_output_fields("default")
    <OutputFieldSuite current_position=<Result class=indicator name=['current_position'] type=array metadata={} settings={}> current_step_indicator=<Result class=indicator name=['current_step_indicator'] type=array metadata={} settings={}> >

    >>> man.power_up("default")

    >>> man.loop_iteration() # Fires events

    >>> from silf.backend.commons.api import Setting, SettingSuite
    >>> man.apply_settings(SettingSuite(
    ...        begin_position = Setting(value=0),
    ...        end_position = Setting(value=100),
    ...        step_count = Setting(value=10),
    ... ))

    >>> man.settings['eng'] == {'step_count': 10, 'begin_position': 0, 'end_position': 100}
    True
    >>> man.settings['volt'] == {}
    True
    """

    DEVICE_MANAGERS = {"volt": MockVoltimeterManager, "eng": ScanningEngineManager}

    MODES = ["default"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._results = ResultSuite()
        self._running = False

    def initialize(self, experiment_callbac):
        super().initialize(experiment_callbac)

        self.install_default_event_managers_for("stop")
        self.install_default_event_managers_for("power_down")
        self.install_default_event_managers_for("power_up")
        self.install_default_event_managers_for("tick")
        self.install_default_event_managers_for("apply_settings")

        self.register_listener("next_position", NextPositionListener())
        self.register_listener("start", NextPositionListener())
        self.register_listener("check_position", PositionListener())
        self.register_listener("get_voltage", VoltageListener())

    @property
    def running(self):
        return self._running

    def loop_iteration(self):
        time.sleep(0.1)
        super().loop_iteration()


class RaiseErrorOnCheckSettings(MockExperimentManager):
    def check_settings(self, mode, settings):
        raise ValidationError.from_args("error", "user", "Test Message")
