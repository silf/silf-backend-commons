# -*- coding: utf-8 -*-
from unittest.case import TestCase
from silf.backend.commons.api.exceptions import ValidationError
from silf.backend.commons.experiment import AbstractExperimentManager
from silf.backend.commons.util import mail
from silf.backend.commons.util.test_util.config_test_utils import create_config
from silf.backend.commons_test.device_manager.noop_device import (
    RaiseErrorOnCheckSettings,
)
from silf.backend.experiment.experiment import load_experiment
from silf.backend.experiment_test.test_experiment import (
    prepend_current_dir_to_config_file,
)


class TestMailSender(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.config = create_config(
            prepend_current_dir_to_config_file("test_config.ini"),
            {
                "Experiment": {
                    "DeviceManagerClass": "silf.backend.commons_test."
                    "device_manager.noop_device."
                    "RaiseErrorOnCheckSettings",
                    "ExperimentManagerClass": "silf.backend.commons.experiment.SimpleExperimentManager",
                    "AvailableLanguages": "pl,en"
                    # do not set TranslationsRootDir so that translation files are not checked during tests
                    # 'TranslationsRootDir': '/tmp'
                }
            },
        )
        cls.experiment = load_experiment(cls.config)
        cls.experiment.configure()

    def test_error_is_raised(self):
        device = RaiseErrorOnCheckSettings("doo", None)
        with self.assertRaises(ValidationError):
            device.check_settings("none", {})

    def test_message_sending(self):

        mail.BorgSender().sender.mock_messages.clear()
        self.assertEqual(len(mail.BorgSender().sender.mock_messages), 0)

        class Content(object):
            content = {}
            namespace = "silf:settings:check"
            id = "foo"
            type = "query"

            def reply(self, *args, **kwargs):
                pass

        self.experiment.mode = "foo"
        self.experiment.queue.put(Content())
        self.experiment._loop_iteration()
        self.assertEqual(len(mail.BorgSender().sender.mock_messages), 0)
