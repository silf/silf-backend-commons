# coding=utf-8
import configparser
import unittest
from unittest import mock

from silf.backend.commons.api import SettingSuite
from silf.backend.commons.util.abc_utils import patch_abc
from silf.backend.commons.util.config import (
    open_configfile,
    prepend_current_dir_to_config_file,
)
from silf.backend.commons.experiment import *
from silf.backend.commons.util.test_util.config_test_utils import create_config


class BaseTests(unittest.TestCase):

    MANAGER_TO_PATCH = AbstractExperimentManager

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.p = patch_abc(cls.MANAGER_TO_PATCH)
        cls.cb = patch_abc(ExperimentCallback)
        cls.p.start()
        cls.cb.start()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.p.stop()
        cls.cb.stop()

    def config_parser(self, overrides):

        config_file_name = prepend_current_dir_to_config_file("test_config.ini")
        cp = create_config(config_file_name, {"Experiment": overrides})
        self.assertIsNotNone(cp)

        return cp


@mock.patch("time.monotonic")
@unittest.skip("Abstract base class for other tests")
class TestManagerAutoShutdownForAbstractManager(BaseTests):

    MANAGER_TO_PATCH = AbstractExperimentManager

    def __add_time(self, monotonic, time_to_add):
        self.__current_time += time_to_add
        monotonic.return_value = self.__current_time

    def __setUp(self, monotonic, series_time_limit=100, experimet_time_limit=-1):
        self.__current_time = 10000
        self.__add_time(monotonic, 0)
        cp = self.config_parser(
            {
                "stop_series_timeout": str(series_time_limit),
                "shutdown_experiment_timeout": str(experimet_time_limit),
            }
        )
        em = self.MANAGER_TO_PATCH(cp)
        em.initialize(mock.MagicMock())
        em.power_up("default")

        return em

    def test_series_is_running_after_start(self, monotonic):

        em = self.__setUp(monotonic)
        em.apply_settings(SettingSuite())
        em.start()
        em.loop_iteration()
        self.assertTrue(em.running)

    def test_series_is_running_before_end(self, monotonic):

        em = self.__setUp(monotonic)
        em.apply_settings(SettingSuite())
        em.start()
        em.loop_iteration()
        self.assertTrue(em.running)
        self.__add_time(monotonic, 99)
        em.loop_iteration()
        self.assertTrue(em.running)

    def test_series_is_stopped_after_timeout(self, monotonic):

        em = self.__setUp(monotonic)
        em.apply_settings(SettingSuite())
        em.start()
        em.loop_iteration()
        self.assertTrue(em.running)
        self.__add_time(monotonic, 101)
        em.loop_iteration()
        self.assertFalse(em.running)
        em.loop_iteration()
        em.loop_iteration()
        self.assertIn(
            mock.call.send_series_done(mock.ANY), em.experiment_callback.method_calls
        )

    def test_proper_message_sent(self, monotonic):
        em = self.__setUp(monotonic)
        em.apply_settings(SettingSuite())
        em.start()
        em.loop_iteration()
        self.assertTrue(em.running)
        self.__add_time(monotonic, 101)
        em.loop_iteration()
        self.assertFalse(em.running)
        self.assertIn(
            mock.call.send_series_done(mock.ANY), em.experiment_callback.method_calls
        )

    def test_will_stop_after_timeout_no_limit(self, monotonic):

        em = self.__setUp(monotonic, -1)
        em.apply_settings(SettingSuite())
        em.start()
        em.loop_iteration()
        self.assertTrue(em.running)
        self.__add_time(
            monotonic, 3600 * 24 * 31
        )  # Will work after a month, should be enough
        em.loop_iteration()
        self.assertTrue(em.running)

    def test_will_not_stop_if_running(self, monotonic):
        em = self.__setUp(monotonic, -1, 1)
        em.apply_settings(SettingSuite())
        em.start()
        self.assertTrue(em.running)
        self.assertTrue(em.powered_up)
        self.__add_time(
            monotonic, 3600 * 24 * 31
        )  # Will work after a month, should be enough
        em.loop_iteration()
        self.assertTrue(em.powered_up)
        self.assertTrue(em.running)

    def test_will_stop_if_not_running(self, monotonic):
        em = self.__setUp(monotonic, 1, 100)
        em.apply_settings(SettingSuite())
        em.start()
        em.stop()
        em.loop_iteration()
        self.assertFalse(em.running)
        self.assertTrue(em.powered_up)
        self.__add_time(monotonic, 102)  # Will work after a month, should be enough
        em.loop_iteration()
        self.assertFalse(em.running)
        self.assertFalse(em.powered_up)

    def test_will_not_shutdown_itself_before_time(self, monotonic):
        em = self.__setUp(monotonic, 1, 100)
        em.loop_iteration()
        self.assertTrue(em.powered_up)
        self.__add_time(monotonic, 99)
        em.loop_iteration()
        self.assertTrue(em.powered_up)

    def test_will_shutdown_at_proper_time(self, monotonic):
        em = self.__setUp(monotonic, 1, 100)
        em.loop_iteration()
        self.assertTrue(em.powered_up)
        self.__add_time(monotonic, 99)
        em.loop_iteration()
        self.assertTrue(em.powered_up)
        self.__add_time(monotonic, 101)
        em.loop_iteration()
        self.assertFalse(em.powered_up)


class TestManagerAutoShutdownForEventMenager(TestManagerAutoShutdownForAbstractManager):

    MANAGER_TO_PATCH = EventExperimentManager


class TestManagerAutoShutdownForSimple(TestManagerAutoShutdownForAbstractManager):

    MANAGER_TO_PATCH = SimpleExperimentManager


if __name__ == "__main__":
    unittest.main()
