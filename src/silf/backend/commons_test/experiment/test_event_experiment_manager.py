# coding=utf-8
import unittest
from unittest import mock
from silf.backend.commons.api import SettingSuite
from silf.backend.commons.experiment import EventExperimentManager
from silf.backend.commons.util.config import (
    prepend_current_dir_to_config_file,
    open_configfile,
)
from silf.backend.commons_test.device_manager.noop_device import NoopDeviceManager


@mock.patch("time.monotonic")
class TestEventsWorkProperly(unittest.TestCase):

    """
    Test for corner case when for methods are called on EventManager with no
    intermediate at the same time
    """

    class TestManager(EventExperimentManager):

        DEVICE_MANAGERS = {"noop": mock.MagicMock()}

    def setUp(self):
        config_file_name = prepend_current_dir_to_config_file("test_config.ini")
        cp = open_configfile(config_file_name)
        self.em = self.TestManager(cp)
        self.em.initialize(mock.MagicMock())
        self.em.install_all_event_managers()

    def test_power_up_apply_settings(self, monotonic):
        monotonic.return_value = 1
        self.em.power_up("pass")
        self.em.apply_settings(SettingSuite())
        monotonic.return_value = 2
        self.em.loop_iteration()

        self.assertEquals(
            self.em.managers["noop"].method_calls[:2],
            [mock.call.power_up("pass"), mock.call.apply_settings(mock.ANY)],
        )

    def test_power_up_apply_settingss_start(self, monotonic):
        monotonic.return_value = 1
        self.em.power_up("pass")
        self.em.apply_settings(SettingSuite())
        self.em.start()
        monotonic.return_value = 2
        self.em.loop_iteration()
        self.em.managers["noop"].assert_has_calls(
            [
                mock.call.power_up("pass"),
                mock.call.apply_settings(mock.ANY),
                mock.call.start(),
            ]
        )
