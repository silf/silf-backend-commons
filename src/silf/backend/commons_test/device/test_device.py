# coding=utf-8

"""
Actual device tests
"""

import unittest
from silf.backend.commons.device import *
from silf.backend.commons.util.abc_utils import patch_abc
from silf.backend.commons.util.config import prepend_current_dir_to_config_file


class TestDeviceConfiguration(unittest.TestCase):
    def setUp(self):
        self.p = patch_abc(Device)
        self.device = Device(
            "ConfigDevice", prepend_current_dir_to_config_file("conf.ini")
        )

    def tear_down(self):
        self.p.stop()

    def test_configuration(self):
        self.assertIsNotNone(
            self.device.config, "Configuration not read, check path to config file"
        )
        self.assertEqual(
            self.device.config["DEFAULT"]["JakJest"],
            "zajebiscie",
            "Wrong value for key JakJest",
        )
        self.assertEqual(
            self.device.config["silf"]["JakJest"],
            "python a FUJ!",
            "Wrong value for key JakJest",
        )
