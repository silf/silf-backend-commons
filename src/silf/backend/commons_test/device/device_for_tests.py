# coding=utf-8

from silf.backend.commons.device import *
import time
import logging

ERROR_MODE = 1

OK_MODE = 0

MOCK_DEVICE_MODES = ["ok_mode", "error_mode"]


class MockDriver(object):
    def __init__(self):
        self.__FOO_SETTING = None
        self.__BAR_SETTING = None
        self.__CURR_POINT = None
        self.__LAST_POINT_SET = None

    def mock_timeout(self):
        time.sleep(0.05)
        pass

    def start_device(self):
        self.mock_timeout()
        self.__CURR_POINT = 0
        self.__LAST_POINT_SET = time.time()

    def get_point(self):
        self.mock_timeout()
        if self.__LAST_POINT_SET and time.time() - self.__LAST_POINT_SET > 1:

            logging.debug("New point {}".format(time.time() - self.__LAST_POINT_SET))

            self.__CURR_POINT += 1
            self.__LAST_POINT_SET = time.time()
            return {
                "bar_result": self.__CURR_POINT * self.__BAR_SETTING,
                "foo_result": self.__CURR_POINT * self.__FOO_SETTING,
            }
        return None

    def set_foo_setting(self, value):
        global __FOO_SETTING
        self.mock_timeout()
        self.__FOO_SETTING = value

    def set_bar_setting(self, value):
        global __BAR_SETTING
        self.mock_timeout()
        self.__BAR_SETTING = value


class MockDevice(Device):

    MAIN_LOOP_INTERVAL = 0.05

    def __init__(self, device_id="default"):
        super().__init__(device_id)
        self.results = []

    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        self._assert_state(DEVICE_STATES[OFF])
        logging.getLogger(self.device_id).info("performed pre power up diagnostics")

    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        self._assert_state(DEVICE_STATES[OFF])
        logging.getLogger(self.device_id).info("performed post power up diagnostics")

    def stop(self):
        global __CURR_POINT, __LAST_POINT_SET
        __CURR_POINT = None
        __LAST_POINT_SET = None

    def start(self):
        self._assert_state(DEVICE_STATES[READY])

        self.device.start_device()
        self.logger.debug("started")

        self.state = DEVICE_STATES[RUNNING]

    def pop_results(self):

        self.logger.debug("pop resuts")

        res = self.results
        self.results = []
        return res

    def _tear_down(self):
        pass

    def apply_settings(self, settings):

        self._assert_state((DEVICE_STATES[STAND_BY], DEVICE_STATES[RUNNING]))

        self.device = MockDriver()
        self.device.set_foo_setting(settings["foo"])
        self.device.set_bar_setting(settings["bar"])

        if self.state == DEVICE_STATES[STAND_BY]:
            self.state = DEVICE_STATES[READY]

    def loop_iteration(self):

        if self.state == DEVICE_STATES[RUNNING]:
            res = self.device.get_point()
            if res is not None:
                self.results.append(res)


class MockDeviceErrorDiag(MockDevice):
    def perform_diagnostics(self):
        raise DiagnosticsException("")
