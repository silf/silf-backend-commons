# coding=utf-8
from silf.backend.commons.device import Device, DIAGNOSTICS_LEVEL, DIAGNOSTICS_SHORT


class NoopDevice(Device):
    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        super().pre_power_up_diagnostics(diagnostics_level)

    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        super().post_power_up_diagnostics(diagnostics_level)

    def _tear_down(self):
        super()._tear_down()
