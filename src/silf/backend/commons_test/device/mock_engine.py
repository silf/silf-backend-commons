import time

__author__ = "jb"

from silf.backend.commons.device import *


def isprime(n):
    """check if integer n is a prime"""
    # make sure n is a positive integer
    n = abs(int(n))
    # 0 and 1 are not primes
    if n < 2:
        return False
    # 2 is the only even prime number
    if n == 2:
        return True
    # all other even numbers are not primes
    if not n & 1:
        return False
    # range starts with 3 and only needs to go up the squareroot of n
    # for all odd numbers
    for x in range(3, int(n ** 0.5) + 1, 2):
        if n % x == 0:
            return False
    return True


class MockDriver(Device):

    MIN_POSITION = 0
    MAX_POSITION = 1024

    TIMEOUT = 0.5

    def __init__(self, device_id="default", config_file=None):
        super().__init__(device_id, config_file)
        if device_id == "error":
            raise DeviceRuntimeException("Error", False)
        self.position = self.MIN_POSITION
        self.current_position = self.MIN_POSITION

    def __check_pos(self):
        if isprime(self.position):
            raise DeviceRuntimeException(
                "Error caused by position being a prime number ({})".format(
                    self.position
                ),
                True,
            )

    def __wait(self):
        time.sleep(self.TIMEOUT)

    def apply_settings(self, settings):

        self.__wait()

        position = settings["position"]

        if self.MAX_POSITION < position < self.MIN_POSITION:
            raise DeviceRuntimeException("Invalid position", True)

        self.position = position

        self.state = DEVICE_STATES[READY]

    def _tear_down(self):

        self.__wait()
        self.__check_pos()

    def start(self):

        self.__wait()

        self.__check_pos()

        self.state = DEVICE_STATES[RUNNING]

    def stop(self):

        self.__wait()

        self.__check_pos()

        self.state = DEVICE_STATES[READY]

    def power_up(self):

        self.__wait()

        self.state = DEVICE_STATES[STAND_BY]

    def power_down(self):

        self.__wait()

        self.state = DEVICE_STATES[OFF]

    def pop_results(self):

        self.__wait()

        return [{"current_position": [self.current_position]}]

    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        self.__wait()

        if diagnostics_level == DIAGNOSTICS_LEVEL[DIAGNOSTICS_LONG]:
            raise DiagnosticsException("Diagnostics exception", False)

    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        pass

    def loop_iteration(self):

        if self.state == DEVICE_STATES[RUNNING]:

            self.__wait()

            if self.position == self.current_position:
                self.state = DEVICE_STATES[READY]
                return

            velocity = max(abs(self.position - self.current_position) / 5, 10)

            if self.position > self.current_position:
                self.current_position = min(
                    self.position, self.current_position + velocity
                )
            else:
                self.current_position = max(
                    self.position, self.current_position - velocity
                )


class FastMockDriver(MockDriver):

    TIMEOUT = 0.01
    MAIN_LOOP_INTERVAL = 0.01
