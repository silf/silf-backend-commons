from silf.backend.commons.device import *

import time

import random

MIN_VOLTAGE = 10

MAX_VOLTAGE = 100


class MockVoltimeter(Device):

    TIMEOUT = 0.5

    def __init__(self, device_id="default", config_file=None):
        super().__init__(device_id, config_file)
        if device_id == "error":
            raise DeviceRuntimeException("Error", False)
        self.target_volatage = MIN_VOLTAGE
        self.current_voltage = MIN_VOLTAGE

    def __wait(self):
        time.sleep(self.TIMEOUT)

    def apply_settings(self, settings):
        super().apply_settings(settings)

    def start(self):
        self.target_volatage = random.randint(MIN_VOLTAGE, MAX_VOLTAGE)
        super().start()

    def pop_results(self):
        return [
            {
                "current_voltage": [self.current_voltage],
                "target_volatage": [self.target_volatage],
            }
        ]

    def loop_iteration(self):

        if self.state == DEVICE_STATES[RUNNING]:

            self.__wait()

            if self.target_volatage == self.current_voltage:
                self.state = DEVICE_STATES[READY]
                return

            velocity = max(abs(self.target_volatage - self.current_voltage) / 2, 30)

            if self.target_volatage > self.current_voltage:
                self.current_voltage = min(
                    self.target_volatage, self.current_voltage + velocity
                )
            else:
                self.current_voltage = max(
                    self.target_volatage, self.current_voltage - velocity
                )

    def _tear_down(self):
        super()._tear_down()

    def post_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        super().post_power_up_diagnostics(diagnostics_level)

    def pre_power_up_diagnostics(
        self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]
    ):
        super().pre_power_up_diagnostics(diagnostics_level)


class FastMockVoltimeter(MockVoltimeter):

    TIMEOUT = 0.01
    MAIN_LOOP_INTERVAL = 0.01
