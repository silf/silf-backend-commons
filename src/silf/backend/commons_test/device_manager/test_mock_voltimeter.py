import time
import unittest


from silf.backend.commons.api.stanza_content._misc import SettingSuite
from silf.backend.commons.device._device import DEVICE_STATES, READY
from silf.backend.commons_test.device_manager.mock_voltimeter_manager import (
    MockVoltimeterManager,
)


class MockVoltimeterTests(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.manager = MockVoltimeterManager("foo", None)

    def test_check_stops_after_respose(self):
        self.manager.power_up("foo")
        self.manager.apply_settings(SettingSuite())
        self.manager.start()
        time.sleep(3)
        self.assertEqual(
            self.manager.device_state,
            DEVICE_STATES[READY],
            self.manager.current_results,
        )

    def test_has_response_(self):
        self.manager.power_up("foo")
        self.manager.apply_settings(SettingSuite())
        self.manager.start()
        time.sleep(3)
        results = self.manager.current_results
        self.assertEqual(
            results.results["current_voltage"], results.results["target_volatage"]
        )


if __name__ == "__main__":
    unittest.main()
