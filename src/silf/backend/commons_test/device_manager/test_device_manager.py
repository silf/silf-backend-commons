import importlib
import unittest
from unittest import mock

from silf.backend.commons.api import *
from silf.backend.commons.device import READY, DEVICE_STATES
from silf.backend.commons.device_manager import *
from silf.backend.commons.util.abc_utils import patch_abc


def load_item_from_module(path):
    mod_name, object_name = path.rsplit(".", 1)
    module = importlib.import_module(mod_name)
    clazz = getattr(module, object_name)

    return clazz


class MockDeviceTest(unittest.TestCase):
    DEVICE_MANAGER_CLASS_NAME = None

    def tearDown(self):
        self.patcher.stop()

    def setUp(self):
        self.device_mock = mock.MagicMock()

        def device_constructor(device_id):
            return self.device_mock

        self.patcher = mock.patch.multiple(
            self.DEVICE_MANAGER_CLASS_NAME,
            # _check_mode=mock.DEFAULT,
            _construct_device=device_constructor,
            get_output_fields=mock.DEFAULT,
            get_input_fields=mock.DEFAULT,
        )
        self.patcher.start()

        clazz = load_item_from_module(self.DEVICE_MANAGER_CLASS_NAME)

        self.manager = clazz("mock_experiment", None)

    def set_device_state(self, state=DEVICE_STATES[READY]):
        self.device_mock.state = state


@unittest.skip("This is not as test, but abstract base class for other tests")
class AbstractDeviceMangerTest(MockDeviceTest):
    def device_not_created_before_power_up(self):
        self.assertIsNone(self.manager.device)

    def device_not_rnning_after_construction(self):
        self.assertFalse(self.manager.running)

    def device_created_on_power_up(self):
        self.manager.power_up("foo")
        self.assertIsNotNone(self.manager.device)

    def device_power_up_called_on_manager_power_up(self):
        self.manager.power_up("foo")
        self.assertEqual(self.manager.device.mock_calls, [mock.call.power_up()])

    def test_apply_settings_calls_convert_settings(self):
        self.manager.power_up("foo")
        self.manager.device.mock_calls = []
        self.manager.apply_settings({"foo": 1.2})
        self.assertIn(
            mock.call().check_and_convert_settings({"foo": 1.2}),
            self.manager.get_input_fields.mock_calls,
        )

    def test_apply_settings_calls_apply_settings_on_a_device(self):
        self.manager.power_up("foo")
        self.manager.device.mock_calls = []
        self.manager.apply_settings({"foo": 1.2})
        self.assertEqual(
            self.manager.device.mock_calls, [mock.call.apply_settings(mock.ANY)]
        )

    def test_start_is_called_on_the_device(self):
        self.manager.power_up("foo")
        self.manager.apply_settings({"foo": 1.2})
        self.manager.device.mock_calls = []
        self.manager.start()
        self.assertIn(
            mock.call.start(callback=None, sync=True), self.manager.device.mock_calls
        )

    def test_stop_is_called_on_the_device(self):
        self.manager.power_up("foo")
        self.manager.apply_settings({"foo": 1.2})
        self.manager.start()
        self.manager.device.mock_calls = []
        self.set_device_state()
        self.manager.stop()
        self.assertIn(mock.call.stop(sync=mock.ANY), self.manager.device.mock_calls)

    def test_power_down_is_called_on_the_device(self):
        self.manager.power_up("foo")
        device = self.manager.device
        self.manager.apply_settings({"foo": 1.2})
        self.manager.start()
        self.set_device_state()
        self.set_device_state()
        self.manager.stop()
        self.manager.device.mock_calls = []
        self.manager.power_down()
        self.assertIn(mock.call.power_down(), device.mock_calls)

    def test_power_down_kills_the_device(self):
        self.manager.power_up("foo")
        device = self.manager.device
        self.manager.apply_settings({"foo": 1.2})
        self.manager.start()
        self.set_device_state()
        self.set_device_state()
        self.manager.stop()
        self.manager.device.mock_calls = []
        self.manager.power_down(wait_time=5)
        self.assertIn(mock.call.kill(5), device.mock_calls)

    def test_power_down_nulls_the_device(self):
        self.manager.power_up("foo")
        self.manager.apply_settings({"foo": 1.2})
        self.manager.start()
        self.set_device_state()
        self.manager.stop()
        self.manager.device.mock_calls = []
        self.manager.power_down(wait_time=5)
        self.assertIsNone(self.manager.device)

    def test_suspend_powers_down_the_device(self):
        self.manager.power_up("foo")
        device = self.manager.device
        self.manager.apply_settings({"foo": 1.2})
        self.manager.start()
        self.set_device_state()
        self.manager.stop()
        self.manager.device.mock_calls = []
        self.manager.power_down(suspend=True)
        self.assertIn(mock.call.power_down(), device.mock_calls)

    def test_suspend_does_not_kill_the_device(self):
        self.manager.power_up("foo")
        device = self.manager.device
        self.manager.apply_settings({"foo": 1.2})
        self.manager.start()
        self.set_device_state()
        self.manager.stop()
        self.manager.device.mock_calls = []
        self.manager.power_down(suspend=True)
        self.assertNotIn(mock.call.kill(mock.ANY), device.mock_calls)

    def test_suspend_does_not_destroy_the_device(self):
        self.manager.power_up("foo")
        self.manager.apply_settings({"foo": 1.2})
        self.manager.start()
        self.set_device_state()
        self.manager.stop()
        self.manager.device.mock_calls = []
        self.manager.power_down(suspend=True)
        self.assertIsNotNone(self.manager.device)

    def test_cleanup_kills_the_device(self):
        self.manager.power_up("foo")
        device = self.manager.device
        self.manager.tear_down()
        self.assertIn(mock.call.kill(mock.ANY), device.mock_calls)

    def test_cleanup_nulls_the_device(self):
        self.manager.power_up("foo")
        self.manager.tear_down()
        self.assertIsNone(self.manager.device)


class DefaultDeviceTest(AbstractDeviceMangerTest):
    DEVICE_MANAGER_CLASS_NAME = (
        "silf.backend.commons.device_manager.DefaultDeviceManager"
    )


class SingleModeDeviceTest(AbstractDeviceMangerTest):
    DEVICE_MANAGER_CLASS_NAME = (
        "silf.backend.commons.device_manager.SingleModeDeviceManager"
    )


class TestExceptionTranslator(unittest.TestCase):
    def __prepare_exception_test(self, raised_instance):
        try:
            with exception_translator():
                raise raised_instance
        except Exception as e:
            return e
        else:
            self.fail("Exception not raised")

    def __prepare_error_test(self, raised_instance):
        exception = self.__prepare_exception_test(raised_instance)
        return exception.errors.errors[0]

    def test_exception_type(self):
        exception = DeviceException("Foo")
        result = self.__prepare_exception_test(exception)
        self.assertIsInstance(result, SILFProtocolError)

    def test_exception_cause(self):
        exception = DeviceException("Foo")
        result = self.__prepare_exception_test(exception)
        self.assertIs(result.__cause__, exception)

    def test_other_exceptions_are_not_caught(self):
        def fun():
            with exception_translator():
                raise ValueError()

        self.assertRaises(ValueError, fun)

    def test_defaut_severity_is_error(self):
        error = self.__prepare_error_test(DeviceException("foo"))
        self.assertEqual(error.severity, "error")


@unittest.skip("This is an abstract test base for other tests")
class TestMethodPropagatesError(MockDeviceTest):
    def test_power_up(self):
        def device_constructor():
            device_mock = mock.MagicMock()
            device_mock.power_up.side_effect = DeviceException("Error", False)
            return device_mock

        self.manager._construct_device = device_constructor

        self.assertRaises(SILFProtocolError, self.manager.power_up, "mode")

    def test_init(self):
        def device_constructor():
            raise DeviceException("Error", False)

        self.manager._construct_device = device_constructor

        self.assertRaises(SILFProtocolError, self.manager.power_up, "mode")

    def make_test_for_method(
        self, device_method_name, manager_method_name=None, *args, **kwargs
    ):
        if manager_method_name is None:
            manager_method_name = device_method_name

        self.manager.power_up("foo")
        getattr(self.manager.device, device_method_name).side_effect = DeviceException(
            "Error", False
        )
        mehtod = getattr(self.manager, manager_method_name)
        self.assertRaises(SILFProtocolError, mehtod, *args, **kwargs)

    def test_apply_settings(self):
        self.make_test_for_method("apply_settings", settings={})

    def test_start(self):
        self.make_test_for_method("start")

    def test_stop(self):
        self.make_test_for_method("stop")

    def test_post_power_up_diagnostics(self):
        self.manager.power_up("foo")
        getattr(
            self.manager.device, "post_power_up_diagnostics"
        ).side_effect = DeviceException("Error", False)
        mehtod = getattr(self.manager, "post_power_up_diagnostics")
        self.assertRaises(DiagnosticsException, mehtod)

    def test_pre_power_up_diagnostics(self):
        def device_constructor():
            device_mock = mock.MagicMock()
            device_mock.pre_power_up_diagnostics.side_effect = DeviceException(
                "Error", False
            )
            return device_mock

        self.manager._construct_device = device_constructor

        mehtod = getattr(self.manager, "pre_power_up_diagnostics")
        self.assertRaises(DiagnosticsException, mehtod)

    def test_power_down(self):
        self.make_test_for_method("power_down")

    def test_power_down_kill(self):
        self.make_test_for_method("kill", "power_down")


class DefaultDeviceTestException(TestMethodPropagatesError):
    DEVICE_MANAGER_CLASS_NAME = (
        "silf.backend.commons.device_manager.DefaultDeviceManager"
    )


class SingleModeDeviceTestException(TestMethodPropagatesError):
    DEVICE_MANAGER_CLASS_NAME = (
        "silf.backend.commons.device_manager.SingleModeDeviceManager"
    )


class TestHasResult(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.p = patch_abc(IDeviceManager)

    @classmethod
    def tearDownClass(cls):
        cls.p.stop()

    def setUp(self):
        self.manager = IDeviceManager("TEST", {})

    def test_integer(self):
        with mock.patch.object(
            self.manager.__class__, "current_results", new_callable=mock.PropertyMock
        ) as cr:
            cr.return_value = ResultSuite(foo=Result(value=0))
            self.assertTrue(self.manager.has_result("foo"))

    def test_float(self):
        with mock.patch.object(
            self.manager.__class__, "current_results", new_callable=mock.PropertyMock
        ) as cr:
            cr.return_value = ResultSuite(foo=Result(value=0.0))
            self.assertTrue(self.manager.has_result("foo"))

    def test_sanity(self):
        self.assertFalse(self.manager.has_result("foo"))
