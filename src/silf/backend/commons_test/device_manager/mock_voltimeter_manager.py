from silf.backend.commons.device_manager._device_manager import SingleModeDeviceManager
from silf.backend.commons.device_manager._result_creator import OverrideResultsCreator
from silf.backend.commons_test.device.mock_voltimeter import FastMockVoltimeter


class MockVoltimeterManager(SingleModeDeviceManager):

    DEVICE_CONSTRUCTOR = FastMockVoltimeter

    DEVICE_ID = "voltimeter1"

    CONTROLS = None

    OUTPUT_FIELDS = None

    RESULT_CREATORS = [
        OverrideResultsCreator("current_voltage"),
        OverrideResultsCreator("target_volatage"),
    ]
