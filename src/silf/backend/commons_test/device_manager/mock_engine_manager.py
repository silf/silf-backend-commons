import types
from silf.backend.commons.api.stanza_content import *
from silf.backend.commons.device import DEVICE_STATES, READY
from silf.backend.commons.device_manager import *
from silf.backend.commons_test.device.mock_engine import FastMockDriver


class MockEngineManager(SingleModeDeviceManager):

    DEVICE_CONSTRUCTOR = FastMockDriver

    DEVICE_ID = "engine1"

    CONTROLS = ControlSuite(
        NumberControl("position", "Target position", min_value=0, max_value=1024)
    )

    OUTPUT_FIELDS = OutputFieldSuite(
        current_position=OutputField("indicator", "current_position")
    )

    RESULT_CREATORS = [OverrideResultsCreator("current_position")]


class ScanningEngineManager(SingleModeDeviceManager):

    DEVICE_CONSTRUCTOR = FastMockDriver

    DEVICE_ID = "scanning_engine"

    CONTROLS = ControlSuite(
        NumberControl("begin_position", "Begin position", min_value=0, max_value=1024),
        NumberControl("end_position", "End position", min_value=0, max_value=1024),
        NumberControl("step_count", "Number of steps", min_value=0, max_value=30),
    )

    OUTPUT_FIELDS = OutputFieldSuite(
        current_step_indicator=OutputField("indicator", "current_step_indicator"),
        current_position=OutputField("indicator", "current_position"),
    )

    RESULT_CREATORS = [OverrideResultsCreator("current_position")]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_step = None
        self.desired_position = None
        self._state = "off"

        self.__current_settings = {}

    @property
    def on_position(self):
        results = self.current_result_map
        if not "current_position" in results:
            return False
        return (
            results["current_position"][0] == self.desired_position
            and self.device.state == DEVICE_STATES[READY]
        )

    @property
    def on_final_position(self):
        return (
            self.desired_position == self.current_settings["end_position"]
            and self.on_position
        )

    def apply_settings(self, settings):
        """
        See :meth:`IDeviceManager.apply_settings`.
        """
        converted = self.get_input_fields(self.current_mode).check_and_convert_settings(
            settings
        )
        self.current_step = None
        self.__current_settings = converted

    @property
    def current_settings(self):
        return types.MappingProxyType(self.__current_settings)

    def start(self, callback=None):
        pass

    def go_to_next_position(self):

        self.clear_current_results()

        if self.current_step is None:
            self.current_step = 0
        else:
            self.current_step += 1

        self.desired_position = self.__get_position()

        with self._exception_translator():
            self.device.apply_settings({"position": self.desired_position})
            self.device.start()

    def __get_position(self):

        end_position = self.current_settings["end_position"]
        begin_position = self.current_settings["begin_position"]
        steps = self.current_settings["step_count"]
        if self.current_step == 0:
            return begin_position

        if self.current_step == steps - 1:
            return end_position

        distance = end_position - begin_position

        step_distance = distance / (steps - 1)
        return int(begin_position + step_distance * self.current_step)
