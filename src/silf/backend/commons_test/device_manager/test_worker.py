import multiprocessing
import time
import unittest
import uuid

from silf.backend.commons.api.exceptions import ExperimentBackendUnresponsive
from silf.backend.commons.device._device import (
    Device,
    DiagnosticsException,
    DEVICE_STATES,
    RUNNING,
    READY,
)
from silf.backend.commons.device_manager import _worker
from silf.backend.commons_test.device.noop_device import NoopDevice

RESULT_WAIT_SLEEP = 1.5


class TestDevice(Device):
    LOOP_RESULT = False

    current_results = None

    def pre_power_up_diagnostics(self, a):
        raise DiagnosticsException()

    def post_power_up_diagnostics(self, a):
        raise DiagnosticsException()

    def apply_settings(self, settings):
        self.settings = settings

    def pop_results(self):
        return [self.settings]

    def start(self):
        self.state = DEVICE_STATES[RUNNING]

    def stop(self):
        self.state = DEVICE_STATES[READY]

    def loop_iteration(self):
        time.sleep(0.1)
        return self.LOOP_RESULT

    def _tear_down(self):
        pass


class SendProcessDevice(TestDevice):
    def pop_results(self):
        return [multiprocessing.current_process().pid]


class WorkerTestCase(unittest.TestCase):
    Device = None

    TEAR_DOWN_TIMEOUT = None

    def setUp(self):
        self.worker = _worker.DeviceWorkerWrapper("foo_bar", self.Device)
        self.worker.auto_pull_results = False
        self.worker.task_delay = 1e6
        self.worker.start_subprocess()

    def tearDown(self):
        self.worker.kill(wait_time=self.TEAR_DOWN_TIMEOUT)

    def assert_results(self, desired_results, msg=None):
        self.worker.pop_results()
        time.sleep(RESULT_WAIT_SLEEP)
        results = self.worker.pop_results()[0]
        self.assertEquals(desired_results, results, msg)

    def assert_not_results(self, desired_results, msg=None):
        self.worker.pop_results()
        time.sleep(RESULT_WAIT_SLEEP)
        results = self.worker.pop_results()[0]
        self.assertEqual(type(desired_results), type(results))
        self.assertNotEqual(desired_results, results, msg)

    def assert_state(self, desired_state, msg=None):
        time.sleep(RESULT_WAIT_SLEEP)
        self.assertEqual(self.worker.state, desired_state, msg)


class TestExceptionOnInitiation(WorkerTestCase):
    """
    Tests whether exceptions are propagated out of methods
    """

    Device = TestDevice

    def initialization(self):
        self.worker.perform_diagnostics("test")
        time.sleep(RESULT_WAIT_SLEEP)
        self.assertRaises(DiagnosticsException, self.worker.loop_iteration)


class TestCallbacks(WorkerTestCase):
    Device = TestDevice

    def _make_test_for_callback_method(self, method, *args, **kwargs):
        self.worker.apply_settings({})
        self.worker.start()
        callback_called = False
        id = None

        def callback(response):
            nonlocal callback_called
            callback_called = True
            self.assertEqual(response.id, id)

        id = method(*args, callback=callback, **kwargs)
        self.worker.start()
        time.sleep(RESULT_WAIT_SLEEP)
        try:
            self.worker.loop_iteration()
        except DiagnosticsException:
            pass
        self.assertTrue(callback_called)

    def test_start_callback(self):
        self._make_test_for_callback_method(self.worker.start)

    def test_stop_callback(self):
        self._make_test_for_callback_method(self.worker.stop)

    def test_ping_results_callback(self):
        self._make_test_for_callback_method(self.worker.ping_results)

    def test_apply_settings_callback(self):
        self._make_test_for_callback_method(self.worker.apply_settings, {})

    def test_perform_diagnostics_callback(self):
        self._make_test_for_callback_method(self.worker.perform_diagnostics, None)


class CheckProcessTestCase(WorkerTestCase):
    Device = SendProcessDevice

    def process_is_different(self):
        self.assert_not_results(multiprocessing.current_process().pid)


class WorkerTests(WorkerTestCase):
    Device = TestDevice

    def test_process_exists(self):
        self.assertTrue(self.worker.process is not None)

    def test_check_state_gets_updated(self):
        self.worker.apply_settings({})
        self.worker.start()
        self.assert_state(DEVICE_STATES[RUNNING])

    def test_check_state_gets_updated(self):
        self.worker.apply_settings({})
        self.worker.stop()
        self.assert_state(DEVICE_STATES[READY])

    def test_wait_for_response(self):
        self.worker.apply_settings({})
        task_id = self.worker.stop(sync=False)
        result = self.worker.wait_for_response(task_id)
        self.assertEqual(result.id, task_id)
        self.assertEqual(result.task_type, "stop")


class TestKillingWorks(WorkerTestCase):
    """
    Tests whether we can forcefully terminate remote process,
    even if it hangs.
    """

    class __Device(Device):
        def start(self):
            time.sleep(30000)

        def _tear_down(self):
            pass

        def perform_diagnostics(self, diagnostics_level=None):
            pass

    Device = __Device

    def test_no_blocks(self):
        self.worker.power_up()
        self.worker.apply_settings({})
        self.worker.start()
        self.worker.kill()


class TestKilligTimeout(WorkerTestCase):
    """
    Tests whether we can exit from remote process without killing
    it if it doesn't hang.
    """

    Device = NoopDevice

    TEAR_DOWN_TIMEOUT = 1000

    def test_no_blocks(self):
        self.worker.power_up()
        self.worker.apply_settings({})
        self.worker.start()
        time.sleep(RESULT_WAIT_SLEEP)
        self.worker.ping_results()
        time.sleep(RESULT_WAIT_SLEEP)
        results = self.worker.ping_results()
        self.assertIsNotNone(results)


class ReturnsTaskId(WorkerTestCase):
    """
    Checks whether all methods return task ids
    """

    Device = TestDevice

    def _make_get_test(self, method_call):
        self.assertIsInstance(method_call(), uuid.UUID)

    def test_start(self):
        self._make_get_test(self.worker.start)

    def test_stop(self):
        self._make_get_test(self.worker.stop)

    def test_ping_results(self):
        self._make_get_test(self.worker.ping_results)

    def test_perform_diagnostics(self):
        self._make_get_test(self.worker.perform_diagnostics)

    def test_apply_settings(self):
        self._make_get_test(self.worker.apply_settings)

    def test_get_results(self):
        self._make_get_test(self.worker.get_results)


TIMEOUT = 10


class TestDeviceTimeout(TestDevice):
    def pop_results(self):
        time.sleep(TIMEOUT)
        return [self.settings]


class WorkerTestTimeouts(WorkerTestCase):
    Device = TestDeviceTimeout

    def test_timeout(self):
        with self.assertRaises(ExperimentBackendUnresponsive):
            self.worker.task_delay = 1
            self.worker.power_up()
            self.worker.apply_settings({})
            self.worker.start()
            time.sleep(1)
            self.worker.ping_results()
            time.sleep(1)
            results = self.worker.ping_results()

    def test_timeout_2(self):
        self.worker.task_delay = 5
        self.worker.power_up()
        self.worker.apply_settings({})
        self.worker.start()
        time.sleep(1)
        self.worker.ping_results()
        time.sleep(1.5)
        results = self.worker.ping_results()
