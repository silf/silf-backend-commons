import time
import unittest

from silf.backend.commons.api.stanza_content._misc import SettingSuite, Setting
from silf.backend.commons.device import *
from silf.backend.commons_test.device_manager.mock_engine_manager import (
    ScanningEngineManager,
    MockEngineManager,
)


class MockDeviceTest(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.manager = MockEngineManager("foo", None)

    def test_check_set_position_sets_position(self):
        self.manager.power_up("foo")
        self.manager.apply_settings(SettingSuite(position=Setting(value=100)))
        self.manager.start()
        time.sleep(2)
        results = self.manager.current_results
        self.assertEqual(results.internal_dict["current_position"].value, [100])

    def test_after_set_posiiton_device_staus_is_ready(self):
        self.manager.power_up("foo")
        self.manager.apply_settings(SettingSuite(position=Setting(value=1)))
        self.manager.start()
        time.sleep(2)
        self.assertEqual(self.manager.device_state, DEVICE_STATES[READY])

    def test_check_engine_raises_error(self):
        self.manager.power_up("foo")
        self.manager.apply_settings(SettingSuite(position=Setting(value=31)))

        with self.assertRaises(DeviceRuntimeException):
            self.manager.start()
            time.sleep(2)
            self.manager.current_results


class MockScanningEngineManagerTests(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.manager = ScanningEngineManager("foo", None)
        self.manager.power_up("foo")
        self.manager.apply_settings(
            SettingSuite(
                begin_position=Setting(value=0),
                end_position=Setting(value=100),
                step_count=Setting(value=3),
            )
        )
        self.manager.start()

    def tearDown(self):
        self.manager.tear_down(None)

    def test_check_scanning_works(self):
        self.manager.go_to_next_position()
        time.sleep(2)
        self.assertEqual(self.manager.current_results["current_position"].value, [0])
        self.assertTrue(self.manager.on_position)

    def test_check_scanning_works_2(self):
        self.manager.go_to_next_position()
        time.sleep(2)
        self.manager.stop()
        self.manager.go_to_next_position()
        time.sleep(2)
        self.assertTrue(self.manager.on_position)
        self.assertEqual(self.manager.current_results["current_position"].value, [50])

    def test_check_scanning_works_3(self):
        self.manager.go_to_next_position()
        time.sleep(2)
        self.manager.stop()
        self.manager.go_to_next_position()
        time.sleep(2)
        self.manager.stop()
        self.manager.go_to_next_position()
        time.sleep(2)
        self.assertTrue(self.manager.on_position)
        self.assertEqual(self.manager.current_results["current_position"].value, [100])
        self.assertTrue(self.manager.on_final_position)
