# coding=utf-8
from silf.backend.commons.api.exceptions import ValidationError
from silf.backend.commons.device_manager._device_manager import (
    DefaultDeviceManager,
    SingleModeDeviceManager,
)
from silf.backend.commons_test.device.noop_device import NoopDevice


class NoopDeviceManager(SingleModeDeviceManager):

    DEVICE_CONSTRUCTOR = NoopDevice


class RaiseErrorOnCheckSettings(SingleModeDeviceManager):

    DEVICE_CONSTRUCTOR = NoopDevice

    def check_settings(self, mode, settings):

        raise ValidationError.from_args("error", "user", "MSG")
