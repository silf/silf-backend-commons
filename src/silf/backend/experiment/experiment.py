# coding=utf-8

import contextlib
from io import StringIO
from queue import Queue, Empty
import logging
import logging.config
import traceback
import uuid
import threading

import time

from silf.backend.commons.api.stanza_content import *
from silf.backend.commons.experiment import *

from silf.backend.client.const import *
from silf.backend.client.client import Client
from silf.backend.commons.util.config import (
    prepend_current_dir_to_config_file,
    open_configfile,
)
from silf.backend.commons.util.mail import ExceptionHandler, EmailSender
from silf.backend.commons.api.exceptions import (
    SILFProtocolError,
    ValidationError,
    ExperimentBackendUnresponsive,
    ConfigurationException,
)
from silf.backend.commons.version import PROTOCOL_VERSIONS, ProtocolVersionUtil
from silf.backend.experiment import reflect_utils

LOGGER = logging.getLogger("Experiment")


def load_experiment(cp):
    cp = open_configfile(cp)
    ClientClass = reflect_utils.load_item_from_module(cp["Experiment"]["ClientClass"])
    ExperimentManagerClass = reflect_utils.load_item_from_module(
        cp["Experiment"]["ExperimentManagerClass"]
    )
    e = Experiment(cp, ExperimentManagerClass, ClientClass(config=cp))
    e.initialize()
    return e


class ClientResetToken:
    """
    Object of this class is put into the queue if client encounters fatal error and must be reset
    """

    pass


class ClientResetException(Exception):
    pass


class ExperimentManagerCallback(ExperimentCallback):
    def __init__(self, client, experiment):
        """
        :param Client client:

        """
        super().__init__()
        self.client = client
        self.experiment = experiment

    def send_series_done(self, message=None):
        self.client.send_standalone_stanza(
            SILF_SERIES_STOP, suite=SeriesStopped(seriesId=self.experiment.seriesId)
        )

    def send_results(self, results):
        self.client.send_standalone_stanza(SILF_RESULTS, results)

    def send_experiment_done(self, message=None):
        self.client.send_standalone_stanza(SILF_EXPERIMENT_STOP)


class Experiment(object):

    LOOP_TIMEOUT = 0.5

    def __init__(self, cp, ManagerClass, client: Client):
        """

        :param cp:
        :param manager:
        :type manager: :class:`silf.backend.commons.experiment.api.BaseExperimentManager`

        :return:
        """
        self.config = cp
        self.manager = ManagerClass(cp)
        self.client = client
        self.client.register_error_callback(self.schedule_client_reset)
        self.ClientClass = type(client)
        self.queue = Queue()
        self.current_stanza = None
        self.experimentId = None
        self.seriesId = None
        self.mode = None
        self.__lock = threading.Lock()
        self.__tear_down_flag = False
        self.__experiment_thread = None
        self.exception_callbacks = []
        self.pre_power_up_diagnostics_performed = False
        self.post_power_up_diagnostics_performed = False
        self.sender = None
        """
        Sends emails to admins of this experiment if any error happens
        """

    def add_exception_callback(self, callback):
        self.exception_callbacks.append(callback)

    @property
    def logger(self):
        return logging.getLogger(str(self.manager.get_experiment_name()))

    def __configure_logging(self):

        logging_type = self.config.get("Logging", "config_type", fallback="basic")

        if logging_type == "basic":
            logging.basicConfig(level=logging.DEBUG)
            logging.root.addHandler(ExceptionHandler())
            return

        if logging_type == "file":
            default = prepend_current_dir_to_config_file("logging.ini")
            logging.config.fileConfig(
                self.config.get("Logging", "logging_file", fallback=default),
                disable_existing_loggers=False,
            )
            return

        raise ConfigurationException(
            "Avilable values for config_type are : 'basic' and 'file'"
        )

    def __configure_timeouts(self):

        new_timeout = self.config.get(
            "Experiment", "LoopTimeout", fallback=self.LOOP_TIMEOUT
        )
        self.LOOP_TIMEOUT = float(new_timeout)

    def configure(self):

        self.sender = EmailSender(self.config)
        self.sender.install_error_function()
        self.__configure_logging()
        self.__configure_timeouts()

    def initialize(self):

        self.logger.info("*" * 40)
        self.logger.info("Initializing experiment")
        self.logger.info("*" * 40)

        self.client.register_callback(self.__callback)
        self.client.initialize()

        self.logger.info("Initialized client")

        self.manager.initialize(ExperimentManagerCallback(self.client, self))

        self.logger.info("Initialized manager")

        thread = threading.Thread(target=self.experiment_loop, daemon=True)
        self.__experiment_thread = thread
        self.logger.info("Started main thread")
        thread.start()

    def tear_down(self):

        with self.__lock:
            self.__tear_down_flag = True

        self.__experiment_thread.join()

        try:
            self.manager.power_down_and_notify(message="Eksperyment jest wyłączany")
        except Exception:
            logging.exception("")

        self.client.disconnect(False)

    def schedule_client_reset(self):
        self.queue.put_nowait(ClientResetToken())

    def __callback(self, state):
        """

        Callback passed to XMPP Client.

        :param state:
        :type state: :class:`silf.backend.client.client.LabdataState`
        :return:
        """
        self.queue.put_nowait(state)

    def __new_uuid(self):
        return uuid.uuid4().urn

    def __on_silf_exception(self, e):
        self.client.send_error(e.errors, self.current_stanza, SILF_MISC_ERROR)
        LOGGER.exception("PROTOCOL:ERROR, errors follow")
        for error in e.errors.errors:
            LOGGER.warn("%s, %s", error.message, error.field)

    @contextlib.contextmanager
    def __wrap_exceptions(self):
        try:
            yield
        except ExperimentBackendUnresponsive as e:
            self.__on_silf_exception(e)
            self.manager.power_down_and_notify("Experiment backend unresponsive")
        except ValidationError as e:
            self.client.send_error(e.errors, self.current_stanza, SILF_MISC_ERROR)
        except SILFProtocolError as e:
            self.__on_silf_exception(e)
        except Exception:
            try:
                self.manager.on_miscellaneous_error()
            except SILFProtocolError as e:
                self.__on_silf_exception(e)
            LOGGER.exception("MISC:ERROR")

    def _check_settings(self, state):
        if self.mode is None:
            raise SILFProtocolError.from_args(
                severity="error",
                error_type="user",
                message="Can't check settings without setting mode",
            )
        self.manager.check_settings(self.mode, state.content)

    def _apply_stanza(self, incoming_stanza):
        """

        :param incoming_stanza:
        :type incoming_stanza: :class:`silf.backend.client.client.LabdataState`

        :return:
        """

        ns = incoming_stanza.namespace

        if incoming_stanza.type != "query":
            return

        if ns == SILF_SETTINGS_CHECK:
            self._check_settings(incoming_stanza)
            incoming_stanza.reply(suite=incoming_stanza.content)
        elif ns == SILF_MODE_GET:
            incoming_stanza.reply(suite=self.manager.modes)
        elif ns == SILF_MODE_SET:
            self.experimentId = self.__new_uuid()
            # TODO: FIX SILF-432
            # TODO: Race condition when client sends start series instanteneoiusly, and it is fired before mode set
            self.mode = incoming_stanza.content.mode

            if not self.pre_power_up_diagnostics_performed:
                self.manager.pre_power_up_diagnostics()
                self.pre_power_up_diagnostics_performed = True

            self.manager.power_up(self.mode)

            if not self.post_power_up_diagnostics_performed:
                self.manager.post_power_up_diagnostics()
                self.post_power_up_diagnostics_performed = True

            mode = self.__set_mode_suite(self.mode)
            incoming_stanza.reply(suite=mode)
        elif ns == SILF_SERIES_START:
            self._check_settings(incoming_stanza)
            settings = incoming_stanza.content
            self.manager.apply_settings(settings)
            self.manager.start()
            self.seriesId = self.__new_uuid()
            label = self.manager.get_series_name(settings)
            incoming_stanza.reply(
                suite=SeriesStartedStanza(
                    seriesId=self.seriesId, initialSettings=settings, label=label
                )
            )

        elif ns == SILF_SERIES_STOP:
            self.manager.stop()
            try:
                incoming_stanza.reply(suite=SeriesStopped(seriesId=self.seriesId))
            finally:
                self.seriesId = None
        elif ns == SILF_EXPERIMENT_STOP:
            self.manager.power_down()
            self.experimentId = None
            self.seriesId = None
            self.mode = None
            self.diagnostics_performed = False
            incoming_stanza.reply()
        elif ns == SILF_SETTINGS_UPDATE:
            settings = incoming_stanza.content
            if not settings.internal_dict:
                # Interface sends empty settings:update stanzas, we ignore then
                return
            self.manager.apply_settings_live(settings)
        elif ns == SILF_VERSION_GET:
            incoming_stanza.reply(suite=ProtocolVersions(PROTOCOL_VERSIONS))
        elif ns == SILF_VERSION_SET:
            # TODO okresl co to jest icmoing_stanza i jak pobrac suite - typ JSON-owy
            ver = incoming_stanza.content.version
            if not ProtocolVersionUtil.is_version_in_list(ver, PROTOCOL_VERSIONS):
                raise SILFProtocolError.from_args(
                    severity="error",
                    error_type="user",
                    message="No supported protocol version",
                )
            incoming_stanza.reply(suite=incoming_stanza.content)
        elif ns == SILF_LANG_SET:
            # as default use first available one
            current_lang = self.manager.get_available_languages()[0]

            # Select first language that both experiment and client agree on.
            # if there is no language in common stick to default.
            for lang in incoming_stanza.content.langs:
                if lang in self.manager.get_available_languages():
                    current_lang = lang
                    break

            self.manager.current_lang = current_lang
            self.manager.set_current_language(current_lang)
            incoming_stanza.reply(suite=Language(current_lang))

    def __set_mode_suite(self, mode):
        controls = self.manager.get_input_fields(mode)
        return ModeSelected(
            mode=mode,
            experimentId=self.experimentId,
            settings=controls.to_input_field_suite(),
            resultDescription=self.manager.get_output_fields(mode),
        )

    def __reset_client(self):
        """
        Reset (rebuild) client
        """
        attempts = 10
        delay = 1

        original_client = self.client

        while True:
            try:
                self.client = original_client.copy()
                self.client.register_error_callback(self.schedule_client_reset)
                self.client.register_callback(self.__callback)
                self.client.initialize()
                self.logger.info("Client reset")
                break
            except Exception as e:
                attempts -= 1
                self.logger.exception(
                    "Client reset did not work, exception: {}".format(e)
                )

                if attempts == 0:
                    raise ClientResetException(
                        "Error occured, resetting client did not work, giving up"
                    )

                self.logger.info(
                    "Waiting {} seconds ({} attempts left)".format(delay, attempts)
                )

                time.sleep(delay)
                delay += delay

    def _loop_iteration(self):

        try:
            with self.__wrap_exceptions():
                self.manager.loop_iteration()

            try:
                incoming_stanza = self.queue.get(timeout=self.LOOP_TIMEOUT)
                self.current_stanza = incoming_stanza
                if isinstance(incoming_stanza, ClientResetToken):
                    self.__reset_client()
                else:
                    with self.__wrap_exceptions():
                        self._apply_stanza(incoming_stanza)
            except Empty:
                pass
            finally:
                self.current_stanza = None

        except Exception as e:
            for cb in self.exception_callbacks:
                try:
                    cb(e)
                except Exception:
                    logging.exception("Error while executing callback")
            else:
                raise

    def experiment_loop(self):

        while True:

            with self.__lock:
                if self.__tear_down_flag:
                    return

            self._loop_iteration()
