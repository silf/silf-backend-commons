#!/usr/bin/env python3
# coding=utf-8
"""
Utility creates translations.

Usage:
    make-translations.py --help
    make-translations.py create --experiment=<experiment_name> [--locales=<locales> --base-dir=<base_dir> --scan-dir=<scan-dir>]
    make-translations.py update --experiment=<experiment_name> [--locales=<locales> --base-dir=<base_dir> --scan-dir=<scan-dir>]
    make-translations.py add --experiment=<experiment_name> --locales=<locales> [--base-dir=<base_dir>]
    make-translations.py compile --experiment=<experiment_name> --locales=<locales> [--base-dir=<base_dir>]

Options:
  -h --help     Show this screen.
  -d --base-dir=<base_dir>  Base directory to store translation data [default: translations]
  -e --experiment=<experiment_name> Name of experiment, must be the same as experiment name in ini file for experiment server
  -l --locales=<locales>   List of locales, coma separated for which translation data is created [default: pl,en]
  -s --scan-dir=<scan-dir>  path to root directory from which scanning for localisation texts is done [default: silf/backend/]

"""
import typing

import docopt
import os
import re

from babel.messages import frontend as babel


def extract_messages(pot_path, dir_to_scan):
    """
    Extract messages from files in dir_to_scan and store them to pot file in pot_path
    :param pot_path: Path to pot file, which will be created with messages
    :param dir_to_scan: root directory to start scanning from, scanning is for marked messages with _() function
    :return:
    """
    babel.CommandLineInterface().run(
        ["pybabel", "extract", "-o", pot_path, dir_to_scan]
    )


def create_localisation(experiment_name, dir_to_scan, base_dir, locales):
    """
    Create localisation data for given experiment
    :param experiment_name: name of the experiment, will be used for naming files with messages
    :param dir_to_scan: root directory to start scanning from, scanning is for marked messages with _() function
    :param base_dir: base directory to store localisation files
    :param locales: locales to create
    :return:
    """
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    pot_path = get_pot_path(base_dir, experiment_name)
    extract_messages(pot_path, dir_to_scan)

    for l in locales:
        add_localisation(experiment_name, base_dir, l)


def compile_localisation(experiment_name, base_dir, locale):
    """
    Compile mo file
    """
    domain = experiment_name
    babel.CommandLineInterface().run(
        ["pybabel", "compile", "-D", domain, "-d", "./" + base_dir, "-l", locale]
    )


def compile_many_localisations(experiment_name, base_dir, locales: typing.List[str]):
    """
    :param locales: List of locales to compile
    """
    for locale in locales:
        print(locale)
        compile_localisation(experiment_name, base_dir, locale)


def add_localisation(experiment_name, base_dir, locale):
    """
    Add additional locales
    :param experiment_name: name of the experiment, will be used for naming files with messages
    :param base_dir: base directory to store localisation files
    :param locales: locales to create
    :return:
    """
    domain = experiment_name
    pot_path = get_pot_path(base_dir, experiment_name)
    babel.CommandLineInterface().run(
        [
            "pybabel",
            "init",
            "-D",
            domain,
            "-i",
            pot_path,
            "-d",
            "./" + base_dir,
            "-l",
            locale,
        ]
    )
    babel.CommandLineInterface().run(
        ["pybabel", "compile", "-D", domain, "-d", "./" + base_dir, "-l", locale]
    )


def update_localisation(experiment_name, pot_path, locale, base_dir):
    """
    Update given locale with new version of messages
    :param experiment_name: name of the experiment, will be used for naming files with messages
    :param pot_path: path to pot file with new version of messages
    :param locale: Locales to be updated
    :param base_dir: base directory to store localisation files
    :return:
    """
    domain = experiment_name
    babel.CommandLineInterface().run(
        [
            "pybabel",
            "update",
            "-D",
            domain,
            "-d",
            "./" + base_dir,
            "-l",
            locale,
            "-i",
            pot_path,
        ]
    )


def update_locales(base_dir, dir_to_scan, experiment_name, locales):
    """

    :param base_dir: base directory to store localisation files
    :param dir_to_scan: root directory to start scanning from, scanning is for marked messages with _() function
    :param experiment_name: name of the experiment, will be used for naming files with messages
    :param locale: Locales to be updated
    :return:
    """
    pot_path = get_pot_path(base_dir, experiment_name)
    extract_messages(pot_path, dir_to_scan)
    for l in locales:
        update_localisation(experiment_name, pot_path, l, base_dir)


def get_pot_path(base_dir, experiment_name):
    pot_path = base_dir + "/" + experiment_name + ".pot"
    return pot_path


def main(commands):
    experiment_name = commands["--experiment"]

    experiment_name = re.sub(r"\s+", "-", experiment_name.lower())

    dir_to_scan = commands["--scan-dir"]
    base_dir = commands["--base-dir"]
    locales = commands["--locales"]
    locales = [l.strip() for l in locales.split(",")]

    if commands["create"]:
        create_localisation(experiment_name, dir_to_scan, base_dir, locales)
    elif commands["update"]:
        update_locales(base_dir, dir_to_scan, experiment_name, locales)
    elif commands["add"]:
        if len(locales) > 1:
            raise ValueError("This subcommand supports only single locale")
        add_localisation(experiment_name, base_dir, locales[0])
    elif commands["compile"]:
        compile_many_localisations(experiment_name, base_dir, locales)
    else:
        raise ValueError("Unknown command.")


if __name__ == "__main__":
    commands = docopt.docopt(__doc__)
    main(commands)
