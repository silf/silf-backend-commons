#!/usr/bin/env python3
# coding=utf-8
"""
Utility that launches experiment.

Usage:
    expmain.py <configfile> --test-smtp-sending
    expmain.py <configfile> [--no-multiprocessing] [--no-default-configfiles] [--additional-cf=<cf>]...
    expmain.py <configfile> [--no-multiprocessing] [--no-default-configfiles] [--additional-cf=<cf>]... debug <host> <port>


"""

import logging
import logging.config
import os
import configparser
import threading

import docopt
from silf.backend.commons.api.exceptions import (
    ExperimentCreationError,
    ConfigurationException,
)
from silf.backend.commons.device_manager._worker import set_multiprocessing
from silf.backend.commons.experiment._experiment_manager import (
    BaseExperimentManager,
    IExperimentManager,
)

from silf.backend.commons.util import reflect_utils
from silf.backend.commons.util.config import (
    open_configfile,
    prepend_current_dir_to_config_file,
    create_config_parser,
)
from silf.backend.commons.util.mail import ExceptionHandler, EmailSender


sender = None


def configure_email_sender(cp):

    global sender

    sender = EmailSender(cp)
    sender.install_error_function()


def configure_logging(cp):

    logging_type = cp.get("Logging", "config_type", fallback="basic")

    if logging_type == "basic":
        logging.basicConfig(level=logging.DEBUG)
        logging.root.addHandler(ExceptionHandler())
        return

    if logging_type == "file":
        default = prepend_current_dir_to_config_file("logging.ini")
        logging.config.fileConfig(
            cp.get("Logging", "logging_file", fallback=default),
            disable_existing_loggers=False,
        )
        return

    raise ConfigurationException(
        "Avilable values for config_type are : 'basic' and 'file'"
    )


def run_old(cp, commands, client, ExperimentManagerClass):

    if commands["--no-multiprocessing"]:
        set_multiprocessing(False)

    from silf.backend.experiment import experiment

    e = experiment.Experiment(cp, ExperimentManagerClass, client)
    e.configure()
    e.initialize()


def run_new(configfile, client, ExperimentManagerClass):
    experiment = ExperimentManagerClass(configfile, client=client)
    try:
        t = threading.Thread(target=lambda: experiment.run())
        t.start()
        t.join()
    finally:
        experiment.stop()


def main(commands, run_loop=True):

    if "<host>" in commands and commands["<host>"]:

        import pydevd

        pydevd.settrace(
            commands["<host>"],
            port=int(commands["<port>"]),
            stdoutToServer=True,
            stderrToServer=True,
        )

    configfile = commands["<configfile>"]
    cp = create_config_parser(commands["<configfile>"])

    if cp is None:
        raise ExperimentCreationError("Couldn't open config file.")

    configure_logging(cp)
    configure_email_sender(cp)

    if commands.get("--test-smtp-sending", False):
        logging.root.error(
            "This is a test sending error messages. If you reclieve it it means that everything works"
        )
        return

    ClientClass = reflect_utils.load_item_from_module(cp["Experiment"]["ClientClass"])

    ExperimentManagerClass = reflect_utils.load_item_from_module(
        cp["Experiment"]["ExperimentManagerClass"]
    )

    client = ClientClass(cp, section="XMPPClient")

    if issubclass(ExperimentManagerClass, IExperimentManager):
        run_old(cp, commands, client, ExperimentManagerClass)
    else:
        run_new(configfile, client, ExperimentManagerClass)


if __name__ == "__main__":

    commands = docopt.docopt(__doc__)

    print(commands)

    main(commands)
