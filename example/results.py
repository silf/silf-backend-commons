from silf.backend.commons.new.result import AggregatedResultsCreator, LiveResultsCreator


class AccumulatedAverageCreator(AggregatedResultsCreator):

    def calculate(self, values):
        return sum(values) / len(values);


class ChartAverageCreator(AggregatedResultsCreator):

    def calculate(self, values):
        return [[len(values), sum(values) / len(values)]]


class LiveSumCreator(LiveResultsCreator):

    def calculate(self, values):
        return sum(values)


class ChartGaussCreator(AggregatedResultsCreator):

    def reset(self):
        super().reset()

    def apply_values(self, values, step):
        self.bins = values.get('bins', 10)
        self.points_to_average = values.get('points_to_average', 100)
        super().apply_values(values, step)

    def convert(self, values):
        return sum(values)

    def calculate(self, values):
        min_value = min(values)
        max_value = max(values)
        interval_len = (max_value - min_value) / self.bins
        results = [[min_value + (i + 0.5) * interval_len, 0] for i in range(self.bins)]
        for point in values:
            for i in range(self.bins):
                if i == 0 or point > results[i][0] - 0.5 * interval_len:
                    if i == self.bins - 1 or point <= results[i+1][0] - 0.5 * interval_len:
                        results[i][1] += 1
                        break
        return results