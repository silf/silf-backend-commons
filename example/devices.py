import os
import time
import random


class RandomDevice:

    def __init__(self):
        self.num = -1

    def init(self, config):
        print('in init for RandomDevice: config =', config)

    def power_up(self):
        time.sleep(.5)
        print('power_up for RandomDevice')

    def start_measurements(self, settings):
        print('start_measurements for RandomDevice with inital settings', settings)

    def apply_settings(self, settings):
        print('apply settings for RandomDevice enter', settings)
        time.sleep(0.5)
        if 'num_from' in settings:
            self.num_from = settings['num_from']
        if 'num_to' in settings:
            self.num_to = settings['num_to']
        print('apply settings for RandomDevice exit')

    def read_state(self):
        time.sleep(0.5)
        return {'1': os.getpid(), 'num': random.uniform(self.num_from, self.num_to)}

    def stop_measurements(self):
        print('stop_measurements for RandomDevice')

    def power_down(self):
        print('power_down for RandomDevice')


class MultiRandomDevice:

    def __init__(self):
        self.num = -1

    def power_up(self):
        time.sleep(0.1)

    def apply_settings(self, settings):
        time.sleep(0.1)
        if 'num_from' in settings:
            self.num_from = settings['num_from']
        if 'num_to' in settings:
            self.num_to = settings['num_to']
        if 'points_to_average' in settings:
            self.points_to_average = settings['points_to_average']

    def read_state(self):
        time.sleep(0.1)
        return {'1': os.getpid(), 'nums': [random.uniform(self.num_from, self.num_to) for i in range(self.points_to_average)]}


class AverageDevice:

    def __init__(self):
        self.num = -1

    def apply_settings(self, settings):
        print('apply settings for AverageDevice enter', settings)
        time.sleep(0.2)
        if 'num_from' in settings:
            self.num_from = settings['num_from']
        if 'num_to' in settings:
            self.num_to = settings['num_to']
        print('apply settings for AverageDevice exit')

    def read_state(self):
        time.sleep(0.3)
        return {'2': os.getpid(), 'avg': (self.num_from + self.num_to) / 2}


from datetime import datetime


class Clock:

    live = True

    def read_state(self):
        return {'clock': str(datetime.now().time())}


class Light:

    live = True

    def __init__(self):
        self.on = False

    def read_state(self):
        return {'light': self.on}

    def apply_settings(self, settings):
        if 'light' in settings:
            print('light updated')
            self.on = settings['light']