
import os
import logging
from silf.backend.commons.api.stanza_content.control._controls import LightControl

from silf.backend.commons.new.result import LiveResultsCreator
from example.devices import RandomDevice, MultiRandomDevice, AverageDevice, Clock, Light
from example.results import AccumulatedAverageCreator, ChartAverageCreator, ChartGaussCreator, LiveSumCreator

from silf.backend.commons.api.stanza_content import ControlSuite, NumberControl, \
    OutputFieldSuite, OutputField, ChartField

from silf.backend.commons.new.experiment import Experiment
from silf.backend.commons.new.experiment_mode import ExperimentMode


logging.basicConfig(level=logging.DEBUG)


class AverageMode(ExperimentMode):

    name = 'average'
    label = 'Running Average'

    controls = ControlSuite(
        NumberControl("num_from", "Start number", min_value=0, max_value=10, step=1, default_value=2),
        NumberControl("num_to", "End number", min_value=0, max_value=10, step=1, default_value=4),
        LightControl('light', 'Light', live=True)
    )

    output_fields = OutputFieldSuite(
        chart=ChartField("chart", ["chart"], "Averaged number of counts from random data source", "Number of averaged items", "Average"),
        clock=OutputField("integer-indicator", ['clock'], label="Aktualny czas"),
        last_average=OutputField("integer-indicator", ['last_average'], label="Ostatnia średnia"),
        expected_average=OutputField("integer-indicator", ['expected_average'], label="Oczekiwana średnia"),
        step=OutputField("integer-indicator", ['step'], label="Krok"),
    )

    result_creators = [
        LiveResultsCreator(name='clock'),
        LiveResultsCreator(name='step'),
        LiveResultsCreator(name='expected_average', read_from='avg'),
        AccumulatedAverageCreator(name='last_average', read_from='num'),
        ChartAverageCreator(name='chart', pragma='append', read_from='num')
    ]

    devices = [Clock, RandomDevice, AverageDevice, Light]

    def get_series_label(self, settings):
        return 'from {} to {}'.format(settings['num_from'].value, settings['num_to'].value)

    def next_step_settings(self, settings, results):
        settings['num_from'] = self.settings['num_from']
        settings['num_to'] = self.settings['num_to']
        return settings


class GaussMode(ExperimentMode):

    name = 'gauss'
    label = 'Gaussian'

    controls = ControlSuite(
        NumberControl("num_from", "Start number", min_value=0, max_value=10, step=1, default_value=2),
        NumberControl("num_to", "End number", min_value=0, max_value=10, step=1, default_value=4),
        NumberControl("points_to_average", "Number of points to average", min_value=1, max_value=100, step=1, default_value=10),
        NumberControl("bins", "Number of bins", min_value=3, max_value=100, step=1, default_value=10),
    )

    output_fields = OutputFieldSuite(
        chart=ChartField("chart", ["chart"], "Sum of set of random numbers", "Sum", "Number of sums that had following random value"),
        clock=OutputField("integer-indicator", ['clock'], label="Aktualny czas"),
        last_sum=OutputField("integer-indicator", ['last_sum'], label="Ostatnio obliczona suma"),
        step=OutputField("integer-indicator", ['step'], label="Krok")
    )

    result_creators = [
        LiveResultsCreator(name='clock'),
        LiveResultsCreator(name='step'),
        LiveSumCreator(name='last_sum', read_from='nums'),
        ChartGaussCreator(name='chart', pragma='replace', read_from='nums')
    ]

    devices = [Clock, MultiRandomDevice]

    def get_series_label(self, settings):
        return 'from {} to {}'.format(settings['num_from'].value, settings['num_to'].value)


class Example(Experiment):

    config = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini')

    modes = [AverageMode, GaussMode]

    def set_mode(self, mode):
        print(mode)
