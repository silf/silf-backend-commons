#!/usr/bin/env python3
# coding=utf-8

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from configparser import ConfigParser

import os

requirements = []
with open(os.path.join(os.path.dirname(__file__), "requirements/base.in"), "r") as f:
    for line in f:
        line = line.strip()
        if not line.startswith("#"):
            requirements.append(line)


if __name__ == "__main__":

    cp = ConfigParser()
    file = os.path.join(
        os.path.dirname(__file__), "src/silf/backend/commons/version.ini"
    )
    with open(file, encoding="utf-8") as f:
        cp.read_file(f)

    setup(
        name="silf-backend-commons",
        description="Base package for backend part of SILF",
        package_dir={"silf": "src/silf"},
        version=cp["VERSION"]["version"],
        packages=[
            "silf.backend.commons",
            "silf.backend.commons.new",
            "silf.backend.commons.simple_server",
            "silf.backend.commons.io",
            "silf.backend.commons.api",
            "silf.backend.commons.api.translations",
            "silf.backend.commons.api.stanza_content",
            "silf.backend.commons.api.stanza_content.control",
            "silf.backend.commons.device",
            "silf.backend.commons.device_manager",
            "silf.backend.commons.experiment",
            "silf.backend.commons.util",
            "silf.backend.experiment",
            "silf.backend.client",
        ],
        package_data={
            "silf.backend.commons": ["version.ini"],
            "silf.backend.experiment": ["logging.ini"],
        },
        scripts=[
            "src/silf/backend/experiment/expmain.py",
            "src/silf/backend/experiment/make-translations.py",
        ],
        install_requires=requirements,
    )
